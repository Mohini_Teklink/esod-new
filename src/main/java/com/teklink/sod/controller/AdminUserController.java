package com.teklink.sod.controller;


/**
 * @author Prateek Raj
 */

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import com.teklink.sod.model.user.User;
import com.teklink.sod.model.user.UserCardinalities;
import com.teklink.sod.service.UserService;


@Controller
public class AdminUserController {


	@Autowired
	private UserService userService;
	
	public UserService getUserService() {
		return userService;
	}

	public void setUserService(UserService userService) {
		this.userService = userService;
	}

	@RequestMapping(value = "/admin/loginSuccess", method = RequestMethod.POST)
	public ModelAndView goToAdminLogin(@Valid @ModelAttribute("userCredentials") UserCardinalities userCardinalities,
			BindingResult bindingResult) {
		if (bindingResult.hasErrors()) {
			return new ModelAndView("adminLogin");
		}
		if (!getUserService().checkUserByUsername(userCardinalities.getUserName()))
			return new ModelAndView("adminLogin").addObject("message_error", "Please enter valid username & password!");
		ModelAndView modelAndView = new ModelAndView("redirect:/admin/dashboard");
		User user = getUserService().validateUserCredential(userCardinalities.getUserName(),
				userCardinalities.getPassword());
		if (user == null) {
			modelAndView = new ModelAndView("adminLogin");
			modelAndView.addObject("message_error", "User doesn't exist");
		}
		return modelAndView;
	}

	@RequestMapping(value = "/admin/dashboard/adminAddNewUser", method = RequestMethod.GET)
	public String goToAdminAddUser(Model model) {
		model.addAttribute("user", new User());
		model.addAttribute("activeNav", "user");
		return "adminAddNewUser";
	}

	@RequestMapping(value = "/admin/dashboard/addSuccess", method = RequestMethod.POST)
	public ModelAndView goToAdminAddUserPOST(@Valid @ModelAttribute("user") User user, BindingResult bindingResult) {
		if (bindingResult.hasErrors()) {
			return new ModelAndView("adminAddNewUser");
		}
		if (getUserService().checkUserByUsernameAndEmail(user.getEmail(), user.getUsername()))
			return new ModelAndView("adminAddNewUser").addObject("message_error", "Email & Username already exist!");
		if (getUserService().checkUserByEmail(user.getEmail()))
			return new ModelAndView("adminAddNewUser").addObject("message_error", "Email Id already exist!");
		if (getUserService().checkUserByUsername(user.getUsername()))
			return new ModelAndView("adminAddNewUser").addObject("message_error", "Username already exist!");

		ModelAndView modelAndView = new ModelAndView("redirect:/admin/dashboard");
		BCryptPasswordEncoder bCryptPasswordEncoder = new BCryptPasswordEncoder();
 		String encodedPass = bCryptPasswordEncoder.encode(user.getPassword());
		user.setPassword(encodedPass);
		boolean checkIfAdded = getUserService().registerUser(user);
		if (checkIfAdded) {
			modelAndView = new ModelAndView("adminAddNewUser");
			modelAndView.addObject("message", "User Added successfully!");
		}
		modelAndView.addObject("activeNav", "user");
		return modelAndView;
	}
	
	@RequestMapping(value="/admin/dashboard/passwordChanged", method = RequestMethod.POST)
	public String changeClientPassword(@RequestParam("old_password") String oldPasswod,
			@RequestParam("new_password") String newPassword, 
			@RequestParam("confirmpassword") String confirmPassword, Model model) {
		if(oldPasswod  == "" || newPassword == "") {
			model.addAttribute("error1","Password can not be blank");
			return "adminChangePassword";	
		}
		//Expert expert = GlobalController.getExpertDetails();
		User user=GlobalController.getAdminDetails();
		BCryptPasswordEncoder bCryptPasswordEncoder = new BCryptPasswordEncoder();
		if(!confirmPassword.equals(newPassword) || !bCryptPasswordEncoder.matches(oldPasswod,user.getPassword())) {
			model.addAttribute("error1","Password not match");
			return "adminChangePassword";
		}
		user.setPassword(bCryptPasswordEncoder.encode(newPassword));
		if(!getUserService().updateUser(user)) {
			model.addAttribute("error1","Password Not Changed");
			return "changePassword";
		}
		return "redirect:/admin/dashboard";
		
	}
}
