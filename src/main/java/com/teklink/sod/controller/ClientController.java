package com.teklink.sod.controller;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import com.teklink.sod.model.client.Client;
import com.teklink.sod.service.client.ClientService;

@Controller
public class ClientController {

	@Autowired
	private ClientService clientService;

	public ClientService getClientService() {
		return clientService;
	}

	public void setClientService(ClientService clientService) {
		this.clientService = clientService;
	}
	
	@RequestMapping(value = "/clientLogin", method = RequestMethod.GET)
	public String gotoClientLoginPage(Model model) {
		if(GlobalController.getAdminDetails()!=null)
			return "redirect:/admin/dashboard";
		if(GlobalController.getExpertDetails()!=null)
			return "redirect:/expert/jdp";
		if(GlobalController.getClientDetails()!=null)
			return "redirect:/client/clientPacks";
		
		return "clientlogin";
	}
	
	@RequestMapping(value="/client/changePassword")
	public String goToChangePassword() {
		return "changePassword";
	}
	
	
	
	@RequestMapping(value="/client/changePassword", method = RequestMethod.POST)
	public String changeClientPassword(@RequestParam("old_password") String oldPasswod,@RequestParam("new_password") String newPassword, 
			@RequestParam("confirmpassword") String confirmPassword, Model model) {
		if(oldPasswod  == "" || newPassword == "") {
			model.addAttribute("error1","Password can not be blank");
			return "changePassword";	
		}
		Client client = GlobalController.getClientDetails();
		BCryptPasswordEncoder bCryptPasswordEncoder = new BCryptPasswordEncoder();
		if(!confirmPassword.equals(newPassword) || !bCryptPasswordEncoder.matches(oldPasswod, client.getClient_password())) {
			model.addAttribute("error1","Password not match");
			return "changePassword";
		}
		client.setClient_password(bCryptPasswordEncoder.encode(newPassword));
		client.setClient_confirmpassword(confirmPassword);
		if(!clientService.updateClient(client)) {
			model.addAttribute("error1","Password Not Changed");
			return "changePassword";
		}
		return "redirect:/client/clientPacks";
		
	}

}
