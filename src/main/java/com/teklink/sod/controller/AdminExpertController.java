package com.teklink.sod.controller;


/**
 * @author Shikha Rohit
 */

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.teklink.sod.model.Mailler;
import com.teklink.sod.model.expert.Expert;
import com.teklink.sod.service.ExpertService;
import com.teklink.sod.service.SkillService;


@Controller
public class AdminExpertController {

	@Autowired
	private ExpertService expertService;

	@Autowired
	private SkillService skillService;

	public SkillService getSkillService() {
		return skillService;
	}

	public void setSkillService(SkillService skillService) {
		this.skillService = skillService;
	}

	public ExpertService getExpertService() {
		return expertService;
	}

	public void setExpertService(ExpertService expertService) {
		this.expertService = expertService;
	}

	@RequestMapping(value = "/admin/dashboard/jobseeker/activeInactive", method = RequestMethod.GET)
	public @ResponseBody String goToAdminJobSeekerDelete(@RequestParam("id") int id,
			@RequestParam("status") boolean status , @RequestParam("clientId") int clientId) {
		if (getExpertService().updateById(id, status , clientId)) {
			
			if (status == true) {
				
				Mailler mailler = new Mailler();
				mailler.setMessage("<h4>Please click on the link below<h4><br>"
						+ "<a href='http://localhost:8080/SOD/client/client_contract_management/'>Click here</a>");

				System.out.println(SecurityContextHolder.getContext().getAuthentication().getName());
				mailler.setTO("clientin81@gmail.com");
				if (mailler.sendMail()) {
					return "success";
				}
			}	
			
		}
		return "failure";
	}

	@RequestMapping(value = "/admin/dashboard/jobseeker/profile", method = RequestMethod.GET)
	public ModelAndView goToAdminJobSeekerProfile(@RequestParam("id") int id) {
		ModelAndView modelAndView = new ModelAndView("adminJobSeekerProfile");
		Expert expert = getExpertService().byId(id);
		modelAndView.addObject("expertDetails", expert);
		modelAndView.addObject("activeNav", "expert");
		modelAndView.addObject("expertSkills", getSkillService().getAllSkillsByExpert(expert.getSkillList()));
		return modelAndView;
	}
	
}
