/*
 * @Author Hardev Singh
 *
 * */
package com.teklink.sod.controller;

import java.io.File;
import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.teklink.sod.model.SaveAttachedFile;
import com.teklink.sod.model.client.ClientPacks;
import com.teklink.sod.model.client.ClientProducts;
import com.teklink.sod.service.SkillService;
import com.teklink.sod.service.client.ClientPacksService;
import com.teklink.sod.service.client.ClientProductsService;

@Controller
public class AdminProductController {

	@Autowired
	private ClientProductsService clientProductService;

	@Autowired
	private SkillService skillsService;
	
	@Autowired
	private ClientPacksService clientPacksService;

	public SkillService getSkillsService() {
		return skillsService;
	}

	public void setSkillsService(SkillService skillsService) {
		this.skillsService = skillsService;
	}

	public ClientProductsService getClientProductService() {
		return clientProductService;
	}

	public void setClientProductService(ClientProductsService clientProductService) {
		this.clientProductService = clientProductService;
	}

	public ClientPacksService getClientPacksService() {
		return clientPacksService;
	}

	public void setClientPacksService(ClientPacksService clientPacksService) {
		this.clientPacksService = clientPacksService;
	}

	@RequestMapping(value = "/admin/dashboard/pricing_table")
	public String pricingtablePage() {
		return "admin_pricing_table";
	}
	
	
	@RequestMapping(value = "/admin/dashboard/update_pricing_table")
	public String updatepricingtablePage(Model model) {
		model.addAttribute("clientPacks", new ClientPacks());
		return "admin_update_pricing_table";
	}
	
	@RequestMapping(value="/admin/dashboard/updatepack", method = RequestMethod.POST)
	public String packUpdate(@Valid @ModelAttribute("clientPacks") ClientPacks clientPacks, BindingResult bindingResult) {
		System.out.println("enter");
		if(bindingResult.hasErrors()) {
			System.out.println("Fail");
			return "admin_update_pricing_table";
		}
		System.out.println("pass");
		String i = clientPacks.getPack_name();
		int id = Integer.parseInt(i);
		clientPacks.setId(id);
		ClientPacks packname = getClientPacksService().getPackById(id);
		clientPacks.setPack_name(packname.getPack_name());
		System.out.println(clientPacks.getPack_name());
		System.out.println(clientPacks.getPack_price());
		System.out.println(clientPacks.getPack_description());
		if(getClientPacksService().packUpdate(clientPacks)) {
			return "redirect:/admin/dashboard/pricing_table";
		}
		return "admin_update_pricing_table";
		
	}

	@RequestMapping(value = "/admin/dashboard/add_product_form")
	public String addproductformPage(@ModelAttribute("prodtype") String prodtype, Model model) {
		model.addAttribute("adminClientProduct", new ClientProducts());
		model.addAttribute("ProdType", prodtype);
		return "admin_add_product_form";
	}

	@RequestMapping(value = "/admin/dashboard/pack_prod_desc/pack/{packid}", method = RequestMethod.GET)
//	@PostAuthorize("adminClientProduct.prodType==principal.prodType")
	public ModelAndView ViewProd(@PathVariable("packid") int pack) {
		String path = null;
		/*returnObject!=null?*/
		switch (pack) {
		case 1:
			path = "admin_package-1_prod_desc";
			break;
		case 2:
			path = "admin_package-2_prod_desc";
			break;
		case 3:
			path = "admin_package-3_prod_desc";
			break;
		}
		ModelAndView modelAndView = new ModelAndView(path);
		List<ClientProducts> adminClientProduct = getClientProductService().viewClientProduct(pack);
		modelAndView.addObject("adminClientProduct", adminClientProduct);
	//	System.out.println(principal.prodType);
		return modelAndView;
	}

	@RequestMapping(value = "/admin/dashboard/prodAddSuccess", method = RequestMethod.POST)
	public String addSuccess(@Valid @ModelAttribute("adminClientProduct") ClientProducts adminClientProduct,
			BindingResult bindingResult, Model model, RedirectAttributes redirectAttributes,
			@RequestParam("productimage") MultipartFile productimage) {
		try {
			if (!productimage.isEmpty()) {
				if(productimage.getSize()>3000000) {
					model.addAttribute("error", "The size of the Image is to large.");
					model.addAttribute("ProdType", adminClientProduct.getProdType());
					return "admin_add_product_form";
					}
				
				if (bindingResult.hasErrors()) {
					model.addAttribute("ProdType", adminClientProduct.getProdType());
					return "admin_add_product_form";
				}
				
				String absolutePath="C:\\Users\\user\\eclipse-workspace\\SOD\\src\\main\\webapp\\assets\\productImages\\";
				File absolutePathofProductimage = new File(absolutePath + File.separator);
				if (!absolutePathofProductimage.exists())
					absolutePathofProductimage.mkdirs();
				/*String absoluteImagePath= adminClientProduct.getProdName() + "-"
						+ new SimpleDateFormat("yyyy.MM.dd.HH.mm.ss").format(new Date());*/
				
 				 String rootPath=System.getProperty("catalina.home"); 
				/*File dir = new File(rootPath + File.separator + "SOD" + File.separator + "Product-image"
						+ File.separator + adminClientProduct.getProdName() + "-"
						+ new SimpleDateFormat("yyyy.MM.dd.HH.mm.ss").format(new Date()) + File.separator);*/
 				File dir = new File(rootPath + File.separator + "SOD" +File.separator + "Product-image"+ File.separator);
				if (!dir.exists())
					dir.mkdirs();
				String prodImagePath = new SaveAttachedFile(productimage, adminClientProduct.getProdName(), dir).uploadFile();
				String prodImageAbsilutePath = new SaveAttachedFile(productimage, adminClientProduct.getProdName(), absolutePathofProductimage).uploadFile();
				if (prodImagePath != null && prodImageAbsilutePath != null) {
					adminClientProduct.setProdImagePath(prodImageAbsilutePath);
					if (getClientProductService().addProduct(adminClientProduct)) {
					//	redirectAttributes.addFlashAttribute("pack", adminClientProduct.getProdType());
						return "redirect:/admin/dashboard/pack_prod_desc/pack/"+adminClientProduct.getProdType();
					}
				}
			}
			model.addAttribute("error", "Please upload the files");
			model.addAttribute("ProdType", adminClientProduct.getProdType());
			return "admin_add_product_form";

		} catch (Exception ex) {
			ex.printStackTrace();
		}
		model.addAttribute("ProdType", adminClientProduct.getProdType());
		return "admin_add_product_form";
	}

	@RequestMapping(value = "/admin/dashboard/product_view", method = RequestMethod.GET)
	public ModelAndView ViewSpecificProduct(@ModelAttribute("ViewProduct") int viewProduct) {
		ModelAndView modelAndView = new ModelAndView("admin_product_view");
		ClientProducts prodView = getClientProductService().viewClientProductById(viewProduct);
		modelAndView.addObject("prodView", prodView);
		return modelAndView;
	}

	@RequestMapping(value = "/admin/dashboard/update_product_form", method = RequestMethod.GET)
	public ModelAndView UpdateSpecificProduct(@ModelAttribute("UpdateProduct") int updateProduct) {
		ModelAndView modelAndView = new ModelAndView("admin_update_product_form");
		ClientProducts UpdateProduct = getClientProductService().viewClientProductById(updateProduct);
		modelAndView.addObject("UpdateProduct", UpdateProduct);
		return modelAndView;
	}

	@RequestMapping(value = "/admin/dashboard/prodUpdatedSuccess", method = RequestMethod.POST)
	public String updateProduct(@Valid @ModelAttribute("UpdateProduct") ClientProducts adminClientProduct,
			BindingResult bindingResult, RedirectAttributes redirectAttributes, @RequestParam("productimage") MultipartFile productimage, Model model) {
		if (bindingResult.hasErrors()) {
			return "admin_update_product_form";
		}
		if(!productimage.isEmpty()) {
			if(productimage.getSize()>3000000) {
				model.addAttribute("error", "The size of the Image is to large.");
				return "admin_update_product_form";
			}
			String absolutePath="C:\\Users\\user\\eclipse-workspace\\SOD\\src\\main\\webapp\\assets\\productImages\\";
			File absolutePathofProductimage = new File(absolutePath + File.separator);
			if (!absolutePathofProductimage.exists())
				absolutePathofProductimage.mkdirs();
			 String rootPath=System.getProperty("catalina.home"); 
				File dir = new File(rootPath + File.separator + "SOD" +File.separator + "Product-image"+ File.separator);
				if (!dir.exists())
					dir.mkdirs();
				String prodImagePath = new SaveAttachedFile(productimage, adminClientProduct.getProdName(), dir).uploadFile();
				String prodImageAbsilutePath = new SaveAttachedFile(productimage, adminClientProduct.getProdName(), absolutePathofProductimage).uploadFile();
				if (prodImagePath != null && prodImageAbsilutePath != null) 
					adminClientProduct.setProdImagePath(prodImageAbsilutePath);
		}
		
		/*redirectAttributes.addFlashAttribute("pack", adminClientProduct.getProdType());*/
		getClientProductService().updateProduct(adminClientProduct);
		return "redirect:/admin/dashboard/pack_prod_desc/pack/"+adminClientProduct.getProdType();
	}
}
