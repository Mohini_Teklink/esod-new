/*
 * @Author Hardev Singh
 *
 * */
package com.teklink.sod.controller;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.ModelAndView;

import com.teklink.sod.model.ClientSkill;
import com.teklink.sod.model.SaveAttachedFile;
import com.teklink.sod.model.client.ClientProducts;
import com.teklink.sod.service.SkillService;
import com.teklink.sod.service.client.ClientProductsService;

@Controller
public class AdminJobListController {

	@Autowired
	private ClientProductsService adminClientProductService;

	@Autowired
	private SkillService skillService;

	public ClientProductsService getAdminClientProductService() {
		return adminClientProductService;
	}

	public void setAdminClientProductService(ClientProductsService adminClientProductService) {
		this.adminClientProductService = adminClientProductService;
	}

	public SkillService getSkillService() {
		return skillService;
	}

	public void setSkillService(SkillService skillService) {
		this.skillService = skillService;
	}

	@RequestMapping(value = "/admin/dashboard/clientlist	")
	public ModelAndView joblistPage() {
		ModelAndView modelAndView = new ModelAndView("adminClientList	");
		List<ClientProducts> clientClientList = getAdminClientProductService().viewClientProduct();
		if (clientClientList != null) {
			List<ClientSkill> clientSkillList = new ArrayList<>();
			for (ClientProducts obj : clientClientList) {
				ClientSkill skillsClient = new ClientSkill();
				skillsClient.setClientProducts(obj);
				skillsClient.setSkillList(getSkillService().getAllSkillsByExpert(obj.getSkills()));
				clientSkillList.add(skillsClient);
			}
			modelAndView.addObject("clientSkillList", clientSkillList);
		}
		return modelAndView;
	}

//	@RequestMapping(value = "/admin/dashboard/view_job_form", method = RequestMethod.GET)
//	public ModelAndView ViewSpecificJob(@ModelAttribute("ViewJob") int ViewJob) {
//		ModelAndView modelAndView = new ModelAndView("admin_view_job_form");
//		ClientSkill client_SkillsList = new ClientSkill();
//		ClientProducts client = getAdminClientProductService().viewClientProductById(ViewJob);
//		client_SkillsList.setClientProducts(client);
//		client_SkillsList.setSkillList(getSkillService().getAllSkillsByExpert(client.getSkills()));
//		modelAndView.addObject("jobView", client_SkillsList);
//		return modelAndView;
//	}
//
//	@RequestMapping(value = "/admin/dashboard/add_job_form")
//	public String addjobformPage(Model model) {
//		model.addAttribute("adminClientProduct", new ClientProducts());
//		return "admin_add_job_form";
//	}
//
//	@RequestMapping(value = "/admin/dashboard/jobAddSuccess", method = RequestMethod.POST)
//	public String jobAddSuccess(@Valid @ModelAttribute("adminClientProduct") ClientProducts adminClientProduct,
//			BindingResult bindingResult, @RequestParam("productimage") MultipartFile productimage, Model model) {
//		
//		try {
//			if(!productimage.isEmpty()) {
//				if(productimage.getSize()>3000000) {
//					model.addAttribute("error", "The size of the Image is to large.");
//					return "admin_add_job_form";
//				}
//				
//				if (bindingResult.hasErrors()) {
//					return "admin_add_job_form";
//			}
//				
//				String absolutePath="C:\\Users\\user\\eclipse-workspace\\SOD\\src\\main\\webapp\\assets\\productImages\\";
//				File absolutePathofProductimage = new File(absolutePath + File.separator);
//				if (!absolutePathofProductimage.exists())
//					absolutePathofProductimage.mkdirs();
//				
//				String rootPath=System.getProperty("catalina.home");
//				File dir = new File(rootPath + File.separator + "SOD" +File.separator + "Product-image"+ File.separator);
//				if (!dir.exists())
//					dir.mkdirs();
//				
//				String prodImagePath = new SaveAttachedFile(productimage, adminClientProduct.getProdName(), dir).uploadFile();
//				String prodImageAbsolutePath = new SaveAttachedFile(productimage, adminClientProduct.getProdName(), absolutePathofProductimage).uploadFile();
//				if(prodImagePath != null) {
//					adminClientProduct.setProdImagePath(prodImageAbsolutePath);	
//					if(getAdminClientProductService().addProduct(adminClientProduct)) {
//					return "redirect:/admin/dashboard/job_list";
//					}
//				}
//		  }
//			model.addAttribute("error", "Please upload the image.");
//			return "admin_add_job_form";
//		}	
//		catch(Exception ex) {
//			ex.printStackTrace();
//		}
//		return "admin_add_job_form";
//	}
//
//	@RequestMapping(value = "/admin/dashboard/update_job_form", method = RequestMethod.GET)
//	public ModelAndView UpdateSpecificJob(@ModelAttribute("updateJob") int updateJob) {
//		ModelAndView modelAndView = new ModelAndView("admin_update_job_form");
//		ClientProducts UpdateJob = getAdminClientProductService().viewClientProductById(updateJob);
//		modelAndView.addObject("UpdateJob", UpdateJob);
//		return modelAndView;
//	}
//
//	@RequestMapping(value = "/admin/dashboard/jobUpdatedSuccess", method = RequestMethod.POST)
//	public String updateJob(@Valid @ModelAttribute("UpdateJob") ClientProducts adminClientProduct, BindingResult bindingResult) {
//		if (bindingResult.hasErrors()) {
//			return "admin_update_job_form";
//		}
//		if(getAdminClientProductService().updateProduct(adminClientProduct))
//		return "redirect:/admin/dashboard/job_list";
//		return "admin_update_job_form";
//	}

}
