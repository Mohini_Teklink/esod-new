package com.teklink.sod.controller;

import javax.websocket.server.PathParam;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

@Controller
public class FinishController {

	@RequestMapping(value = "/client/thankyou", method = RequestMethod.GET)
	public ModelAndView goToClientThankYouWindow(Model model, ModelAndView modelAndView) {
		/*, @PathParam("msg") boolean msg- argument*/
		/*model.addAttribute("msg",msg);*/
		modelAndView.setViewName("thankyou_page");
		return modelAndView;
	}
}
