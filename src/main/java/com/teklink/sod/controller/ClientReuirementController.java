package com.teklink.sod.controller;

import java.util.Date;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.teklink.sod.model.Mailler;
import com.teklink.sod.model.admin.AdminContract;
import com.teklink.sod.model.client.ClientRequirement;
import com.teklink.sod.service.client.ClientRequirementService;

@Controller
public class ClientReuirementController {

	@Autowired
	private ClientRequirementService clientRequirementService;
	
	public ClientRequirementService getClientRequirementService() {
		return clientRequirementService;
	}

	public void setClientRequirementService(ClientRequirementService clientRequirementService) {
		this.clientRequirementService = clientRequirementService;
	}
	
	@RequestMapping(value = "/clientRequirement", method = RequestMethod.POST)
	public String submitClientContract(@Valid @ModelAttribute("clientRequirement") ClientRequirement clientRequirement ,
			BindingResult bindingResult, Model model) {

		try {
/*			if (!sign.isEmpty() && !photo.isEmpty()) {*/
				/*if (bindingResult.hasErrors()) {
					model.addAttribute("clientRequirement", null);//getClientPacksService().getPackById(id));
					return "clientRequirement";
				}*/
				
				if (clientRequirement  != null ) {
//					clientContract.setImagePath(phtPath);
//					clientContract.setSignPath(signPath);
					/*clientContract.setProdId(id);
					clientContract.setProdType(packName);*/
					clientRequirement.setCreatedDate(new Date());
					clientRequirement.setIsActive(true);
					if (getClientRequirementService().saveClientRequirement(clientRequirement)) {
						return "thankyou_page";
					}
					//model.addAttribute("clientRequirement", getClientRequirementService());
					model.addAttribute("error", "Something went wrong!<br>Please try after sometime.");
					return "clientPacksDescriptions";

				}
			/*}*/

		} catch (Exception exp) {
			exp.printStackTrace();

		}
		model.addAttribute("clientPack", getClientRequirementService());
		model.addAttribute("error", "Please upload the files");
		return "clientContractManagement";
	}
	
	@RequestMapping(value = "/get_client_list", method = RequestMethod.GET)
	public String getClientList(Model model) {
		
		model.addAttribute("clientList", getClientRequirementService().getClientList());
		return "client_requirement_list";
	}
	
	@RequestMapping(value = "/admin/dashboard/submit_client_requirement_request", method = RequestMethod.GET)
	public String saveClientRequirementRequest() {
		
		Mailler mailler = new Mailler();
		mailler.setMessage("<h4>Your Request has been submitted<h4>");

		System.out.println(SecurityContextHolder.getContext().getAuthentication().getName());
		mailler.setTO("clientin81@gmail.com");
		if (mailler.sendMail()) {
			
			
		}
		
		
		return "client_requirement_list";

			}
	
	@RequestMapping(value = "/admin/dashboard/tbd_client_requirement_request", method = RequestMethod.GET)
	public String tbdClientRequirementRequest() {
		
		Mailler mailler = new Mailler();
		mailler.setMessage("<h4>We need to discuss more about requirement.<h4>");

		System.out.println(SecurityContextHolder.getContext().getAuthentication().getName());
		mailler.setTO("clientin81@gmail.com");
		if (mailler.sendMail()) {
			
			
		}
		
		
		return "client_requirement_list";

		
	
	}
}
