package com.teklink.sod.controller;


/**
 * @author Prateek Raj
 */

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

@Controller
public class MyController {

	@RequestMapping(value = "/", method = RequestMethod.GET)
	public String gotoIndex(Model model) {
		if(GlobalController.getAdminDetails()!=null)
			return "redirect:/admin/dashboard";
		if(GlobalController.getExpertDetails()!=null)
			return "redirect:/expert/jdp";
		if(GlobalController.getClientDetails()!=null)
			return "redirect:/client/clientPacks";
		return "index";
	}

	@RequestMapping(value = "/AboutUs", method = RequestMethod.GET)
	public String goToAbout(Model model) {
		return "AboutUs";
	}

	@RequestMapping(value = "/ContactUs", method = RequestMethod.GET)
	public String goToContact(Model model) {
		return "ContactUs";
	}

	@RequestMapping(value = "/Technologies", method = RequestMethod.GET)
	public String goToTechnologies(Model model) {
		return "Technologies";
	}

}