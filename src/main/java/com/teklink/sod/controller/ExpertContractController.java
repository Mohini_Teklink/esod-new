package com.teklink.sod.controller;

/**
 * @author Prateek Raj
 */

import java.io.File;
import java.text.SimpleDateFormat;
import java.util.Date;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.teklink.sod.model.Mailler;
import com.teklink.sod.model.SaveAttachedFile;
import com.teklink.sod.model.expert.Expert;
import com.teklink.sod.model.expert.ExpertContractManagement;
import com.teklink.sod.service.ExpertContractService;
import com.teklink.sod.service.ExpertService;
import com.teklink.sod.service.SkillService;

@Controller
public class ExpertContractController {

	@Autowired
	private ExpertContractService expertContractService;

	@Autowired
	private ExpertService expertService;

	@Autowired
	private SkillService skillService;

	public SkillService getSkillService() {
		return skillService;
	}

	public void setSkillService(SkillService skillService) {
		this.skillService = skillService;
	}

	public ExpertService getExpertService() {
		return expertService;
	}

	public void setExpertService(ExpertService expertService) {
		this.expertService = expertService;
	}

	public ExpertContractService getExpertContractService() {
		return expertContractService;
	}

	public void setExpertContractService(ExpertContractService expertContractService) {
		this.expertContractService = expertContractService;
	}

	@RequestMapping(value = "/expert/jdp/{productId}/application/submit", method = RequestMethod.GET)
	public String gotToJobsPage(
			@Valid @ModelAttribute("expertContractForm") ExpertContractManagement expertContractManagement,
			BindingResult bindingResult, @PathVariable("productId") int prodId, Model model, RedirectAttributes rdA,
			MultipartFile photo, MultipartFile sign) {
		// @RequestParam("photo") MultipartFile photo, @RequestParam("sign")
		// MultipartFile sign,
		int expId = GlobalController.getExpertDetails().getId();
		System.out
				.println(expertContractManagement.toString() + "-----------------------\n" + bindingResult.toString());
		try {
			if (prodId > 0) {
				if (bindingResult.hasErrors()) {
					model.addAttribute("expert", getExpertService().byId(expId));
					model.addAttribute("productId", expertContractManagement.getProductId());
					return "expertContractManagement";
				}

				String rootPath = System.getProperty("catalina.home");
				File dir = new File(rootPath + File.separator + "SOD" + File.separator + "Expert-Doc" + File.separator
						+ expertContractManagement.getProductId() + "-" + expertContractManagement.getFirstName() + "-"
						+ new SimpleDateFormat("yyyy.MM.dd.HH.mm.ss").format(new Date()) + File.separator);
				if (!dir.exists())
					dir.mkdirs();
//				String phtPath = new SaveAttachedFile(photo, "img", dir).uploadFile();
//				String signPath = new SaveAttachedFile(sign, "sign", dir).uploadFile();
				String phtPath = null, signPath = null;

				if (phtPath == null && signPath == null) {
					// expertContractManagement.setImagePath(phtPath);
					// expertContractManagement.setSignPath(signPath);
					System.out.println("path null");
					expertContractManagement.setAppliedby(expId);
					if (getExpertContractService().saveExpertContract(expertContractManagement)) {
						// GlobalController expert = new GlobalController();
						Expert exp = GlobalController.getExpertDetails();
						String msg = "Hi " + exp.getFirstName() + " " + exp.getLastName() + "\r\n" + "\r\n"
								+ "Thanks for your interest\r\n"
								+ "Our team has taken your request and will be contacting you very soon.\r\n" + "\r\n"
								+ "Your Application Details: \r\n" + "Name:  " + exp.getFirstName() + " "
								+ exp.getLastName() + "\r\n" + "Contact Number : " + exp.getMobileNumber() + "\r\n"
								+ "Skills : " + getSkillService().getAllSkillsStrByExpert(exp.getSkillList()) + " \r\n"
								+ "\r\n" + "All the best!\r\n" + "\r\n" + "Regards, \r\n" + "Team Teklink \r\n"
								+ "Website URL � www.teklinkinternational.com\r\n" + "\r\n" + "";
						Mailler mailler = new Mailler();
						mailler.setMessage(msg);
						System.out.println(SecurityContextHolder.getContext().getAuthentication().getName());
						mailler.setTO(getExpertService().byId(expId).getEmail());
						mailler.sendMail();
						rdA.addFlashAttribute("expert", getExpertService().byId(expId));
//						return "redirect:/expert/jdp";
						return "redirect:/expert/finishWindow?msg=true";
					}
					model.addAttribute("expert", getExpertService().byId(expId));
					model.addAttribute("productId", expertContractManagement.getProductId());
					model.addAttribute("error", "Something went wrong!<br>Please try after sometime.");
					return "expertContractManagement";

				}
			}
		} catch (Exception exp) {
			exp.printStackTrace();
		}
//		model.addAttribute("error", "Please upload the files");
//		model.addAttribute("expert", getExpertService().byId(expId));
		model.addAttribute("productId", expertContractManagement.getProductId());
		return "expertContractManagement";
	}

}
