package com.teklink.sod.controller;


/**
 * @author Prateek Raj
 */

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.teklink.sod.service.client.ClientPacksService;
import com.teklink.sod.service.client.ClientProductsService;

@Controller
public class ClientPacksController {

	@Autowired
	private ClientPacksService clientPacksService;

	@Autowired
	private ClientProductsService clientProductsService;
	
	
	public ClientProductsService getClientProductsService() {
		return clientProductsService;
	}

	public void setClientProductsService(ClientProductsService clientProductsService) {
		this.clientProductsService = clientProductsService;
	}

	public ClientPacksService getClientPacksService() {
		return clientPacksService;
	}

	public void setClientPacksService(ClientPacksService clientPacksService) {
		this.clientPacksService = clientPacksService;
	}
	
	
	@RequestMapping(value = "/client/clientPacks", method = RequestMethod.GET)
	public String gotoClientPacks(Model model) {
		model.addAttribute("clientPacks", getClientPacksService().getAllPacksList());
		return "clientPacks";
	}
	
	@RequestMapping(value = "/client/clientPacks/{id}/{packName}", method = RequestMethod.GET)
	public String goToPackProducts(@PathVariable("packName") String packName, Model model, @PathVariable("id") int id) {
		model.addAttribute("clientPack", getClientPacksService().getPackById(id));
		model.addAttribute("packageId", id);
		return "clientPackProductList";
	}

	@RequestMapping(value = "/client/clientPacks/{pack_id}/{packName}/{product_id}/{productName}", method = RequestMethod.GET)
	public String goToPackDescription(@PathVariable("packName") String packName, Model model,
			@PathVariable("pack_id") int packId, @PathVariable("productName") String productName,
			@PathVariable("product_id") int productId) {
		model.addAttribute("packageId", packId);
		model.addAttribute("clientPack", getClientPacksService().getPackById(packId));
		return "clientPacksDescriptions";
	}

	/*@RequestMapping(value = "/client/clientPacks/{pack_id}/{packName}/{product_id}/{productName}/clientContract", method = RequestMethod.GET)
	public String gotoClientContract(Model model, @PathVariable("pack_id") int id,
			@PathVariable("packName") String packName) {
		model.addAttribute("clientPack", getClientPacksService().getPackById(id));
		model.addAttribute("clientContract", new ClientContract());
		return "clientContractManagement";
	}*/

	
}
