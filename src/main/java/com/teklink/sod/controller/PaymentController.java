package com.teklink.sod.controller;


import javax.websocket.server.PathParam;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.teklink.sod.service.PaymentService;

@Controller
public class PaymentController {


	@Autowired
	private PaymentService paymentSevice;
	
	public PaymentService getPaymentSevice() {
		return paymentSevice;
	}

	public void setPaymentSevice(PaymentService paymentSevice) {
		this.paymentSevice = paymentSevice;
	}

/*	@RequestMapping(value = "/payment", method = RequestMethod.GET)
	public String goToPayment(Model model) {
		model.addAttribute("payment", new Payment());
		return "payment";
	}

	@RequestMapping(value = "/paymentAttempt", method = RequestMethod.POST)
	public String goToPaymentAttempt(@Valid @ModelAttribute("payment") Payment payment, BindingResult bindingResult,
			Model model) {
		if (bindingResult.hasErrors())
			return "payment";
		payment.setDateofTransaction(new Date());
		if (getPaymentSevice().savePaymentEntries(payment)) {
			Mailler mailler = new Mailler();
			mailler.setTO("r.prateek11@gmail.com");
			if (mailler.sendMail()) {
				model.addAttribute("message", "success");
			}
			return "redirect:/finishWindow";
		}
		model.addAttribute("message", "failed");
		return "redirect:/finishWindow";
	}
*/
	@RequestMapping(value = "/client/finishWindow", method = RequestMethod.GET)
	public String goToClientFinishWindow(Model model, @PathParam("msg") boolean msg) {
		model.addAttribute("msg",msg);
		return "finishWindow";
	}
	
	@RequestMapping(value = "/expert/finishWindow", method = RequestMethod.GET)
	public String goToExpertFinishWindow(Model model, @PathParam("msg") boolean msg) {
		model.addAttribute("msg", msg);
		return "finishWindow";
	}


}
