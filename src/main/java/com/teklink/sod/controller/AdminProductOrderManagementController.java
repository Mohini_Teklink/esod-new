package com.teklink.sod.controller;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import com.teklink.sod.model.client.ClientProductOrderManagement;
import com.teklink.sod.model.client.ClientProducts;
import com.teklink.sod.model.expert.Expert;
import com.teklink.sod.model.expert.ExpertContractManagement;
import com.teklink.sod.service.ExpertContractService;
import com.teklink.sod.service.ExpertService;
import com.teklink.sod.service.client.ClientProductsService;

@Controller
public class AdminProductOrderManagementController {
	
	@Autowired
	ClientProductsService clientProductsService;
	
	@Autowired
	ExpertContractService expertContractService;
	
	@Autowired
	ExpertService expertService;
	
	public ExpertService getExpertService() {
		return expertService;
	}

	public void setExpertService(ExpertService expertService) {
		this.expertService = expertService;
	}

	public ExpertContractService getExpertContractService() {
		return expertContractService;
	}

	public void setExpertContractService(ExpertContractService expertContractService) {
		this.expertContractService = expertContractService;
	}

	public ClientProductsService getClientProductsService() {
		return clientProductsService;
	}

	public void setClientProductsService(ClientProductsService clientProductsService) {
		this.clientProductsService = clientProductsService;
	}

	@RequestMapping(value="/admin/dashboard/order_management")
	public ModelAndView orderManagement() {
		ModelAndView modelAndView =new ModelAndView("admin_order_management");
		List<ExpertContractManagement> expertContractList= getExpertContractService().getAllExpertContract();
		if(expertContractList != null) {
			List<ClientProductOrderManagement> orderList = new ArrayList<>();
			for(ExpertContractManagement obj:expertContractList) {
				if(getClientProductsService().getClientProductByIdAndStatus(obj.getProductId(),true) != null) {
					System.out.println(obj.getId());
					ClientProductOrderManagement clientProductOrderManagement = new ClientProductOrderManagement();
					clientProductOrderManagement.setExpertContractManagement(obj);
					clientProductOrderManagement.setClientProducts(getClientProductsService().getClientProductByIdAndStatus(obj.getProductId(),true));
					orderList.add(clientProductOrderManagement);
				}	
			}
			modelAndView.addObject("ProductOrderList", orderList);
		}
		return modelAndView;
	}
	
	@RequestMapping(value="/admin/dashboard/order_cancel")
	public ModelAndView orderCancelList() {
		ModelAndView modelAndView =new ModelAndView("admin_order_cancel");
		List<ExpertContractManagement> expertContractList= getExpertContractService().getAllExpertContract();
		if(expertContractList != null) {
			List<ClientProductOrderManagement> canceledOrderList = new ArrayList<>();
			for(ExpertContractManagement obj:expertContractList) {
				if(getClientProductsService().getClientProductByIdAndStatus(obj.getProductId(),false) != null) {
					ClientProductOrderManagement clientProductOrderManagement = new ClientProductOrderManagement();
					clientProductOrderManagement.setClientProducts(getClientProductsService().getClientProductByIdAndStatus(obj.getProductId(),false));
					clientProductOrderManagement.setExpertContractManagement(obj);
					canceledOrderList.add(clientProductOrderManagement);
				}	
			}
			modelAndView.addObject("CanceledProductOrderList", canceledOrderList);
		}
		return modelAndView;
		
	}

	@RequestMapping(value="/admin/dashboard/order_description/id/{orderId}", method=RequestMethod.GET)
	public ModelAndView cancelOrderDesc(@PathVariable("orderId") int orderId) {
		ModelAndView modelAndView=new ModelAndView("admin_order_description");
		ExpertContractManagement expertContract = getExpertContractService().getExpertById(orderId);
		System.out.println("hello");
		System.out.println(expertContract.getProductId());
		ClientProducts clientProduct = getClientProductsService().viewClientProductById(expertContract.getProductId());
		Expert expertDetail = getExpertService().byId(expertContract.getAppliedby());
		modelAndView.addObject("orderProduct", clientProduct);
		modelAndView.addObject("expertContract", expertContract);
		modelAndView.addObject("expertDetail", expertDetail);
		return modelAndView;
	}
}
