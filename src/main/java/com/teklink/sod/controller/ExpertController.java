package com.teklink.sod.controller;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.validation.Valid;

import org.omg.CORBA.PUBLIC_MEMBER;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;


import com.teklink.sod.model.ClientSkill;
import com.teklink.sod.model.client.ClientProducts;
import com.teklink.sod.model.expert.Expert;
import com.teklink.sod.model.expert.ExpertCardinalities;
import com.teklink.sod.model.expert.ExpertContractManagement;
import com.teklink.sod.service.ExpertService;
import com.teklink.sod.service.SkillService;
import com.teklink.sod.service.client.ClientProductsService;
import com.teklink.sod.service.impl.FileService;

@Controller
public class ExpertController {
	
	
	@Autowired
    private FileService fileService;

	@Autowired
	private ExpertService expertService;

	@Autowired
	private ClientProductsService clientProductsService;

	public FileService getFileService() {
		return fileService;
	}

	public void setFileService(FileService fileService) {
		this.fileService = fileService;
	}

	@Autowired
	private SkillService skillService;

	public ClientProductsService getClientProductsService() {
		return clientProductsService;
	}

	public void setClientProductsService(ClientProductsService clientProductsService) {
		this.clientProductsService = clientProductsService;
	}

	public SkillService getSkillService() {
		return skillService;
	}

	public void setSkillService(SkillService skillService) {
		this.skillService = skillService;
	}

	public ExpertService getExpertService() {
		return expertService;
	}

	public void setExpertService(ExpertService expertService) {
		this.expertService = expertService;
	}

	@RequestMapping(value = "/expertLogin", method = RequestMethod.GET)
	public String gotoExpertLoginPage(Model model) {
		if (GlobalController.getAdminDetails() != null)
			return "redirect:/admin/dashboard";
		if (GlobalController.getExpertDetails() != null)
			return "redirect:/expert/jdp";
		if (GlobalController.getClientDetails() != null)
			return "redirect:/client/clientPacks";
		return "expertLogin";
	}

	@RequestMapping(value = "/expert/expertLoginProcess", method = RequestMethod.GET)
	public String loginPage(Model model) {
		return "expertLogin";
	}

	@RequestMapping(value = "/expertLoginSuccess", method = RequestMethod.POST)
	public ModelAndView gotoExpertLoginSuccess(

			@Valid @ModelAttribute("expertModelCardinalities") ExpertCardinalities expertCardinalities,
			BindingResult bindingResult, RedirectAttributes redir) {
		if (bindingResult.hasErrors()) {
			return new ModelAndView("expertLogin");
		}
		ModelAndView modelAndView = new ModelAndView("redirect:/jdp");
		Expert expert = getExpertService().validateExpertCredential(expertCardinalities.getExpert_email(),
				expertCardinalities.getExpert_password());
		if (expert == null) {
			modelAndView = new ModelAndView("expertLogin");
			modelAndView.addObject("error", "User doesn't exist");
		} else if (!expert.isStatus()) {
			modelAndView = new ModelAndView("expertLogin");
			modelAndView.addObject("error", "You are blocked ! <br>Kindly contact support.");
		}
		redir.addFlashAttribute("jobseeker", expert);
		return modelAndView;
	}

	/*
	 * @RequestMapping(value="/jdp", method= RequestMethod.GET) public String
	 * gotToJobsPage(Model model,@ModelAttribute("expert") Expert expert) {
	 * System.out.println(expert.toString()); model.addAttribute("expert", expert);
	 * return "jobDescription"; }
	 */

	/*
	 * Pass ClientProducts object as model attribute which contain the list of Jobs
	 */
	@RequestMapping(value = "/expert/jdp", method = RequestMethod.GET)
	public String gotToJobsPage(Model model) {

		List<ClientProducts> clientJobList = getClientProductsService().viewClientProduct();
		if (clientJobList != null) {
			List<ClientSkill> clientSkillList = new ArrayList<>();
			for (ClientProducts obj : clientJobList) {
				ClientSkill skillsClient = new ClientSkill();
				skillsClient.setClientProducts(obj);
				skillsClient.setSkillStr(getSkillService().getAllSkillsStrByExpert(obj.getSkills()));
				clientSkillList.add(skillsClient);
			}
			model.addAttribute("productList", clientSkillList);
		}

		return "jobDescription";
	}

	/*
	 * 
	 * */
	@RequestMapping(value = "/expert/jdp/{id}/application", method = RequestMethod.GET)
	public String gotToJobsApplication(Model model, @PathVariable("id") int id) {
		model.addAttribute("expertContractForm", new ExpertContractManagement());
		model.addAttribute("productId", id);
		return "expertContractManagement";
	}

	@RequestMapping(value = "/expert/changePassword")
	public String goToChangePassword() {
		return "changePassword";
	}

	@RequestMapping(value = "/expert/changePassword", method = RequestMethod.POST)
	public String changeClientPassword(@RequestParam("old_password") String oldPasswod,
			@RequestParam("new_password") String newPassword, @RequestParam("confirmpassword") String confirmPassword,
			Model model) {
		if (oldPasswod == "" || newPassword == "") {
			model.addAttribute("error1", "Password can not be blank");
			return "changePassword";
		}
		Expert expert = GlobalController.getExpertDetails();
		System.out.println(expert.getFirstName());
		BCryptPasswordEncoder bCryptPasswordEncoder = new BCryptPasswordEncoder();
		if (!confirmPassword.equals(newPassword) || !bCryptPasswordEncoder.matches(oldPasswod, expert.getPassword())) {
			model.addAttribute("error1", "Password not match");
			return "changePassword";
		}
		expert.setPassword(bCryptPasswordEncoder.encode(newPassword));
		expert.setConfirmPassword(confirmPassword);
		if (!expertService.updateClient(expert)) {
			model.addAttribute("error1", "Password Not Changed");
			return "changePassword";
		}
		return "redirect:/expert/jdp";
		

	}
	// to open user profile page...
		@RequestMapping(value = "/expert/profile", method = RequestMethod.GET)
		public String expertProfile(@RequestParam(name = "message", required = false) String message, Model model) {
			Expert expert = GlobalController.getExpertDetails();
			model.addAttribute("title", "User Profile | sod.com");
			model.addAttribute("expert", expert);
			if (message != null) {
				if (message.equals("profileNotUpdated")) {
					model.addAttribute("profileNotUpdated", "Profile Not Updated!!!");
				}
			}
			// return "edit_profile";
			return "jobDescription";
		}
				// to open edit profile page...
		@RequestMapping(value = "/expert/profileEdit" , method = RequestMethod.GET)
		public String  expertProfileUpdate(ModelAndView modelAndView) {
			
			Expert expert = GlobalController.getExpertDetails();
			modelAndView.addObject("title", "Edit User Profile | sod.com");
			modelAndView.setViewName("expertProfileEdit");
			modelAndView.addObject("expert", expert);
			return "edit_profile";
			
		}
		// to open user profile page...
		@RequestMapping(value = "/expert/profile", method = RequestMethod.POST)
		public String expertProfileUpdate(
				@Valid @ModelAttribute("expert") Expert expert,
				BindingResult bindingResult, Model model) {
			// String change = null;
			// Expert loggedInUser = GlobalController.getUserDetails();

			/*new UserProfileValidator().validate(expert, bindingResult);*/

			if (bindingResult.hasErrors()) {
				model.addAttribute("title", "Edit User Profile | sod.com");
				model.addAttribute("expert", expert);
				return "expertProfileEdit";
			}
			return "redirect:/expert/jdp";
		
		
		}
		
	/* to upload Resume */
			

		    @RequestMapping(value = "/expert/upload", method = RequestMethod.POST)
		    public ModelAndView uploadFile(@RequestParam("file") MultipartFile multipartFile){
		        long fileSize = multipartFile.getSize();
		        String fileName = multipartFile.getOriginalFilename();
		        ModelAndView modelAndView = new ModelAndView("upload-success");
		        if(fileService.saveFile(multipartFile)){
		            Map<String, Object> modelMap = new HashMap<>();
		            modelMap.put("fileName", fileName);
		            modelMap.put("fileSize", fileSize);
		            modelAndView.addAllObjects(modelMap);
		            return modelAndView;
		        }
		        return new ModelAndView("upload-failed");
		    }

	

		}
		




