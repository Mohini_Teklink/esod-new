package com.teklink.sod.controller;

import java.util.Calendar;
import java.util.Locale;
import java.util.UUID;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import com.teklink.sod.model.Mailler;
import com.teklink.sod.model.client.Client;
import com.teklink.sod.model.client.ClientForgotPasswordToken;
import com.teklink.sod.service.client.ClientForgotPasswordTokenService;

@Controller
public class ClientForgotPasswordController {
	
	@Autowired
	private ClientForgotPasswordTokenService clientForgotPasswordTokenService;
	
	@RequestMapping(value = "/clientResetPassword", method = RequestMethod.POST)
	 public String resetPassword(Model model, HttpServletRequest request, @RequestParam("email") String Email) {
	     try {
	 
	         Client client = clientForgotPasswordTokenService.getVerifiedClientbyEmail(Email);
	         if (client == null) {
	        	 model.addAttribute("resetEmailWrong",1);
	             model.addAttribute("noUser", "Email does not exist");
	             return "clientlogin";
	         }
	         String token = UUID.randomUUID().toString();
	         ClientForgotPasswordToken clientPasswordResetToken=clientForgotPasswordTokenService.checkClientForgotPasswordToken(client);
	         if(clientPasswordResetToken != null) {
	        	 System.out.println("updated");
	        	 clientForgotPasswordTokenService.updateForgotPasswordToken(clientPasswordResetToken, token);
	         }else {
	        	 clientForgotPasswordTokenService.createForgotPasswordToken(client, token);	        	 
	         }
	         String appUrl = "http://" + request.getServerName() + ":" + request.getServerPort()
	                 + request.getContextPath();
	         
	         String url = appUrl + "/clientChangePassword?id=" + client.getId() + "&token=" + token;
		     String message = "Click on the reset Password Link";
		     Mailler mailler = new Mailler();
				mailler.setMessage(message + " " + url);
				System.out.println(SecurityContextHolder.getContext().getAuthentication().getName());
				mailler.setTO(client.getClient_email());
	         if(mailler.sendMail()) {
	        	 model.addAttribute("Success", "Check email for reset password link");
	        	 model.addAttribute("resetEmailWrong",1);
	             return "clientlogin";
	         }
	     } catch (Exception e) {
	    	 e.printStackTrace();
	     }
	     model.addAttribute("resetEmailWrong",1);
         model.addAttribute("failed", "Some error ocuurs");
         return "clientlogin";
	 }
	
	 @RequestMapping(value = "/clientChangePassword", method = RequestMethod.GET)
	 public String showChangePasswordPage(Locale locale, Model model, @RequestParam("id") long id,
	         @RequestParam("token") String token) {
		 System.out.println(id);
		 System.out.println(token);
		 System.out.println("Pass0");
	     try {
	    	 System.out.println("try");
	         ClientForgotPasswordToken passToken = clientForgotPasswordTokenService.getForgotPasswordClient(token);
	 
	         if ((passToken == null) || (passToken.getClient().getId() != id)) {
	        	 System.out.println("invalid");
	             return "invalidToken";
	         }
	 
	         Calendar cal = Calendar.getInstance();
	         if ((passToken.getExpiryDate().getTime() - cal.getTime().getTime()) <= 0) {
	             model.addAttribute("failed", "Your Reset password Link has been Exipired request for new link");
	             model.addAttribute("resetEmailWrong",1);
	             return "clientlogin";
	         }
	         Client client = passToken.getClient();
	         System.out.println("email");
	         System.out.println(client.getClient_email());
	         model.addAttribute("passwordUpdate", client);
	     } catch (Exception e) {
	    	 e.printStackTrace();
	     }
	     model.addAttribute("Success", "You Email has been Verified");
	     return "clientCeateNewPassword";
	 }
	 
	 @RequestMapping(value = "/clientSaveNewPassword", method = RequestMethod.POST)
	 public String saveNewPassword(@Valid @ModelAttribute("passwordUpdate") Client passwordUpdate,Model model, BindingResult bindingResult) {
	     try {
	    	 if(bindingResult.hasErrors()) {
	    		 System.out.println("error");
	             model.addAttribute("failed1", "password feild Should not be blank");
	             model.addAttribute("passwordUpdate", passwordUpdate);
	             return "clientCeateNewPassword";
	         }
	    	 String password = passwordUpdate.getClient_password();
	         BCryptPasswordEncoder bCryptPasswordEncoder = new BCryptPasswordEncoder();
	 		String encodedPass = bCryptPasswordEncoder.encode(password);
	 		passwordUpdate.setClient_password(encodedPass);
	 		ClientForgotPasswordToken clientPasswordResetToken=clientForgotPasswordTokenService.checkClientForgotPasswordToken(passwordUpdate);
	 		if(clientForgotPasswordTokenService.updateClientPassword(passwordUpdate)) {
	 			clientForgotPasswordTokenService.deleteForgotPasswordToken(clientPasswordResetToken);
	 			model.addAttribute("passwordChanged", "Your Password has been successfully changed");
	 			return "clientlogin";
	 		}
	         
	     } catch (Exception e) {
	         e.getStackTrace();
	     }
	     model.addAttribute("passwordUpdate", passwordUpdate);
         return "clientCeateNewPassword";
	 }
	 

}

