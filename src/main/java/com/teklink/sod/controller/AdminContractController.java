package com.teklink.sod.controller;
/**
 * @author Shikha Rohit
 */

import java.io.File;
import java.text.SimpleDateFormat;
import java.util.Date;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;

import com.teklink.sod.model.Mailler;
import com.teklink.sod.model.SaveAttachedFile;
import com.teklink.sod.model.admin.AdminContract;
import com.teklink.sod.service.admin.AdminContractService;

@Controller
public class AdminContractController {

	@Autowired

	private AdminContractService adminContractService;

	public AdminContractService getAdminContractService() {
		return adminContractService;

	}

	public void setAdminContractService(AdminContractService adminContractService) {
		this.adminContractService = adminContractService;
	}

	@RequestMapping(value = "/admin/dashboard/adminContractManagement", method = RequestMethod.GET)
	public String adminContractForm(@ModelAttribute("adminContract") AdminContract adminContract) {

		/*
		 * Mailler mailler = new Mailler();
		 * mailler.setMessage("Please click on the link below");
		 * System.out.println(SecurityContextHolder.getContext().getAuthentication().
		 * getName()); mailler.setTO("shikha2591@gmail.com"); if(mailler.sendMail()) {
		 * //model.addAttribute("Success", "Check email for reset password link");
		 * //model.addAttribute("resetEmailWrong",1); return "adminContractManagement";
		 * }
		 */

		// model.addAttribute("adminContractManagement", "adminContractManagement");
		return "adminContractManagement";
	}

	@RequestMapping(value = "/admin/dashboard/saveAdminContractManagement", method = RequestMethod.POST)
	public String saveAdminContractFormDetail(
			@ModelAttribute("adminContract") AdminContract adminContract) { /*
																			 * BindingResult bindingResult, Model model,
																			 * 
																			 * @RequestParam("photo") MultipartFile
																			 * photo,
																			 * 
																			 * @RequestParam("sign") MultipartFile sign
																			 */

		/*
		 * try { if (!sign.isEmpty() && !photo.isEmpty()) { if
		 * (bindingResult.hasErrors()) { //model.addAttribute("clientPack",
		 * getClientPacksService().getPackById(id)); return "adminContractManagement"; }
		 * String rootPath = System.getProperty("catalina.home"); File dir = new
		 * File(rootPath + File.separator + "SOD" + File.separator + "Admin-Doc" +
		 * File.separator + adminContract.getContractNum() + "-" +
		 * adminContract.getName() + "-" + new
		 * SimpleDateFormat("yyyy.MM.dd.HH.mm.ss").format(new Date()) + File.separator);
		 * if (!dir.exists()) dir.mkdirs(); String phtPath = new SaveAttachedFile(photo,
		 * "img", dir).uploadFile(); String signPath = new SaveAttachedFile(sign,
		 * "sign", dir).uploadFile();
		 * 
		 * if (phtPath == null && signPath == null) { //
		 * clientContract.setImagePath(phtPath); //
		 * clientContract.setSignPath(signPath); //adminContract.setProdId(id);
		 * //adminContract.setProdType(packName); if
		 * (getAdminContractService().saveContract(adminContract)) { return
		 * "adminContractManagement"; } //model.addAttribute("clientPack",
		 * getClientPacksService().getPackById(id)); model.addAttribute("error",
		 * "Something went wrong!<br>Please try after sometime."); return
		 * "adminContractManagement";
		 * 
		 * } } }catch (Exception e) {
		 * 
		 * }
		 */
		Mailler mailler = new Mailler();
		mailler.setMessage("<h4>Please click on the link below<h4><br>"
				+ "<a href='http://localhost:8080/SOD/admin/dashboard/adminContractManagement'>Click here</a>");

		System.out.println(SecurityContextHolder.getContext().getAuthentication().getName());
		mailler.setTO("clientin81@gmail.com");
		if (mailler.sendMail()) {
		}
		// model.addAttribute("Success", "Check email for reset password link");
		// model.addAttribute("resetEmailWrong",1); return "adminContractManagement";
		// model.addAttribute("adminContractManagement", "adminContractManagement");
		return "adminDashboard";

	}

}
