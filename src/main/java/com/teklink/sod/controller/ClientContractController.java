package com.teklink.sod.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.teklink.sod.model.Mailler;
import com.teklink.sod.model.client.ClientContract;
import com.teklink.sod.model.expert.Expert;
import com.teklink.sod.service.ExpertService;
import com.teklink.sod.service.client.ClientContractService;

@Controller
public class ClientContractController {

	@Autowired
	private ClientContractService clientContractService;
	
	public ClientContractService getClientContractService() {
		return clientContractService;
	}

	public void setClientContractService(ClientContractService clientContractService) {
		this.clientContractService = clientContractService;
	}

	@Autowired
	private ExpertService expertService;
	
	public ExpertService getExpertService() {
		return expertService;
	}

	public void setExpertService(ExpertService expertService) {
		this.expertService = expertService;
	}
	
	@RequestMapping(value = "/client/client_contract_management/{expertId}", method = RequestMethod.GET)
	public String ContractForm(@PathVariable("expertId") Integer expertId, Model model ) {
		System.out.println("expertId==> "+expertId);
		
		/* clientContractModel = new ClientContract(); */
			Expert expert = expertService.byId(expertId);
			ClientContract clientContractModel = new ClientContract();
					clientContractModel.setName(expert.getFirstName());
			clientContractModel.setAddress(expert.getAddress());
			 model.addAttribute("cl",clientContractModel ); 
		
		// model.addAttribute("adminContractManagement", "adminContractManagement");
		
		return "contractManagements";
	}

	@RequestMapping(value = "/client/saveContractManagement", method = RequestMethod.POST)
	public String saveContractFormDetail(
			@ModelAttribute("clientContractModel") ClientContract clientContractModel) { 
																			 /* BindingResult bindingResult, Model model,
																			  
																			  @RequestParam("photo") MultipartFile
																			  photo,
																			  
																			  @RequestParam("sign") MultipartFile sign*/
																			 

		
		/*
		 * try { if (!sign.isEmpty() && !photo.isEmpty()) { if
		 * (bindingResult.hasErrors()) { //model.addAttribute("clientPack",
		 * getClientPacksService().getPackById(id)); return "adminContractManagement"; }
		 * String rootPath = System.getProperty("catalina.home"); File dir = new
		 * File(rootPath + File.separator + "SOD" + File.separator + "Admin-Doc" +
		 * File.separator + adminContract.getContractNum() + "-" +
		 * adminContract.getName() + "-" + new
		 * SimpleDateFormat("yyyy.MM.dd.HH.mm.ss").format(new Date()) + File.separator);
		 * if (!dir.exists()) dir.mkdirs(); String phtPath = new SaveAttachedFile(photo,
		 * "img", dir).uploadFile(); String signPath = new SaveAttachedFile(sign,
		 * "sign", dir).uploadFile();
		 * 
		 * if (phtPath == null && signPath == null) { //
		 * clientContract.setImagePath(phtPath); //
		 * clientContract.setSignPath(signPath); //adminContract.setProdId(id);
		 * //adminContract.setProdType(packName); if
		 * (getAdminContractService().saveContract(adminContract)) { return
		 * "adminContractManagement"; } //model.addAttribute("clientPack",
		 * getClientPacksService().getPackById(id)); model.addAttribute("error",
		 * "Something went wrong!<br>Please try after sometime."); return
		 * "adminContractManagement";
		 * 
		 * } } }catch (Exception e) {
		 * 
		 * }
		 */
		 System.out.println(clientContractModel.getName());
		 System.out.println(clientContractModel.getAddress());
		 System.out.println(clientContractModel.getContractNum());
		 System.out.println(clientContractModel);
		 if(clientContractService.saveContract(clientContractModel)) {
			 return "thankyou_page";
		 }
		 //clientContractService.saveContractForm(clientContractModel, null, null, null);
		/*
		 * Mailler mailler = new Mailler();
		 * mailler.setMessage("<h4>Please click on the link below<h4><br>" +
		 * "<a href='http://localhost:8080/SOD/client/contractManagement'>Click here</a>"
		 * );
		 * 
		 * System.out.println(SecurityContextHolder.getContext().getAuthentication().
		 * getName()); mailler.setTO("clientin81@gmail.com"); if (mailler.sendMail()) {
		 * }
		 */
		// model.addAttribute("Success", "Check email for reset password link");
		// model.addAttribute("resetEmailWrong",1); return "adminContractManagement";
		// model.addAttribute("adminContractManagement", "adminContractManagement");
		return "contractManagements";

	}


}
