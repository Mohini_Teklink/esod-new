/*
 * @Author Hardev Singh
 *
 * */
package com.teklink.sod.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.teklink.sod.model.client.Client;
import com.teklink.sod.service.SkillService;
import com.teklink.sod.service.client.ClientService;

@Controller
public class AdminClientController {

	public ClientService getClientService() {
		return clientService;
	}

	public void setClientService(ClientService clientService) {
		this.clientService = clientService;
	}

	public SkillService getSkillService() {
		return skillService;
	}

	public void setSkillService(SkillService skillService) {
		this.skillService = skillService;
	}

	@Autowired
	private ClientService clientService;

	@Autowired
	private SkillService skillService;

//	@RequestMapping(value="/admin/dashboard/statusUpdate", method=RequestMethod.GET)
//	public @ResponseBody String updateStatus(@RequestParam("id") int id, @RequestParam("status") boolean status) {
//		getClientProductService().productStatusUpdate(id, status);
//		return "success";
//		
//	}

	/*
	 * @RequestMapping(value = "/admin/dashboard/clientlist/activeInactive", method
	 * = RequestMethod.GET) public @ResponseBody String
	 * goToAdminClientListDelete(@RequestParam("id") Integer
	 * id,@RequestParam("status") Boolean status) {
	 * 
	 * if (getClientService().updateByID(id,status)) { return "success";
	 * 
	 * }
	 * 
	 * return "failure";
	 * 
	 * }
	 */

	@RequestMapping(value = "/admin/dashboard/clientlist/activeInactive", method = RequestMethod.GET)
	public @ResponseBody String goToAdminClientListDelete(@RequestParam("id") int id,
			@RequestParam("status") Boolean status) {
		if (getClientService().updateById(id, status)) {
			return "success";
		}
		return "failure";
	}
	@RequestMapping(value = "/admin/dashboard/clientlist", method = RequestMethod.GET)
	public String  goToAdminClientList(Model model){
		List<Client>  clientList = getClientService().getAllClient();
		  if (clientList != null && ! clientList.isEmpty()) { 
			 
		  	model.addAttribute("clientMasterList", clientList);
		  }
		  System.out.println(clientList.isEmpty());
		 model.addAttribute("activeNav", "client"); 
		return "adminClientList";
		
	}

	@RequestMapping(value = "/admin/dashboard/clientlist/profile", method = RequestMethod.GET)
	public ModelAndView goToAdminClientListProfile(@RequestParam("id") int id) {
		ModelAndView modelAndView = new ModelAndView("adminClientListProfile");
		Client client = getClientService().byId(id);
		modelAndView.addObject("clientDetails", client);
		modelAndView.addObject("activeNav", "client");
		return modelAndView;
	}
}
