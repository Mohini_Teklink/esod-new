package com.teklink.sod.controller;

import java.util.Calendar;
import java.util.Locale;
import java.util.UUID;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import com.teklink.sod.model.Mailler;
import com.teklink.sod.model.expert.Expert;
import com.teklink.sod.model.expert.ExpertForgotPasswordToken;
import com.teklink.sod.service.ExpertForgotPasswordTokenService;
import com.teklink.sod.service.ExpertService;

@Controller
public class ExpertForgotPasswordController {
	
	@Autowired
	private ExpertForgotPasswordTokenService expertForgotPasswordTokenService;
	
	@Autowired
	private ExpertService expertService;
	
	@RequestMapping(value = "/resetPassword", method = RequestMethod.POST)
	 public String resetPassword(Model model, HttpServletRequest request, @RequestParam("email") String Email) {
	     try {
	         Expert expert = expertForgotPasswordTokenService.getVerifiedExpertbyEmail(Email);
	         if (expert == null) {
	        	 model.addAttribute("resetEmailWrong",1);
	             model.addAttribute("noUser", "Email does not exist");
	             return "expertLogin";
	         }
	         String token = UUID.randomUUID().toString();
	         ExpertForgotPasswordToken expertPasswordResetToken=expertForgotPasswordTokenService.checkExpertForgotPasswordToken(expert);
	         if(expertPasswordResetToken != null) {
	        	 System.out.println("updated");
	        	 expertForgotPasswordTokenService.updateForgotPasswordToken(expertPasswordResetToken, token);
	         }else {
	        	 expertForgotPasswordTokenService.createForgotPasswordToken(expert, token);	        	 
	         }
	         String appUrl = "http://" + request.getServerName() + ":" + request.getServerPort()
	                 + request.getContextPath();
	         String url = appUrl + "/changePassword?id=" + expert.getId() + "&token=" + token;
		     String message = "Click on the reset Password Link";
		     Mailler mailler = new Mailler();
				mailler.setMessage(message + " " + url);
				System.out.println(SecurityContextHolder.getContext().getAuthentication().getName());
				mailler.setTO(expertService.byId(expert.getId()).getEmail());
	         if(mailler.sendMail()) {
	        	 model.addAttribute("Success", "Check email for reset password link");
	        	 model.addAttribute("resetEmailWrong",1);
	             return "expertLogin";
	         }
	     } catch (Exception e) {
	    	 e.printStackTrace();
	     }
	     model.addAttribute("resetEmailWrong",1);
         model.addAttribute("failed", "Some error ocuurs");
         return "expertLogin";
	 }
	 
	 @RequestMapping(value = "/changePassword", method = RequestMethod.GET)
	 public String showChangePasswordPage(Locale locale, Model model, @RequestParam("id") long id,
	         @RequestParam("token") String token) {
	     try {
	         ExpertForgotPasswordToken passToken = expertForgotPasswordTokenService.getForgotPasswordexpert(token);
	         if ((passToken == null) || (passToken.getExpert().getId() != id)) {
	             return "invalidToken";
	         }
	         Calendar cal = Calendar.getInstance();
	         if ((passToken.getExpiryDate().getTime() - cal.getTime().getTime()) <= 0) {
	             model.addAttribute("failed", "Your Reset password Link has been Exipired request for new link");
	             return "expertForgetPassword";
	         }
	         Expert expert = passToken.getExpert();
	         model.addAttribute("passwordUpdate", expert);
	     } catch (Exception e) {
	    	 e.getStackTrace();
	     }
	     model.addAttribute("Success", "You Email has been Verified");
	     return "expertCeateNewPassword";
	 }
	 
	 @RequestMapping(value = "/saveNewPassword", method = RequestMethod.POST)
	 public String saveNewPassword(@Valid @ModelAttribute("passwordUpdate") Expert passwordUpdate,Model model) {
	     try {
	         String password = passwordUpdate.getPassword();
	         if (password.equalsIgnoreCase("")) {
	             model.addAttribute("failed1", "password feild Should not be blank");
	             model.addAttribute("userList", passwordUpdate);
	             return "expertCeateNewPassword";
	         }
	         BCryptPasswordEncoder bCryptPasswordEncoder = new BCryptPasswordEncoder();
	 		String encodedPass = bCryptPasswordEncoder.encode(password);
	 		passwordUpdate.setPassword(encodedPass);
	 		ExpertForgotPasswordToken expertPasswordResetToken=expertForgotPasswordTokenService.checkExpertForgotPasswordToken(passwordUpdate);
	 		if(expertForgotPasswordTokenService.updateForgotPasswordExpert(passwordUpdate)) {
	 			expertForgotPasswordTokenService.deleteForgotPasswordToken(expertPasswordResetToken);
	 			model.addAttribute("passwordChanged", "Your Password has been successfully changed");	
	 			return "expertLogin";
	 		}
	     } catch (Exception e) {
	         e.getStackTrace();
	     }
	     model.addAttribute("userList", passwordUpdate);
         return "expertCeateNewPassword";
	 }
	 

}
