/*
 * @Author Hardev Singh
 *
 * */
package com.teklink.sod.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.authority.AuthorityUtils;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ModelAttribute;

import com.teklink.sod.model.Skills;
import com.teklink.sod.model.client.Client;
import com.teklink.sod.model.client.ClientIndustryType;
import com.teklink.sod.model.client.ClientPacks;
import com.teklink.sod.model.expert.Expert;
import com.teklink.sod.model.user.User;
import com.teklink.sod.service.SkillService;
import com.teklink.sod.service.client.ClientIndustryTypeService;
import com.teklink.sod.service.client.ClientPacksService;

@ControllerAdvice
public class GlobalController {

	@Autowired
	private ClientPacksService clientPacksService;

	@Autowired
	private SkillService SkillsService;

	@Autowired
	private ClientIndustryTypeService clientIndustryTypeService;

	@ModelAttribute("packageList")
	public List<ClientPacks> listOfPackages() {
		return clientPacksService.getAllPacksList();
	}

	@ModelAttribute("industryList")
	public List<ClientIndustryType> listOfIndustry() {
		return clientIndustryTypeService.getIndustryTypesList();
	}

	@ModelAttribute("skillsList")
	public List<Skills> listOfSkills() {
		return SkillsService.getAllSkills();
	}

	@ModelAttribute("expert")
	public static Expert getExpertDetails() {
		try {
			if (SecurityContextHolder.getContext() != null
					&& !(SecurityContextHolder.getContext().getAuthentication().getPrincipal()).equals("anonymousUser")
					&& SecurityContextHolder.getContext().getAuthentication().getAuthorities()
							.equals(AuthorityUtils.createAuthorityList("ROLE_EXPERT"))) {
				return (Expert) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
			}
		} catch (Exception exp) {
			exp.printStackTrace();
		}
		return null;
	}

	@ModelAttribute("client")
	public static Client getClientDetails() {
		try {
			if (SecurityContextHolder.getContext() != null
					&& !(SecurityContextHolder.getContext().getAuthentication().getPrincipal()).equals("anonymousUser")
					&& SecurityContextHolder.getContext().getAuthentication().getAuthorities()
							.equals(AuthorityUtils.createAuthorityList("ROLE_CLIENT"))) {
				return (Client) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
			}
		} catch (Exception exp) {
			exp.printStackTrace();
		}
		return null;
	}

	@ModelAttribute("admin")
	public static User getAdminDetails() {
		try {
			if (SecurityContextHolder.getContext() != null
					&& !(SecurityContextHolder.getContext().getAuthentication().getPrincipal()).equals("anonymousUser")
					&& (SecurityContextHolder.getContext().getAuthentication().getAuthorities()
							.equals(AuthorityUtils.createAuthorityList("ROLE_SUPERADMIN"))
							|| SecurityContextHolder.getContext().getAuthentication().getAuthorities()
									.equals(AuthorityUtils.createAuthorityList("ROLE_EXPERTADMIN"))
							|| SecurityContextHolder.getContext().getAuthentication().getAuthorities()
									.equals(AuthorityUtils.createAuthorityList("ROLE_CLIENTADMIN")))) {
				return (User) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
			}
		} catch (Exception exp) {
			exp.printStackTrace();
		}
		return null;
	}

}
