package com.teklink.sod.controller;


/**
 * @author Prateek Raj
 */

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.teklink.sod.model.ExpertSkill;
import com.teklink.sod.model.Mailler;
import com.teklink.sod.model.client.Client;
import com.teklink.sod.model.expert.Expert;
import com.teklink.sod.service.ExpertService;
import com.teklink.sod.service.SkillService;
import com.teklink.sod.service.client.ClientService;


@Controller
public class AdminController {

	@Autowired
	private ExpertService expertService;
	
	@Autowired
	private SkillService skillService;
	
	
	public ExpertService getExpertService() {
		return expertService;
	}

	public void setExpertService(ExpertService expertService) {
		this.expertService = expertService;
	}

	public SkillService getSkillService() {
		return skillService;
	}

	public void setSkillService(SkillService skillService) {
		this.skillService = skillService;
	}
	
	@RequestMapping(value = "/adminLogin", method = RequestMethod.GET)
	public String goToAdmin(Model model) {
		//model.addAttribute("userCredentials", new UserCardinalities());
		if(GlobalController.getAdminDetails()!=null)
			return "redirect:/admin/dashboard";
		if(GlobalController.getExpertDetails()!=null)
			return "redirect:/expert/jdp";
		if(GlobalController.getClientDetails()!=null)
			return "redirect:/client/clientPacks";
		
		return "adminLogin";
	}

	@RequestMapping(value = "/admin/dashboard", method = RequestMethod.GET)
	public String goToDashboard(Model model) {
		model.addAttribute("activeNav", "dashboard");
		return "adminDashboard";
	}

	@Autowired
	private ClientService clientService; 
	@RequestMapping(value = "/admin/dashboard/jobseeker", method = RequestMethod.GET)
	public String goToAdminJobSeeker(Model model, boolean status) {
		List<Expert> listExp = getExpertService().getAllExpert();
		if (listExp != null) {
			List<ExpertSkill> skillList = new ArrayList<>();
			
			List<Client> clientList = clientService.getAllClient();
			List<Client> clients = null ; 
			/* List<Client> clientLists = new ArrayList<>(); */
			for (Expert expert : listExp) {
				clients = new ArrayList<Client>();
				ExpertSkill skillExp = new ExpertSkill();
				/*
				 * if(clientList != null && ! clientList.isEmpty() ) { clients =
				 * clientList.stream().collect(Collectors.toList());
				 * skillExp.setClientList(clients); }
				 */
				skillExp.setClientList(clientList);
				skillExp.setExpert(expert);
				skillExp.setSkillList(getSkillService().getAllSkillsByExpert(expert.getSkillList()));
				skillList.add(skillExp);
				
				
					// model.addAttribute("Success", "Check email for reset password link");
					// model.addAttribute("resetEmailWrong",1); return "adminContractManagement";
					// model.addAttribute("adminContractManagement", "adminContractManagement");
				/* return "adminJobSeeker"; */
			
			
			model.addAttribute("expertMasterList", skillList);
			}
				
		}
		model.addAttribute("activeNav", "expert");
		return "adminJobSeeker";
		
		
		  
	}
	
	@RequestMapping(value="/admin/dashboard/changePassword")
	public String goToChangePassword() {
		return "adminChangePassword";
	}
	
	

}
