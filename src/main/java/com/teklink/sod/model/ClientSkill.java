package com.teklink.sod.model;

import java.util.List;

import com.teklink.sod.model.client.ClientProducts;
import com.teklink.sod.model.Skills;

public class ClientSkill {
	private ClientProducts clientProducts;
	private List<Skills> skillList;
	private String skillStr;
	
	public String getSkillStr() {
		return skillStr;
	}

	public void setSkillStr(String skillStr) {
		this.skillStr = skillStr;
	}

	public ClientProducts getClientProducts() {
		return clientProducts;
	}

	public void setClientProducts(ClientProducts clientProducts) {
		this.clientProducts = clientProducts;
	}

	public List<Skills> getSkillList() {
		return skillList;
	}

	public void setSkillList(List<Skills> skillList) {
		this.skillList = skillList;
	}

}
