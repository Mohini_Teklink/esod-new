package com.teklink.sod.model;


/**
 * @author Prateek Raj
 */

import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileOutputStream;

import org.apache.commons.io.FilenameUtils;
import org.springframework.web.multipart.MultipartFile;

public class SaveAttachedFile {

	
	private MultipartFile file;
	private String folder;
	private File dir;

	public MultipartFile getFile() {
		return file;
	}

	public void setFile(MultipartFile file) {
		this.file = file;
	}

	public String getFolder() {
		return folder;
	}

	public void setFolder(String folder) {
		this.folder = folder;
	}

	public File getDir() {
		return dir;
	}

	public void setDir(File dir) {
		this.dir = dir;
	}

	public SaveAttachedFile(MultipartFile file, String folder, File dir) {
		super();
		this.file = file;
		this.folder = folder;
		this.dir = dir;
	}
	
	
	public String uploadFile() {
		String name = getFolder() + "." + FilenameUtils.getExtension(getFile().getOriginalFilename());
		try {
			byte[] bytes = getFile().getBytes();
			File serverFile = new File(getDir().getAbsolutePath() + File.separator + name);
			BufferedOutputStream stream = new BufferedOutputStream(new FileOutputStream(serverFile));
			stream.write(bytes);
			stream.close();
			return name;
			//return serverFile.getAbsolutePath();

		} catch (Exception e) {
			System.out.println("You failed to upload " + name + " => " + e.getMessage());
		}
		return null;
	}
	
	
}
