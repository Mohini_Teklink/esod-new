package com.teklink.sod.model;


import java.io.Serializable;

/**
 * @author Prateek Raj
 */

import java.math.BigInteger;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;
import javax.validation.constraints.NotNull;

import org.hibernate.annotations.CreationTimestamp;
import org.springframework.format.annotation.DateTimeFormat;

@Entity
@Table(name="tbl_paymentDetails")
public class Payment implements Serializable{
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy= GenerationType.IDENTITY)
	private int id;
	
	@NotNull(message = "Required")
	@Column(nullable = false, name = "col_cardNumber",columnDefinition="Decimal(16,0)")
	private BigInteger cardNumber;
	
	@NotNull(message = "Required")
	@Column(nullable=false,name="col_cardType",length=20)
	private String cardType;
	
	@NotNull(message = "Required")
	@Column(nullable=false,name="col_cardHolderName",length=60)
	private String cardHolderName;
	
	@NotNull(message = "Required")
	@Column(nullable = false, name = "col_expiryDate",columnDefinition="Date")
	@DateTimeFormat(pattern = "yyyy-MM")
	private Date expiryDate;
	
	@NotNull(message="Required")
	@Transient
	private Integer cardCvv;
	
	@CreationTimestamp
	@Temporal(TemporalType.TIMESTAMP)
	@Column(nullable = false, name = "col_dateOfTransaction")
	private Date dateofTransaction;
	
	@Override
	public String toString() {
		return "Payment [id=" + id + ", cardNumber=" + cardNumber + ", cardType=" + cardType + ", cardHolderName="
				+ cardHolderName + ", expiryDate=" + expiryDate + ", cardCvv=" + cardCvv + ", dateofTransaction="
				+ dateofTransaction + "]";
	}

	public BigInteger getCardNumber() {
		return cardNumber;
	}

	public void setCardNumber(BigInteger cardNumber) {
		this.cardNumber = cardNumber;
	}

	public String getCardHolderName() {
		return cardHolderName;
	}

	public void setCardHolderName(String cardHolderName) {
		this.cardHolderName = cardHolderName;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public Date getExpiryDate() {
		return expiryDate;
	}

	public void setExpiryDate(Date expiryDate) {
		this.expiryDate = expiryDate;
	}

	public Date getDateofTransaction() {
		return dateofTransaction;
	}

	public void setDateofTransaction(Date dateofTransaction) {
		this.dateofTransaction = dateofTransaction;
	}

	public Integer getCardCvv() {
		return cardCvv;
	}

	public void setCardCvv(Integer cardCvv) {
		this.cardCvv = cardCvv;
	}
	
	public String getCardType() {
		return cardType;
	}

	public void setCardType(String cardType) {
		this.cardType = cardType;
	}

	public Payment() {
	}

}
