package com.teklink.sod.model.user;


/**
 * @author Prateek Raj
 */

import javax.validation.constraints.NotBlank;


public class UserCardinalities {

	@NotBlank(message="Required")
	private String userName;
	
	@NotBlank(message="Required")
	private String password;

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}
	
}
