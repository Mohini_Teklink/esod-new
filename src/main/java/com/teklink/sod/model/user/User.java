package com.teklink.sod.model.user;


import java.util.Collection;

/**
 * @author Prateek Raj
 */

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.validation.constraints.Email;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

import org.hibernate.validator.constraints.Length;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.AuthorityUtils;
import org.springframework.security.core.userdetails.UserDetails;

@Entity
@Table(name="tbl_userdetails")
public class User implements UserDetails{

	/**
	 * 
	 */
	private static final long serialVersionUID = -1558088850050061615L;

	@Id
	@GeneratedValue(strategy =  GenerationType.IDENTITY)
	@Column(name="id")
	private int id;
	
	@NotBlank(message="Required")
	@Column(name="col_username")
	@Length(min=6,max=20,message="Length should be between 6 and 20.")
	private String username;
	
	@NotBlank(message="Required")
	@Column(name="col_email")
	@Email
	private String email;
	
	@NotBlank(message="Required")
	@Column(name="col_firstname")
	private String firstName;
	
	
	@Column(name="col_lastname")
	private String lastName;
	
	
	@Column(name="col_website")
	private String website;
	
	
	@Column(name="col_sendUserNotification")
	private String sendUserNotification;
	
	@NotBlank(message="Required")
	@Column(name="col_password",columnDefinition="varchar(200) not null")
	private String password;

	@NotNull(message="Required")
	@Column(name="col_role")
	private String role;
	
	
	public String getRole() {
		return role;
	}

	public void setRole(String role) {
		this.role = role;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public String getWebsite() {
		return website;
	}

	public void setWebsite(String website) {
		this.website = website;
	}

	public String getSendUserNotification() {
		return sendUserNotification;
	}

	public void setSendUserNotification(String sendUserNotification) {
		this.sendUserNotification = sendUserNotification;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	@Override
	public Collection<? extends GrantedAuthority> getAuthorities() {
		return AuthorityUtils.createAuthorityList(getRole());
	}

	@Override
	public boolean isAccountNonExpired() {
		return true;
	}

	@Override
	public boolean isAccountNonLocked() {
		return true;
	}

	@Override
	public boolean isCredentialsNonExpired() {
		return true;
	}

	@Override
	public boolean isEnabled() {
		return true;
	}
}
