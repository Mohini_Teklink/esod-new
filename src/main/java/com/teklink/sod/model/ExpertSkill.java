package com.teklink.sod.model;

/**
 * @author Prateek Raj
 */

import java.util.List;

import com.teklink.sod.model.client.Client;
import com.teklink.sod.model.expert.Expert;

public class ExpertSkill {

	private Expert expert;
	private List<Skills> skillList;
	private List<Client> clientList;

	public Expert getExpert() {
		return expert;
	}

	public void setExpert(Expert expert) {
		this.expert = expert;
	}

	public List<Skills> getSkillList() {
		return skillList;
	}

	public void setSkillList(List<Skills> skillList) {
		this.skillList = skillList;
	}

	public List<Client> getClientList() {
		return clientList;
	}

	public void setClientList(List<Client> clientList) {
		this.clientList = clientList;
	}

}
