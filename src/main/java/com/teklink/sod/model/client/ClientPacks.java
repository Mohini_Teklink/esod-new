
package com.teklink.sod.model.client;


/**
 * @author Prateek Raj
 */

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

@Entity
@Table(name="tbl_packs")
public class ClientPacks implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name="id")
	private int id;
	
	@NotNull
	@Column(name="col_packName")
	private String pack_name;

	@NotBlank(message="required")
	@Column(name="col_packPrice")
	private String pack_price;
	
	
	@NotNull
	@Column(name="col_packDesc")
	private String pack_description;
	
	
	
	public String getPack_description() {
		return pack_description;
	}

	public void setPack_description(String pack_description) {
		this.pack_description = pack_description;
	}

	public String getPack_price() {
		return pack_price;
	}

	public void setPack_price(String pack_price) {
		this.pack_price = pack_price;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getPack_name() {
		return pack_name;
	}

	public void setPack_name(String pack_name) {
		this.pack_name = pack_name;
	}
	
}
