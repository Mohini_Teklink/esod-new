package com.teklink.sod.model.client;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;


@Entity
@Table(name = "tbl_clientForgotPasswordToken")
public class ClientForgotPasswordToken implements Serializable {

	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "col_resetTokenid", unique = true, nullable = false)
	private long resetTokenid;

	@Column(name = "col_token")
	private String token;

	@OneToOne(cascade = { CascadeType.PERSIST }, fetch = FetchType.EAGER)
	@JoinColumn(nullable = false, name = "col_clientId")
	private Client client;

	@Column(name = "col_ExpiryDate")
	private Date expiryDate;

	public long getResetTokenid() {
		return resetTokenid;
	}

	public void setResetTokenid(long resetTokenid) {
		this.resetTokenid = resetTokenid;
	}

	public String getToken() {
		return token;
	}

	public void setToken(String token) {
		this.token = token;
	}

	public Client getClient() {
		return client;
	}

	public void setClient(Client client) {
		this.client = client;
	}

	public Date getExpiryDate() {
		return expiryDate;
	}

	public void setExpiryDate(Date expiryDate) {
		this.expiryDate = expiryDate;
	}
	
	public boolean isExpired() {
		return new Date().after(this.expiryDate);
	}
	
}
