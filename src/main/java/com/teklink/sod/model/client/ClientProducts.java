package com.teklink.sod.model.client;


/**
 * @author Prateek Raj
 */

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;


@Entity
@Table(name = "tbl_clientProduct")
public class ClientProducts {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "col_id")
	private int prodId;

	@NotBlank
	@Column(name = "col_prodName")
	private String prodName;

	@NotBlank
	@Column(name = "col_prodDescription")
	private String prodDescription;

	@NotNull
	@Column(name = "col_prodPrice")
	private String prodPrice;


	public void setProdPrice(String prodPrice) {
		this.prodPrice = prodPrice;
	}

	@Column(name = "col_prodType")
	private Integer prodType;

	@NotBlank
	@Column(name = "col_skills")
	private String skills;	

	@NotBlank
	@Column(name = "col_industryType")
	private String industryType;

	@NotBlank
	@Column(name = "col_employmentType")
	private String employmentType;

	@NotBlank
	@Column(name = "col_jobPaid")
	private String jobPaid;
	
	@Column(name="col_status")
	private boolean status;

	@Column(name="col_imagePath")
	private String prodImagePath;
	
	public String getProdImagePath() {
		return prodImagePath;
	}

	public void setProdImagePath(String prodImagePath) {
		this.prodImagePath = prodImagePath;
	}

	public String getProdPrice() {
		return prodPrice;
	}

	public int getProdId() {
		return prodId;
	}

	public void setProdId(int prodId) {
		this.prodId = prodId;
	}

	public String getProdName() {
		return prodName;
	}

	public void setProdName(String prodName) {
		this.prodName = prodName;
	}

	public String getProdDescription() {
		return prodDescription;
	}

	public void setProdDescription(String prodDescription) {
		this.prodDescription = prodDescription;
	}

	public Integer getProdType() {
		return prodType;
	}

	public void setProdType(Integer prodType) {
		this.prodType = prodType;
	}

	public String getSkills() {
		return skills;
	}

	public void setSkills(String skills) {
		this.skills = skills;
	}

	public String getIndustryType() {
		return industryType;
	}

	public void setIndustryType(String industryType) {
		this.industryType = industryType;
	}

	public String getEmploymentType() {
		return employmentType;
	}

	public void setEmploymentType(String employmentType) {
		this.employmentType = employmentType;
	}

	public String getJobPaid() {
		return jobPaid;
	}

	public void setJobPaid(String jobPaid) {
		this.jobPaid = jobPaid;
	}

	public boolean isStatus() {
		return status;
	}

	public void setStatus(boolean status) {
		this.status = status;
	}
	
}
