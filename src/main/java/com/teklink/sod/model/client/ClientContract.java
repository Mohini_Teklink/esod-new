package com.teklink.sod.model.client;


import java.io.Serializable;

/**
 * @author Prateek Raj
 */

import java.util.Date;
import java.util.UUID;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Transient;
import javax.validation.constraints.AssertTrue;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;

import org.springframework.format.annotation.DateTimeFormat;

/**
 * @author Shikha Rohit
 *
 */
@Entity
@Table(name = "tbl_clientContract")
public class ClientContract implements Serializable {

	
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "col_id")
	private Integer id;

	@NotEmpty(message = "Required")
	@Column(name = "col_name", length = 60, nullable = false)
	private String name;

	@NotEmpty(message = "Required")
	@Column(name = "col_address", length = 255, nullable = false)
	private String address;

	@NotNull(message = "Required")
	@Column(name = "col_contractNum", length = 45, nullable = false)
	private UUID contractNum = UUID.randomUUID();
	
	
	@NotNull(message = "Required")
	@Column(nullable = false, name = "col_startDate")
	@DateTimeFormat(pattern = "yyyy-MM-dd")
	private Date startDate;

	@NotNull(message = "Required")
	@Column(nullable = false, name = "col_endDate")
	@DateTimeFormat(pattern = "yyyy-MM-dd")
	private Date endDate;

	@NotNull(message = "Required")
	@Column(nullable = false, name = "col_pricing")
	private String price;

	@Column(nullable = true, name = "col_imgpath")
	private String imagePath;

	@Column(nullable = true, name = "col_signpath")
	private String signPath;

	@AssertTrue(message = "Please Accept the Terms & Policies")
	@Column(nullable = true, name = "col_accepted")
	private Boolean accepted;

	@Column(nullable = true, name = "col_productid")
	private Integer prodId;

	@Column(nullable = true, name = "col_prodType")
	private String prodType;

	public String getProdType() {
		return prodType;
	}

	public void setProdType(String prodType) {
		this.prodType = prodType;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Integer getProdId() {
		return prodId;
	}

	public void setProdId(Integer prodId) {
		this.prodId = prodId;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	
	public UUID getContractNum() {
		return contractNum;
	}

	public void setContractNum(UUID contractNum) {
		this.contractNum = contractNum;
	}


	

	public Date getStartDate() {
		return startDate;
	}

	public void setStartDate(Date startDate) {
		this.startDate = startDate;
	}

	public Date getEndDate() {
		return endDate;
	}

	public void setEndDate(Date endDate) {
		this.endDate = endDate;
	}

	public String getPrice() {
		return price;
	}

	public void setPrice(String price) {
		this.price = price;
	}

	public String getImagePath() {
		return imagePath;
	}

	public Boolean getAccepted() {
		return accepted;
	}

	public void setAccepted(Boolean accepted) {
		this.accepted = accepted;
	}

	public void setImagePath(String imagePath) {
		this.imagePath = imagePath;
	}

	public String getSignPath() {
		return signPath;
	}

	public void setSignPath(String signPath) {
		this.signPath = signPath;
	}

	

	@Override
	public String toString() {
		return "ClientContract [id=" + id + ", name=" + name + ", address=" + address + ", contractNum=" + contractNum
				+ ", startDate=" + startDate + ", endDate=" + endDate + ", price=" + price + ", imagePath=" + imagePath
				+ ", signPath=" + signPath + ", accepted=" + accepted + ", prodId=" + prodId + ", prodType=" + prodType
				+ "]";
	}

	
}
