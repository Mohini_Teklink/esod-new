package com.teklink.sod.model.client;
/*
 * @Author Hardev Singh
 * */
import com.teklink.sod.model.expert.Expert;
import com.teklink.sod.model.expert.ExpertContractManagement;

public class ClientProductOrderManagement {
	
	private ClientProducts clientProducts;
	
	private ExpertContractManagement expertContractManagement;
	
	private Expert expert;

	public ClientProducts getClientProducts() {
		return clientProducts;
	}

	public void setClientProducts(ClientProducts clientProducts) {
		this.clientProducts = clientProducts;
	}

	public ExpertContractManagement getExpertContractManagement() {
		return expertContractManagement;
	}

	public void setExpertContractManagement(ExpertContractManagement expertContractManagement) {
		this.expertContractManagement = expertContractManagement;
	}

	public Expert getExpert() {
		return expert;
	}

	public void setExpert(Expert expert) {
		this.expert = expert;
	}
	
	

}
