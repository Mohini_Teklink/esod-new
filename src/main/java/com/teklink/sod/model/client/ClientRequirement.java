package com.teklink.sod.model.client;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

import org.springframework.format.annotation.DateTimeFormat;

@Entity
@Table(name = "tbl_client_requirement")
public class ClientRequirement implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -7567423812354134106L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name="id")
	private int id;
	
	@NotNull
	@Column(name="resource_role")
	private String resourceRole;

	@NotBlank(message="required")
	@Column(name="skill_required")
	private String skillRequired;
	
	
	//@Column(name="start_date")
	@NotNull(message = "Required")
	@Column(nullable = false, name = "start_date")
	@DateTimeFormat(pattern = "yyyy-MM-dd")
	private Date startDate;
	
	@NotNull
	private Long duration;
	
	@NotNull
	private String location;

	@NotNull
	@Column(name="created_date")
	private Date createdDate;
	
	@Column(name="is_active")
	private Boolean isActive;

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getResourceRole() {
		return resourceRole;
	}

	public void setResourceRole(String resourceRole) {
		this.resourceRole = resourceRole;
	}

	public String getSkillRequired() {
		return skillRequired;
	}

	public void setSkillRequired(String skillRequired) {
		this.skillRequired = skillRequired;
	}

	public Date getStartDate() {
		return startDate;
	}

	public void setStartDate(Date startDate) {
		this.startDate = startDate;
	}

	public Long getDuration() {
		return duration;
	}

	public void setDuration(Long duration) {
		this.duration = duration;
	}

	public String getLocation() {
		return location;
	}

	public void setLocation(String location) {
		this.location = location;
	}

	public Date getCreatedDate() {
		return createdDate;
	}

	public void setCreatedDate(Date createdDate) {
		this.createdDate = createdDate;
	}

	public Boolean getIsActive() {
		return isActive;
	}

	public void setIsActive(Boolean isActive) {
		this.isActive = isActive;
	}

	@Override
	public String toString() {
		return "ClientRequirement [id=" + id + ", resourceRole=" + resourceRole + ", skillRequired=" + skillRequired
				+ ", startDate=" + startDate + ", duration=" + duration + ", location=" + location + ", createdDate="
				+ createdDate + ", isActive=" + isActive + "]";
	}
	
	
}
