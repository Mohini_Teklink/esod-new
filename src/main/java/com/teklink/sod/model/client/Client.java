package com.teklink.sod.model.client;

/**
 * @author Prateek Raj
 */

import java.io.Serializable;
import java.util.Collection;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Transient;
import javax.validation.constraints.Email;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

import org.hibernate.validator.constraints.Length;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.AuthorityUtils;
import org.springframework.security.core.userdetails.UserDetails;

import com.teklink.sod.model.expert.Expert;

@Entity
@Table(name = "tbl_clientDetails")
public class Client implements Serializable, UserDetails {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id")
	private int id;

	@NotBlank(message = "Required")
	@Length(min = 6, max = 20)
	@Column(name = "client_username")
	private String client_username;

	@NotBlank(message = "Required")
	@Email
	@Column(name = "client_email")
	private String client_email;

	@NotBlank(message = "Required")
	@Column(name = "client_password", columnDefinition = "varchar(200) not null")

	private String client_password;

	@NotBlank(message = "Required")
	@Transient
	private String client_confirmpassword;

	
	  @OneToMany(mappedBy = "clientId" , cascade = CascadeType.ALL) private
	  List<Expert> expertList;
	  
	 
	public Boolean getStatus() {
		return status;
	}

	public void setStatus(Boolean status) {
		this.status = status;
	}

	@NotNull(message = "Required")
	@Min(value = 1000000000)
	@Column(name = "client_phone")
	private Long client_phonenum;

	@NotBlank(message = "Required")
	@Column(name = "client_address")
	private String client_address;

	@Column(name = "col_status")
	private Boolean status = true;

	@Column(name = "client_role", columnDefinition = "varchar(20) not null")
	private String client_role = "ROLE_CLIENT";

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getClient_role() {
		return client_role;
	}

	public void setClient_role(String client_role) {
		this.client_role = client_role;
	}

	public String getClient_username() {
		return client_username;
	}

	public void setClient_username(String client_username) {
		this.client_username = client_username;
	}

	public String getClient_email() {
		return client_email;
	}

	public void setClient_email(String client_email) {
		this.client_email = client_email;
	}

	public String getClient_password() {
		return client_password;
	}

	public void setClient_password(String client_password) {
		this.client_password = client_password;
	}

	public String getClient_confirmpassword() {
		return client_confirmpassword;
	}

	public void setClient_confirmpassword(String client_confirmpassword) {
		this.client_confirmpassword = client_confirmpassword;
	}

	public Long getClient_phonenum() {
		return client_phonenum;
	}

	public void setClient_phonenum(Long client_phonenum) {
		this.client_phonenum = client_phonenum;
	}

	public String getClient_address() {
		return client_address;
	}

	public void setClient_address(String client_address) {
		this.client_address = client_address;
	}

	@Override
	public Collection<? extends GrantedAuthority> getAuthorities() {
		return AuthorityUtils.createAuthorityList(getClient_role());
	}

	@Override
	public String getPassword() {
		return getClient_password();
	}

	@Override
	public String getUsername() {
		return getClient_email();
	}

	@Override
	public boolean isAccountNonExpired() {
		return true;
	}

	@Override
	public boolean isAccountNonLocked() {
		return true;
	}

	@Override
	public boolean isCredentialsNonExpired() {
		return true;
	}

	@Override
	public boolean isEnabled() {
		return true;
	}

	public List<Expert> getExpertList() {
		return expertList;
	}

	public void setExpertList(List<Expert> expertList) {
		this.expertList = expertList;

	}

	@Override
	public String toString() {
		return "Client [id=" + id + ", client_username=" + client_username + ", client_email=" + client_email
				+ ", client_password=" + client_password + ", client_confirmpassword=" + client_confirmpassword
				+ ", client_phonenum=" + client_phonenum + ", client_address=" + client_address + ", status=" + status
				+ ", client_role=" + client_role + "]";
	}

	/*
	 * public void setExpertList(List<Expert> allExpertByClientId) { // TODO
	 * Auto-generated method stub
	 * 
	 * }
	 */

}
