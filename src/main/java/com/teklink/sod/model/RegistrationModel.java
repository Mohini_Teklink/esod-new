package com.teklink.sod.model;


/**
 * @author Prateek Raj
 */

import java.io.Serializable;

import com.teklink.sod.model.client.Client;
import com.teklink.sod.model.client.ClientPacks;
import com.teklink.sod.model.expert.Expert;

public class RegistrationModel implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private Client client;
	private Expert expert;
	private ClientPacks clientPacks;

	public Client getClient() {
		return client;
	}

	public void setClient(Client client) {
		this.client = client;
	}

	public Expert getExpert() {
		return expert;
	}

	public void setExpert(Expert expert) {
		this.expert = expert;
	}
	
	public ClientPacks getClientPacks() {
		return clientPacks;
	}
	
	public void setClientPacks(ClientPacks clientPacks) {
		this.clientPacks= clientPacks;
	}
	
}
