package com.teklink.sod.model;


/**
 * @author Prateek Raj
 */

import java.util.Properties;

import javax.mail.Authenticator;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.PasswordAuthentication;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;

public class Mailler {

	private final String HOST = "mail.teklink.in";
	private final String USER = "shikha.r@teklink.in";
	private final String PASSWORD = "teklink@1234";
	private String TO;
	private Properties props;
	private Session session;
	private String message;
	
	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public String getTO() {
		return TO;
	}

	public void setTO(String tO) {
		TO = tO;
	}

	public String getHOST() {
		return HOST;
	}

	public String getUSER() {
		return USER;
	}

	public String getPASSWORD() {
		return PASSWORD;
	}

	public Properties getProps() {
		return props;
	}

	public Session getSession() {
		return session;
	}

	public Mailler() {
		this.props = new Properties();
		this.props.put("mail.smtp.host", getHOST());
		this.props.put("mail.smtp.auth", "true");
		this.props.put("mail.smtp.port", "587"); //TLS Port
		this.props.put("mail.smtp.auth", "true"); //enable authentication
		this.props.put("mail.smtp.starttls.enable", "true"); //enable STARTTLS
		
		this.session = Session.getDefaultInstance(getProps(), new Authenticator() {
			protected PasswordAuthentication getPasswordAuthentication() {
				return new PasswordAuthentication(getUSER(), getPASSWORD());
			}
		});
	}
	
	public boolean sendMail() {
		   //Compose the message  
	    try {  
	     MimeMessage message = new MimeMessage(getSession());  
	     message.setFrom(new InternetAddress(getUSER()));  
	     message.setContent(getMessage(), "text/html");
	     message.addRecipient(Message.RecipientType.TO,new InternetAddress(getTO()));
	   //  message.addRecipient(Message.RecipientType.CC,new InternetAddress(getUSER()));
	     message.setSubject("SOD powered by teklink");  
	    // message.setText(getMessage());  
	     Transport.send(message);  
	    
	     return true;
	     } catch (MessagingException e) {e.printStackTrace();}  
		
		return false;
	}

	@Override
	public String toString() {
		return "Mailler [HOST=" + HOST + ", USER=" + USER + ", PASSWORD=" + PASSWORD + ", TO=" + TO + ", props=" + props
				+ ", session=" + session + "]";
	}
	
	
}
