package com.teklink.sod.model.expert;

/**
 * @author Prateek Raj
 */

import java.io.Serializable;
import java.util.Collection;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Transient;
import javax.validation.constraints.Email;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.AuthorityUtils;
import org.springframework.security.core.userdetails.UserDetails;

@Entity
@Table(name = "tbl_expertdetails")
public class Expert implements Serializable, UserDetails {

	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id")
	private int id;

	@NotBlank(message = "Required")
	@Column(name = "col_firstName")
	private String firstName;

	@NotBlank(message = "Required")
	@Column(name = "col_lastName")
	private String lastName;

	@NotNull(message = "Required")
	@Column(name = "col_mobileNumber")
	@Min(value = 1000000000)
	private Long mobileNumber;

	@Column(name = "col_faxNumber")
	private Long faxNumber;

	@NotBlank(message = "Required")
	@Column(name = "col_address")
	private String address;

	@NotBlank(message = "Required")
	@Column(name = "col_email")
	@Email
	private String email;

	@NotBlank(message = "Required")
	@Column(name = "col_password", columnDefinition = "varchar(200) not null")
	private String password;

	@Transient
	private String confirmPassword;

	@Column(name = "col_portfolioWebsite")
	private String portfolio_website = null;

	@NotBlank(message = "Required")
	@Column(name = "col_positionAppliedFor")
	private String position_appliedfor;

	@Column(name = "col_expSalary")
	private Long exp_salary;

	@Column(name = "col_prevCompany")
	private String prev_companyName = null;

	@Column(name = "col_message")
	private String message = null;

	@Column(name = "col_status")
	private boolean status = true;

	@NotBlank(message = "Required")
	@Column(name = "col_skills")
	private String skillList;

	@Column(name = "col_role")
	private String role;

	@Column(name = "client_id")
	private Integer clientId;

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getSkillList() {
		return skillList;
	}

	public void setSkillList(String skillList) {
		this.skillList = skillList;
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public Long getMobileNumber() {
		return mobileNumber;
	}

	public void setMobileNumber(Long mobileNumber) {
		this.mobileNumber = mobileNumber;
	}

	public Long getFaxNumber() {
		return faxNumber;
	}

	public void setFaxNumber(Long faxNumber) {
		this.faxNumber = faxNumber;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getConfirmPassword() {
		return confirmPassword;
	}

	public void setConfirmPassword(String confirmPassword) {
		this.confirmPassword = confirmPassword;
	}

	public String getPortfolio_website() {
		return portfolio_website;
	}

	public void setPortfolio_website(String portfolio_website) {
		this.portfolio_website = portfolio_website;
	}

	public String getPosition_appliedfor() {
		return position_appliedfor;
	}

	public void setPosition_appliedfor(String position_appliedfor) {
		this.position_appliedfor = position_appliedfor;
	}

	public Long getExp_salary() {
		return exp_salary;
	}

	public void setExp_salary(Long exp_salary) {
		this.exp_salary = exp_salary;
	}

	public String getPrev_companyName() {
		return prev_companyName;
	}

	public void setPrev_companyName(String prev_companyName) {
		this.prev_companyName = prev_companyName;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public boolean isStatus() {
		return status;
	}

	public void setStatus(boolean status) {
		this.status = status;
	}

	@Override
	public String toString() {
		return "Expert [id=" + id + ", firstName=" + firstName + ", lastName=" + lastName + ", mobileNumber="
				+ mobileNumber + ", faxNumber=" + faxNumber + ", address=" + address + ", email=" + email
				+ ", password=" + password + ", confirmPassword=" + confirmPassword + ", portfolio_website="
				+ portfolio_website + ", position_appliedfor=" + position_appliedfor + ", exp_salary=" + exp_salary
				+ ", prev_companyName=" + prev_companyName + ", message=" + message + ", status=" + status
				+ ", skillList=" + skillList + ", role=" + role + ", clientId=" + clientId + "]";
	}

	public String getRole() {
		return role;
	}

	public void setRole(String role) {
		this.role = role;
	}

	@Override
	public Collection<? extends GrantedAuthority> getAuthorities() {
		return AuthorityUtils.createAuthorityList(getRole());
	}

	@Override
	public String getUsername() {
		return getEmail();
	}

	public String getPassword() {
		return password;
	}

	@Override
	public boolean isAccountNonExpired() {
		return true;
	}

	@Override
	public boolean isAccountNonLocked() {
		return true;
	}

	@Override
	public boolean isCredentialsNonExpired() {
		return true;
	}

	@Override
	public boolean isEnabled() {
		return true;
	}

	public Integer getClientId() {
		return clientId;
	}

	public void setClientId(Integer clientId) {
		this.clientId = clientId;
	}

}
