package com.teklink.sod.model.expert;


/**
 * @author Prateek Raj
 */

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.AssertTrue;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;

import org.hibernate.annotations.CreationTimestamp;

@Entity
@Table(name="tbl_expertContract")
public class ExpertContractManagement {

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private int id;
	
	@NotNull(message="Required")
	@NotEmpty(message="Required")
	@Column(name="col_firstName",columnDefinition="varchar(50) not null")
	private String firstName;
	
	@NotNull(message="Required")
	@NotEmpty(message="Required")
	@Column(name="col_lastName",columnDefinition="varchar(50) not null")
	private String lastName;
	
	@NotNull(message="Required")
	@NotEmpty(message="Required")
	@Column(name="col_summary",columnDefinition="varchar(500) not null")
	private String summary;
	
	@Column(name="col_imagePath",columnDefinition="varchar(225) not null")
	private String imagePath = "null";
	
	@Column(name="col_signPath",columnDefinition="varchar(225) not null")
	private String signPath = "null";
	
	@AssertTrue(message = "Please Accept the Terms & Policies")
	@Column(name="col_accepted",columnDefinition="bit(1) not null")
	private boolean accepted;
	
	@CreationTimestamp
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="col_dateOfApplication",columnDefinition="datetime not null")
	private Date dateOfapplication;
	
	@Column(name="col_appliedBy",columnDefinition="int(11) not null")
	private Integer appliedby;
	
	@Column(name="col_productId",columnDefinition="int(11) not null")
	private Integer productId;

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getFirstName() {
		return firstName;
	}

	public Integer getProductId() {
		return productId;
	}

	public void setProductId(Integer productId) {
		this.productId = productId;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public String getSummary() {
		return summary;
	}

	public void setSummary(String summary) {
		this.summary = summary;
	}

	public String getImagePath() {
		return imagePath;
	}

	public void setImagePath(String imagePath) {
		this.imagePath = imagePath;
	}

	public String getSignPath() {
		return signPath;
	}

	public void setSignPath(String signPath) {
		this.signPath = signPath;
	}

	public boolean isAccepted() {
		return accepted;
	}

	public void setAccepted(boolean accepted) {
		this.accepted = accepted;
	}

	public Date getDateOfapplication() {
		return dateOfapplication;
	}

	public void setDateOfapplication(Date dateOfapplication) {
		this.dateOfapplication = dateOfapplication;
	}

	public Integer getAppliedby() {
		return appliedby;
	}

	public void setAppliedby(Integer appliedby) {
		this.appliedby = appliedby;
	}

	@Override
	public String toString() {
		return "ExpertContractManagement [id=" + id + ", firstName=" + firstName + ", lastName=" + lastName
				+ ", summary=" + summary + ", imagePath=" + imagePath + ", signPath=" + signPath + ", accepted="
				+ accepted + ", dateOfapplication=" + dateOfapplication + ", appliedby=" + appliedby + ", productId="
				+ productId + "]";
	}

	
}
