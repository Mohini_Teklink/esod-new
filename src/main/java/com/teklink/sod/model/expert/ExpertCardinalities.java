package com.teklink.sod.model.expert;


/**
 * @author Prateek Raj
 */

import java.io.Serializable;

import javax.validation.constraints.Email;
import javax.validation.constraints.NotEmpty;

public class ExpertCardinalities implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@NotEmpty(message="Required")
	@Email
	private String expert_email;
	
	@NotEmpty(message="Required")
	private String expert_password;

	public String getExpert_email() {
		return expert_email;
	}

	public void setExpert_email(String expert_email) {
		this.expert_email = expert_email;
	}

	public String getExpert_password() {
		return expert_password;
	}

	public void setExpert_password(String expert_password) {
		this.expert_password = expert_password;
	}

}
