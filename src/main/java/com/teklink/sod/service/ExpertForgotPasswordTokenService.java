package com.teklink.sod.service;

import com.teklink.sod.model.expert.Expert;
import com.teklink.sod.model.expert.ExpertForgotPasswordToken;

public interface ExpertForgotPasswordTokenService {
	
 public Expert getVerifiedExpertbyEmail(String email);
	 
	 public void createForgotPasswordToken(Expert expert, String token);

	public ExpertForgotPasswordToken getForgotPasswordexpert(String token);

	public boolean updateForgotPasswordExpert(Expert password);
	
	public boolean deleteForgotPasswordToken(ExpertForgotPasswordToken expert);
	
	public ExpertForgotPasswordToken checkExpertForgotPasswordToken(Expert expert);

	public boolean updateForgotPasswordToken(ExpertForgotPasswordToken expertPasswordResetToken, String token);

}
