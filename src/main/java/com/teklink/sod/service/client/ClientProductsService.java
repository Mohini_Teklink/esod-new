package com.teklink.sod.service.client;


/**
 * @author Prateek Raj
 */

import java.util.List;

import com.teklink.sod.model.client.ClientProducts;

public interface ClientProductsService {
	public abstract boolean addProduct(ClientProducts clientProducts);

	public abstract boolean updateProduct(ClientProducts clientProducts);

	public abstract List<ClientProducts> viewClientProduct(int prodType);

	public abstract ClientProducts viewClientProductById(int prodId);

	public abstract List<ClientProducts> viewClientProduct();

	public abstract boolean productStatusUpdate(int id, boolean status);

	public abstract ClientProducts getClientProductByIdAndStatus(int id, boolean status);

	
}
