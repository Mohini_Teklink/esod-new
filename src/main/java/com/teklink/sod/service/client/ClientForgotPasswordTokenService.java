package com.teklink.sod.service.client;

import com.teklink.sod.model.client.Client;
import com.teklink.sod.model.client.ClientForgotPasswordToken;

public interface ClientForgotPasswordTokenService {
	
	public Client getVerifiedClientbyEmail(String email);
	 
	 public void createForgotPasswordToken(Client client, String token);

	public ClientForgotPasswordToken getForgotPasswordClient(String token);

	public boolean updateClientPassword(Client password);
	
	public boolean deleteForgotPasswordToken(ClientForgotPasswordToken client);
	
	public ClientForgotPasswordToken checkClientForgotPasswordToken(Client client);

	public boolean updateForgotPasswordToken(ClientForgotPasswordToken clientForgotPasswordToken, String token);

}
