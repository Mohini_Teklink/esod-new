package com.teklink.sod.service.client;


import java.util.List;

/**
 * @author Prateek Raj
 */

import com.teklink.sod.model.client.Client;

public interface ClientService {
	public abstract Client validateClientCredential(String email, String password);
	public abstract boolean registerClient(Client client);
	public abstract List<Client> getAllClient();
	public abstract boolean updateClient(Client client);
	public Client byId(int id);
	public abstract Client byEmail(String email);
	public abstract Boolean updateById(Integer id, Boolean status);
	
}