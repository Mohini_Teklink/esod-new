package com.teklink.sod.service.client;

/**
 * @author Prateek Raj
 */

import java.util.List;

import com.teklink.sod.model.client.ClientPacks;

public interface ClientPacksService {

	public ClientPacks getPackById(int id);
	public abstract List<ClientPacks> getAllPacksList();
	public boolean packUpdate(ClientPacks clientPacks);
}
