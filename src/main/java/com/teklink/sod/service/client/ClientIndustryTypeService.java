package com.teklink.sod.service.client;


/**
 * @author Prateek Raj
 */

import java.util.List;

import com.teklink.sod.model.client.ClientIndustryType;

public interface ClientIndustryTypeService {
	public abstract List<ClientIndustryType> getIndustryTypesList();
}
