package com.teklink.sod.service.client;

import java.util.List;

import com.teklink.sod.model.client.ClientRequirement;

public interface ClientRequirementService {

	public Boolean saveClientRequirement(ClientRequirement clientRequirement) ;
	
	public List<ClientRequirement> getClientList(); 
}
