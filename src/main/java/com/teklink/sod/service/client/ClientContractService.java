package com.teklink.sod.service.client;


import org.springframework.binding.message.MessageContext;
import org.springframework.web.multipart.MultipartFile;

/**
 * @author Prateek Raj
 */

import com.teklink.sod.model.client.ClientContract;

public interface ClientContractService {

	public abstract boolean saveContract(ClientContract clientContract);
	
	public String saveContractForm(ClientContract clientContractModel, MultipartFile inputImageFile,MultipartFile inputSignFile,MessageContext msgCnt) ;
}
