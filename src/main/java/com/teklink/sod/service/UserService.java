package com.teklink.sod.service;


/**
 * @author Prateek Raj
 */

import com.teklink.sod.model.user.User;

public interface UserService {
	public abstract User validateUserCredential(String username, String password);
	public abstract boolean registerUser(User user);
	public abstract boolean checkUserByEmail(String email);
	public abstract boolean checkUserByUsername(String username);
	public abstract boolean checkUserByUsernameAndEmail(String email,String username);
	public abstract boolean updateUser(User user);
}
