package com.teklink.sod.service;


/**
 * @author Prateek Raj
 */

import com.teklink.sod.model.Payment;

public interface PaymentService {
	public abstract boolean savePaymentEntries(Payment payment);
}
