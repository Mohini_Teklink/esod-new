package com.teklink.sod.service;


import java.util.List;

/**
 * @author Prateek Raj
 */

import com.teklink.sod.model.expert.ExpertContractManagement;

public interface ExpertContractService {

	public abstract boolean saveExpertContract(ExpertContractManagement contractManagement);
	public abstract List<ExpertContractManagement> getAllExpertContract();
	public abstract ExpertContractManagement getExpertById(int id);

}
