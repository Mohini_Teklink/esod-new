package com.teklink.sod.service;


/**
 * @author Prateek Raj
 */

import java.util.List;

import com.teklink.sod.model.Skills;

public interface SkillService {
	
	public abstract List<Skills> getAllSkills();
	public abstract List<Skills> getAllSkillsByExpert(String skillList);
	public abstract String getAllSkillsStrByExpert(String skillList);
}
