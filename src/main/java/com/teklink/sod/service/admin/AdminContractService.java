package com.teklink.sod.service.admin;

import com.teklink.sod.model.admin.AdminContract;

/**
 * @author Shikha
 *
 */
public interface AdminContractService {
	
	public abstract boolean saveContract(AdminContract adminContract);
	

}
