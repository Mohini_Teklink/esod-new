package com.teklink.sod.service.impl.client;


import java.util.List;

/**
 * @author Shikha Rohit
 */

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import com.teklink.sod.dao.client.ClientDAO;
import com.teklink.sod.model.client.Client;
import com.teklink.sod.service.ExpertService;
import com.teklink.sod.service.client.ClientService;

@Service("clientService")
public class ClientServiceImpl implements ClientService,UserDetailsService {

	@Autowired
	private ClientDAO clientDAO;

	public ClientDAO getClientDAO() {
		return clientDAO;
	}

	public void setClientDAO(ClientDAO clientDAO) {
		this.clientDAO = clientDAO;
	}

	@Autowired
	private ExpertService expertService;
	
	public ExpertService getExpertService() {
		return expertService;
	}

	public void setExpertService(ExpertService expertService) {
		this.expertService = expertService;
	}

	@Override
	public Client validateClientCredential(String email, String password) {
		System.out.println("email" + email + "   password ==:>"+password);
		Client client = getClientDAO().getClientDetailsByEmailAndPassword(email, password);
		 Integer id = client.getId(); 
		 System.out.println("client id ==> "+id);
		client.setExpertList(expertService.getAllExpertByClientId(id));
		return client;
	}

	@Override
	public boolean registerClient(Client client) {
		boolean isRegister = false;
		boolean saveClient = getClientDAO().saveClient(client);
		if (saveClient)
			isRegister = true;
		return isRegister;
	}

	@Override
	public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
		System.out.println(username);
		return getClientDAO().byEmail(username);
	}

	@Override
	public Client byEmail(String email) {
		System.out.println(email);
		return getClientDAO().byEmail(email);
		
	}

	/*@Override
	public Client byId(int id) {
		return getClientDAO().byId(id);
	}*/

	@Override
	public boolean updateClient(Client client) {
		boolean isRegister = false;
		boolean saveClient = getClientDAO().updateClient(client);
		if (saveClient)
			isRegister = true;
		return isRegister;
	}

	@Override
	public List<Client> getAllClient() {
		
		return  getClientDAO().getClientList();
	}

	@Override
	public Client byId(int id) {
		// TODO Auto-generated method stub
		Client client = getClientDAO().byId(id);
		return client;
	}

	@Override
	public Boolean updateById(Integer id, Boolean status) {
		// TODO Auto-generated method stub
		return getClientDAO().updateById(id, status);
	}

}
