package com.teklink.sod.service.impl.client;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.teklink.sod.dao.client.ClientProductsDAO;
import com.teklink.sod.model.client.ClientProducts;
import com.teklink.sod.service.client.ClientProductsService;

@Service("clientProductService")
public class ClientProductsServiceImpl implements ClientProductsService {

	@Autowired
	private ClientProductsDAO clientProductsDAO;

	public ClientProductsDAO getClientProductsDAO() {
		return clientProductsDAO;
	}

	public void setClientProductsDAO(ClientProductsDAO clientProductsDAO) {
		this.clientProductsDAO = clientProductsDAO;
	}

	@Override
	public boolean addProduct(ClientProducts clientProducts) {
		boolean isRegister = false;
		boolean saveStudent = getClientProductsDAO().saveProduct(clientProducts);
		if (saveStudent)
			isRegister = true;
		return isRegister;
	}

	@Override
	public boolean updateProduct(ClientProducts clientProducts) {
		boolean isRegister = false;
		boolean saveStudent = getClientProductsDAO().updateProduct(clientProducts);
		if (saveStudent)
			isRegister = true;
		return isRegister;
	}

	@Override
	public List<ClientProducts> viewClientProduct(int prodType) {
		return getClientProductsDAO().getProductDetailbyProdType(prodType);
	}

	@Override
	public ClientProducts viewClientProductById(int prodId) {
		return getClientProductsDAO().getProductDetailbyprodId(prodId);
	}

	@Override
	public List<ClientProducts> viewClientProduct() {
		return getClientProductsDAO().getAllProductDetail();
	}

	@Override
	public boolean productStatusUpdate(int id, boolean status) {
		return getClientProductsDAO().updateStatusbyId(id, status);
	}
	@Override
	public ClientProducts getClientProductByIdAndStatus(int id, boolean status) {
		return getClientProductsDAO().getProductDetailByIdAndStatus(id, status);
	}

}
