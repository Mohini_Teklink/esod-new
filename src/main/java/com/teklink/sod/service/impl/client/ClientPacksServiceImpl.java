package com.teklink.sod.service.impl.client;

/**
 * @author Prateek Raj
 * 
 */

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.teklink.sod.dao.client.ClientPacksDAO;
import com.teklink.sod.model.client.ClientPacks;
import com.teklink.sod.service.client.ClientPacksService;


@Service("clientPackServiceImpl")
public class ClientPacksServiceImpl implements ClientPacksService{

	@Autowired
	private ClientPacksDAO clientPacksDAO;
	
	public ClientPacksDAO getClientPacksDAO() {
		return clientPacksDAO;
	}

	public void setClientPacksDAO(ClientPacksDAO clientPacksDAO) {
		this.clientPacksDAO = clientPacksDAO;
	}

	@Override
	public List<ClientPacks> getAllPacksList() {
		return getClientPacksDAO().getListOfPacks();
	}

	@Override
	public ClientPacks getPackById(int id) {
		return getClientPacksDAO().getPackById(id);
	}

	@Override
	public boolean packUpdate(ClientPacks clientPacks) {
		return getClientPacksDAO().updatePack(clientPacks);
	}

}
