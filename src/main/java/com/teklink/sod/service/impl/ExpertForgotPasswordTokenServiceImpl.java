package com.teklink.sod.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.teklink.sod.dao.ExpertForgotPasswordDAO;
import com.teklink.sod.model.expert.Expert;
import com.teklink.sod.model.expert.ExpertForgotPasswordToken;
import com.teklink.sod.service.ExpertForgotPasswordTokenService;

@Service("expertForgotPasswordTokenService")
public class ExpertForgotPasswordTokenServiceImpl implements ExpertForgotPasswordTokenService {
	
	@Autowired
	ExpertForgotPasswordDAO expertForgotPasswordDAO;

	@Override
	public Expert getVerifiedExpertbyEmail(String email) {
		return expertForgotPasswordDAO.getVerifiedUser(email);
	}

	@Override
	public void createForgotPasswordToken(Expert expert, String token) {
		expertForgotPasswordDAO.createPasswordResetToken(expert, token);	
	}

	@Override
	public ExpertForgotPasswordToken getForgotPasswordexpert(String token) {
		return expertForgotPasswordDAO.getPasswordResetUser(token);
	}

	@Override
	public boolean updateForgotPasswordExpert(Expert password) {
		return expertForgotPasswordDAO.updateUserPassword(password);
	}

	@Override
	public boolean deleteForgotPasswordToken(ExpertForgotPasswordToken expert) {
		return expertForgotPasswordDAO.deletetokenPassword(expert);
	}

	@Override
	public ExpertForgotPasswordToken checkExpertForgotPasswordToken(Expert expert) {
		return expertForgotPasswordDAO.checkExpertForgotPassword(expert);
	}

	@Override
	public boolean updateForgotPasswordToken(ExpertForgotPasswordToken expertPasswordResetToken, String token) {
		return expertForgotPasswordDAO.updatePasswordResetToken(expertPasswordResetToken, token);
	}

}
