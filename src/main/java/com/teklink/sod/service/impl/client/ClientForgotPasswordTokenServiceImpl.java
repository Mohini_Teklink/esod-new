package com.teklink.sod.service.impl.client;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.teklink.sod.dao.client.ClientForgotPasswordDAO;
import com.teklink.sod.model.client.Client;
import com.teklink.sod.model.client.ClientForgotPasswordToken;
import com.teklink.sod.service.client.ClientForgotPasswordTokenService;

@Service("clientForgotPasswordTokenService")
public class ClientForgotPasswordTokenServiceImpl implements ClientForgotPasswordTokenService {

	@Autowired
	ClientForgotPasswordDAO clientForgotPasswordDAO;
	
	@Override
	public Client getVerifiedClientbyEmail(String email) {
		return clientForgotPasswordDAO.getVerifiedUser(email);
	}

	@Override
	public void createForgotPasswordToken(Client client, String token) {
		clientForgotPasswordDAO.createPasswordResetToken(client, token);
	}

	@Override
	public ClientForgotPasswordToken getForgotPasswordClient(String token) {
		return clientForgotPasswordDAO.getPasswordResetUser(token);
	}

	@Override
	public boolean updateClientPassword(Client password) {
		return clientForgotPasswordDAO.updateUserPassword(password);
	}

	@Override
	public boolean deleteForgotPasswordToken(ClientForgotPasswordToken client) {
		return clientForgotPasswordDAO.deletetokenPassword(client);
	}

	@Override
	public ClientForgotPasswordToken checkClientForgotPasswordToken(Client client) {
		return clientForgotPasswordDAO.checkClientForgotPassword(client);
	}

	@Override
	public boolean updateForgotPasswordToken(ClientForgotPasswordToken clientForgotPasswordToken, String token) {
		return clientForgotPasswordDAO.updatePasswordResetToken(clientForgotPasswordToken, token);
	}
	
	

}
