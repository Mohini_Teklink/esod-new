package com.teklink.sod.service.impl;


/**
 * @author Prateek Raj
 */

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.teklink.sod.dao.SkillsDAO;
import com.teklink.sod.model.Skills;
import com.teklink.sod.service.SkillService;

@Service("skillService")
public class SkillServiceImpl implements SkillService{

	@Autowired
	private SkillsDAO skillDAO;
	
	public SkillsDAO getSkillDAO() {
		return skillDAO;
	}

	public void setSkillDAO(SkillsDAO skillDAO) {
		this.skillDAO = skillDAO;
	}

	@Override
	public List<Skills> getAllSkills() {
		return getSkillDAO().getSkillList();
	}

	@Override
	public List<Skills> getAllSkillsByExpert(String skillList) {
		return getSkillDAO().getSkillListByExpert(skillList);
	}

	@Override
	public String getAllSkillsStrByExpert(String skillList) {
		return getSkillDAO().getSkillListStrByExpert(skillList);
	}
	
}
