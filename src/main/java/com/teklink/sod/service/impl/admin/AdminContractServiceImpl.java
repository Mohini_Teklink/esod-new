package com.teklink.sod.service.impl.admin;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.teklink.sod.dao.admin.AdminContractDAO;
import com.teklink.sod.model.admin.AdminContract;
import com.teklink.sod.service.admin.AdminContractService;

/**
 * @author Shikha Rohit
 *
 */

@Service("dminContractService")
public class AdminContractServiceImpl implements AdminContractService {
	
	@Autowired
	private AdminContractDAO adminContractDAO;

	public AdminContractDAO getAdminContractDAO() {
		return adminContractDAO;
	}

	public void setAdminContractDAO(AdminContractDAO adminContractDAO) {
		this.adminContractDAO = adminContractDAO;
	}
	  @Override
	  public boolean saveContract(AdminContract adminContract) {
	  return getAdminContractDAO().saveContract(adminContract);
	  

	  }
	
}
