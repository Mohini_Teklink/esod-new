package com.teklink.sod.service.impl.client;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.teklink.sod.dao.client.ClientIndustryTypeDAO;
import com.teklink.sod.model.client.ClientIndustryType;
import com.teklink.sod.service.client.ClientIndustryTypeService;


@Service("clientIndustryTypeService")
public class ClientIndustryTypeServiceImpl implements ClientIndustryTypeService{

	@Autowired 
	private ClientIndustryTypeDAO clientIndustryTypeDAO;
	
	public ClientIndustryTypeDAO getClientIndustryTypeDAO() {
		return clientIndustryTypeDAO;
	}

	public void setClientIndustryTypeDAO(ClientIndustryTypeDAO clientIndustryTypeDAO) {
		this.clientIndustryTypeDAO = clientIndustryTypeDAO;
	}

	@Override
	public List<ClientIndustryType> getIndustryTypesList() {
		return getClientIndustryTypeDAO().getIndustryTypes();
	}

}
