package com.teklink.sod.service.impl.client;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.teklink.sod.dao.client.ClientRequirementDao;
import com.teklink.sod.model.client.ClientRequirement;
import com.teklink.sod.service.client.ClientRequirementService;

@Service("clientRequirementService")
public class ClientRequirementServiceImpl implements ClientRequirementService {

	@Autowired
	private ClientRequirementDao clientRequirementDao;
	
	public ClientRequirementDao getClientRequirementDao() {
		return clientRequirementDao;
	}

	public void setClientRequirementDao(ClientRequirementDao clientRequirementDao) {
		this.clientRequirementDao = clientRequirementDao;
	}
	@Override
	public Boolean saveClientRequirement(ClientRequirement clientRequirement) {
		return getClientRequirementDao().saveClientRequirement(clientRequirement);
	}

	@Override
	public List<ClientRequirement> getClientList() {
		return getClientRequirementDao().getClientList();
		
	}
}
