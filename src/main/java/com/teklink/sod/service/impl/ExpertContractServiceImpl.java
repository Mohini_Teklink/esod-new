package com.teklink.sod.service.impl;


import java.util.List;

/**
 * @author Prateek Raj
 */

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.teklink.sod.dao.ExpertContractDAO;
import com.teklink.sod.model.expert.ExpertContractManagement;
import com.teklink.sod.service.ExpertContractService;

@Service("expertContractService")
public class ExpertContractServiceImpl implements ExpertContractService{

	@Autowired
	private ExpertContractDAO expertContractDAO;

	public ExpertContractDAO getExpertContractDAO() {
		return expertContractDAO;
	}

	public void setExpertContractDAO(ExpertContractDAO expertContractDAO) {
		this.expertContractDAO = expertContractDAO;
	}

	@Override
	public boolean saveExpertContract(ExpertContractManagement contractManagement) {
		return getExpertContractDAO().saveContract(contractManagement);
	}
	

	@Override
	public List<ExpertContractManagement> getAllExpertContract() {
		List<ExpertContractManagement> expContractList = getExpertContractDAO().getAllExpertContract();
		return expContractList;
	}

	@Override
	public ExpertContractManagement getExpertById(int id) {
		return getExpertContractDAO().getExpertById(id);
	}
	
	
}
