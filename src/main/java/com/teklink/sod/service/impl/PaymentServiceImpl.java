package com.teklink.sod.service.impl;


/**
 * @author Prateek Raj
 */

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.teklink.sod.dao.PaymentDAO;
import com.teklink.sod.model.Payment;
import com.teklink.sod.service.PaymentService;

@Service("/paymentService")
public class PaymentServiceImpl implements PaymentService {

	@Autowired
	private PaymentDAO paymentDAO;

	public PaymentDAO getPaymentDAO() {
		return paymentDAO;
	}

	public void setPaymentDAO(PaymentDAO paymentDAO) {
		this.paymentDAO = paymentDAO;
	}

	@Override
	public boolean savePaymentEntries(Payment payment) {
		return getPaymentDAO().savePaymentEntries(payment);
	}

}
