package com.teklink.sod.service.impl.client;


import java.io.File;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * @author Prateek Raj
 */

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.binding.message.MessageBuilder;
import org.springframework.binding.message.MessageContext;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import com.teklink.sod.dao.client.ClientContractDAO;
import com.teklink.sod.model.SaveAttachedFile;
import com.teklink.sod.model.client.ClientContract;
import com.teklink.sod.service.client.ClientContractService;

@Service("clientContractService")
public class ClientContractServiceImpl implements ClientContractService {

	@Autowired 
	private ClientContractDAO clientContractDAO;

	public ClientContractDAO getClientContractDAO() {
		return clientContractDAO;
	}

	public void setClientContractDAO(ClientContractDAO clientContractDAO) {
		this.clientContractDAO = clientContractDAO;
	}

	@Override
	public boolean saveContract(ClientContract clientContract) {
		return getClientContractDAO().saveContract(clientContract);
	}
	
	@Override
	public String saveContractForm(ClientContract clientContractModel, MultipartFile inputImageFile,MultipartFile inputSignFile,MessageContext msgCnt) {
		try {
			System.out.println(clientContractModel);
			String rootPath = System.getProperty("catalina.home");
			File dir = new File(rootPath + File.separator + "SOD" + File.separator + "Client-Doc" + File.separator
					+ clientContractModel.getContractNum() + "-" + clientContractModel.getName() + "-"
					+ new SimpleDateFormat("yyyy.MM.dd.HH.mm.ss").format(new Date()) + File.separator);
			if (!dir.exists())
				dir.mkdirs();
			String phtPath = new SaveAttachedFile(inputImageFile, "img", dir).uploadFile();
			String signPath = new SaveAttachedFile(inputSignFile, "sign", dir).uploadFile();
			if (phtPath == null && signPath == null) {
			clientContractModel.setImagePath(phtPath);
      		clientContractModel.setSignPath(signPath);
				if (saveContract(clientContractModel)) {
					System.out.println("success");
					return "success";
				}
				return "failure";

			}			
		}catch(Exception exp) {
			msgCnt.addMessage(new MessageBuilder().error().defaultText("Please Upload Files!!").build());
		}
		
		return "failure";

	}
	
}
