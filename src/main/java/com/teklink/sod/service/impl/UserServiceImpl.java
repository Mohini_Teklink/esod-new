package com.teklink.sod.service.impl;


/**
 * @author Prateek Raj
 */

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import com.teklink.sod.dao.UserDAO;
import com.teklink.sod.model.user.User;
import com.teklink.sod.service.UserService;

@Service("userService")
public class UserServiceImpl implements UserService,UserDetailsService {

	@Autowired
	private UserDAO userDAO;

	public UserDAO getUserDAO() {
		return userDAO;
	}

	public void setUserDAO(UserDAO userDAO) {
		this.userDAO = userDAO;
	}

	@Override
	public User validateUserCredential(String username, String password) {
		User user = getUserDAO().getUserByUserNameAndPassword(username, password);
		return user;
	}

	@Override
	public boolean registerUser(User user) {
		boolean isRegister = false;
		boolean isSave = getUserDAO().saveUser(user);
		if (isSave)
			isRegister = true;
		return isRegister;
	}
	
	@Override
	public boolean updateUser(User user) {
		boolean isRegister = false;
		boolean isSave = getUserDAO().updateUser(user);
		if (isSave)
			isRegister = true;
		return isRegister;
	}

	@Override
	public boolean checkUserByEmail(String email) {
		return getUserDAO().checkUserExitByEmail(email);
	}

	@Override
	public boolean checkUserByUsername(String username) {
		return getUserDAO().checkUserExitByUsername(username);
	}

	@Override
	public boolean checkUserByUsernameAndEmail(String email, String username) {
		return getUserDAO().checkUserExitByEmailAndUserName(email, username);
	}

	@Override
	public UserDetails loadUserByUsername(String arg0) throws UsernameNotFoundException {
		return getUserDAO().getUserByUserName(arg0);
	}

}
