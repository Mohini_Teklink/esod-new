package com.teklink.sod.service.impl;


/**
 * @author Prateek Raj
 */

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import com.teklink.sod.dao.ExpertDAO;
import com.teklink.sod.model.expert.Expert;
import com.teklink.sod.service.ExpertService;

@Service("expertService")
public class ExpertServiceImpl implements ExpertService,UserDetailsService{

	@Autowired
	private ExpertDAO expertDAO;

	public ExpertDAO getExpertDAO() {
		return expertDAO;
	}

	public void setExpertDAO(ExpertDAO expertDAO) {
		this.expertDAO = expertDAO;
	}
	
	@Override
	public Expert validateExpertCredential(String email, String password) {
		Expert expert = getExpertDAO().getExpertByEmailAndPassword(email, password);
		return expert;
	}

	@Override
	public boolean registerExpert(Expert expert) {
		boolean isRegister = false;
		boolean saveExpert = getExpertDAO().saveExpert(expert);
		if (saveExpert)
			isRegister = true;
		return isRegister;
	}

	@Override
	public List<Expert> getAllExpert() {
		List<Expert> expList = getExpertDAO().getAllExpert();
		return expList;
	}

	@Override
	public List<Expert> getAllExpertByClientId(Integer id) {
		List<Expert> expList = getExpertDAO().byClientId(id);
		return expList;
	} 
	@Override
	public Expert byId(int id) {
		Expert expert = getExpertDAO().byId(id);
		return expert;
	}

	@Override
	public boolean updateById(int id,boolean status , int clientId) {
		return getExpertDAO().updateById(id,status, clientId);
		
	}

	@Override
	public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
		System.out.println("Inside -->>"+ username);
		return getExpertDAO().byEmail(username);
	}

	@Override
	public boolean updateClient(Expert expert) {
		boolean isRegister = false;
		boolean saveExpert = getExpertDAO().updateExpert(expert);
		if (saveExpert)
			isRegister = true;
		return isRegister;
	}


}
