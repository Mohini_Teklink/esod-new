package com.teklink.sod.service;


/**
 * @author Prateek Raj
 */

import java.util.List;

import com.teklink.sod.model.expert.Expert;

public interface ExpertService {

	public abstract Expert validateExpertCredential(String email, String password);
	public abstract boolean registerExpert(Expert expert) ;
	public abstract List<Expert> getAllExpert(); 
	public List<Expert> getAllExpertByClientId(Integer id) ;
	public abstract Expert byId(int id);
	public abstract boolean updateById(int id,boolean status,int clientId);
	public abstract boolean updateClient(Expert expert);
	
}
