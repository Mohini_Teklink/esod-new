package com.teklink.sod.dao.client;

import java.util.List;

import com.teklink.sod.model.client.ClientRequirement;

/**
 * @author Shikha
 *
 */
public interface ClientRequirementDao {

	public abstract Boolean saveClientRequirement(ClientRequirement clientRequirement);
	
	public List<ClientRequirement> getClientList() ;
}
