package com.teklink.sod.dao.client;


/**
 * @author Prateek Raj
 */

import java.util.List;

import com.teklink.sod.model.client.Client;
import com.teklink.sod.model.client.ClientPacks;

public interface ClientDAO {
	public abstract boolean saveClient(Client client);
	public Client getClientDetailsByEmailAndPassword(String email, String password);
	public Client byEmail(String email);
	public List<ClientPacks> getPackName();
//	public Client byId(int id);
	public List<Client> getClientList();
	public abstract boolean updateClient(Client client);
	public Boolean updateById(int id,Boolean status);
	public abstract Client byId(int id);
}
