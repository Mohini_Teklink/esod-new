package com.teklink.sod.dao.client;

import java.util.List;

import com.teklink.sod.model.client.ClientProducts;

public interface ClientProductsDAO {
	public abstract boolean saveProduct(ClientProducts clientProducts);
	public boolean updateProduct(ClientProducts clientProducts);
	public ClientProducts getProductDetailbyprodId(int prodId);
	public List<ClientProducts> getProductDetailbyProdType(int prodType);
	public List<ClientProducts> getAllProductDetail();
	public boolean updateStatusbyId(int id, boolean statusId);
	public ClientProducts getProductDetailByIdAndStatus(int prodId, boolean status);
}
