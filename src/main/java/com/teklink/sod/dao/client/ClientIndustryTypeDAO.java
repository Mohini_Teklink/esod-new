package com.teklink.sod.dao.client;

import java.util.List;

import com.teklink.sod.model.client.ClientIndustryType;

public interface ClientIndustryTypeDAO {
	public List<ClientIndustryType> getIndustryTypes();
}
