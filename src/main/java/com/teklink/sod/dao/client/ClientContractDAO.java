package com.teklink.sod.dao.client;


/**
 * @author Prateek Raj
 */

import com.teklink.sod.model.client.ClientContract;

public interface ClientContractDAO {

	public abstract boolean saveContract(ClientContract clientContract);
}
