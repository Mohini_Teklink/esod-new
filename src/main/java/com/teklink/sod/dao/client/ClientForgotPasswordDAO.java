package com.teklink.sod.dao.client;

import com.teklink.sod.model.client.Client;
import com.teklink.sod.model.client.ClientForgotPasswordToken;


public interface ClientForgotPasswordDAO {

	public Client getVerifiedUser(String email);
	 
	 public void createPasswordResetToken(Client client, String token);

	public ClientForgotPasswordToken getPasswordResetUser(String token);

	public boolean updateUserPassword(Client passwordDto);
	
	public boolean deletetokenPassword(ClientForgotPasswordToken client);
	
	public ClientForgotPasswordToken checkClientForgotPassword(Client client);

	public boolean updatePasswordResetToken(ClientForgotPasswordToken clientForgotPasswordToken, String token);

}
