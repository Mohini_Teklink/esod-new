package com.teklink.sod.dao.client;


/**
 * @author Prateek Raj
 */

import java.util.List;

import com.teklink.sod.model.client.ClientPacks;

public interface ClientPacksDAO {
	public ClientPacks getPackById(int id);
	public abstract List<ClientPacks> getListOfPacks();
	public boolean updatePack(ClientPacks clientPacks);
}
