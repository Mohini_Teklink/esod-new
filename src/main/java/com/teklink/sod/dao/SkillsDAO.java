package com.teklink.sod.dao;


/**
 * @author Prateek Raj
 */

import java.util.List;

import com.teklink.sod.model.Skills;

public interface SkillsDAO {

	public abstract List<Skills> getSkillList();
	public abstract List<Skills> getSkillListByExpert(String SkillList);
	public abstract String getSkillListStrByExpert(String skillList);
}
