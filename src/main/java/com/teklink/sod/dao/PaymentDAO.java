package com.teklink.sod.dao;


/**
 * @author Prateek Raj
 */

import com.teklink.sod.model.Payment;

public interface PaymentDAO {
	public abstract boolean savePaymentEntries(Payment payment);
}
