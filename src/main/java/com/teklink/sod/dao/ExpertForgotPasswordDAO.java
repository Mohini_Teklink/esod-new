package com.teklink.sod.dao;

import com.teklink.sod.model.expert.Expert;
import com.teklink.sod.model.expert.ExpertForgotPasswordToken;

public interface ExpertForgotPasswordDAO {
	
	 public Expert getVerifiedUser(String email);
	 
	 public void createPasswordResetToken(Expert expert, String token);

	public ExpertForgotPasswordToken getPasswordResetUser(String token);

	public boolean updateUserPassword(Expert passwordDto);
	
	public boolean deletetokenPassword(ExpertForgotPasswordToken expert);
	
	public ExpertForgotPasswordToken checkExpertForgotPassword(Expert expert);

	public boolean updatePasswordResetToken(ExpertForgotPasswordToken expertPasswordResetToken, String token);

}
