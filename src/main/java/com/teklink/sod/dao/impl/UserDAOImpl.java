package com.teklink.sod.dao.impl;


/**
 * @author Prateek Raj
 */

import java.util.List;

import org.hibernate.Session;
import org.hibernate.Transaction;
import org.hibernate.criterion.DetachedCriteria;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.orm.hibernate5.HibernateTemplate;
import org.springframework.stereotype.Repository;

import com.teklink.sod.dao.UserDAO;
import com.teklink.sod.model.user.User;

@Repository
public class UserDAOImpl implements UserDAO {

	@Autowired
	private HibernateTemplate hibernateTemplate;

	public HibernateTemplate getHibernateTemplate() {
		return hibernateTemplate;
	}

	public void setHibernateTemplate(HibernateTemplate hibernateTemplate) {
		this.hibernateTemplate = hibernateTemplate;
	}

	@Override
	public boolean saveUser(User expert) {
		int id = (Integer) getHibernateTemplate().save(expert);
		if (id > 0)
			return true;
		return false;
	}

	@SuppressWarnings("unchecked")
	@Override
	public User getUserByUserNameAndPassword(String userName, String password) {
		DetachedCriteria detachedCriteria= DetachedCriteria.forClass(User.class);
		detachedCriteria.add(Restrictions.eq("username", userName));
		detachedCriteria.add(Restrictions.eq("password", password));
		List<User> list = (List<User>) getHibernateTemplate().findByCriteria(detachedCriteria);
		if(list.size()>0 && list!=null) {
			return list.get(0);
		}
		
		return null;
	}

	
	
	@SuppressWarnings("unchecked")
	@Override
	public boolean checkUserExitByEmailAndUserName(String email, String username) {
		DetachedCriteria detachedCriteria = DetachedCriteria.forClass(User.class);
		detachedCriteria.add(Restrictions.eq("email", email));
		detachedCriteria.add(Restrictions.eq("username", username));
		List<User> list = (List<User>) getHibernateTemplate().findByCriteria(detachedCriteria);
		if(list!=null && list.size()>0)return true;
		return false;
	}

	@SuppressWarnings("unchecked")
	@Override
	public boolean checkUserExitByEmail(String email) {
		DetachedCriteria detachedCriteria = DetachedCriteria.forClass(User.class);
		detachedCriteria.add(Restrictions.eq("email", email));
		List<User> list = (List<User>) getHibernateTemplate().findByCriteria(detachedCriteria);
		if(list!=null && list.size()>0)return true;
		return false;
	}

	@SuppressWarnings("unchecked")
	@Override
	public boolean checkUserExitByUsername(String username) {
		DetachedCriteria detachedCriteria = DetachedCriteria.forClass(User.class);
		detachedCriteria.add(Restrictions.eq("username", username));
		List<User> list = (List<User>) getHibernateTemplate().findByCriteria(detachedCriteria);
		if(list!=null && list.size()>0)return true;
		return false;
	}

	@SuppressWarnings("unchecked")
	@Override
	public User getUserByUserName(String username) {
		DetachedCriteria detachedCriteria= DetachedCriteria.forClass(User.class);
		detachedCriteria.add(Restrictions.eq("username", username));
		List<User> list = (List<User>) getHibernateTemplate().findByCriteria(detachedCriteria);
		if(list.size()>0 && list!=null) {
			return list.get(0);
		}
		
		return null;
	}

	@Override
	public boolean updateUser(User user) {
		try {
			Session session = getHibernateTemplate().getSessionFactory().openSession();
			Transaction transaction = session.beginTransaction();
			session.saveOrUpdate(user);
			transaction.commit();
			session.close();
		    return true;
		    
    } catch (Exception e) {
    	e.printStackTrace();
    }
    return false;
	}
}
