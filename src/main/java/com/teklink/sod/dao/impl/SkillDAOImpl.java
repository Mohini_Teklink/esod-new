package com.teklink.sod.dao.impl;


/**
 * @author Prateek Raj
 */

import java.util.List;

import org.hibernate.criterion.DetachedCriteria;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.orm.hibernate5.HibernateTemplate;
import org.springframework.stereotype.Repository;

import com.teklink.sod.dao.SkillsDAO;
import com.teklink.sod.model.Skills;

@Repository
public class SkillDAOImpl implements SkillsDAO {

	@Autowired
	private HibernateTemplate hibernateTemplate;

	public HibernateTemplate getHibernateTemplate() {
		return hibernateTemplate;
	}

	public void setHibernateTemplate(HibernateTemplate hibernateTemplate) {
		this.hibernateTemplate = hibernateTemplate;
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<Skills> getSkillList() {
		DetachedCriteria detachedCriteria = DetachedCriteria.forClass(Skills.class);
		List<Skills> skillList = (List<Skills>) getHibernateTemplate().findByCriteria(detachedCriteria);
		if (skillList.size() > 0 && skillList != null) {
			return skillList;
		}
		return null;
	}

	@SuppressWarnings({ "unchecked", "deprecation" })
	@Override
	public List<Skills> getSkillListByExpert(String skillList) {

		if (skillList != null) {
			try {
				String[] skill = (skillList.split(","));
				String list = new String();
				for (int i = 0; i < skill.length; i++) {
					if (i == skill.length - 1) {
						list += skill[i];
					} else {
						list += skill[i] + ",";
					}
				}
				String query = " FROM Skills where id in(" + list + ")";
				List<Skills> skill_List = (List<Skills>) getHibernateTemplate().find(query);
				if (skill_List.size() > 0 && skill_List != null) {
					return skill_List;
				}
			} catch (Exception exp) {
				exp.printStackTrace();
			}
		}

		return null;
	}

	@Override
	public String getSkillListStrByExpert(String skillList) {
		List<Skills> skill = getSkillListByExpert(skillList);
		String skill1 = new String();
		int i=0;
		for(Skills skl : skill) {
			skill1 +=skl.getSkillName();
			i++;
			if(i!=skill.size()) {
				skill1+=",";
			}
		}
		return skill1;
	}

}
