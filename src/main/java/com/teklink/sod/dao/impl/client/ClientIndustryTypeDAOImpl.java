package com.teklink.sod.dao.impl.client;

import java.util.List;

import org.hibernate.criterion.DetachedCriteria;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.orm.hibernate5.HibernateTemplate;
import org.springframework.stereotype.Repository;

import com.teklink.sod.dao.client.ClientIndustryTypeDAO;
import com.teklink.sod.model.client.ClientIndustryType;

@Repository
public class ClientIndustryTypeDAOImpl implements ClientIndustryTypeDAO {

	@Autowired
	private HibernateTemplate hibernateTemplate;

	public HibernateTemplate getHibernateTemplate() {
		return hibernateTemplate;
	}

	public void setHibernateTemplate(HibernateTemplate hibernateTemplate) {
		this.hibernateTemplate = hibernateTemplate;
	}

	 @SuppressWarnings("unchecked")
		@Override
		public List<ClientIndustryType> getIndustryTypes() {
			DetachedCriteria detachedCriteria = DetachedCriteria.forClass(ClientIndustryType.class);
			  List<ClientIndustryType> IndustryTypeList = (List<ClientIndustryType>)
			  getHibernateTemplate().findByCriteria(detachedCriteria);
			   if (IndustryTypeList != null && IndustryTypeList.size() > 0) 
				   return IndustryTypeList;
			return null;
		}

	
}
