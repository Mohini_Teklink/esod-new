package com.teklink.sod.dao.impl.client;


/**
 * @author Prateek Raj
 */

import java.util.List;

import org.hibernate.Session;
import org.hibernate.Transaction;
import org.hibernate.criterion.DetachedCriteria;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.orm.hibernate5.HibernateTemplate;
import org.springframework.stereotype.Repository;

import com.teklink.sod.dao.client.ClientPacksDAO;
import com.teklink.sod.model.client.ClientPacks;


@Repository
public class ClientPacksDAOImpl implements ClientPacksDAO{

	
	@Autowired
	private HibernateTemplate hibernateTemplate;
	
	public HibernateTemplate getHibernateTemplate() {
		return hibernateTemplate;
	}

	public void setHibernateTemplate(HibernateTemplate hibernateTemplate) {
		this.hibernateTemplate = hibernateTemplate;
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<ClientPacks> getListOfPacks() {
		DetachedCriteria detachedCriteria = DetachedCriteria.forClass(ClientPacks.class);
		List<ClientPacks> list = (List<ClientPacks>) getHibernateTemplate().findByCriteria(detachedCriteria);
		if(list.size()>0 && list!=null) return list;
		return null;
	}

	@SuppressWarnings("unchecked")
	@Override
	public ClientPacks getPackById(int id) {
		DetachedCriteria detachedCriteria = DetachedCriteria.forClass(ClientPacks.class);
		detachedCriteria.add(Restrictions.eq("id",id));
		List<ClientPacks> list = (List<ClientPacks>) getHibernateTemplate().findByCriteria(detachedCriteria);
		if(list.size()>0 && list!=null) return list.get(0);
		return null;
	}

	@Override
	public boolean updatePack(ClientPacks clientPacks) {
		try {
			Session session = getHibernateTemplate().getSessionFactory().openSession();
			Transaction transaction = session.beginTransaction();
			session.saveOrUpdate(clientPacks);
			transaction.commit();
			session.close();
			return true;
		} catch (Exception ex) {
			ex.getStackTrace();
		}
		return false;
		
	}

}
