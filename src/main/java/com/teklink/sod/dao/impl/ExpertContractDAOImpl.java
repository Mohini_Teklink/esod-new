package com.teklink.sod.dao.impl;

import java.util.List;

import org.hibernate.criterion.DetachedCriteria;
import org.hibernate.criterion.Restrictions;

/**
 * @author Prateek Raj
 */

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.orm.hibernate5.HibernateTemplate;
import org.springframework.stereotype.Repository;

import com.teklink.sod.dao.ExpertContractDAO;
import com.teklink.sod.model.expert.ExpertContractManagement;

@Repository
public class ExpertContractDAOImpl implements ExpertContractDAO {

	@Autowired
	private HibernateTemplate hibernateTemplate;

	public HibernateTemplate getHibernateTemplate() {
		return hibernateTemplate;
	}

	public void setHibernateTemplate(HibernateTemplate hibernateTemplate) {
		this.hibernateTemplate = hibernateTemplate;
	}

	@Override
	public boolean saveContract(ExpertContractManagement expertContractManagement) {
		int id = (Integer) getHibernateTemplate().save(expertContractManagement);
		if (id > 0)
			return true;
		return false;
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<ExpertContractManagement> getAllExpertContract() {
		DetachedCriteria detachedCriteria = DetachedCriteria.forClass(ExpertContractManagement.class);
		List<ExpertContractManagement> list = (List<ExpertContractManagement>) getHibernateTemplate()
				.findByCriteria(detachedCriteria);
		if (list.size() > 0 && list != null)
			return list;
		return null;
	}

	@SuppressWarnings("unchecked")
	@Override
	public ExpertContractManagement getExpertById(int id) {
		DetachedCriteria detachedCriteria = DetachedCriteria.forClass(ExpertContractManagement.class);
		detachedCriteria.add(Restrictions.eq("id", id));
		List<ExpertContractManagement> findByCriteria = (List<ExpertContractManagement>) getHibernateTemplate()
				.findByCriteria(detachedCriteria);
		if (findByCriteria != null && findByCriteria.size() > 0)
			return findByCriteria.get(0);
		else
			return null;
	}

}
