package com.teklink.sod.dao.impl;


/**
 * @author Prateek Raj
 */

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.orm.hibernate5.HibernateTemplate;
import org.springframework.stereotype.Repository;

import com.teklink.sod.dao.PaymentDAO;
import com.teklink.sod.model.Payment;

@Repository
public class PaymentDAOImpl implements PaymentDAO{

	@Autowired
	private HibernateTemplate hibernateTemplate;

	public HibernateTemplate getHibernateTemplate() {
		return hibernateTemplate;
	}

	public void setHibernateTemplate(HibernateTemplate hibernateTemplate) {
		this.hibernateTemplate = hibernateTemplate;
	}

	@Override
	public boolean savePaymentEntries(Payment payment) {
		int id =(Integer) getHibernateTemplate().save(payment);
		if(id>0)return true;
		return false;
	}
	
	
}
