package com.teklink.sod.dao.impl;


/**
 * @author Prateek Raj
 */

import java.util.List;

import javax.persistence.Query;

import org.hibernate.Session;
import org.hibernate.Transaction;
import org.hibernate.criterion.DetachedCriteria;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.orm.hibernate5.HibernateTemplate;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Repository;

import com.teklink.sod.dao.ExpertDAO;
import com.teklink.sod.model.expert.Expert;

@Repository
public class ExpertDAOImpl implements ExpertDAO {

	@Autowired
	private HibernateTemplate hibernateTemplate;

	public HibernateTemplate getHibernateTemplate() {
		return hibernateTemplate;
	}

	public void setHibernateTemplate(HibernateTemplate hibernateTemplate) {
		this.hibernateTemplate = hibernateTemplate;
	}

	@Override
	public boolean saveExpert(Expert expert) {
		BCryptPasswordEncoder bCryptPasswordEncoder = new BCryptPasswordEncoder();
		String encodedPass = bCryptPasswordEncoder.encode(expert.getPassword());
		expert.setPassword(encodedPass);
		int id = (Integer) hibernateTemplate.save(expert);
		if (id > 0)
			return true;
		return false;
	}

	@SuppressWarnings("unchecked")
	@Override
	public Expert getExpertByEmailAndPassword(String email, String password) {
		DetachedCriteria detachedCriteria = DetachedCriteria.forClass(Expert.class);
		detachedCriteria.add(Restrictions.eq("email", email));
		detachedCriteria.add(Restrictions.eq("password", password));
		List<Expert> findByCriteria = (List<Expert>) hibernateTemplate.findByCriteria(detachedCriteria);
		if (findByCriteria != null && findByCriteria.size() > 0)
			return findByCriteria.get(0);
		else
			return null;
	}

	@SuppressWarnings("unchecked")
	@Override
	public Expert byEmail(String email) {
		DetachedCriteria detachedCriteria = DetachedCriteria.forClass(Expert.class);
		detachedCriteria.add(Restrictions.eq("email", email));
		List<Expert> findByCriteria = (List<Expert>) hibernateTemplate.findByCriteria(detachedCriteria);
		if (findByCriteria != null && findByCriteria.size() > 0)
			return findByCriteria.get(0);
		else
			return null;

	}

	@SuppressWarnings("unchecked")
	@Override
	public List<Expert> getAllExpert() {
		DetachedCriteria detachedCriteria = DetachedCriteria.forClass(Expert.class);
		List<Expert> list = (List<Expert>) getHibernateTemplate().findByCriteria(detachedCriteria);
		if (list.size() > 0 && list != null)
			return list;
		return null;
	}

	@SuppressWarnings("unchecked")
	@Override
	public Expert byId(int id) {
		DetachedCriteria detachedCriteria = DetachedCriteria.forClass(Expert.class);
		detachedCriteria.add(Restrictions.eq("id", id));
		List<Expert> findByCriteria = (List<Expert>) getHibernateTemplate().findByCriteria(detachedCriteria);
		if (findByCriteria != null && findByCriteria.size() > 0)
			return findByCriteria.get(0);
		else
			return null;
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<Expert> byClientId(int clientId) {
		
		  DetachedCriteria detachedCriteria = DetachedCriteria.forClass(Expert.class);
		  detachedCriteria.add(Restrictions.eq("clientId", clientId));
		  List<Expert> findByCriteria = (List<Expert>) getHibernateTemplate().findByCriteria(detachedCriteria);
		
		  if (findByCriteria != null && findByCriteria.size() > 0) 
			  return findByCriteria;
		  else
			return null;
	}
	@Override
	public boolean updateById(int id, boolean status ,int clientId) {
		boolean isUpdated = false;
		
		try {
			Session session = getHibernateTemplate().getSessionFactory().openSession();
			Transaction tx = session.beginTransaction();
			Query query = session.createQuery("UPDATE Expert SET status = :status , clientId =:clientId WHERE id =:id");
			query.setParameter("status", status);
			query.setParameter("id", id);
			query.setParameter("clientId", clientId);
			query.executeUpdate();
			tx.commit();
			session.close();
			isUpdated = true;
		} catch (Exception exp) {
			exp.printStackTrace();
		}
		return isUpdated;
	}

	@Override
	public boolean updateExpert(Expert expert) {
		try {
			Session session = getHibernateTemplate().getSessionFactory().openSession();
			Transaction transaction = session.beginTransaction();
			session.saveOrUpdate(expert);
			transaction.commit();
			session.close();
		    return true;
		    
    } catch (Exception e) {
    	e.printStackTrace();
    }
    return false;
	}
	

}
