package com.teklink.sod.dao.impl.client;

import java.util.List;

import javax.persistence.Query;

import org.hibernate.Session;
import org.hibernate.Transaction;
import org.hibernate.criterion.DetachedCriteria;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.orm.hibernate5.HibernateTemplate;
import org.springframework.stereotype.Repository;

import com.teklink.sod.dao.client.ClientProductsDAO;
import com.teklink.sod.model.client.ClientProducts;

@Repository
public class ClientProductsDAOImpl implements ClientProductsDAO {

	@Autowired
	private HibernateTemplate hibernateTemplate;

	public HibernateTemplate getHibernateTemplate() {
		return hibernateTemplate;
	}

	public void setHibernateTemplate(HibernateTemplate hibernateTemplate) {
		this.hibernateTemplate = hibernateTemplate;
	}

	@Override
	public boolean saveProduct(ClientProducts clientProducts) {
		int id = (Integer) hibernateTemplate.save(clientProducts);
		if (id > 0)
			return true;
		return false;
	}

	@Override
	public boolean updateProduct(ClientProducts clientProducts) {
		try {
			Session session = getHibernateTemplate().getSessionFactory().openSession();
			Transaction transaction = session.beginTransaction();
			session.saveOrUpdate(clientProducts);
			transaction.commit();
			session.close();
			return true;
		} catch (Exception ex) {
			ex.getStackTrace();
		}
		return false;
	}

	@SuppressWarnings("unchecked")
	@Override
	public ClientProducts getProductDetailbyprodId(int prodId) {
		DetachedCriteria detachedCriteria = DetachedCriteria.forClass(ClientProducts.class);
		detachedCriteria.add(Restrictions.eq("prodId", prodId));
		List<ClientProducts> findByCriteria = (List<ClientProducts>) hibernateTemplate.findByCriteria(detachedCriteria);
		if (findByCriteria != null && findByCriteria.size() > 0)
			return findByCriteria.get(0);
		else
			return null;
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<ClientProducts> getProductDetailbyProdType(int prodType) {
		DetachedCriteria detachedCriteria = DetachedCriteria.forClass(ClientProducts.class);
		detachedCriteria.add(Restrictions.eq("prodType", prodType));
		List<ClientProducts> findByCriteria = (List<ClientProducts>) hibernateTemplate.findByCriteria(detachedCriteria);
		if (findByCriteria != null && findByCriteria.size() > 0)
			return findByCriteria;
		else
			return null;
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<ClientProducts> getAllProductDetail() {
		DetachedCriteria detachedCriteria = DetachedCriteria.forClass(ClientProducts.class);
		List<ClientProducts> findByCriteria = (List<ClientProducts>) hibernateTemplate.findByCriteria(detachedCriteria);
		if (findByCriteria != null && findByCriteria.size() > 0)
			return findByCriteria;
		else
			return null;
	}

	@Override
	public boolean updateStatusbyId(int id, boolean statusId) {
		try {
			Session session=getHibernateTemplate().getSessionFactory().openSession();
			Transaction transaction=session.beginTransaction();
			Query query=session.createQuery("update ClientProducts set status=:status where prodId=:prodId");
			query.setParameter("prodId", id);
			query.setParameter("status", statusId);
			query.executeUpdate();
			transaction.commit();
			session.close();
			return true;
		}catch(Exception exp) {
			exp.printStackTrace();
		}
		return false;
	}

	@SuppressWarnings("unchecked")
	@Override
	public ClientProducts getProductDetailByIdAndStatus(int prodId, boolean status) {
		DetachedCriteria detachedCriteria = DetachedCriteria.forClass(ClientProducts.class);
		detachedCriteria.add(Restrictions.eq("prodId", prodId));
		detachedCriteria.add(Restrictions.eq("status", status));
		List<ClientProducts> findByCriteria = (List<ClientProducts>) hibernateTemplate.findByCriteria(detachedCriteria);
		if (findByCriteria != null && findByCriteria.size() > 0)
			return findByCriteria.get(0);
		else
			return null;
	}


}
