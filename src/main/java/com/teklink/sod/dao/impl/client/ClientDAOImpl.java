package com.teklink.sod.dao.impl.client;


/**
 * @author Prateek Raj
 */

import java.util.List;

import javax.persistence.Query;

import org.hibernate.Session;
import org.hibernate.Transaction;
import org.hibernate.criterion.DetachedCriteria;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.orm.hibernate5.HibernateTemplate;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Repository;

import com.teklink.sod.dao.client.ClientDAO;
import com.teklink.sod.model.client.Client;
import com.teklink.sod.model.client.ClientPacks;
import com.teklink.sod.model.expert.Expert;

@Repository
public class ClientDAOImpl implements ClientDAO {

	@Autowired
	private HibernateTemplate hibernateTemplate;

	public HibernateTemplate getHibernateTemplate() {
		return hibernateTemplate;
	}

	public void setHibernateTemplate(HibernateTemplate hibernateTemplate) {
		this.hibernateTemplate = hibernateTemplate;
	}

	@Override
	public boolean saveClient(Client client) {
		BCryptPasswordEncoder newPass = new BCryptPasswordEncoder();
		client.setClient_password(newPass.encode(client.getClient_password()));
		int id = (Integer) hibernateTemplate.save(client);
		if (id > 0)
			return true;
		return false;
	}

	@SuppressWarnings("unchecked")
	@Override
	public Client getClientDetailsByEmailAndPassword(String email, String password) {
		DetachedCriteria detachedCriteria = DetachedCriteria.forClass(Client.class);
		detachedCriteria.add(Restrictions.eq("client_email", email));
		detachedCriteria.add(Restrictions.eq("client_password", password));
		List<Client> findByCriteria = (List<Client>) hibernateTemplate.findByCriteria(detachedCriteria);
		if (findByCriteria != null && findByCriteria.size() > 0)
			return findByCriteria.get(0);
		else
			return null;
	}

	@SuppressWarnings("unchecked")
	@Override
	public Client byEmail(String email) {
		DetachedCriteria detachedCriteria = DetachedCriteria.forClass(Client.class);
		detachedCriteria.add(Restrictions.eq("client_email", email));
		List<Client> findByCriteria = (List<Client>) hibernateTemplate.findByCriteria(detachedCriteria);
		if (findByCriteria != null && findByCriteria.size() > 0)
			return findByCriteria.get(0);
		else
			return null;
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<ClientPacks> getPackName() {
		DetachedCriteria detachedCriteria = DetachedCriteria.forClass(ClientPacks.class);
		List<ClientPacks> list = (List<ClientPacks>) getHibernateTemplate().findByCriteria(detachedCriteria);
		if (list != null && list.size() > 0)
			return list;
		else
			return null;
	}

	/*@SuppressWarnings("unchecked")
	@Override
	public Client byId(int id) {
		DetachedCriteria detachedCriteria = DetachedCriteria.forClass(Client.class);
		detachedCriteria.add(Restrictions.eq("id", id));
		List<Client> findByCriteria = (List<Client>) hibernateTemplate.findByCriteria(detachedCriteria);
		if (findByCriteria != null && findByCriteria.size() > 0)
			return findByCriteria.get(0);
		else
			return null;
	}*/

	@Override
	public boolean updateClient(Client client) {
		try {
			Session session = getHibernateTemplate().getSessionFactory().openSession();
			Transaction transaction = session.beginTransaction();
			session.saveOrUpdate(client);
			transaction.commit();
			session.close();
		    return true;
		    
    } catch (Exception e) {
    	e.printStackTrace();
    }
    return false;
	}

	@Override
	public List<Client> getClientList() {
		DetachedCriteria detachedCriteria = DetachedCriteria.forClass(Client.class);
		@SuppressWarnings("unchecked")
		List<Client> clientList = (List<Client>) getHibernateTemplate().findByCriteria(detachedCriteria);
		return clientList;
	}

	@Override
	public Boolean updateById(int id, Boolean status) {
		// TODO Auto-generated method stub
             Boolean isUpdated = false;
		
		try {
			Session session = getHibernateTemplate().getSessionFactory().openSession();
			Transaction tx = session.beginTransaction();
			Query query = session.createQuery("UPDATE Expert SET status = :status WHERE id =:id");
			query.setParameter("status", status);
			query.setParameter("id", id);
			query.executeUpdate();
			tx.commit();
			session.close();
			isUpdated = true;
		} catch (Exception exp) {
			exp.printStackTrace();
		}
		return isUpdated;


	}

	@Override
	public Client byId(int id) {
		// TODO Auto-generated method stub
		
		DetachedCriteria detachedCriteria = DetachedCriteria.forClass(Client.class);
		detachedCriteria.add(Restrictions.eq("id", id));
		@SuppressWarnings("unchecked")
		List<Client> findByCriteria = (List<Client>) getHibernateTemplate().findByCriteria(detachedCriteria);
		if (findByCriteria != null && findByCriteria.size() > 0)
			return findByCriteria.get(0);
		else
			return null;
	}

}
