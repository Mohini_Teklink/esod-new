package com.teklink.sod.dao.impl.client;

import java.sql.Timestamp;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import org.hibernate.Session;
import org.hibernate.Transaction;
import org.hibernate.criterion.DetachedCriteria;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.orm.hibernate5.HibernateTemplate;
import org.springframework.stereotype.Repository;

import com.teklink.sod.dao.client.ClientForgotPasswordDAO;
import com.teklink.sod.model.client.Client;
import com.teklink.sod.model.client.ClientForgotPasswordToken;

@Repository
public class ClientForgotPasswordDAOImpl implements ClientForgotPasswordDAO {

	@Autowired
	private HibernateTemplate hibernateTemplate;

	public HibernateTemplate getHibernateTemplate() {
		return hibernateTemplate;
	}

	public void setHibernateTemplate(HibernateTemplate hibernateTemplate) {
		this.hibernateTemplate = hibernateTemplate;
	}
	
	private static final int EXPIRATION = 60*24;
	
	@SuppressWarnings("unchecked")
	@Override
	public Client getVerifiedUser(String email) {
		try {
	          DetachedCriteria criteria = DetachedCriteria.forClass(Client.class);
	          criteria.add(Restrictions.eq("client_email", email));
	          List<Client> recordList = (List<Client>)getHibernateTemplate().findByCriteria(criteria);
	          if (recordList != null && recordList.size() > 0)
	  			return recordList.get(0);
	  		else
	  			return null;
	         
	      } catch (Exception e) {
	          throw e;
	      }
	}

	@Override
	public void createPasswordResetToken(Client client, String token) {
		try {
			  ClientForgotPasswordToken resetToken = new ClientForgotPasswordToken();
	          resetToken.setClient(client);
	          resetToken.setToken(token);
	          resetToken.setExpiryDate(calculateExpiryDate(EXPIRATION));
	          getHibernateTemplate().save(resetToken);
	      } catch (Exception e) {
            e.printStackTrace();
	      }
		
	}
	
	private Date calculateExpiryDate(int expiryTimeInMinutes) {
        Calendar cal = Calendar.getInstance();
        cal.setTime(new Timestamp(cal.getTime().getTime()));
        cal.add(Calendar.MINUTE, expiryTimeInMinutes);
        return new Date(cal.getTime().getTime());
    }

	@SuppressWarnings("unchecked")
	@Override
	public ClientForgotPasswordToken getPasswordResetUser(String token) {
		try {
	          DetachedCriteria criteria = DetachedCriteria.forClass(ClientForgotPasswordToken.class);
	          criteria.add(Restrictions.eq("token", token));
	          List<ClientForgotPasswordToken> recordList = (List<ClientForgotPasswordToken>)getHibernateTemplate().findByCriteria(criteria);
	          if (recordList != null && recordList.size() > 0) {
	  			return recordList.get(0);
	          }
	  		else {
	  			return null;
	  		}
	      } catch (Exception e) {
	    	  e.getStackTrace();
	      }
		return null;
	}

	@Override
	public boolean updateUserPassword(Client passwordDto) {
		 try {
				Session session = getHibernateTemplate().getSessionFactory().openSession();
				Transaction transaction = session.beginTransaction();
				session.saveOrUpdate(passwordDto);
				transaction.commit();
				session.close();
			    return true;
			    
	    } catch (Exception e) {
	    	e.printStackTrace();
	    }
	    return false;
	}

	@Override
	public boolean deletetokenPassword(ClientForgotPasswordToken client) {
		Session session = getHibernateTemplate().getSessionFactory().openSession();
		Transaction transaction = session.beginTransaction();
		session.delete(client);
		transaction.commit();
		session.close();
		return true;
	}

	@SuppressWarnings("unchecked")
	@Override
	public ClientForgotPasswordToken checkClientForgotPassword(Client client) {
		DetachedCriteria criteria = DetachedCriteria.forClass(ClientForgotPasswordToken.class);
        criteria.add(Restrictions.eq("client", client));
        List<ClientForgotPasswordToken> recordList = (List<ClientForgotPasswordToken>)getHibernateTemplate().findByCriteria(criteria);
	       if (recordList != null && recordList.size() > 0) {
	  			return recordList.get(0);
	          }
		return null;
	}

	@Override
	public boolean updatePasswordResetToken(ClientForgotPasswordToken clientForgotPasswordToken, String token) {
		clientForgotPasswordToken.setToken(token);
		clientForgotPasswordToken.setExpiryDate(calculateExpiryDate(EXPIRATION));
		Session session = getHibernateTemplate().getSessionFactory().openSession();
		Transaction transaction = session.beginTransaction();
		session.saveOrUpdate(clientForgotPasswordToken);
		transaction.commit();
		session.close();
		return true;
	}

}
