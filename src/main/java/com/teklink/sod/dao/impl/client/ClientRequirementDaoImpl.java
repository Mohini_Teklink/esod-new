package com.teklink.sod.dao.impl.client;

import java.util.List;

import org.hibernate.criterion.DetachedCriteria;
import org.hibernate.criterion.Order;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.orm.hibernate5.HibernateTemplate;
import org.springframework.stereotype.Repository;

import com.teklink.sod.dao.client.ClientRequirementDao;
import com.teklink.sod.model.client.ClientRequirement;

@Repository
public class ClientRequirementDaoImpl implements ClientRequirementDao {

	@Autowired
	HibernateTemplate hibernateTemplate;

	public HibernateTemplate getHibernateTemplate() {
		return hibernateTemplate;
	}

	public void setHibernateTemplate(HibernateTemplate hibernateTemplate) {
		this.hibernateTemplate = hibernateTemplate;
	}	

	@Override
	public Boolean saveClientRequirement(ClientRequirement clientRequirement) {
		Object obj =getHibernateTemplate().save(clientRequirement);
		Integer id =(Integer) obj;
		if(id > 0) {
			return true;
		}
		
		return false;
	}
	
	@SuppressWarnings("unchecked")
	@Override
	public List<ClientRequirement> getClientList() {
		DetachedCriteria detachedCriteria = DetachedCriteria.forClass(ClientRequirement.class);
		detachedCriteria.addOrder(Order.desc("id"));
		List<ClientRequirement> findClientRequirementsByDESC = (List<ClientRequirement>) hibernateTemplate.findByCriteria(detachedCriteria);
		if (findClientRequirementsByDESC != null && findClientRequirementsByDESC.size() > 0)
			return findClientRequirementsByDESC;
		else
			return null;
	}
}
