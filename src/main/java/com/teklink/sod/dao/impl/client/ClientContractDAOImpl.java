package com.teklink.sod.dao.impl.client;


/**
 * @author Prateek Raj
 */

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.orm.hibernate5.HibernateTemplate;
import org.springframework.stereotype.Repository;

import com.teklink.sod.dao.client.ClientContractDAO;
import com.teklink.sod.model.client.ClientContract;


@Repository("")
public class ClientContractDAOImpl implements ClientContractDAO{
	
	@Autowired
	private HibernateTemplate hibernateTemplate;

	public HibernateTemplate getHibernateTemplate() {
		return hibernateTemplate;
	}

	public void setHibernateTemplate(HibernateTemplate hibernateTemplate) {
		this.hibernateTemplate = hibernateTemplate;
	}

	@Override
	public boolean saveContract(ClientContract clientContract) {
		int id =(Integer) getHibernateTemplate().save(clientContract);
		if(id>0)return true;
		return false;
	}
	
	
	
}
