package com.teklink.sod.dao.impl;

import java.sql.Timestamp;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import org.hibernate.Session;
import org.hibernate.Transaction;
import org.hibernate.criterion.DetachedCriteria;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.orm.hibernate5.HibernateTemplate;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.teklink.sod.dao.ExpertForgotPasswordDAO;
import com.teklink.sod.model.expert.Expert;
import com.teklink.sod.model.expert.ExpertForgotPasswordToken;

@Repository
public class ExpertForgotPasswordDAOImpl implements ExpertForgotPasswordDAO {
	
	@Autowired
	private HibernateTemplate hibernateTemplate;

	public HibernateTemplate getHibernateTemplate() {
		return hibernateTemplate;
	}

	public void setHibernateTemplate(HibernateTemplate hibernateTemplate) {
		this.hibernateTemplate = hibernateTemplate;
	}
	
	private static final int EXPIRATION = 60*24;

	@SuppressWarnings("unchecked")
	@Override
	public Expert getVerifiedUser(String email) {
		try {
	          DetachedCriteria criteria = DetachedCriteria.forClass(Expert.class);
	          criteria.add(Restrictions.eq("email", email));
	          List<Expert> recordList = (List<Expert>) 
	                      getHibernateTemplate().findByCriteria(criteria);
	          if (recordList != null && recordList.size() > 0)
	  			return recordList.get(0);
	  		else
	  			return null;
	         
	      } catch (Exception e) {
	          throw e;
	      }
	}

	@Override
	 @Transactional(readOnly = false)
	public void createPasswordResetToken(Expert expert, String token) {
		 try {
	          ExpertForgotPasswordToken resetToken = new ExpertForgotPasswordToken();
	          resetToken.setExpert(expert);
	          resetToken.setToken(token);
	          resetToken.setExpiryDate(calculateExpiryDate(EXPIRATION));
	          getHibernateTemplate().save(resetToken);
	      } catch (Exception e) {
              e.printStackTrace();
	      }
		
	}
	
	private Date calculateExpiryDate(int expiryTimeInMinutes) {
        Calendar cal = Calendar.getInstance();
        cal.setTime(new Timestamp(cal.getTime().getTime()));
        cal.add(Calendar.MINUTE, expiryTimeInMinutes);
        return new Date(cal.getTime().getTime());
    }

	@SuppressWarnings("unchecked")
	@Override
	public ExpertForgotPasswordToken getPasswordResetUser(String token) {
		try {
	          DetachedCriteria criteria = DetachedCriteria.forClass(ExpertForgotPasswordToken.class);
	          criteria.add(Restrictions.eq("token", token));
	          List<ExpertForgotPasswordToken> recordList = (List<ExpertForgotPasswordToken>)getHibernateTemplate().findByCriteria(criteria);
	          if (recordList != null && recordList.size() > 0) {
	  			return recordList.get(0);
	          }
	  		else {
	  			return null;
	  		}
	      } catch (Exception e) {
	    	  e.getStackTrace();
	      }
		return null;
	}
	
	@Transactional
	@Override
	public boolean updateUserPassword(Expert passwordDto) {
	    try {
				Session session = getHibernateTemplate().getSessionFactory().openSession();
				Transaction transaction = session.beginTransaction();
				session.saveOrUpdate(passwordDto);
				transaction.commit();
				session.close();
			    return true;
			    
	    } catch (Exception e) {
	    	e.printStackTrace();
	    }
	    return false;
	}

	@SuppressWarnings("unchecked")
	@Override
	public ExpertForgotPasswordToken checkExpertForgotPassword(Expert expert) {
		 DetachedCriteria criteria = DetachedCriteria.forClass(ExpertForgotPasswordToken.class);
         criteria.add(Restrictions.eq("expert", expert));
         List<ExpertForgotPasswordToken> recordList = (List<ExpertForgotPasswordToken>)getHibernateTemplate().findByCriteria(criteria);
	       if (recordList != null && recordList.size() > 0) {
	  			return recordList.get(0);
	          }
		return null;
	}

	@Override
	public boolean updatePasswordResetToken(ExpertForgotPasswordToken expertPasswordResetToken , String token) {
		expertPasswordResetToken.setToken(token);
         expertPasswordResetToken.setExpiryDate(calculateExpiryDate(EXPIRATION));
		Session session = getHibernateTemplate().getSessionFactory().openSession();
		Transaction transaction = session.beginTransaction();
		session.saveOrUpdate(expertPasswordResetToken);
		transaction.commit();
		session.close();
		return true;
	}

	@Override
	public boolean deletetokenPassword(ExpertForgotPasswordToken expert) {
		Session session = getHibernateTemplate().getSessionFactory().openSession();
		Transaction transaction = session.beginTransaction();
		session.delete(expert);
		transaction.commit();
		session.close();
		return true;
	}

	
}
