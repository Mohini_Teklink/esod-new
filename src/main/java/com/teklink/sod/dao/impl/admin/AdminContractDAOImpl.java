package com.teklink.sod.dao.impl.admin;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.orm.hibernate5.HibernateTemplate;
import org.springframework.stereotype.Repository;

import com.teklink.sod.dao.admin.AdminContractDAO;
import com.teklink.sod.model.admin.AdminContract;

/**
 * @author Shikha Rohit
 *
 */

@Repository("")
public class AdminContractDAOImpl implements AdminContractDAO {
	
	@Autowired
	private HibernateTemplate hibernateTemplate;

	public HibernateTemplate getHibernateTemplate() {
		return hibernateTemplate;
	}

	public void setHibernateTemplate(HibernateTemplate hibernateTemplate) {
		this.hibernateTemplate = hibernateTemplate;
	} 
	
	@Override

	public boolean saveContract(AdminContract adminContract) {
		int id =(Integer) getHibernateTemplate().save(adminContract);
		if(id>0)return true;
		return false;
	
			}

}
