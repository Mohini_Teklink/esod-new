package com.teklink.sod.dao;


import java.util.List;

/**
 * @author Prateek Raj
 */

import com.teklink.sod.model.expert.ExpertContractManagement;

public interface ExpertContractDAO {

	public abstract boolean saveContract(ExpertContractManagement expertContractManagement);
	public List<ExpertContractManagement> getAllExpertContract();
	public abstract ExpertContractManagement getExpertById(int id);

	
}
