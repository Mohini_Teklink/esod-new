package com.teklink.sod.dao;


/**
 * @author Prateek Raj
 */

import java.util.List;

import com.teklink.sod.model.expert.Expert;

public interface ExpertDAO {
	public abstract boolean saveExpert(Expert expert);
	public Expert getExpertByEmailAndPassword(String email, String password);
	public Expert byEmail(String email);
	public List<Expert> getAllExpert();
	public Expert byId(int id);
	public List<Expert> byClientId(int id) ;
	public boolean updateById(int id,boolean status, int clientId);
	public abstract boolean updateExpert(Expert expert);
	
}
