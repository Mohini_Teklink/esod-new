package com.teklink.sod.dao;


/**
 * @author Prateek Raj
 */

import com.teklink.sod.model.user.User;

public interface UserDAO {
	public abstract boolean saveUser(User expert);
	public User getUserByUserNameAndPassword(String userName, String password);
	public User getUserByUserName(String username);
	public boolean checkUserExitByEmail(String email);
	public boolean checkUserExitByUsername(String username);
	public boolean checkUserExitByEmailAndUserName(String email, String username);
	public abstract boolean updateUser(User user);
}
