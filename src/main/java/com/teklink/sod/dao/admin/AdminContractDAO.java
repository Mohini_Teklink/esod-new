package com.teklink.sod.dao.admin;

import com.teklink.sod.model.admin.AdminContract;

/**
 * @author Shikha Rohit
 *
 */
public interface AdminContractDAO {

	public abstract boolean saveContract(AdminContract adminContract);
	
	
}
