package com.teklink.sod.handler;


/**
 * @author Prateek Raj
 */

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.binding.message.MessageBuilder;
import org.springframework.binding.message.MessageContext;
import org.springframework.stereotype.Component;

import com.teklink.sod.dao.impl.ExpertDAOImpl;
import com.teklink.sod.dao.impl.SkillDAOImpl;
import com.teklink.sod.dao.impl.client.ClientDAOImpl;
import com.teklink.sod.model.RegistrationModel;
import com.teklink.sod.model.Skills;
import com.teklink.sod.model.client.Client;
import com.teklink.sod.model.client.ClientCardinalities;
import com.teklink.sod.model.client.ClientPacks;
import com.teklink.sod.model.expert.Expert;
import com.teklink.sod.model.expert.ExpertCardinalities;

@Component
public class RegistrationHandler {

	@Autowired
	private ClientDAOImpl clientDAOImpl;
	
	@Autowired
	private ExpertDAOImpl expertDAOImpl;

	@Autowired
	private SkillDAOImpl skillDAOImpl;
	
	public RegistrationModel init() {
		return new RegistrationModel();
	}

	public void addClient(RegistrationModel clientRegistrationModel, Client client) {
		clientRegistrationModel.setClient(client);
	}
	
	public String saveClient(RegistrationModel clientRegistrationModel) {
		if (clientDAOImpl.saveClient(clientRegistrationModel.getClient()))
			return "Success";
		else
			return "Failure";
	}
	
	public String validateClient(Client client, MessageContext msgCnt) {
		String transitionValue = "success";
		if (!(client.getClient_password().equals(client.getClient_confirmpassword()))) {
			msgCnt.addMessage(new MessageBuilder()
					.error()
					.source("client_confirmpassword")
					.defaultText("Password doesn't match the confirm password!")
					.build());
			transitionValue = "failure";
		}
		if (clientDAOImpl.byEmail(client.getClient_email())!=null) {
			msgCnt.addMessage(new MessageBuilder()
					.error()
					.source("client_email")
					.defaultText("User already exist!")
					.build());
			transitionValue = "failure";
		}
		return transitionValue;
	}
	
	
	public List<ClientPacks> getPacksNavBar(){
		return clientDAOImpl.getPackName();
	}
	
	public List<Skills> getAllSkillList(){
		return skillDAOImpl.getSkillList();
	}
	
	public void addExpert(RegistrationModel registrationModel, Expert expert) {
		registrationModel.setExpert(expert);;
	}
	
	public String saveExpert(RegistrationModel registrationModel) {
		if (expertDAOImpl.saveExpert(registrationModel.getExpert()))
			return "Success";
		else
			return "Failure";
	}
	
	
	
	public String validateExpert(Expert expert, MessageContext msgCnt) {
		String transitionValue = "success";
		if (!(expert.getPassword().equals(expert.getConfirmPassword()))) {
			msgCnt.addMessage(new MessageBuilder()
					.error()
					.source("confirmPassword")
					.defaultText("Password doesn't match the confirm password!")
					.build());
			transitionValue = "failure";
		}
		if (expertDAOImpl.byEmail(expert.getEmail())!=null) {
			msgCnt.addMessage(new MessageBuilder()
					.error()
					.source("email")
					.defaultText("User already exist!")
					.build());
			transitionValue = "failure";
		}
		
		return transitionValue;
	}
	
	public String validateClientLogin(ClientCardinalities clientCardinalities, MessageContext msgCnt) {
		if(clientDAOImpl.getClientDetailsByEmailAndPassword(clientCardinalities.getClient_email(), clientCardinalities.getClient_password())==null) {
			msgCnt.addMessage(new MessageBuilder()
					.error()
					.source("client_password")
					.defaultText("Please enter the valid email & password!")
					.build());
			return "failure";
		}
		return "success";
	}
	
	public String validateExpertLogin(ExpertCardinalities expertCardinalities, MessageContext msgCnt) {
		if(expertDAOImpl.getExpertByEmailAndPassword(expertCardinalities.getExpert_email(), expertCardinalities.getExpert_password())==null) {
			msgCnt.addMessage(new MessageBuilder()
					.error()
					.source("expert_password")
					.defaultText("Please enter the valid email & password!")
					.build());
			return "failure";
		}
		return "success";
	}
}
