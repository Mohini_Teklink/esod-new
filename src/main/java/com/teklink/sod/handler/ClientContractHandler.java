package com.teklink.sod.handler;

import java.io.File;
import java.text.SimpleDateFormat;
import java.util.Date;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.binding.message.MessageBuilder;
import org.springframework.binding.message.MessageContext;
import org.springframework.expression.spel.CodeFlow.ClinitAdder;
import org.springframework.stereotype.Component;
import org.springframework.web.multipart.MultipartFile;

import com.teklink.sod.controller.GlobalController;
import com.teklink.sod.model.Mailler;
import com.teklink.sod.model.SaveAttachedFile;
import com.teklink.sod.model.client.Client;
import com.teklink.sod.model.client.ClientContract;
import com.teklink.sod.service.PaymentService;
import com.teklink.sod.service.client.ClientContractService;
import com.teklink.sod.service.client.ClientPacksService;
import com.teklink.sod.service.client.ClientProductsService;
import com.teklink.sod.service.client.ClientService;

@Component
public class ClientContractHandler {

	@Autowired
	private ClientService clientService;

	@Autowired
	private ClientProductsService clientProductService;

	@Autowired
	private ClientContractService clientContractService;

	@Autowired
	private ClientPacksService clientPacksService;

	@Autowired
	private PaymentService paymentService;
	
	public PaymentService getPaymentService() {
		return paymentService;
	}

	public void setPaymentService(PaymentService paymentService) {
		this.paymentService = paymentService;
	}

	public ClientPacksService getClientPacksService() {
		return clientPacksService;
	}

	public void setClientPacksService(ClientPacksService clientPacksService) {
		this.clientPacksService = clientPacksService;
	}

	public ClientService getClientService() {
		return clientService;
	}

	public void setClientService(ClientService clientService) {
		this.clientService = clientService;
	}

	public ClientProductsService getClientProductService() {
		return clientProductService;
	}

	public void setClientProductService(ClientProductsService clientProductService) {
		this.clientProductService = clientProductService;
	}

	public ClientContractService getClientContractService() {
		return clientContractService;
	}

	public void setClientContractService(ClientContractService clientContractService) {
		this.clientContractService = clientContractService;
	}

	/*
	 * returnLoginedClient returns the Client Object of the current client login.
	 * */
	public Client returnLoginedClient() {
		//GlobalController gb = new GlobalController();
		return GlobalController.getClientDetails();
	}

	/*
	 * returnClientPack returns the clientPacks object which is selected by the client.
	 * */
	/*public ClientPacks returnClientPack(int id) {
		*return getClientPacksService().getPackById(id);
	}* */

	/*
	 * validateContractForm is used to validate the contract form attach file is empty or not.
	 * */
//	public String validateContractForm(ClientContract clientContractModel, MultipartFile inputImageFile,
//			MultipartFile inputsignFile, MessageContext msgCnt) {
//		String state = "failure";
//		if (inputImageFile.isEmpty() && inputsignFile.isEmpty()) {
//			state = "success";
//		} else {
//			msgCnt.addMessage(new MessageBuilder().error().defaultText("Please Upload Files!!").build());
//		}
//		return state;
//	}
	
	/*
	 * saveContractForm method which is used to save the contract form details in the database and Image Uploads on the server.
	 * */
	public String saveContractForm(ClientContract clientContractModel, MultipartFile inputImageFile,MultipartFile inputSignFile,MessageContext msgCnt) {
		try {
			System.out.println(clientContractModel);
			String rootPath = System.getProperty("catalina.home");
			File dir = new File(rootPath + File.separator + "SOD" + File.separator + "Client-Doc" + File.separator
					+ clientContractModel.getContractNum() + "-" + clientContractModel.getName() + "-"
					+ new SimpleDateFormat("yyyy.MM.dd.HH.mm.ss").format(new Date()) + File.separator);
			if (!dir.exists())
				dir.mkdirs();
			String phtPath = new SaveAttachedFile(inputImageFile, "img", dir).uploadFile();
			String signPath = new SaveAttachedFile(inputSignFile, "sign", dir).uploadFile();
			if (phtPath == null && signPath == null) {
			clientContractModel.setImagePath(phtPath);
      		clientContractModel.setSignPath(signPath);
				if (getClientContractService().saveContract(clientContractModel)) {
					System.out.println("success");
					return "success";
				}
				return "failure";

			}			
		}catch(Exception exp) {
			msgCnt.addMessage(new MessageBuilder().error().defaultText("Please Upload Files!!").build());
		}
		
		return "failure";
	  // }
	/*
	 * returns the new Object of Payment Type to be passed as modelAttribute in the payment view.
	 * */
//	public Payment init() {
//		return new Payment();
//	}
//	/*
//	 * save the payment input and also mail the concern client after transaction
//	 * */
//	public String savePayment(Payment paymentModel,MessageContext messageContext){
//		paymentModel.setDateofTransaction(new Date());
//		if (getPaymentService().savePaymentEntries(paymentModel)) {
//			Mailler mailler = new Mailler();
//			mailler.setTO("r.prateek11@gmail.com");
//			mailler.setMessage("Thank you for your Order\nRegards\nTeam Teklink");
//			if (mailler.sendMail()) {
//				return "success";
//			}
//			
//		}
		//return "failure";
	}
}
