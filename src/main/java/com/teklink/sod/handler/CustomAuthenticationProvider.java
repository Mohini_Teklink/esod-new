package com.teklink.sod.handler;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.AuthenticationProvider;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Component;

import com.teklink.sod.dao.client.ClientDAO;
import com.teklink.sod.model.client.Client;
import com.teklink.sod.service.ExpertService;

@Component("customClientAuthPro")
public class CustomAuthenticationProvider implements AuthenticationProvider{

	@Autowired
	private ClientDAO clientDAO;
	
	public ClientDAO getClientDAO() {
		return clientDAO;
	}

	public void setClientDAO(ClientDAO clientDAO) {
		this.clientDAO = clientDAO;
	}

	@Autowired
	private ExpertService expertService;
	
	public ExpertService getExpertService() {
		return expertService;
	}

	public void setExpertService(ExpertService expertService) {
		this.expertService = expertService;
	}
	@Override
	public Authentication authenticate(Authentication arg0) throws AuthenticationException {
		// It is used for Authentication
		UsernamePasswordAuthenticationToken usernamePasswordAuthenticationToken = (UsernamePasswordAuthenticationToken)arg0;
		Client client = getClientDAO().byEmail(usernamePasswordAuthenticationToken.getName());
		BCryptPasswordEncoder bCryptPasswordEncoder = new BCryptPasswordEncoder();
		
		client.setExpertList(expertService.getAllExpertByClientId(client.getId()));
		if(client ==null || !bCryptPasswordEncoder.matches(usernamePasswordAuthenticationToken.getCredentials().toString(), client.getClient_password())) {
			throw new BadCredentialsException("Invalid Email or password!");
		}
		return new UsernamePasswordAuthenticationToken(client, client.getClient_password(),client.getAuthorities());
	}

	@Override
	public boolean supports(Class<?> arg0) {
		return UsernamePasswordAuthenticationToken.class.equals(arg0);
	}

}
