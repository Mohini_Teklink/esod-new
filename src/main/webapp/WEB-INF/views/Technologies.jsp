<!-- @author Prateek Raj -->

<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@taglib prefix="jstl" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">

<jstl:set var="context" value="${pageContext.request.contextPath}" />
<spring:url var="bootstrapCss"
	value="resources/bootstrap-4.0.0-dist/css" />
<spring:url var="bootstrapJs" value="resources/bootstrap-4.0.0-dist/js" />
<spring:url var="bootstrapJQuery"
	value="resources/bootstrap-4.0.0-dist/jquery" />
<spring:url var="bootstrapFontAwesomeCss"
	value="resources/bootstrap-4.0.0-dist/font-awesome/css" />
<spring:url var="css" value="resources/css" />
<spring:url var="images" value="resources/images" />



<html>
<head>

<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<meta charset="utf-8">
<meta name="viewport"
	content="width=device-width, initial-scale=1, shrink-to-fit=no">
<!--For Fabvicon Icon-->
<link rel="shortcut icon" type="image/png"
	href="${pageContext.request.contextPath}/${images}/favicon.png" />
<link
	href="${pageContext.request.contextPath}/${bootstrapCss}/bootstrap.min.css"
	rel="stylesheet">
<!--For Toggle button Responsive on Mobile-->
<script
	src="${pageContext.request.contextPath}/${bootstrapJQuery}/jquery-3.3.1.min.js"></script>
<!--For Toggle button Responsive on Mobile-->
<script
	src="${pageContext.request.contextPath}/${bootstrapJs}/bootstrap.min.js"></script>
<!--For Fa icon-->
<link
	href="${pageContext.request.contextPath}/${bootstrapFontAwesomeCss}/font-awesome.min.css"
	rel="stylesheet">
<!--Custom Css-->
<link href="${pageContext.request.contextPath}/${css}/style_1.css"
	rel="stylesheet" type="text/css">
<!--Responsive Css-->
<link href="${pageContext.request.contextPath}/${css}/responsive.css"
	rel="stylesheet" type="text/css">

<title>Technologies</title>
<style type="text/css">
   img {
    max-width: 100%;
    height: auto;
    display: block;
}
.top-company .image {
    width: 120px;
    margin: 0 auto;
}

</style>
</head>

<body>

	<!-- Navigation -->
	<%@include file="shared/Header.jsp"%>
	<!-- /.Navigation -->



	<!--header-------------------------------------------------------->
	<header class="techn_main text-center">
	<div class="container">
		<div class="row">
			<div
				class="col-xs-12 col-sm-12 col-md-12 col-xl-12 col-lg-12 mx-auto">
				<div class="text-uppercase about_main_head">
					<p>
						<span>Technologies</span>
					</p>
				</div>
				<div class="aboutus_pg">
					<p>As partners, we provide solutions & services in following
						technologies & ecosystem.</p>
				</div>
			</div>
		</div>
	</div>
	</header>


	<!---------Technologies------------------->
	<%
		if (session.getAttribute("user") != null) {
	%>
	<section>
	<div class="container">
		<div class="row  mt-4">
			<div class="col-xs-12 col-sm-12 col-md-12">
				<div>
					<h1 class="text-center">Our Technologies</h1>
					<hr class="light my-4 demad">
				</div>
			</div>
		</div>
	</div>

	<!-- <div class="container-fluid"> -->
		<div class="row  mt-4 tech_mrow">
			<div class="col-xs-6 col-sm-6 col-md-6">
				<div>
					<h3>SAP</h3>
				</div>
				<div>
					<p>SAP SE is a German-based European multinational software
						corporation that makes enterprise software to manage business
						operations and customer relations. SAP S/4HANA, the newest
						generation of the SAP Business Suite. It was written natively for
						the SAP HANA platform. SAP has acquired several companies that
						sell cloud-based products, with several multibillion-dollar
						acquisitions seen by analysts as an attempt to challenge
						competitor Oracle.</p>
				</div>
			</div>

			<div class="col-xs-6 col-sm-6 col-md-6">
				<div class="row text-center">
					<div class="col-xs-4 col-sm-6 col-md-6 col-lg-4 col-xl-3">
						<div class="img_alltech">
							<a
								href="${pageContext.request.contextPath}/payment/paymnetSuccess">
								<img
								src="${pageContext.request.contextPath}/resources/images/SAP_BOne.png"
								width="120">
							</a>
						</div>
					</div>
					<div class="col-xs-3 col-sm-6 col-md-6 col-lg-4 col-xl-3">
						<div class="img_alltech">
							<a
								href="${pageContext.request.contextPath}/payment/paymnetSuccess"><img
								src="${pageContext.request.contextPath}/resources/images/SAP-ByD.png"
								width="120"></a>
						</div>
					</div>
					<div class="col-xs-3 col-sm-6 col-md-6 col-lg-4 col-xl-3">
						<div class="img_alltech">
							<a
								href="${pageContext.request.contextPath}/payment/paymnetSuccess"><img
								src="${pageContext.request.contextPath}/resources/images/successfactors.png"
								width="120"></a>
						</div>
					</div>
					<div class="col-xs-3 col-sm-6 col-md-6 col-lg-4 col-xl-3">
						<div class="img_alltech">
							<a
								href="${pageContext.request.contextPath}/payment/paymnetSuccess">
								<img
								src="${pageContext.request.contextPath}/resources/images/SAP_S4HANA.png"
								width="120">
							</a>
						</div>
					</div>
					<div class="col-xs-3 col-sm-6 col-md-6 col-lg-4 col-xl-3">
						<div class="img_alltech">
							<a
								href="${pageContext.request.contextPath}/payment/paymnetSuccess"><img
								src="${pageContext.request.contextPath}/resources/images/hybris-logo.jpg"
								width="120"></a>
						</div>
					</div>
					<div class="col-xs-3 col-sm-6 col-md-6 col-lg-4 col-xl-3">
						<div class="img_alltech">
							<a
								href="${pageContext.request.contextPath}/payment/paymnetSuccess">
								<img
								src="${pageContext.request.contextPath}/resources/images/Sap-ariba.jpg"
								width="120">
							</a>
						</div>
					</div>
				</div>
			</div>


		</div>



		<div class="row tech_mrow tech_mpad">
			<div class="col-xs-6 col-sm-6 col-md-6">
				<div class="row text-center">
					<div class="col-xs-3 col-sm-6 col-md-6 col-md-6 col-lg-4 col-xl-3">
						<div class="img_alltech">
							<a
								href="${pageContext.request.contextPath}/payment/paymnetSuccess">
								<img
								src="${pageContext.request.contextPath}/resources/images/microsoft.jpg"
								width="120">
							</a>
						</div>
					</div>
					<div class="col-xs-3 col-sm-6 col-md-6 col-md-6 col-lg-4 col-xl-3">
						<div class="img_alltech">
							<a
								href="${pageContext.request.contextPath}/payment/paymnetSuccess">
								<img
								src="${pageContext.request.contextPath}/resources/images/microexcel.jpg"
								width="120">
							</a>
						</div>
					</div>
				</div>
			</div>

			<div class="col-xs-6 col-sm-6 col-md-6">
				<div>
					<h3>Microsoft</h3>
				</div>
				<div>
					<p>Microsoft Corporation is an American multinational
						technology company with headquarters in Redmond, Washington. It
						develops, manufactures, licenses, supports and sells computer
						software, consumer electronics, personal computers, and services.
						Its best known software products are the Microsoft Windows line of
						operating systems, the Microsoft Office suite, and the Internet
						Explorer and Edge web browsers.</p>
				</div>
			</div>
		</div>
		<div class="row tech_mrow  tech_mpad">
			<div class="col-xs-6 col-sm-6 col-md-6 tech_data">
				<div>
					<h3>Oracle</h3>
				</div>
				<div>
					<p>Oracle Corporation is an American multinational computer
						technology corporation, headquartered in Redwood Shores,
						California. The company specializes primarily in developing and
						marketing database software and technology, cloud engineered
						systems and enterprise software products — particularly its own
						brands of database management systems. In 2015, Oracle was the
						second-largest software maker by revenue, after Microsoft.</p>
				</div>
			</div>

			<div class="col-xs-6 col-sm-6 col-md-6">
				<div class="row text-center">
					<div class="col-xs-3 col-sm-6 col-md-6 col-md-6 col-lg-4 col-xl-3">
						<div class="img_alltech">
							<a
								href="${pageContext.request.contextPath}/payment/paymnetSuccess">
								<img
								src="${pageContext.request.contextPath}/resources/images/oracle.jpg"
								width="120">
							</a>
						</div>
					</div>
					<div class="col-xs-3 col-sm-6 col-md-6 col-md-6 col-lg-4 col-xl-3">
						<div class="img_alltech">
							<a
								href="${pageContext.request.contextPath}/payment/paymnetSuccess"><img
								src="${pageContext.request.contextPath}/resources/images/java.jpg"
								width="120"></a>
						</div>
					</div>
				</div>
			</div>
		</div>



	</div>
	</section>
	<%
		} else {
	%>

	<%-- <section>
	<div class="container">
		<div class="row  mt-4">
			<div class="col-xs-12 col-sm-12 col-md-12">
				<div>
					<h1 class="text-center">Our Technologies</h1>
					<hr class="light my-4 demad">
				</div>
			</div>
		</div>
	</div>

	<div class="container-fluid">
		<div class="row  mt-4 tech_mrow">
			<div class="col-xs-6 col-sm-6 col-md-6">
				<div>
					<h3>SAP</h3>
				</div>
				<div>
					<p>SAP SE is a German-based European multinational software
						corporation that makes enterprise software to manage business
						operations and customer relations. SAP S/4HANA, the newest
						generation of the SAP Business Suite. It was written natively for
						the SAP HANA platform. SAP has acquired several companies that
						sell cloud-based products, with several multibillion-dollar
						acquisitions seen by analysts as an attempt to challenge
						competitor Oracle.</p>
				</div>
			</div>

			<div class="col-xs-6 col-sm-6 col-md-6">
				<div class="row text-center">
					<div class="col-xs-4 col-sm-6 col-md-6 col-lg-4 col-xl-3">
						<div class="img_alltech">
							<img
								src="${pageContext.request.contextPath}/resources/images/SAP_BOne.png"
								width="120">
						</div>
					</div>
					<div class="col-xs-3 col-sm-6 col-md-6 col-lg-4 col-xl-3">
						<div class="img_alltech">
							<img
								src="${pageContext.request.contextPath}/resources/images/SAP-ByD.png"
								width="120">
						</div>
					</div>
					<div class="col-xs-3 col-sm-6 col-md-6 col-lg-4 col-xl-3">
						<div class="img_alltech">
							<img
								src="${pageContext.request.contextPath}/resources/images/successfactors.png"
								width="120">
						</div>
					</div>
					<div class="col-xs-3 col-sm-6 col-md-6 col-lg-4 col-xl-3">
						<div class="img_alltech">
							<img
								src="${pageContext.request.contextPath}/resources/images/SAP_S4HANA.png"
								width="120">
						</div>
					</div>
					<div class="col-xs-3 col-sm-6 col-md-6 col-lg-4 col-xl-3">
						<div class="img_alltech">
							<img
								src="${pageContext.request.contextPath}/resources/images/hybris-logo.jpg"
								width="120">
						</div>
					</div>
					<div class="col-xs-3 col-sm-6 col-md-6 col-lg-4 col-xl-3">
						<div class="img_alltech">
							<img
								src="${pageContext.request.contextPath}/resources/images/Sap-ariba.jpg"
								width="120">
						</div>
					</div>
				</div>
			</div>


		</div>



		<div class="row tech_mrow tech_mpad">
			<div class="col-xs-6 col-sm-6 col-md-6">
				<div class="row text-center">
					<div class="col-xs-3 col-sm-6 col-md-6 col-md-6 col-lg-4 col-xl-3">
						<div class="img_alltech">
							<img
								src="${pageContext.request.contextPath}/resources/images/microsoft.jpg"
								width="120">
						</div>
					</div>
					<div class="col-xs-3 col-sm-6 col-md-6 col-md-6 col-lg-4 col-xl-3">
						<div class="img_alltech">
							<img
								src="${pageContext.request.contextPath}/resources/images/microexcel.jpg"
								width="120">
						</div>
					</div>
				</div>
			</div>

			<div class="col-xs-6 col-sm-6 col-md-6">
				<div>
					<h3>Microsoft</h3>
				</div>
				<div>
					<p>Microsoft Corporation is an American multinational
						technology company with headquarters in Redmond, Washington. It
						develops, manufactures, licenses, supports and sells computer
						software, consumer electronics, personal computers, and services.
						Its best known software products are the Microsoft Windows line of
						operating systems, the Microsoft Office suite, and the Internet
						Explorer and Edge web browsers.</p>
				</div>
			</div>
		</div>
		<div class="row tech_mrow  tech_mpad">
			<div class="col-xs-6 col-sm-6 col-md-6 tech_data">
				<div>
					<h3>Oracle</h3>
				</div>
				<div>
					<p>Oracle Corporation is an American multinational computer
						technology corporation, headquartered in Redwood Shores,
						California. The company specializes primarily in developing and
						marketing database software and technology, cloud engineered
						systems and enterprise software products — particularly its own
						brands of database management systems. In 2015, Oracle was the
						second-largest software maker by revenue, after Microsoft.</p>
				</div>
			</div>

			<div class="col-xs-6 col-sm-6 col-md-6">
				<div class="row text-center">
					<div class="col-xs-3 col-sm-6 col-md-6 col-md-6 col-lg-4 col-xl-3">
						<div class="img_alltech">
							<img
								src="${pageContext.request.contextPath}/resources/images/oracle.jpg"
								width="120">
						</div>
					</div>
					<div class="col-xs-3 col-sm-6 col-md-6 col-md-6 col-lg-4 col-xl-3">
						<div class="img_alltech">
							<img
								src="${pageContext.request.contextPath}/resources/images/java.jpg"
								width="120">
						</div>
					</div>
				</div>
			</div>
		</div>



	</div>
	</section>
 --%>
 <div class="section-title bb">
              <h2 class="text-center">Our Technology Ecosystem</h2>
              <p class="text-center" style="    margin-bottom: 70px;">As partners, we provide solutions &amp; services in following technologies.</p>
            </div>
 	<div class="container">
        <div class="row">
          <div class="col-sm-10 col-sm-offset-1 col-md-8 col-md-offset-2">
            
          </div>
        </div>
        <div class="row gap-20 top-company-wrapper alt-item-mb">
        <div class="col-xss-6 col-xs-4 col-sm-3 col-md-2">
            <div class="top-company">
              <div class="image"> <img src="http://teklinkinternational.com/images/technology/sap.jpg" alt="sap"></div> 
            </div>
          </div>
          <div class="col-xss-6 col-xs-4 col-sm-3 col-md-2">
            <div class="top-company">
              <div class="image"> <img src="http://teklinkinternational.com/images/technology/SAP_S4HANA.png" alt="SAP-S4HANA"> </div>
            </div>
          </div>
          <div class="col-xss-6 col-xs-4 col-sm-3 col-md-2">
            <div class="top-company">
              <div class="image"> <img src="http://teklinkinternational.com/images/technology/SAP-ByD.png" alt="SAP-ByD"> </div>
            </div>
          </div>
          <div class="col-xss-6 col-xs-4 col-sm-3 col-md-2">
            <div class="top-company">
              <div class="image"> <img src="http://teklinkinternational.com/images/technology/SAP_BOne.png" alt="SAP-B-one"> </div>
            </div>
          </div>
          <div class="col-xss-6 col-xs-4 col-sm-3 col-md-2">
            <div class="top-company">
              <div class="image"> <img src="http://teklinkinternational.com/images/technology/hybris.png" alt="SAP-Hybris"> </div>
            </div>
          </div>
          <div class="col-xss-6 col-xs-4 col-sm-3 col-md-2">
            <div class="top-company">
              <div class="image"> <img src="http://teklinkinternational.com/images/technology/Sap-ariba.jpg" alt="SAP-Hybris"> </div>
            </div>
          </div>
          <div class="col-xss-6 col-xs-4 col-sm-3 col-md-2">
            <div class="top-company">
              <div class="image"> <img src="http://teklinkinternational.com/images/technology/successfactors.png" alt="successfactors"> </div>
            </div>
          </div>
          <div class="col-xss-6 col-xs-4 col-sm-3 col-md-2">
            <div class="top-company">
              <div class="image"> <img src="http://teklinkinternational.com/images/technology/abeau.jpg" alt="abeau"> </div>
            </div>
          </div>
          <div class="col-xss-6 col-xs-4 col-sm-3 col-md-2">
            <div class="top-company">
              <div class="image"> <img src="http://teklinkinternational.com/images/technology/logo-sap-lumira.png" alt="sap-lumira"> </div>
            </div>
          </div>
          <div class="col-xss-6 col-xs-4 col-sm-3 col-md-2">
            <div class="top-company">
              <div class="image"> <img src="http://teklinkinternational.com/images/technology/oracle.jpg" alt="oracle"> </div>
            </div>
          </div>
          <div class="col-xss-6 col-xs-4 col-sm-3 col-md-2">
            <div class="top-company">
              <div class="image"> <img src="http://teklinkinternational.com/images/technology/oracle-erp.jpg" alt="oracle ERP"> </div>
            </div>
          </div>
          <div class="col-xss-6 col-xs-4 col-sm-3 col-md-2">
            <div class="top-company">
              <div class="image"> <img src="http://teklinkinternational.com/images/technology/oracle-JDE_2_976x497.png" alt="oracle erp"> </div>
            </div>
          </div>
          <div class="col-xss-6 col-xs-4 col-sm-3 col-md-2">
            <div class="top-company">
              <div class="image"> <img src="http://teklinkinternational.com/images/technology/java.jpg" alt="jdedwards"> </div>
            </div>
          </div>
          
          <div class="col-xss-6 col-xs-4 col-sm-3 col-md-2">
            <div class="top-company">
              <div class="image"> <img src="http://teklinkinternational.com/images/technology/jdedwards.jpg" alt="java"> </div>
            </div>
          </div>
          <div class="col-xss-6 col-xs-4 col-sm-3 col-md-2">
            <div class="top-company">
              <div class="image"> <img src="http://teklinkinternational.com/images/technology/hyperion.png" alt="hyperion"> </div>
            </div>
          </div>
          <div class="col-xss-6 col-xs-4 col-sm-3 col-md-2">
            <div class="top-company">
              <div class="image"> <img src="http://teklinkinternational.com/images/technology/microsoft.jpg" alt="microsoft"> </div>
            </div>
          </div>
          <div class="col-xss-6 col-xs-4 col-sm-3 col-md-2">
            <div class="top-company">
              <div class="image"> <img src="http://teklinkinternational.com/images/technology/ibm.jpg" alt="IBN"> </div>
            </div>
          </div>
          <div class="col-xss-6 col-xs-4 col-sm-3 col-md-2">
            <div class="top-company">
              <div class="image"> <img src="http://teklinkinternational.com/images/technology/infor_400x200.png" alt="infor"> </div>
            </div>
          </div>
         
          <div class="col-xss-6 col-xs-4 col-sm-3 col-md-2">
            <div class="top-company">
              <div class="image"> <img src="http://teklinkinternational.com/images/technology/hortonworks.jpg" alt="hortonworks"> </div>
            </div>
          </div>
          <div class="col-xss-6 col-xs-4 col-sm-3 col-md-2">
            <div class="top-company">
              <div class="image"> <img src="http://teklinkinternational.com/images/technology/html5.jpg" alt="HTML5"> </div>
            </div>
          </div>
           <div class="col-xss-6 col-xs-4 col-sm-3 col-md-2">
            <div class="top-company">
              <div class="image"> <img src="http://teklinkinternational.com/images/technology/angular-card.png" alt="HTML5"> </div>
            </div>
          </div>

          <div class="col-xss-6 col-xs-4 col-sm-3 col-md-2">
            <div class="top-company">
              <div class="image"> <img src="http://teklinkinternational.com/images/technology/leferay.jpg" alt="liferay"> </div>
            </div>
          </div>
          <div class="col-xss-6 col-xs-4 col-sm-3 col-md-2">
            <div class="top-company">
              <div class="image"> <img src="http://teklinkinternational.com/images/technology/microexcel.jpg" alt="microexcel"> </div>
            </div>
          </div>

          <div class="col-xss-6 col-xs-4 col-sm-3 col-md-2">
            <div class="top-company">
              <div class="image"> <img src="http://teklinkinternational.com/images/technology/openbraou.jpg" alt="openbraou"> </div>
            </div>
          </div>
          
          <div class="col-xss-6 col-xs-4 col-sm-3 col-md-2">
            <div class="top-company">
              <div class="image"> <img src="http://teklinkinternational.com/images/technology/prevedere.jpg" alt="prevedere"> </div>
            </div>
          </div>
          <div class="col-xss-6 col-xs-4 col-sm-3 col-md-2">
            <div class="top-company">
              <div class="image"> <img src="http://teklinkinternational.com/images/technology/quintiq.jpg" alt="quintiq"> </div>
            </div>
          </div>
          <div class="col-xss-6 col-xs-4 col-sm-3 col-md-2">
            <div class="top-company">
              <div class="image"> <img src="http://teklinkinternational.com/images/technology/ruby-on-rails.jpg" alt="ruby-on-rails"> </div>
            </div>
          </div>
          <div class="col-xss-6 col-xs-4 col-sm-3 col-md-2">
            <div class="top-company">
              <div class="image"> <img src="http://teklinkinternational.com/images/technology/salesforce.jpg" alt="salesforce"> </div>
            </div>
          </div>

          <div class="col-xss-6 col-xs-4 col-sm-3 col-md-2">
            <div class="top-company">
              <div class="image"> <img src="http://teklinkinternational.com/images/technology/amazon.jpg" alt="ABW"> </div>
            </div>
          </div>
          <div class="col-xss-6 col-xs-4 col-sm-3 col-md-2">
            <div class="top-company">
              <div class="image"> <img src="http://teklinkinternational.com/images/technology/xml.jpg" alt="xml"> </div>
            </div>
          </div>
          <div class="col-xss-6 col-xs-4 col-sm-3 col-md-2">
            <div class="top-company">
              <div class="image"> <img src="http://teklinkinternational.com/images/technology/greytip.jpg" alt="greytip"> </div>
            </div>
          </div>
          

        </div>
      </div>
 	
	<%
		}
	%>

	<!-- Footer -->
	<%@include file="shared/Footer.jsp"%>
	<!-- /.Footer -->

</body>
</html>
