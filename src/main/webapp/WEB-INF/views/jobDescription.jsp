<!-- @author Prateek Raj -->

<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="jstl" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="sf" uri="http://www.springframework.org/tags/form"%>
<!DOCTYPE html >
<jstl:set var="context" value="${pageContext.request.contextPath}" />
<spring:url var="css" value="/resources/css"></spring:url>
<spring:url var="images" value="/resources/images"></spring:url>
<spring:url var="boots400" value="/resources/bootstrap-4.0.0-dist"></spring:url>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="shortcut icon" type="image/png" href="${images}/favicon.png" />
<link rel="stylesheet"
	href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css">
<script
	src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
<script
	src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js"></script>
<script
	src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js"></script>
<%-- <link href="${boots400}/css/bootstrap.min.css" rel="stylesheet">
<!--For Toggle button Responsive on Mobile-->
<script src="${boots400}/jquery/jquery-3.3.1.min.js"></script>
<!--For Toggle button Responsive on Mobile-->
<script src="${boots400}/js/bootstrap.min.js"></script>
<!--For Fa icon-->--%>
<link href="${boots400}/font-awesome/css/font-awesome.min.css"
	rel="stylesheet">
<link href="${css}/style_1.css" rel="stylesheet" type="text/css">
<link href="${css}/responsive.css" rel="stylesheet" type="text/css">
<title>Home | SOD</title>
<style>
.error {
	padding: 1 1;
	border-color: red;
	background-color: #ffeaea;
	font-size: 12px;
}
</style>

</head>
<body>
	<%@include file="shared/Header.jsp"%>
	<div class="container job_desc_main">
		<div class="row">
			<div class="col-xs-12 col-sm-12 col-md-12">
				<div class=" row page-header text-muted">
					<!-- <div
						class="col-xs-12 col-sm-12 col-md-12 text-right prof_updt_seek">
						<button class="btn btn-info register" type="button">
							<a href="user_profile.html">User Profile Update</a>
						</button>
					</div> -->
					<div class="col-xs-12 col-sm-12 col-md-12">
						<div class="basic_head">
							<h1>Profile</h1>
						</div>
					</div>

					<div class="col-xs-12 col-sm-12 col-md-12"></div>
				</div>

				<!-- <div class="userProfilesection"> -->


					<div class="tab">
						<button class="tablinks" onclick="openCity(event, 'Profile')"
							id="defaultOpen">Profile</button>
						<button class="tablinks" onclick="openCity(event, 'Optiontwo')">Upload
							Resume</button>
						
					</div>

					<div id="Profile" class="tabcontent">
						<h3 class="sectionHeading">Profile</h3>
						<div class="expertdetailsSection">
							<div class="expertdetailsSectionarea">
								<table style="width: 100%;">
									<tbody>
										<tr>
											<td class="title">First Name</td>
											<td>${expert.firstName}</td>
										</tr>
										<tr>
											<td class="title">Last Name</td>
											<td>${expert.lastName}</td>
										</tr>
										<tr>
											<td class="title">Gender</td>
											<td>Male/Female</td>
										</tr>
										<tr>
											<td class="title">Date of birth</td>
											<td>date - month - year</td>
										</tr>
										<!-- <tr>
								<td class="title">Bio</td>
								<td class="bio">-</td>
							</tr> -->
										<tr>
											<td class="title">Mobile Number</td>
											<td>${expert.mobileNumber}</td>
										</tr>
										<tr>
											<td class="title">location</td>
											<td>${expert.address}</td>
										</tr>
										<tr>
											<td class="title">E-mail id</td>
											<td>${expert.email}</td>
										</tr>

									</tbody>
								</table>


								<div class="links">
									<!-- <button class="tablinks" onclick="openCity(event, 'EdirForm')">Edit
							Profile</button> -->
									<a href="/SOD/expert/profileEdit"><button
											class="tablinks btn btn-primary">Edit details</button></a>
								</div>
							</div>
						</div>
					</div>

		          <div id="Optiontwo" class="tabcontent">
				<h3>Browse And Upload your File</h3>
				
				<form method="POST" action="/SOD/expert/upload?${_csrf.parameterName}=${_csrf.token}" enctype="multipart/form-data">
				
					<input id="file-id" type="file" name="file" /> <br /> <input
						type="submit" value="Upload" />
				</form>
			</div>

						</div>
						</div>

						<script>
			function openCity(evt, cityName) {
				var i, tabcontent, tablinks;
				tabcontent = document.getElementsByClassName("tabcontent");
				for (i = 0; i < tabcontent.length; i++) {
					tabcontent[i].style.display = "none";
				}
				tablinks = document.getElementsByClassName("tablinks");
				for (i = 0; i < tablinks.length; i++) {
					tablinks[i].className = tablinks[i].className.replace(
							" active", "");
				}
				document.getElementById(cityName).style.display = "block";
				evt.currentTarget.className += " active";
			}

			// Get the element with id="defaultOpen" and click on it
			document.getElementById("defaultOpen").click();
		</script>


						<!-- 				**************** List of Job start **************** -->
						<!-- 				<div id="accordion" class="accordion"> -->
						<!-- 					**************** For Loop List of Job start **************** -->
						<%-- 					<jstl:forEach items="${productList}" var="plst"> --%>
						<!-- 					<div class="card common_jobcard"> -->
						<!-- 							**************** Product Name in header **************** -->
						<!-- 							<div class="card-header collapsed" data-toggle="collapse" -->
						<%-- 								href="#collapse${plst.clientProducts.prodId}"> --%>
						<%-- 								<a class="card-title job_heading"> ${plst.clientProducts.prodName} </a> --%>
						<!-- 							</div> -->
						<!-- 							**************** Short Description, Skills are here **************** -->
						<%-- 							<div id="collapse${plst.clientProducts.prodId}" class="collapse" data-parent="#accordion"> --%>
						<!-- 								<div class=" job_descr_cont"> -->
						<!-- 									<div class="card-body text-muted"> -->
						<%-- 										<p>${plst.clientProducts.prodDescription}</p> --%>
						<!-- 										<ul class=" list_common"> -->
						<%-- 											<li>Skills: <strong>${plst.skillStr}</strong></li> --%>
						<%-- 											<li>Job Type: <strong>${plst.clientProducts.employmentType}</strong></li> --%>
						<!-- 										</ul> -->
						<%-- 										<a href="/SOD/expert/jdp/${plst.clientProducts.prodId}/application" --%>
						<!-- 											class="btn btn-info detail_button" type="button">Apply</a> -->
						<!-- 									</div> -->
						<!-- 								</div> -->

						<!-- 							</div> -->
						<!-- 						</div> -->
						<%-- 					</jstl:forEach> --%>
						<!-- 					**************** For Loop List of Job end **************** -->

						<!-- 				</div> -->
						<!-- 				**************** List of Job end **************** -->

				
						<%@include file="shared/Footer.jsp"%></body>
</html>