<!-- @author Prateek Raj -->

<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@taglib prefix="jstl" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib uri="http://www.springframework.org/security/tags"
	prefix="sec"%>
<!DOCTYPE html >
<jstl:set var="context" value="${pageContext.request.contextPath}" />
<spring:url var="css" value="/resources/css"></spring:url>
<spring:url var="images" value="/resources/images"></spring:url>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="shortcut icon" type="image/png" href="${images}/favicon.png" />
<link rel="stylesheet"
	href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css">
<script
	src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
<script
	src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js"></script>
<script
	src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js"></script>
<link rel="stylesheet"
	href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
<link href="${css}/style_1.css" rel="stylesheet" type="text/css">
<link href="${css}/responsive.css" rel="stylesheet" type="text/css">
<style>
.error {
	padding: 1 1;
	border-color: red;
	background-color: #ffeaea;
}

.error {
	padding: 1 1;
	border: none;
	border-radius: 0px;
	border-left: 5px solid red;
	background-color: #ffeaea;
	font-size: 12px;
	font-style: italic;
}

.footer {
	margin-top: 0;
}
</style>
<title>Skill On Demand | Login</title>
</head>

<body>
	<%@include file="shared/Header.jsp"%>
	<!--section--login------------------------------------------------>

	<div class="container-fluid log_in">
		<div class="row login_row">
			<div class="col-sm-7"
				style="overflow: hidden; padding-left: 0; padding-right: 0;">
			<%-- 	<img src="${images}/client_login.jpg" class="img_login"> --%>
				<img src="https://de.allyouneed.com/magazin/wp-content/uploads/2017/03/Monitor-an-Laptop-anschlie%C3%9Fen-Anleitung-1.jpg" class="img_login" style="width: 65vw;">
			</div>
			<div class="col-sm-5">
				<div class="main_head text-center">
					<h1>Login to Skills On Demand</h1>
					<hr class="light my-4 demad">
				</div>
				<jstl:url value="/client/clientLoginProcess" var="loginVar" />
				<form action="${loginVar}" method="POST">
					<div class="form-group">
						<label>Email:</label> <input type="email" name="clientemail"
							class="form-control" />
					</div>
					<div class="form-group">
						<label>Password:</label> <input type="password"
							name="clientpassword" class="form-control" id="myInput" />
					</div>
					
					<div class="form-check">
					
					<input type="checkbox" onclick="myFunction()"> Show Password
					</div>
					
					<div class="form-check">
					<input type="checkbox" id="remember"
							name="client-remember-me" value="true"
							style="vertical-align: middle; top: 2px; left: 0; height: 13px; width: 13px; background-color: #eee;" />
						<label for="remember" class="form-check-label"> Remember
							Me</label> 
					</div>
					<sec:csrfInput />
					<c:if test="${param.error}">
						<p class="alert alert-warning text-center">Please enter valid
							Email & password</p>
					</c:if>
					<a type="button" class="btn btn-danger sub_btn register"
						href="${context}">Cancel</a> <input type="submit"
						class="btn btn-info sub_btn register" value="Login" />

					<div class="change_link change_link_common">
						<p>
							Not a member yet ? <a href="${context}/client-register"
								class="to_register">Join us</a>
						</p>
					</div>
				</form>
				<div class="inbox-body">
					<a href="#myModal" data-toggle="modal" title="Compose"
						class="forget_psw"> Forgot Password </a>

					<!-- Modal -->
					<div aria-hidden="true" aria-labelledby="myModalLabel"
						role="dialog" tabindex="-1" id="myModal" class="modal fade"
						style="display: none;">
						<div class="modal-dialog">
							<div class="modal-content">
								<div class="modal-header">
									<h4 class="modal-title">Forgot Password</h4>
									<button aria-hidden="true" data-dismiss="modal" class="close"
										type="button">�</button>

								</div>
								<div class="modal-body">
									<div class="text-center">
										<h3>
											<i class="fa fa-lock fa-4x"></i>
										</h3>
										<h2 class="text-center">Forgot Password?</h2>
										<div class="panel-body">
											<p class="Error">${Success}</p>
											<p class="Error1">${noUser}</p>
											<p class="Error1">${failed}</p>
											<form:form class="form" 
												action="/SOD/clientResetPassword" method="post">
												<fieldset>
													<div class="form-group">
														<div class="input-group">
															<div class="input-group-append">
																<button class="btn btn-info" type="button">
																	<i class="fa fa-envelope"></i>
																</button>
															</div>
															<input id="emailInput" placeholder="email address" name="email"
																class="form-control" type="email"
																oninvalid="setCustomValidity('Please enter a valid email address!')"
																onchange="try{setCustomValidity('')}catch(e){}"
																required="">
														</div>
													</div>
													<div class="form-group">
														<input class="btn btn-lg btn-info btn-block" value="Send"
															type="submit">
													</div>
												</fieldset>
												</form:form>
										</div>
									</div>
								</div>
							</div>
							<!-- /.modal-content -->
						</div>
						<!-- /.modal-dialog -->
					</div>
					<!-- /.modal -->
				</div>

			</div>

		</div>
	</div>

	<%@include file="shared/Footer.jsp"%>

	<script>
		$(document).ready(function() {
			var wrongemail = ${resetEmailWrong};
			if (wrongemail == 1)
				$("#myModal").modal("show");

		});
	</script>
	<script>
function myFunction() {
    var x = document.getElementById("myInput");
    if (x.type === "password") {
        x.type = "text";
    } else {
        x.type = "password";
    }
}
</script>
</body>
</html>
