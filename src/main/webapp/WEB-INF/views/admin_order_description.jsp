<!--  @author Hardev Singh -->
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>

<%@taglib prefix="jstl" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@taglib prefix="sf" uri="http://www.springframework.org/tags/form"%>

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">

<spring:url var="css" value="/resources/css"></spring:url>
<spring:url var="images" value="/resources/images"></spring:url>
<spring:url var="js" value="/resources/js"></spring:url>
<spring:url var="vendor" value="/resources/vendor"></spring:url>
<spring:url var="dataTable" value="/resources/DataTables"></spring:url>
<spring:url var="productImage" value="/resources/productImages"></spring:url>


<html>
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="description" content="">
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta name="robots" content="all,follow">
<link rel="shortcut icon" type="image/png" href="${images}/favicon.jpg" />
<!-- Bootstrap CSS-->
<link rel="stylesheet" href="${vendor}/bootstrap/css/bootstrap.min.css">
<!-- Font Awesome CSS-->
<link rel="stylesheet"
	href="${vendor}/font-awesome/css/font-awesome.min.css">
<!-- Fontastic Custom icon font-->
<link rel="stylesheet" href="${css}/fontastic.css">
<!-- Google fonts - Roboto -->
<link rel="stylesheet"
	href="https://fonts.googleapis.com/css?family=Roboto:300,400,500,700">
<!-- jQuery Circle-->
<link rel="stylesheet"
	href="${css}/grasp_mobile_progress_circle-1.0.0.min.css">
<!-- Custom Scrollbar-->
<link rel="stylesheet"
	href="${vendor}/malihu-custom-scrollbar-plugin/jquery.mCustomScrollbar.css">
<!-- theme stylesheet-->
<link rel="stylesheet" href="${css}/style.default.css"
	id="theme-stylesheet">
<!-- Custom stylesheet - for your changes-->
<link rel="stylesheet" href="${css}/custom.css">
<!-- Toogle Slider -->
<link rel="stylesheet" href="${css}/toggle_slider.css">
<!-- Favicon-->
<link rel="shortcut icon" href="${images}/favicon.ico">

<title>Cancel Order description</title>
<!-- Tweaks for older IEs-->
<!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script><![endif]-->



</head>
<body>
	<%@ include file="shared/shared-admin/Header.jsp"%>

	<div class="page">
		<%@include file="shared/shared-admin/Navbar.jsp"%>
		<!--Main Section Start-->
		<section class="product_main">

		<div class="row">
			<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 col-xl-12">
				<div class="x_panel text-muted">
					<div class="x_title">
						<h1>Product Name</h1>
						<div class="clearfix"></div>
					</div>
					<div class="x_content">
						<p>
							${orderProduct.prodName}
						</p>
					</div>
					<div class="row order_prd_img">
						<div class="col-xs-12 col-sm-12 col-md-6 col-lg-6 col-xl-6">
							<div class="row">
								<div class="col-xs-6 col-sm-6 col-md-6 col-lg-6 col-xl-6">
									<div class="images_prod">
										<img src="${productImage}/${orderProduct.prodImagePath}" width="100%" height="auto">
									</div>
								</div>
								<div class="col-xs-6 col-sm-6 col-md-6 col-lg-6 col-xl-6">
									<div class="order_prd_nm">
										<p>
											<strong>Product ID</strong>
										</p>
									</div>
									<div class="text-danger">
										<p>
											<strong>${orderProduct.prodId}</strong>
										</p>
									</div>
									<div>
										<p>
											<strong>Status :</strong> <span class="text-danger">
											<jstl:choose>
											<jstl:when test="${orderProduct.status==true}"> Active</jstl:when>
											<jstl:otherwise>Cancel </jstl:otherwise>
											</jstl:choose>
												</span>
										</p>
									</div>
								</div>

							</div>


							<div class="sale_prd_head">
								<h4>Product Description :</h4>
							</div>
							<div class="alert alert-info">This Project is compatible
								with all Visual Studio Versions from 2018-2020</div>
							<div class="user-html">
								${orderProduct.prodDescription}
							</div>
						</div>


						<div class="col-xs-12 col-sm-12 col-md-6 col-lg-6 col-xl-6">
							<div class=" basic_prd_desc">
								<div class="row cust_order_desc">
									<div class="col-xs-6 col-sm-6 col-md-3 col-lg-3 col-xl-3">
										<div class="user_ord_img">
											<img src="${images}/user.png">
										</div>
									</div>
									<div
										class="col-xs-6 col-sm-6 col-md-6 col-lg-6 col-xl-6 cus_ord_detail">
										<div>
											<p>
												<strong>CUSTOMER NAME :</strong>&nbsp;&nbsp;${expertContract.firstName} &nbsp;${expertContract.lastName} 
											</p>
										</div>
										<div class="text-danger">
											<p>
												<strong>Id-A123456</strong>
											</p>
										</div>
										<div>
											<p>
												<strong>Status :</strong> <jstl:choose>
											<jstl:when test="${orderProduct.status==true}"> Active</jstl:when>
											<jstl:otherwise>Cancel </jstl:otherwise>
											</jstl:choose>
											</p>
										</div>
									</div>
								</div>
								<div>
									<p>
										<strong>Email: </strong> ${expertDetail.email}
									</p>
								</div>
								<div>
									<p>
										<strong>Address: </strong> ${expertDetail.address}
									</p>
								</div>
								<div>
									<p>
										<strong>Contact No.: </strong> ${expertDetail.mobileNumber}
									</p>
								</div>
					
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
		</section>
	</div>
	<!--Main Section End-->


	<!--Footer Section Start-->
	<%@ include file="shared/shared-admin/Footer.jsp"%>
</body>
</html>