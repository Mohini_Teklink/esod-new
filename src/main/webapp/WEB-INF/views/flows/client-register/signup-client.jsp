<!-- @author Prateek Raj -->

<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="jstl" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="sf" uri="http://www.springframework.org/tags/form"%>
<!DOCTYPE html >
<jstl:set var="context" value="${pageContext.request.contextPath}" />
<spring:url var="css" value="/resources/css"></spring:url>
<spring:url var="images" value="/resources/images"></spring:url>
<spring:url var="boots400" value="/resources/bootstrap-4.0.0-dist"></spring:url>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="shortcut icon" type="image/png" href="${images}/favicon.png" />
<link rel="stylesheet"
	href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css">
<script
	src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
<script
	src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js"></script>
<script
	src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js"></script>
<link href="${boots400}/css/bootstrap.min.css" rel="stylesheet">
<!--For Toggle button Responsive on Mobile-->
<script src="${boots400}/jquery/jquery-3.3.1.min.js"></script>
<!--For Toggle button Responsive on Mobile-->
<script src="${boots400}/js/bootstrap.min.js"></script>
<!--For Fa icon-->
<link href="${boots400}/font-awesome/css/font-awesome.min.css"
	rel="stylesheet">
<link href="${css}/style_1.css" rel="stylesheet" type="text/css">
<link href="${css}/responsive.css" rel="stylesheet" type="text/css">
<title>Sign Up | SOD</title>
<style>
.error {
	padding: 1 1;
	border-color: red;
	background-color: #ffeaea;
}

.footer {
	margin-top: 0;
}
</style>

</head>
<body class="">
	<%@include file="../../shared/Header.jsp"%>


	<div class="container-fluid sign_up">
		<div class="row">
			<div class="col-sm-6"
				style="overflow: hidden; padding-left: 0; padding-right: 0;">
				<img src="${images}/client_signup.jpg" class="img_signup">
			</div>

			<div class="col-sm-6" style="padding-bottom: 10px;">
				<div class="main_head">
					<h1 style="">Sign Up with Skills On Demand</h1>
					<hr class="light my-4 demad">
				</div>

				<!-- **************Client Registration Start************** -->
				<sf:form method="post" modelAttribute="clientModel">
					<div class="form-group">
						<label>Username:</label>
						<sf:input path="client_username" cssClass="form-control" />
						<sf:errors path="client_username"
							cssClass="form-control error text-danger" />
					</div>
					<div class="form-group">
						<label>Mobile No:</label>
						<sf:input path="client_phonenum" cssClass="form-control" />
						<sf:errors path="client_phonenum"
							cssClass="form-control error text-danger" />
					</div>
					<div class="form-group">
						<label>Address:</label>
						<sf:input path="client_address" cssClass="form-control" />
						<sf:errors path="client_address"
							cssClass="form-control error text-danger" />
					</div>
					<div class="form-group">
						<label>Email:</label>
						<sf:input path="client_email" cssClass="form-control" />
						<sf:errors path="client_email"
							cssClass="form-control error text-danger" />
					</div>

					<div class="form-group">
						<label>Password:</label>
						<sf:password path="client_password" cssClass="form-control" />
						<sf:errors path="client_password"
							cssClass="form-control error text-danger" />
					</div>

					<div class="form-group">
						<label>Please confirm your password: </label>
						<sf:password path="client_confirmpassword" cssClass="form-control" />
						<sf:errors path="client_confirmpassword"
							cssClass="form-control error text-danger" />
					</div>
					<div class="form-check">
						<label class="form-check-label"> <%-- <sf:checkbox
								path="remember" cssClass="form-check-input" />Keep me loged on --%>
						</label>
					</div>
					<button type="submit" class="btn btn-info sub_btn register"
						name="_eventId_submit">Sign Up</button>
					<button type="submit" class="btn btn-danger sub_btn register"
						name="_eventId_home">Cancel</button>
					<div class="change_link change_link_common">
						<p>
							Already a member ? <a href="${context}/clientLogin"
								class="to_register">Login</a>
						</p>
					</div>
				</sf:form>
				<!-- **************Client Registration Start************** -->


			</div>
		</div>
	</div>

	<%@include file="../../shared/Footer.jsp"%>
</body>
</html>