<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="jstl" uri="http://java.sun.com/jsp/jstl/core"%>
<spring:url var="images" value="/resources/images" />

<nav class="navbar navbar-expand-lg bg-dark navbar-light fixed-top"
	id="mainNav">
	<div class="container">
		<a href="${context}"><img src="${images}/logo-SOD.png"
			alt="sod logo" class="sod-logo"> </a>
		<button class="navbar-toggler navbar-toggler-right" type="button"
			data-toggle="collapse" data-target="#navbarResponsive"
			aria-controls="navbarResponsive" aria-expanded="false"
			aria-label="Toggle navigation">
			<span class="navbar-toggler-icon"></span>
		</button>
		<div class="collapse navbar-collapse" id="navbarResponsive">
			<ul class="navbar-nav ml-auto">
				<li class="nav-item"><a class="nav-link js-scroll-trigger"
					href="${context}">Home</a></li>
				<li class="nav-item"><a class="nav-link js-scroll-trigger"
					href="${context}/AboutUs">About us</a></li>
				<%-- <li class="nav-item"><a class="nav-link js-scroll-trigger"
					href="${pageContext.request.contextPath}/services/servicePage">Services</a></li> --%>
				<li class="nav-item"><a class="nav-link js-scroll-trigger"
					href="${context}/Technologies">Technologies</a></li>
				<li class="nav-item"><a class="nav-link js-scroll-trigger"
					href="${context}/ContactUs">Contact us</a></li>
			  
				
				

			</ul>
		</div>

			<div id="drop" class="dropdown ">
				<a href="#" class="dropdown-toggle user_main" data-toggle="dropdown"
					role="button" aria-haspopup="true" aria-expanded="true" onclick="myFunc();">
					Packages<span class="caret"></span>
				</a>
				<ul id="menu" class="dropdown-menu">
					<%-- <li>${pack}</li> --%>
					<jstl:forEach items="${packNameList}" var="packs">
						<li><a href="${context}/packs/${packs.id}"><jstl:out value="${packs.pack_name}" /></a></li>
					</jstl:forEach>
					<!-- <li><a href="#" target="_blank">Basic</a></li>
					<li><a href="#">Pro</a></li>
					<li><a href="#">Premium</a></li> -->
				</ul>
			</div>
		<form class="form-inline my-2 my-lg-0">

			<div class="dropdown user_logout">
				<a href="#" class="dropdown-toggle user_main" data-toggle="dropdown"
					role="button" aria-haspopup="true" aria-expanded="false"> <span
					class="fa fa-user-circle user_nav_icon" aria-hidden="true">
				</span> Demo <span class="caret"></span></a>
				<ul class="dropdown-menu">
					<!-- <li><a href="#" target="_blank"><span
							class="glyphicon glyphicon-cog" aria-hidden="true"></span> User
							Settings</a></li> -->
					<li><a href="/customer/logoutCust"><span
							class="glyphicon glyphicon-log-out" aria-hidden="true"></span>
							Logout</a></li>
				</ul>
			</div>


			<%-- <div class="dropdown user_logout">
                <a href="#" class="dropdown-toggle user_main" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">
          <span class="fa fa-user-circle user_nav_icon" aria-hidden="true">
          </span><%=session.getAttribute("user").toString() %><span class="caret"></span></a>
                <ul class="dropdown-menu">
                    <li><a href="#" target="_blank"><span class="glyphicon glyphicon-cog" aria-hidden="true"></span> User Settings</a></li>
                    <li><a href="/SkillsOnDemand/customer/logoutCust"><span class="glyphicon glyphicon-log-out" aria-hidden="true"></span> Logout</a></li>
                </ul>
            </div> --%>

		</form>


		<%-- <div class="header-search-wrapper">
			<span class="search-main"> <i
				class="fa fa-search nav_search_icon"></i>
				<div class="search-form-main clearfix">
					<form role="search" method="get" class="search-form"
						action="/searchP/searchRes">
						<label> <span class="screen-reader-text"></span> <input
							type="search" class="search-field" placeholder="search"
							name="searchTxt">
						</label> <input type="submit" class="search-submit" value="search">
					</form>
				</div>
			</span>
		</div> --%>

		<%-- 
		<!-- <%
			if (session.getAttribute("jobseeker") != null) {
		%>

		<form class="form-inline my-2 my-lg-0">

			<div class="dropdown user_logout">
				<a href="#" class="dropdown-toggle user_main" data-toggle="dropdown"
					role="button" aria-haspopup="true" aria-expanded="false"> <span
					class="fa fa-user-circle user_nav_icon" aria-hidden="true">
				</span><%=session.getAttribute("jobseeker").toString()%><span
					class="caret"></span></a>
				<ul class="dropdown-menu">
					<!-- <li><a href="#" target="_blank"><span
							class="glyphicon glyphicon-cog" aria-hidden="true"></span> User
							Settings</a></li> -->
					<li><a
						href="${pageContext.request.contextPath}/jobseeker/logoutJobSeeker"><span
							class="glyphicon glyphicon-log-out" aria-hidden="true"></span>
							Logout</a></li>
				</ul>
			</div>


			<div class="dropdown user_logout">
                <a href="#" class="dropdown-toggle user_main" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">
          <span class="fa fa-user-circle user_nav_icon" aria-hidden="true">
          </span><%=session.getAttribute("user").toString() %><span class="caret"></span></a>
                <ul class="dropdown-menu">
                    <li><a href="#" target="_blank"><span class="glyphicon glyphicon-cog" aria-hidden="true"></span> User Settings</a></li>
                    <li><a href="/SkillsOnDemand/customer/logoutCust"><span class="glyphicon glyphicon-log-out" aria-hidden="true"></span> Logout</a></li>
                </ul>
            </div>

		</form>


		<div class="header-search-wrapper">
			<span class="search-main"><i
				class="fa fa-search nav_search_icon"></i>
				<div class="search-form-main clearfix">
					<form role="search" method="get" class="search-form"
						action="${pageContext.request.contextPath}/searchP/searchRes">
						<label> <span class="screen-reader-text"></span> <input
							type="search" class="search-field" placeholder="search"
							name="searchTxt">
						</label> <input type="submit" class="search-submit" value="search">
					</form>
				</div></span>
		</div>
		<%
			}
		%> -->
 --%>
	</div>
</nav>
<script type="text/javascript">
	function myFunc(){
		var dropdown = document.getElementById("drop");
		var menu = document.getElementById("menu");
		if(dropdown.classList.contains("show")){
			dropdown.classList.remove("show");
			menu.classList.remove("show");
		}else{
			dropdown.classList.add("show");
			menu.classList.add("show");
		}
	}
</script>