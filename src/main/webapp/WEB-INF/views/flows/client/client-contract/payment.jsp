<%-- <%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="jstl" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<!DOCTYPE HTML>
<spring:url var="css" value="/resources/css"></spring:url>
<spring:url var="images" value="/resources/images"></spring:url>
<spring:url var="js" value="/resources/js"></spring:url>
<spring:url var="bootstrapCss"
	value="/resources/bootstrap-4.0.0-dist/css" />
<spring:url var="bootstrapJs" value="/resources/bootstrap-4.0.0-dist/js" />
<spring:url var="bootstrap" value="/resources/bootstrap-4.0.0-dist" />
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<meta charset="utf-8">
<meta name="viewport"
	content="width=device-width, initial-scale=1, shrink-to-fit=no">
<!--For Fabvicon Icon-->
<link rel="shortcut icon" type="image/png" href="${images}/favicon.png" />
<link href="${bootstrapCss}/bootstrap.min.css" rel="stylesheet">
<!--For Toggle button Responsive on Mobile-->
<script src="${bootstrap}/jquery/jquery-3.3.1.min.js"></script>
<!--For Toggle button Responsive on Mobile-->
<script src="${bootstrapJs}/bootstrap.min.js"></script>
<!--For Fa icon-->
<link href="${bootstrap}/font-awesome/css/font-awesome.min.css"
	rel="stylesheet">
<!--Custom Css-->
<link href="${css}/style_1.css" rel="stylesheet" type="text/css">
<!--Responsive Css-->
<link href="${css}/responsive.css" rel="stylesheet" type="text/css">

<title>Payment Gateway</title>
</head>
<body oncontextmenu="return false;"  >
	<!-- Navigation -->
	<nav class="navbar navbar-expand-lg bg-dark navbar-light fixed-top"
		id="mainNav">
		<div class="container">
			<a href="index.html"><img src="${images}/logo-SOD.png"
				alt="sod logo" class="sod-logo"> </a>
			
		</div>
	</nav>


	<!--payment---gateway-------->
	<section class="payment_gateway">
		<div class="container-fluid">
			<div class="row payment_detail">
				<!--<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 col-xl-12 ">-->
				<!--<div class="back_page">-->
				<!--<a class="fa fa-arrow-left" href="contract_management.html"> Go To Back</a>-->
				<!--</div>-->
				<!--</div>-->
				<div
					class="col-xs-6 col-sm-12 col-md-12 col-lg-10 col-xl-5 mx-auto payment_main">

					<div class="row payment_head ">
						<div class="col-xs-6 col-sm-6 col-md-6 col-lg-6 col-xl-6 ">
							<h3>Payment Details</h3>
						</div>
						<div class="col-xs-6 col-sm-6 col-md-6 col-lg-6 col-xl-6">
							<img class="img-responsive pull-right"
								src="http://i76.imgup.net/accepted_c22e0.png">
						</div>
					</div>


					<div
						class="col-xs-12 col-sm-12 col-md-12 col-lg-12 col-xl-12 payment_form">
						<!--************* Payment Form starts *************-->
						 <form:form method="POST" modelAttribute="paymentModel" >

							<div class="form-group">
								<label>CARD NUMBER:</label>
								<div class="input-group">
									 <form:input path="cardNumber" type="text" cssClass="form-control" 
										placeholder="XXXX XXXX XXXX XXXX" autocomplete="cc-number"
										required="true" id="cardNum"  pattern=".{13,16}"  title="Input valid card number" maxlength="16"/>
									
									<div class="input-group-append">
										<span class="input-group-text red lighten-3" id="basic-text1"><a
											href="#"><i class="fa fa-credit-card" aria-hidden="true"></i></a></span>
									</div>
									<form:errors path="cardNumber"/>
								</div>
							</div>
							
							<div class="form-group">
								<label>CARD TYPE</label> <form:select id="CreditCardType" path="cardType"
									name="CreditCardType" cssClass="form-control" required="true" readonly="true">
									<form:option  value="null">Card Type</form:option>
									<form:option value="Visa">Visa</form:option>
									<form:option value="MasterCard">MasterCard</form:option>
									<form:option value="AmericanExpress">American Express</form:option>
									<form:option value="Discover">Discover</form:option>
								</form:select>
								<form:errors path="cardType"/>
							</div>
							<div class="form-group">
								<label>CARD HOLDER NAME:</label>
								<div class="input-group">
									<form:input type="text" cssClass="form-control" path="cardHolderName" 
										placeholder="Card Holder Name" 
										required="true"  />
									<form:errors path="cardHolderName"/>
								</div>
							</div>
							<div class="row">
								<div class="col-xs-6 col-sm-12 col-md-6 col-lg-6 col-xl-6 ">
									<div class="form-group">
										<label>Card Expiry Date</label> <form:input type="month" path="expiryDate"
											cssClass="form-control" name="cardExpiry" placeholder="MM / YY"
											id="dateExpiry" autocomplete="cc-exp" required="true" />
											<form:errors path="expiryDate"/>
									</div>
								</div>
								<div class="col-xs-6 col-sm-12 col-md-6 col-lg-6 col-xl-6">
									<div class="form-group">
										<label for="cardCVC">CVV/CVC CODE</label> <form:password
											cssClass="form-control" name="cardCVC" placeholder="CVV/CVC"
											autocomplete="cc-csc" required="true" id="cvvCard" path="cardCvv"/>
										<p id="whatCvv" style="font-size: 10px; color: #000000;">
											Where is my CVV?</p>
										<form:errors path="cardCvv"/>
										<div id="windowCvv"
											style="top: 0px; right: 0px; width: 250px; display: none; position: absolute; overflow: visible; 
											background-color: #dedede; font-size: 10px; padding: 4px 4px; border: 1px solid #ccc; border-radius: 5px;">
											<b>Visa�, Mastercard�, and Discover� cardholders:</b>
											<p>Turn your card over and look at the signature box. You
												should see either the entire 16-digit credit card number or
												just the last four digits followed by a special 3-digit
												code. This 3-digit code is your CVV number / Card Security
												Code.</p>
											<b>American Express� cardholders:</b>
											<p>Look for the 4-digit code printed on the front of your
												card just above and to the right of your main credit card
												number. This 4-digit code is your Card Identification Number
												(CID). The CID is the four-digit code printed just above the
												Account Number.</p>
											<!-- <iframe  src="https://www.cvvnumber.com/cvv.html" style="background-color: #ffffff; ">
										</iframe> -->
										</div>
									</div>
								</div>
							</div>
							<!-- <div class="form-group">
								<label for="couponCode">COUPON CODE</label> <input type="text"
									class="form-control" name="couponCode" />
							</div> -->
							<button class="btn btn-info btn-lg btn-block" type="submit" name="_eventId_submitPay">Submit</button>

						</form:form> 
						<!--************* Payment Form starts *************-->
						
						
					</div>


				</div>
			</div>
		</div>


	</section>
	<!--------------footer-------------->
	<footer class="footer bg-dark">
		<div class="container">
						<div class="row">
				<div class="col-xs-4 col-sm-4 col-md-4 col-lg-4 col-xl-3">
					<div class="">
						<p class="text-white">SKILLS ON DEMAND</p>
						<p style="color: #ccc;">
							In publishing and graphic design, lorem ipsum is a placeholder
							text commonly used to demonstrate the visual... <a
								href="about_us.html">READ MORE</a>
						</p>
						<!-- <img src="" alt="Logo" class="text-white"> -->

					</div>
				</div>
				<div class="col-xs-4 col-sm-4 col-md-4 col-lg-4 col-xl-3">
					<div class="recent_news">
						<p class="text-white">RECENT NEWS</p>
					</div>

					<div class="footer_pg_list">
						<ul>
							<li><a href="index.html">Home</a></li>
							<li><a href="about_us.html">About us</a></li>
							<li><a href="contact_us.html">Contact Us</a></li>
							<!--<li><a href="services.html">Services</a></li>-->
							<li><a href="application_form.html">Careers</a></li>
						</ul>
					</div>
				</div>
				<div class="col-xs-4 col-sm-4 col-md-4 col-lg-4 col-xl-3">
					<div class="footer_categories">
						<p class="text-white">CATEGORIES</p>
					</div>

					<div class="footer_pg_list">
						<ul>
							<li><a href="#">Business</a></li>
							<li><a href="#">Design</a></li>
							<li><a href="#">Real life</a></li>
							<li><a href="#">Science</a></li>
							<li><a href="#">Tech</a></li>
						</ul>
					</div>
				</div>
				<div class="col-xs-4 col-sm-4 col-md-4 col-lg-4 col-xl-3">
					<div class="footer_subsc">
						<p class=" text-white">SUBSCRIBE</p>
					</div>
					<div class="Footer_res">
						<p style="color: #fff;">Get monthly updates and free
							resources.</p>
					</div>
					<div class="media-container-column" data-form-type="formoid">
						<form class="form-inline">
							<input type="hidden" value="" data-form-email="true">
							<div class="form-group">
								<input type="email"
									class="form-control input-sm input-inverse my-2" name="email"
									required="" data-form-field="Email" placeholder="Email"
									id="email-footer-3h">
							</div>
							<div class="input-group-btn">
								<button href="" class="btn btn-info footer_button" type="submit"
									role="button">Subscribe</button>
							</div>
						</form>
					</div>

				</div>
			</div>


			<div
				class="col-xs-12 col-sm-12 col-md-12 col-lg-12 col-xl-12 footer-lower">
				<hr class="footer_line">
				<div class="row">
					<div class="col-xs-6 col-sm-6 col-md-6 col-lg-6 col-xl-7">
						<div class="footer_copyright">
							<p class="text-white">� Copyright 2018 Teklink - All Rights
								Reserved</p>
						</div>
					</div>
					<!-- 	<div
						class="col-xs-6 col-sm-6 col-md-6 col-lg-6 col-xl-5 text-right">
						<div class="social_list">
							<ul>
								<li><a href="#" target="_blank"> <span
										class="fa fa-twitter" media-simple="true"></span>
								</a></li>
								<li><a href="#" target="_blank"> <span
										class="fa fa-facebook" media-simple="true"></span>
								</a></li>
								<li><a href="#" target="_blank"> <span
										class="fa fa-instagram" media-simple="true"></span>
								</a></li>
								<li><a href="#" target="_blank"> <span
										class="fa fa-youtube" media-simple="true"></span>
								</a></li>
								<li><a href="#" target="_blank"> <span
										class="fa fa-google-plus" media-simple="true"></span>
								</a></li>
							</ul>

						</div>
					</div> -->
				</div>
			</div>
		</div>
	</footer>

	<script type="text/javascript" src="https://js.stripe.com/v2/"></script>
	<script type="text/javascript" src="${js}/myjs/myjs.js"></script>

</body>
</html>




 --%>