<!-- @author Prateek Raj -->

<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="jstl" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="sf" uri="http://www.springframework.org/tags/form"%>
<!DOCTYPE html >
<jstl:set var="context" value="${pageContext.request.contextPath}" />
<spring:url var="css" value="/resources/css"></spring:url>
<spring:url var="images" value="/resources/images"></spring:url>
<spring:url var="boots400" value="/resources/bootstrap-4.0.0-dist"></spring:url>
<spring:url var="js" value="/resources/js"></spring:url>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="stylesheet" href="${css}/bootstrap-select.css">
<link rel="stylesheet"
	href="https://maxcdn.bootstrapcdn.com/bootstrap/4.1.0/css/bootstrap.min.css">
<link rel="stylesheet"
	href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
<link rel="shortcut icon" type="image/png" href="${images}/favicon.png" />
<!-- <link rel="stylesheet"
	href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css"> -->
<%-- <link href="${boots400}/css/bootstrap.min.css" rel="stylesheet">
<!--For Toggle button Responsive on Mobile-->
<script src="${boots400}/jquery/jquery-3.3.1.min.js"></script>
<!--For Toggle button Responsive on Mobile-->
<script src="${boots400}/js/bootstrap.min.js"></script>
<!--For Fa icon-->
<link href="${boots400}/font-awesome/css/font-awesome.min.css"
	rel="stylesheet"> --%>
<link href="${css}/style_1.css" rel="stylesheet" type="text/css">
<link href="${css}/responsive.css" rel="stylesheet" type="text/css">
<link href="${css}/bootstrap-select.css" rel="stylesheet"
	type="text/css">
<title>Sign Up | SOD</title>
<style>
.error {
	padding: 1 1;
	border-color: red;
	background-color: #ffeaea;
	font-size: 12px;
}
</style>

</head>
<body class="app_form_main">
	<%@include file="../../shared/Header.jsp"%>


	<div class="container">
		<div class="row">
			<div
				class="col-xs-6 col-sm-12 col-md-6 col-lg-12 col-xl-7 mx-auto column_form">
				<div class="form_heading main_head text-center">
					<h1>Employment Application Form</h1>
					<hr class="light my-4 demad">
				</div>
				<sf:form modelAttribute="expertModel" method="post">
					<div class="row">
						<div class="col-xs-6 col-sm-12 col-md-6 col-lg-6 col-xl-6 ">
							<div class="form-group">
								<label class="name_form">First name : *</label>
								<sf:input path="firstName" cssClass="form-control"
									placeholder="Enter First Name" />
								<sf:errors path="firstName"
									cssClass="form-control error text-danger" />
							</div>
						</div>
						<div class="col-xs-6 col-sm-12 col-md-6 col-lg-6 col-xl-6 ">
							<div class="form-group">
								<label class="name_form">Last name : *</label>
								<sf:input path="lastName" cssClass="form-control"
									placeholder="Enter Last Name" />
								<sf:errors path="lastName"
									cssClass="form-control error text-danger" />
							</div>
						</div>
					</div>
					<div class="form-group">
						<label class="name_form">Address : *</label>
						<sf:input path="address" cssClass="form-control"
							placeholder="Enter Address" />
						<sf:errors path="address"
							cssClass="form-control error text-danger" />
					</div>
					<div class="form-group">
						<label class="name_form">Email : *</label>
						<sf:input path="email" cssClass="form-control"
							placeholder="Enter Email" />
						<sf:errors path="email" cssClass="form-control error text-danger" />
					</div>
					<div class="form-group">
						<label class="name_form">Password :</label>
						<sf:password path="password" cssClass="form-control"
							placeholder="Enter Password" />
						<sf:errors path="password"
							cssClass="form-control error text-danger" />
					</div>

					<div class="form-group">
						<label class="name_form">Please confirm your password :</label>
						<sf:password path="confirmPassword" cssClass="form-control"
							placeholder="Re-enter Password" />
					</div>
					<div class="form-group">
						<label class="name_form">Portfolio website :</label>
						<sf:input path="portfolio_website" cssClass="form-control"
							placeholder="http://" />
					</div>
					<div class="form-group">
						<label class="name_form">Position you are applying for : *</label>
						<sf:input path="position_appliedfor" cssClass="form-control"
							placeholder="Position" />
						<sf:errors path="position_appliedfor"
							cssClass="form-control error text-danger" />
					</div>
					<div class="form-group">
						<label class="name_form">Skills: *</label> <sf:select id="skill"
							cssClass="selectpicker form-control" title="Select Skills"
							path="skillList"
							 multiple="multiple"
							 tabindex="10"
							>
							
							<jstl:set var="skillGroupVar" value="" />
							<jstl:forEach items="${skillList}" var="skillListVar">
								<jstl:if test="${empty skillGroupVar}">
									<jstl:set var="skillGroupVar"
										value="${skillListVar.skillGroup}" />
									<optgroup label="${skillGroupVar}">
								</jstl:if>
								<jstl:if test="${skillGroupVar != skillListVar.skillGroup}">
									</optgroup>
									<jstl:set var="skillGroupVar"
										value="${skillListVar.skillGroup}" />
									<optgroup label="${skillGroupVar}">
								</jstl:if>
								<sf:option value="${skillListVar.id}">${skillListVar.skillName}</sf:option>
							</jstl:forEach>
							</optgroup>
						</sf:select>
						<sf:errors path="skillList"	cssClass="form-control error text-danger" />
					</div>
					<div class="form-group">
						<label class="name_form">Expected Salary :</label>
						<sf:input path="exp_salary" cssClass="form-control"
							placeholder="in numbers" />
					</div>
					<div class="row">
						<div class="col-xs-6 col-sm-12 col-md-6 col-lg-6 col-xl-6 ">
							<div class="form-group">
								<label class="name_form">Mobile No. : *</label>
								<sf:input path="mobileNumber" cssClass="form-control"
									placeholder="Enter Mobile Number" />
								<sf:errors path="mobileNumber"
									cssClass="form-control error text-danger" />
							</div>
						</div>
						<div class="col-xs-6 col-sm-12 col-md-6 col-lg-6 col-xl-6 ">
							<div class="form-group">
								<label class="name_form">Fax :</label>
								<sf:input path="faxNumber" cssClass="form-control"
									placeholder="Enter Fax" />
							</div>
						</div>
					</div>
					<div class="form-group">
						<label class="name_form">Last company you work for :</label>
						<sf:input path="prev_companyName" cssClass="form-control"
							placeholder="Enter Company Name" />
					</div>
					<div class="form-group" data-for="message">
						<label class="name_form">Reference / Comments / Question :</label>
						<sf:textarea path="message" cssClass="form-control" rows="7" />
					</div>
					<sf:hidden path="role" value="ROLE_EXPERT"/>
					<div class="row">
						<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 col-xl-12">
							<span class="input-group-btn ">
								<button type="submit" class="btn btn-danger  sub_btn register"
									name="_eventId_home">Cancel</button>
								<button type="submit" class="btn btn-info  sub_btn register"
									name="_eventId_login">Register</button>
							</span>
						</div>
					</div>
					<div><p style="color:#fff;">Already a member ?&ensp; <a href="${context}/expertLogin" style="color:#fff;">Login</a> </p>
					</div>
				</sf:form>
			</div>

		</div>
	</div>



	<%@include file="../../shared/Footer.jsp"%>


	<script
		src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
	<script
		src="https://maxcdn.bootstrapcdn.com/bootstrap/4.1.0/js/bootstrap.bundle.min.js"></script>
	<script src="${js}/bootstrap-select/bootstrap-select.js"></script>
	<script>
		$(document).ready(function() {
			$(".selectpicker").selectpicker({
				liveSearch : true
			});
		});
	</script>
</body>
</html>