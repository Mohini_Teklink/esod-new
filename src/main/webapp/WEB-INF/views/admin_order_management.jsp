<!--  @author Hardev Singh -->
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>

<%@taglib prefix="jstl" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@taglib prefix="sf" uri="http://www.springframework.org/tags/form"%>

<!DOCTYPE html >

<spring:url var="css" value="/resources/css"></spring:url>
<spring:url var="images" value="/resources/images"></spring:url>
<spring:url var="js" value="/resources/js"></spring:url>
<spring:url var="vendor" value="/resources/vendor"></spring:url>
<spring:url var="dataTable" value="/resources/DataTables"></spring:url>
<spring:url var="productImage" value="/resources/productImages"></spring:url>


<html>
<head>
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="description" content="">
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta name="robots" content="all,follow">
<link rel="shortcut icon" type="image/png" href="${images}/favicon.jpg" />
<!-- Bootstrap CSS-->
<link rel="stylesheet" href="${vendor}/bootstrap/css/bootstrap.min.css">
<!-- Font Awesome CSS-->
<link rel="stylesheet"
	href="${vendor}/font-awesome/css/font-awesome.min.css">
<!-- Fontastic Custom icon font-->
<link rel="stylesheet" href="${css}/fontastic.css">
<!-- Google fonts - Roboto -->
<link rel="stylesheet"
	href="https://fonts.googleapis.com/css?family=Roboto:300,400,500,700">
<!-- jQuery Circle-->
<link rel="stylesheet"
	href="${css}/grasp_mobile_progress_circle-1.0.0.min.css">
<!-- Custom Scrollbar-->
<link rel="stylesheet"
	href="${vendor}/malihu-custom-scrollbar-plugin/jquery.mCustomScrollbar.css">
<!-- theme stylesheet-->
<link rel="stylesheet" href="${css}/style.default.css"
	id="theme-stylesheet">
<!-- Custom stylesheet - for your changes-->
<link rel="stylesheet" href="${css}/custom.css">
<!-- Toogle Slider -->
<link rel="stylesheet" href="${css}/toggle_slider.css">
<!-- Favicon-->
<link rel="shortcut icon" href="${images}/favicon.ico">

<link rel="stylesheet" type="text/css"
	href="${dataTable}/datatables.css">



<title>Order Management</title>
<!-- Tweaks for older IEs-->
<!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script><![endif]-->

<!-- Query for click handler in toogle  -->
<script
	src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>

</head>
<body>
	<%@ include file="shared/shared-admin/Header.jsp"%>
	<div class="page">
		<%@include file="shared/shared-admin/Navbar.jsp"%>
		<!--Main Section Start-->

		<section class="dashboard-counts">
			<section class="product_main">
				<div class="row">
					<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 col-xl-12">

						<div class="x_panel text-muted">
							<div class="x_title">
								<h1>Order Management List</h1>
								<div class="clearfix"></div>
							</div>

							<table id="table_id" class="display">
								<thead>
									<tr>
										<th>Image</th>
										<th>Order no</th>
										<th>Product Id</th>
										<th>Product Name</th>
										<th>Product Description</th>
										<th>Project Under</th>
										<th>Project Price</th>
										<th>Status</th>
										<th>View</th>
									</tr>
								</thead>
								<jstl:forEach items="${ProductOrderList}" var="orderlist">
									<tr style="height: 90px;">
										<td class="hovereffect"><img
											src="${productImage}/${orderlist.clientProducts.prodImagePath}"
											class="Img_prd_a img-responsive"></td>
										<!-- <td width="10%">
									<ul class="list-inline">
										<li><img src=${productImage}/${orderlist.clientProducts.prodImagePath}" width="75px" height="auto" 
											alt="Avatar"></li>
									</ul>
								</td> -->
										<td width="4%"><jstl:out
												value="${orderlist.expertContractManagement.id}" /></td>
										<td width="4%"><jstl:out
												value="${orderlist.clientProducts.prodId}" /></td>
										<td>${orderlist.clientProducts.prodName}</td>
										<td><jstl:out
												value="${orderlist.clientProducts.prodDescription}" /></td>
										<td>${orderlist.expertContractManagement.firstName}
											&nbsp;${orderlist.expertContractManagement.lastName}</td>
										<td width="8%">
											<h4>
												<jstl:out value="${orderlist.clientProducts.prodPrice}" />
											</h4>
										</td>
										<td><jstl:choose>
												<jstl:when test="${orderlist.clientProducts.status==true}"> Active</jstl:when>
												<jstl:otherwise>Cancel </jstl:otherwise>
											</jstl:choose></td>

										<td width="4%">
											<div class="pr_desc_button">
												<a
													href="/SOD/admin/dashboard/order_description/id/${orderlist.expertContractManagement.id}"
													class="btn btn-primary btn-xs"> View </a>
											</div>
										</td>
									</tr>
								</jstl:forEach>
							</table>
						</div>
					</div>
				</div>
			</section>
		</section>
		<%@ include file="shared/shared-admin/Footer.jsp"%>
	</div>
	<!--Main Section End
      <!--Footer Section Start-->

	<script type="text/javascript" charset="utf8"
		src="${dataTable}/datatables.js"></script>

	<script type="text/javascript">
		$(document).ready(function() {
			$('#table_id').DataTable();
		});
	</script>
</body>
</html>

