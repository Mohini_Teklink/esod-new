
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="jstl" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<!DOCTYPE HTML>
<spring:url var="css" value="/resources/css"></spring:url>
<spring:url var="images" value="/resources/images"></spring:url>
<spring:url var="js" value="/resources/js"></spring:url>
<spring:url var="bootstrapCss"
	value="/resources/bootstrap-4.0.0-dist/css" />
<spring:url var="bootstrapJs" value="/resources/bootstrap-4.0.0-dist/js" />
<spring:url var="bootstrap" value="/resources/bootstrap-4.0.0-dist" />
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<meta charset="utf-8">
<meta name="viewport"
	content="width=device-width, initial-scale=1, shrink-to-fit=no">
<!--For Fabvicon Icon-->
<link rel="shortcut icon" type="image/png" href="${images}/favicon.png" />
<link href="${bootstrapCss}/bootstrap.min.css" rel="stylesheet">
<!--For Toggle button Responsive on Mobile-->
<script src="${bootstrap}/jquery/jquery-3.3.1.min.js"></script>
<!--For Toggle button Responsive on Mobile-->
<script src="${bootstrapJs}/bootstrap.min.js"></script>
<!--For Fa icon-->
<link href="${bootstrap}/font-awesome/css/font-awesome.min.css"
	rel="stylesheet">
<!--Custom Css-->
<link href="${css}/style_1.css" rel="stylesheet" type="text/css">
<!--Responsive Css-->
<link href="${css}/responsive.css" rel="stylesheet" type="text/css">
</head>


<body>

	<%@include file="shared/Header.jsp"%>
	

	<!-- <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 col-xl-12 "> -->
		<div class="basic_head">
			<h1>Edit Profile</h1>
		</div>
	</div>
	<!-- <div class="row contract_main">
		<div class="col-xs-6 col-sm-6 col-md-6 form_cmgmt"> -->


			<form method="POST" action="/SOD/expert/profileEdit"
				enctype="multipart/form-data">
				<div class="container">
					<!-- <h2>Profile</h2> -->
					<!-- Trigger the modal with a button -->
					<h3>
						Name:   ${expert.firstName}&nbsp;${expert.lastName }
						<!-- <button type="button" data-toggle="modal" data-target="#myModal">Edit</button> -->
						<br> <br>
					</h3>
					<h3>
						Email:   ${expert.email}
						<button type="button" data-toggle="modal" data-target="#myModal2">Edit</button>
					</h3>
					<h3>
						Mobile:   ${expert.mobileNumber}
						<button type="button" data-toggle="modal" data-target="#myModal3">Edit</button>
					</h3>
					<!-- Modal -->
					
					
					<div class="modal fade" id="myModal" role="dialog">
						<div class="modal-dialog">

							<!-- Modal content-->
							<div class="modal-content">
								<div class="modal-header">
									<button type="button" class="close" data-dismiss="modal">&times;</button>
									<h4 class="modal-title">Change Name</h4>
								</div>
								<%-- <form:form action="${contextRoot}/update-name" method="post"> --%>
								<div class="modal-body">
									<p>Enter the new Name</p>
									<input type="text" name="new_name" required>
								</div>
								<!-- <div class="form-group">
			<input class="btn btn-default" value="Send" type="submit">  
		</div> -->
								<div class="modal-footer">
									<input class="btn btn-default" value="Update Name"
										type="submit">
									<button type="button" class="btn btn-default"
										data-dismiss="modal">Close</button>
								</div>
								<%-- </form:form> --%>
							</div>

						</div>
					</div>
				</div>
			</form>
		</div>
		<!-- Modal -->
		<div class="modal fade" id="myModal2" role="dialog">
			<div class="modal-dialog">

				<!-- Modal content-->
				<div class="modal-content">
					<div class="modal-header">
						<button type="button" class="close" data-dismiss="modal">&times;</button>
						<h4 class="modal-title">Change email</h4>
					</div>
					<form:form action="${contextRoot}/update-email" method="POST">
						<div class="modal-body">
							<p>Enter the new email to change. After updating the email id
								you will have to login again.</p>
							<input id="emailInput" placeholder="email address" name="email"
								class="form-control" type="email"
								oninvalid="setCustomValidity('Please enter a valid email address!')"
								onchange="try{setCustomValidity('')}catch(e){}" required="">
						</div>
						<div class="modal-footer">
							<input class="btn btn-default" value="Update Email" type="submit">
							<button type="button" class="btn btn-default"
								data-dismiss="modal">Close</button>
						</div>
					</form:form>
				</div>

			</div>
		</div>

		<!-- Modal -->
		<div class="modal fade" id="myModal3" role="dialog">
			<div class="modal-dialog">

				<!-- Modal content-->
				<div class="modal-content">
					<div class="modal-header">
						<button type="button" class="close" data-dismiss="modal">&times;</button>
						<h4 class="modal-title">Change email</h4>
					</div>
					<form:form name="myForm"
						action="${contextRoot}/update-mobileNumber" method="post"
						onsubmit="return checkInp()">
						<div class="modal-body">
							<p>Enter the new mobile number to change. After updating the
								mobile number you will have to login again.</p>
							<input placeholder="Enter 10-digit Mobile Number"
								name="mobileNumber" class="form-control" type="text"
								oninvalid="setCustomValidity('Please enter a valid mobile number!')"
								onchange="try{setCustomValidity('')}catch(e){}" required="">
						</div>
						<div class="modal-footer">
							<input class="btn btn-default" value="Update Number"
								type="submit">
							<button type="button" class="btn btn-default"
								data-dismiss="modal">Close</button>
						</div>
					</form:form>
				</div>

			</div>
		</div>

	</div>

	<script>
		function checkInp() {
			var x = document.forms["myForm"]["mobileNumber"].value;
			var intRegex = /[0-9 -()+]+$/;
			if ((x.length < 10) || (!intRegex.test(x))) {
				alert('Please enter a valid phone number.');
				return false;
			}

		}
	</script>
	<%@include file="shared/Footer.jsp"%>
</body>
</html>
