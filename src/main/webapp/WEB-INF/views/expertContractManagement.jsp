<!-- @author Prateek Raj -->
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="jstl" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib uri="http://www.springframework.org/security/tags"
	prefix="sec"%>
<!DOCTYPE HTML>
<spring:url var="css" value="/resources/css"></spring:url>
<spring:url var="images" value="/resources/images"></spring:url>
<spring:url var="bootstrapCss"
	value="/resources/bootstrap-4.0.0-dist/css" />
<spring:url var="bootstrapJs" value="/resources/bootstrap-4.0.0-dist/js" />
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<meta charset="utf-8">
<meta name="viewport"
	content="width=device-width, initial-scale=1, shrink-to-fit=no">
<meta name="description" content="">
<meta name="author" content="Prateek Raj">
<link rel="shortcut icon" type="image/png" href="${images}/favicon.png" />
<link href="${bootstrapCss}/bootstrap.min.css" rel="stylesheet">
<script
	src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
<script src="${bootstrapJs}/bootstrap.min.js"></script>
<link href="${css}/style_1.css" rel="stylesheet" type="text/css">
<link href="${css}/responsive.css" rel="stylesheet" type="text/css">
<link rel="stylesheet"
	href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
<style type="text/css">
.error {
	padding: 4px 4px;
	border: none;
	border-radius: 0px;
	border-left: 5px solid red;
	font-size: 12px;
}
</style>
<title>Application Form</title>
</head>
<body>
	<%@ include file="shared/Header.jsp"%>
	<section class="term_condition_main">
		<div class="container">
			<div class="row">
				<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 col-xl-12 ">
					<div class="back_page">
						<a class="fa fa-arrow-left" href="/SOD/expert/jdp">&nbsp; Back</a>
					</div>
				</div>
				<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 col-xl-12 ">
					<div class="basic_head">
						<h1>Contract Management</h1>
					</div>
				</div>
			</div>
			<div class="row">
				<div class="col-xs-12 col-sm-12 col-md-12">
					<div>
						<p>A contract is a promise or set of promises which the law
							will enforce. Construction contracts typically relate to the
							supply of goods or services as part of the delivery of a built
							asset. Traditionally, suppliers might have been considered to be
							organisations contracted to provide physical supplies such as
							goods.</p>
					</div>
				</div>
			</div>

			<div class="row contract_main">
				<div class="col-xs-6 col-sm-6 col-md-6 form_cmgmt">

					<!--***************** Expert Contract Form Start *****************-->
					<%-- value="/client/finishWindow" --%>
					<jstl:url
						value="/expert/jdp/${productId}/application/submit?${_csrf.parameterName}=${_csrf.token}"
						var="register" />
					<form:form action="${register}" method="GET"
						modelAttribute="expertContractForm">
						<div class="form-group">
							<label>First Name</label>
							<form:input path="firstName" cssClass="form-control"
								placeholder="Enter First Name" />
							<%-- <form:errors path="firstName" cssClass="text-danger error" /> --%>
						</div>
						<form:hidden path="productId" value="${productId}" />

						<div class="form-group">
							<label>Last Name</label>
							<form:input path="lastName" cssClass="form-control"
								placeholder="Enter Last Name" />
							<form:errors path="lastName" cssClass="text-danger error" />
						</div>
						<div class="form-group">
							<label>Summary</label>
							<form:textarea cssClass="form-control" rows="7" path="summary"
								placeholder="Tell us a bit about yourself"></form:textarea>
							<form:errors path="summary" cssClass="text-danger error" />
						</div>
						<div class="row">
							<!-- <div class="col-xs-6 col-sm-6 col-md-6">
								<div class="form-group">
									<label>Upload Image:</label>
									<div class="form-group inputDnD">
										<label class="sr-only" for="inputFile">File Upload</label> <input
											type="file" name="photo"
											class="form-control-file text-info font-weight-bold"
											id="inputFile" accept="image/*" onchange="readUrl(this)"
											data-title="Drag and drop a file" />
									</div>
								</div>
							</div> -->
							<!-- <div class="col-xs-6 col-sm-6 col-md-6">
								<div class="form-group">
									<label>Upload Signature:</label>
									<div class="form-group inputDnD">
										<label class="sr-only" for="inputFile">Scan Signature
											File Upload</label> <input type="file" name="sign"
											class="form-control-file text-info font-weight-bold"
											id="inputsignFile" accept="image/*" onchange="readUrl(this)"
											data-title="Drag and drop a file">
									</div>
								</div>
							</div> -->
						</div>
						<div class="form-group">
							<form:checkbox path="accepted" id="checkbox" />
							I agree to the <label class="text-danger font-weight-bold">Terms
								of Service</label><br />
							<form:errors path="accepted" cssClass="text-danger error" />
						</div>
						<div>
							<jstl:if test="${! empty error}">
								<div class="alert alert-danger">${error}</div>
							</jstl:if>
							<button class="btn btn-success register" type="submit">Submit</button>
						</div>
					</form:form>
					<!--***************** Expert Contract Form Start *****************-->
				</div>


				<div class="col-xs-6 col-sm-6 col-md-6">
					<div class="scrollbar" id="style-default">
						<div class="force-overflow">
							<div class="tc_row">
								<h3 class="tc_head">General Terms :</h3>
								<p class="trm_cnd_pgrph">In publishing and graphic design,
									lorem ipsum is a placeholder text commonly used to demonstrate
									the visual form of a document without relying on meaningful
									content (also called greeking).
								<ol class="term_row">
									<li>Read all ours terms below moreover we can change our
										terms at any time without any prior notice.</li>
									<li>And management decision will be final in any legal
										issue.</li>
								</ol>
							</div>
							<div class="tc_row">
								<h3 class="tc_head">Terms of Service :</h3>
								<p class="trm_cnd_pgrph">In publishing and graphic design,
									lorem ipsum is a placeholder text commonly used to demonstrate
									the visual form of a document without relying on meaningful
									content (also called greeking). Replacing the actual content
									with placeholder text allows designers to design the form of
									the content.</p>
								<ol class="term_row">
									<li>Read all ours terms below moreover we can change our
										terms at any time without any prior notice.</li>
									<li>And management decision will be final in any legal
										issue.</li>
								</ol>
							</div>

							<div class="tc_row">
								<h3 class="tc_head">Privacy Policy :</h3>
								<ol class="term_row">
									<li>Read all ours terms below moreover we can change our
										terms at any time without any prior notice. And management
										decision will be final in any legal issue.</li>
									<li>Read all ours terms below moreover we can change our
										terms at any time without any prior notice. And management
										decision will be final in any legal issue.</li>
								</ol>
							</div>
							<div class="tc_row">
								<h3 class="tc_head">Payment Terms :</h3>
								<p class="trm_cnd_pgrph">In publishing and graphic design,
									lorem ipsum is a placeholder text commonly used to demonstrate
									the visual form of a document without relying on meaningful
									content (also called greeking). Replacing the actual content
									with placeholder text allows designers to design the form of
									the content.</p>
								<ol class="term_row">
									<li>Read all ours terms below moreover we can change our
										terms at any time without any prior notice.</li>
									<li>And management decision will be final in any legal
										issue.</li>
								</ol>
							</div>
							<div class="tc_row">
								<h3 class="tc_head">Payment Methods :</h3>
								<ol class="term_row">
									<li>Read all ours terms below moreover we can change our
										terms at any time without any prior notice.</li>
									<li>And management decision will be final in any legal
										issue.</li>
								</ol>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</section>




	<%@ include file="shared/Footer.jsp"%>

	<script>
		$("form").submit(function() {
			console.log($(this).serialize());
			console.log("Hello");
		});
	</script>
</body>
</html>