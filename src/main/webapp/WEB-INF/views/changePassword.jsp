<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="jstl" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib uri="http://www.springframework.org/security/tags"
	prefix="sec"%>
<!DOCTYPE html >
<html>
<head>
	<title>	Change password</title>
	<meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css">
<style type="text/css">
	.ForgetPage {
    margin-top: 90px;
        width: max-content;
    background: #ffffffc9;
    box-shadow: 0px 0 35px 6px #0000002b;
    border-radius: 10px;
    height: 50vh;
}
.ForgetPageHedading {
    margin-top: 20px;
}
</style>
<style>
.error {
	padding: 1 1;
	border: none;
	margin-top: 8px;
	border-radius: 0px;
	border-left: 5px ;
	background-color: #dc354557;
	font-size: 12px;
	font-style: italic;
}
</style>
</head>
<body>
   <div class="container ForgetPage">
     <div class="row">     
      <div class="col-md-12">
      	<div class="ForgetPageHedading">
      		<h2 class="text-center">Change password</h2>
      	</div>
      	
      	<jstl:if test="${!empty expert}">
      	<jstl:set var="loginUser" value="expert"></jstl:set>
      	</jstl:if>
      	<jstl:if test="${!empty client}">
      	<jstl:set var="loginUser" value="client"></jstl:set>
      	</jstl:if>
      	<div class="ForgetPageForm">
      	  <form  method="POST" action="/SOD/${loginUser}/changePassword?${_csrf.parameterName}=${_csrf.token}"
      	  onsubmit="false" >
      	  <div class="form-group">
		     <label for="exampleInputPassword">Old Password</label>
		     <input name="old_password" type="password"  placeholder="old password" class="form-control" />
		   	</div>
		   
			<div class="form-group">
		     <label for="exampleInputPassword">New Password</label>
		     <input name="new_password" type="password"  placeholder="new password" class="form-control" />
		   	</div>
			<div class="form-group">
			 <label for="exampleInputPassword">Re-Enter New Password</label>
			 <input name="confirmpassword" type="password"  placeholder="re enterPassword" class="form-control" />
			 </div>
			 <sec:csrfInput />
			 
			 <jstl:if test="${!empty error1}">
			  <div class="form-group">
						<p class="form-control error">${error1}
							</p>
							</div>
					</jstl:if>
			 <!-- <span id="error2" class="form-control error "></span> -->
			<button type="submit" class="btn btn-primary" id="btnSubmit">Submit</button>
		</form>	
      	</div>
      	
      </div>
     </div>
   </div>



  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js"></script>
  
  <script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.8.3/jquery.min.js"></script>
<script type="text/javascript">
    $(function () {
        $("#btnSubmit").click(function () {
        	var oldpassword = $("$txtoldpassword").val();
            var password = $("#txtPassword").val();
            console.log(oldpassword);
            var confirmPassword = $("#txtConfirmPassword").val();
            var message = document.getElementById("error2");
            if (password == "" ) {
            	message.innerHTML="Password Field can not be empty.";
            	alter("bvhhvh");
                return false;
            }
            if (password != confirmPassword) {
            	message.innerHTML="Passwords do not match.";
                return false;
            }
                        
            return true;
        });
    });
</script>
  
</body>
</html>