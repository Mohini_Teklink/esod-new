<!-- @author Prateek Raj -->


<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@taglib prefix="jstl" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<jstl:set var="context" value="${pageContext.request.contextPath}" />
<spring:url var="bootstrapCss"
	value="resources/bootstrap-4.0.0-dist/css" />
<spring:url var="bootstrapJs" value="resources/bootstrap-4.0.0-dist/js" />
<spring:url var="bootstrapJQuery"
	value="resources/bootstrap-4.0.0-dist/jquery" />
<spring:url var="bootstrapFontAwesomeCss"
	value="resources/bootstrap-4.0.0-dist/font-awesome/css" />
<spring:url var="css" value="resources/css" />
<spring:url var="images" value="resources/images" />

<html>
<head>

<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<meta charset="utf-8">
<meta name="viewport"
	content="width=device-width, initial-scale=1, shrink-to-fit=no">
<!--For Fabvicon Icon-->
<link rel="shortcut icon" type="image/png"
	href="${pageContext.request.contextPath}/${images}/favicon.png" />
<link
	href="${pageContext.request.contextPath}/${bootstrapCss}/bootstrap.min.css"
	rel="stylesheet">
<!--For Toggle button Responsive on Mobile-->
<script
	src="${pageContext.request.contextPath}/${bootstrapJQuery}/jquery-3.3.1.min.js"></script>
<!--For Toggle button Responsive on Mobile-->
<script
	src="${pageContext.request.contextPath}/${bootstrapJs}/bootstrap.min.js"></script>
<!--For Fa icon-->
<link
	href="${pageContext.request.contextPath}/${bootstrapFontAwesomeCss}/font-awesome.min.css"
	rel="stylesheet">
<!--Custom Css-->
<link href="${pageContext.request.contextPath}/${css}/style_1.css"
	rel="stylesheet" type="text/css">
<!--Responsive Css-->
<link href="${pageContext.request.contextPath}/${css}/responsive.css"
	rel="stylesheet" type="text/css">

<title>Contact Us</title>
</head>

<body>

	<%@include file="shared/Header.jsp"%>

	<!-------------contactus------------------------->
	<section class="contact_us_main">
	<div class="container-fluid">
		<div class="row">
			<div
				class="col-xs-12 col-sm-12 col-md-12 col-xl-12 col-lg-12 contactus_map">
				<div id="googlemap" class="contact_google_map">
					<iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3149.33002658036!2d144.7561863153207!3d-37.87596397974107!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x6ad6884b9cef7bf5%3A0xb0c588e65c33e8e9!2s6+Stanton+Ct%2C+Seabrook+VIC+3028%2C+Australia!5e0!3m2!1sen!2sus!4v1538894185307" frameborder="0" style="border-bottom: 2px solid #ccc" width="100%" height="450px" allowfullscreen></iframe>
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-xs-12 col-sm-12 col-md-12 col-xl-12 col-lg-12">
				<div class="contact_ushead text-center">
					<h1>Contact Us</h1>
					<hr class="light my-4 demad">
				</div>
			</div>
		</div>
	</div>
	<div class="container contactus_row">
		<div class="row">
			<div class="col-xs-12 col-sm-12 col-md-6 col-xl-6 col-lg-6">
				<div class="contact_des_head">
					<h5>CUSTOMER CARE :</h5>
				</div>
				<div class="contact_des_pg">
					<p>
						Please call on our customer care no. <span class="text-primary">+61 3 9005 8366</span>
					</p>
					<p>Drop an email if you have any questions about our Products
						or Services.</p>
					<span><a href="#"><i class="fa fa-envelope fa-1x"
							aria-hidden="true"></i> <a href="mailto:inf@example.com">sod@teklinkinternational.com</a>
					</a></span>
				</div>
				<br>
				<br>
				<br>
				
				<h5>OUR LOCATION:</h5>
				<br>
				<address>
                      5a Hartnett Close, Mulgrave, 
                                        <br>VIC 3170, Australia.
                                <br>Contact No.<a href="tel:+61 390058366"> +61 3 9005 8366</a>
                                </address>
				
				
				
			</div>
			<!--<div class="col-xs-12 col-sm-12 col-md-6 col-xl-6 col-lg-6">-->
			<!--<div class="contact_des_head">-->
			<!--<h5>COMPLAINT</h5>-->
			<!--</div>-->
			<!--<div class="contact_des_pg">-->
			<!--<p>In publishing and graphic design, lorem ipsum is a placeholder text-->
			<!--commonly used to demonstrate the visual form of a document without relying on meaningful content.-->
			<!--</p>-->
			<!--<span><a href="#"><i class="fa fa-envelope fa-1x" aria-hidden="true"></i>-->
			<!--<a href="mailto:inf@example.com">complain@sod.com</a>-->
			<!--</a></span>-->
			<!--</div></div>-->
			<!--<div class="col-xs-12 col-sm-12 col-md-6 col-xl-6 col-lg-6">-->
			<!--<div class="contact_des_head">-->
			<!--<h5>FEEDBACK</h5>-->
			<!--</div>-->
			<!--<div class="contact_des_pg">-->
			<!--<p>In publishing and graphic design, lorem ipsum is a placeholder text-->
			<!--commonly used to demonstrate the visual form of a document without relying on meaningful content.-->
			<!--</p>-->
			<!--<span><a href="#"><i class="fa fa-envelope fa-1x" aria-hidden="true"></i>-->
			<!--<a href="mailto:inf@example.com">feedback@sod.com</a>-->
			<!--</a></span>-->
			<!--</div></div>-->
			<div class="col-xs-12 col-sm-12 col-md-6 col-xl-6 col-lg-6">
				<div class="contact_des_head">
					<h5>OTHER ENQUIRIES :</h5>
				</div>
				<div class="contact_cst_frm">
					<div class="contact_des_pg">
						<form>
							<div class="form-group">
								<label for="name"> Name</label> <input type="text"
									class="form-control" id="name" placeholder="Enter name"
									required="required" />
							</div>
							<div class="form-group">
								<label for="email"> Email Address</label>
								<div class="input-group">
									<span class="input-group-addon"><span
										class="glyphicon glyphicon-envelope"></span> </span> <input
										type="email" class="form-control" id="email"
										placeholder="Enter email" required="required" />
								</div>
							</div>
							<div class="form-group">
								<label for="subject"> Subject</label> <select id="subject"
									name="subject" class="form-control" required="required">
									<option value="na" selected="">Choose One:</option>
									<option value="service">General Customer Service</option>
									<option value="suggestions">Suggestions</option>
									<option value="product">Product Support</option>
								</select>
							</div>
							<div class="form-group">
								<label for="name"> Message</label>
								<textarea name="message" id="message" class="form-control"
									rows="3" cols="25" required="required" placeholder="Message"></textarea>
							</div>
							<div>
								<button type="submit" class="btn btn-info" id="btnContactUs">
									Send Message</button>
							</div>
						</form>
					</div>
				</div>
			</div>
		</div>
	</div>

	</section>
	<%-- <%@include file="Footer.jsp" %> --%>
	<!--------------footer-------------->
	<%-- <footer class="footer bg-dark">
    <div class="container">
        <div class="row">
            <div class="col-xs-4 col-sm-4 col-md-4 col-lg-4 col-xl-3">
                <div class="">
                    <p class="text-white">
                        SKILLS ON DEMAND
                    </p>
                    <p style="color: #ccc;">In publishing and graphic design, lorem ipsum is a placeholder text commonly used to demonstrate
                        the visual... <a href="${pageContext.request.contextPath}/aboutUs/about">READ MORE</a></p>
                    <img src="${pageContext.request.contextPath}/resources/images/logo-SOD.png" alt="Logo" class="text-white">

                </div>
            </div>
            <div class="col-xs-4 col-sm-4 col-md-4 col-lg-4 col-xl-3">
                <div class="recent_news">
                    <p class="text-white">
                        RECENT NEWS
                    </p>
                </div>

                <div class="footer_pg_list">
                    <ul>
                        <li> <a href="${pageContext.request.contextPath}">Home</a></li>
                        <li><a href="${pageContext.request.contextPath}/aboutUs/about">About us</a></li>
                        <li><a href="${pageContext.request.contextPath}/contactUs/contact">Contact Us</a></li>
                        <li><a href="${pageContext.request.contextPath}/services/servicePage">Services</a></li>
                        <li><a href="${pageContext.request.contextPath}/jobseeker/loginJobSeeker">Careers</a></li>
                    </ul>
                </div>
            </div>
            <div class="col-xs-4 col-sm-4 col-md-4 col-lg-4 col-xl-3">
                <div class="footer_categories">
                    <p class="text-white">CATEGORIES </p>
                </div>

                <div class="footer_pg_list">
                    <ul>
                        <li> <a href="#">Business</a></li>
                        <li><a href="#">Design</a></li>
                        <li><a href="#">Real life</a></li>
                        <li><a href="#">Science</a></li>
                        <li><a href="#">Tech</a></li>
                    </ul>
                </div>
            </div>
            <div class="col-xs-4 col-sm-4 col-md-4 col-lg-4 col-xl-3">
                <div class="footer_subsc">
                    <p class=" text-white">
                        SUBSCRIBE
                    </p>
                </div>
                <div class="Footer_res">
                    <p style="color: #fff;">
                        Get monthly updates and free resources.
                    </p>
                </div>
                <div class="media-container-column" data-form-type="formoid">
                    <form class="form-inline">
                        <input type="hidden" value="" data-form-email="true">
                        <div class="form-group">
                            <input type="email" class="form-control input-sm input-inverse my-2" name="email" required="" data-form-field="Email" placeholder="Email" id="email-footer-3h">
                        </div>
                        <div class="input-group-btn">
                            <button href="" class="btn btn-info footer_button" type="submit" role="button">Subscribe</button>
                        </div>
                    </form>
                </div>



            </div>
        </div>


        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 col-xl-12 footer-lower">
            <hr class="footer_line">
            <div class="row">
                <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6 col-xl-7">
                    <div class="footer_copyright">
                        <p class="text-white">
                            � Copyright 2017 Footer Template - All Rights Reserved
                        </p>
                    </div>
                </div>
                <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6 col-xl-5 text-right">
                    <div class="social_list">
                        <ul>
                            <li>
                                <a href="http://www.twitter.com" target="_blank">
                                    <span class="fa fa-twitter" media-simple="true"></span>
                                </a>
                            </li>
                            <li>
                                <a href="http://www.facebook.com/" target="_blank">
                                    <span class="fa fa-facebook" media-simple="true"></span>
                                </a>
                            </li>
                            <li>
                                <a href="http://www.instagram.com" target="_blank">
                                    <span class="fa fa-instagram" media-simple="true"></span>
                                </a>
                            </li>
                            <li>
                                <a href="http://www.youtube.com" target="_blank">
                                    <span class="fa fa-youtube" media-simple="true"></span>
                                </a>
                            </li>
                            <li>
                                <a href="https://plus.google.com/discover" target="_blank">
                                    <span class="fa fa-google-plus" media-simple="true"></span>
                                </a>
                            </li>
                        </ul>

                    </div>
                </div>
            </div>
        </div>
    </div>
</footer> --%>
	<%@include file="shared/Footer.jsp"%>



</body>
</html>