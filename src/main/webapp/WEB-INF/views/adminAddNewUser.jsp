<!-- @author Prateek Raj -->

<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="jstl" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="sf" uri="http://www.springframework.org/tags/form"%>
<!DOCTYPE html>
<spring:url var="css" value="/resources/admin/css"></spring:url>
<spring:url var="vendor" value="/resources/admin/vendor"></spring:url>
<spring:url var="images" value="/resources/admin/images"></spring:url>
<html>
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="description" content="">
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta name="robots" content="all,follow">
<link rel="shortcut icon" type="image/png" href="${images}/favicon.jpg" />
<!-- Bootstrap CSS-->
<link rel="stylesheet" href="${vendor}/bootstrap/css/bootstrap.min.css">
<!-- Font Awesome CSS-->
<link rel="stylesheet"
	href="${vendor}/font-awesome/css/font-awesome.min.css">
<!-- Fontastic Custom icon font-->
<link rel="stylesheet" href="${css}/fontastic.css">
<!-- Google fonts - Roboto -->
<link rel="stylesheet"
	href="https://fonts.googleapis.com/css?family=Roboto:300,400,500,700">
<!-- jQuery Circle-->
<link rel="stylesheet"
	href="${css}/grasp_mobile_progress_circle-1.0.0.min.css">
<!-- Custom Scrollbar-->
<link rel="stylesheet"
	href="${vendor}/malihu-custom-scrollbar-plugin/jquery.mCustomScrollbar.css">
<!-- theme stylesheet-->
<link rel="stylesheet" href="${css}/style.default.css"
	id="theme-stylesheet">
<!-- Custom stylesheet - for your changes-->
<link rel="stylesheet" href="${css}/custom.css">
<style>
.error {
	padding: 1 1;
	border:none;
	border-radius:0px;
	border-left:5px solid red;
	background-color: #ffeaea;
	font-size: 12px;
	font-style: italic;
}
</style>
<title>Add New User</title>
<!-- Tweaks for older IEs-->
<!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script><![endif]-->
</head>
<body>
	<!-- Side Navbar -->
	<%@include file="shared/shared-admin/Header.jsp"%>
	<div class="page">
		<!-- navbar-->
		<%@include file="shared/shared-admin/Navbar.jsp"%>
		<!--Main Section Start-->
		<section class="product_main">
			<div class="container">
				<div class="row">
					<div class="col-md-12 col-sm-12 col-xs-12">
						<div class="x_panel text-muted">
							<div class="x_title">
								<h1>Add New User</h1>
								<div class="clearfix"></div>
							</div>
							<div class="x_content">
								<jstl:if test="${not empty message}">
									<p class="text-success"
										style="background-color: #ccffcc; font-size: 20px; font-style: bold; padding: 4px 4px; text-align: center; border-left: 10px solid #33b35a;">${message}</p>
								</jstl:if>
								<jstl:if test="${not empty message_error}">
									<p class="text-danger"
										style="background-color: #fcbdbd; font-size: 20px; font-style: bold; padding: 4px 4px; text-align: center; border-left: 10px solid #ce0202;">${message_error}</p>
								</jstl:if>
							</div>
							<div class="text-right">
								<a class="btn btn-primary" type="button">Refresh <i
									class="fa fa-refresh"></i></a>
							</div>
							<div class="x_content">
								<div class="row">
									<div class="col-xs-6 col-sm-12 col-md-6 mx-auto">
										<sf:form action="/SOD/admin/dashboard/addSuccess"
											method="post" modelAttribute="user">
											<div class="form-group">
												<label>User Name<span class="required">*</span>
												</label>
												<div class="form-group">
													<sf:input cssClass="form-control" path="username" />
													<sf:errors path="username"
														cssClass="form-control error text-danger" />
												</div>
											</div>

											<div class="form-group">
												<label>Email<span class="required">*</span>
												</label>
												<div class="form-group">
													<sf:input cssClass="form-control" path="email" />
													<sf:errors path="email"
														cssClass="form-control error text-danger" />
												</div>
											</div>

											<div class="form-group">
												<label>First Name </label>
												<div class="form-group">
													<sf:input cssClass="form-control" path="firstName" />
													<sf:errors path="firstName"
														cssClass="form-control error text-danger" />
												</div>
											</div>

											<div class="form-group">
												<label>Last Name </label>
												<div class="form-group">
													<sf:input cssClass="form-control" path="lastName" />
													<sf:errors path="lastName"
														cssClass="form-control error text-danger" />
												</div>
											</div>

											<div class="form-group">
												<label>Website </label>
												<div class="form-group">
													<sf:input cssClass="form-control" path="website" />
													<sf:errors path="website"
														cssClass="form-control error text-danger" />
												</div>
											</div>

											<div class="form-group">
												<label>Password </label>
												<div class="form-group">
													<sf:password path="password" class="form-control" />
													<sf:errors path="password"
														cssClass="form-control error text-danger" />
												</div>
											</div>

											<div class="form-group">
												<label>Send User Notification </label>
												<div class="form-group">
													<sf:input cssClass="form-control"
														path="sendUserNotification" />
													<sf:errors path="sendUserNotification"
														cssClass="form-control error text-danger" />
												</div>
											</div>

											<div class="form-group">
												<label>Role </label>
												<div class="form-group">
													<sf:select cssClass="form-control" path="role">
														<sf:option value="ROLE_SUPERADMIN">Super Admin</sf:option>
														<sf:option value="ROLE_EXPERTADMIN">Expert Admin</sf:option>
														<sf:option value="ROLE_CLIENTADMIN">Client Admin</sf:option>
													</sf:select>
													<sf:errors path="role"
														cssClass="form-control error text-danger" />
												</div>
											</div>
											<div class="ln_solid"></div>
											<div class="form-group">
												<div>
													<input type="submit" class="btn btn-danger"
														value="Add
														New User" />
												</div>
											</div>
										</sf:form>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</section>

		<!--Main Section End-->


		<!--Footer Section Start-->
		<footer class="main-footer bg-light">
			<div class="text-right footer_logo_img">
				<img src="${images}/logo-SOD.png">
				<!-- Please do not remove the backlink to us unless you support further theme's development at https://bootstrapious.com/donate. It is part of the license conditions and it helps me to run Bootstrapious. Thank you for understanding :)-->
			</div>
			<div class="pull-right">
				<p>Copyright � 2018-2019 All Rights Reserved.</p>
			</div>
		</footer>

		<!--Footer Section End-->
	</div>
	<!-- JavaScript files-->
	<script src="${vendor}/jquery/jquery.min.js"></script>
	<script src="${vendor}/popper.js/umd/popper.min.js">
		
	</script>
	<script src="${vendor}/bootstrap/js/bootstrap.min.js"></script>
	<script src="${js}/grasp_mobile_progress_circle-1.0.0.min.js"></script>
	<script src="${vendor}/jquery.cookie/jquery.cookie.js">
		
	</script>
	<script src="${vendor}/chart.js/Chart.min.js"></script>
	<script src="${vendor}/jquery-validation/jquery.validate.min.js"></script>
	<script
		src="${vendor}/malihu-custom-scrollbar-plugin/jquery.mCustomScrollbar.concat.min.js"></script>
	<script src="${js}/charts-home.js"></script>
	<!-- Main File-->
	<script src="${js}/front.js"></script>
</body>
</html>