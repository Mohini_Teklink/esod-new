<!--  @author Hardev Singh -->
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>

<%@taglib prefix="jstl" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@taglib prefix="sf" uri="http://www.springframework.org/tags/form"%>

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">

<spring:url var="css" value="/resources/css"></spring:url>
<spring:url var="images" value="/resources/images"></spring:url>
<spring:url var="js" value="/resources/js"></spring:url>
<spring:url var="vendor" value="/resources/vendor"></spring:url>
<spring:url var="dataTable" value="/resources/DataTables"></spring:url>
<spring:url var="productImage" value="/resources/productImages"></spring:url>

<html>
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="description" content="">
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta name="robots" content="all,follow">
<link rel="shortcut icon" type="image/png" href="${images}/favicon.jpg" />
<!-- Bootstrap CSS-->
<link rel="stylesheet" href="${vendor}/bootstrap/css/bootstrap.min.css">
<!-- Font Awesome CSS-->
<link rel="stylesheet"
	href="${vendor}/font-awesome/css/font-awesome.min.css">
<!-- Fontastic Custom icon font-->
<link rel="stylesheet" href="${css}/fontastic.css">
<!-- Google fonts - Roboto -->
<link rel="stylesheet"
	href="https://fonts.googleapis.com/css?family=Roboto:300,400,500,700">
<!-- jQuery Circle-->
<link rel="stylesheet"
	href="${css}/grasp_mobile_progress_circle-1.0.0.min.css">
<!-- Custom Scrollbar-->
<link rel="stylesheet"
	href="${vendor}/malihu-custom-scrollbar-plugin/jquery.mCustomScrollbar.css">
<!-- theme stylesheet-->
<link rel="stylesheet" href="${css}/style.default.css"
	id="theme-stylesheet">
<!-- Custom stylesheet - for your changes-->
<link rel="stylesheet" href="${css}/custom.css">
<!-- Toogle Slider -->
<link rel="stylesheet" href="${css}/toggle_slider.css">
<!-- Favicon-->
<link rel="shortcut icon" href="${images}/favicon.ico">
<link rel="stylesheet" type="text/css"
	href="${dataTable}/datatables.css">

<title>Premium Product Description</title>
<!-- Tweaks for older IEs-->
<!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script><![endif]-->

<!-- Query for click handler in toogle  -->
<script
	src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>

</head>
<body>
	<!-- Side Navbar -->
	<%@ include file="shared/shared-admin/Header.jsp" %>
	<div class="page">
<%@include file="shared/shared-admin/Navbar.jsp"%>
		<!--Main Section Start-->

		<section class="dashboard-counts"> <section
			class="product_main">
		<div class="row">
			<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 col-xl-12">
				<div class="text-right">
					<div>
						<a class="btn btn-primary" href="/SOD/admin/dashboard/add_product_form?prodtype=3">ADD
							NEW PRODUCT</a>
					</div>
				</div>
				<div class="x_panel text-muted">
					<div class="x_title">
						<h1>Package-3 Product List</h1>
						<div class="clearfix"></div>
					</div>
					<table id="table_id" class="display">
					<thead>
						<tr>
							<th>ID</th>
							<th>Product Name</th>
							<th>Product Image</th>
							<th>Product Description</th>
							<th>Product Price</th>
							<th>Edit</th>
							<th>Status</th>
						</tr>
						</thead>
						<jstl:forEach items="${adminClientProduct}" var="client">
						<tr style="height: 90px">
						<td width="4%"><jstl:out value="${client.prodId}" /></td>
								<td width="13%">	
							<jstl:out value="${client.prodName}" />
								</td>
								<td width="10%">
								<ul class="list-inline">
										<li><img src="${productImage}/${client.prodImagePath}" width="75px" height="auto" 
											alt="Avatar"></li>
									</ul>
								</td>
								<td>			
								<jstl:out value="${client.prodDescription}" />
								</td>
								<td width="8%">
								<h4>
									<jstl:out value="${client.prodPrice}" />
								</h4>
								</td>
								<td width="8%"><div class="pr_desc_button">
									<a href="/SOD/admin/dashboard/product_view?ViewProduct=${client.prodId}"
										class="btn btn-primary btn-xs"> View </a> <a
										href="/SOD/admin/dashboard/update_product_form?UpdateProduct=${client.prodId}"
										class="btn btn-info btn-xs"><i class="fa fa-pencil"></i>
										Edit </a> 
								</div>
								</td>
								<td width="5%"><label class="switch "> <jstl:choose>
											<jstl:when test="${client.status==true}">
												<input id="status${client.prodId}" type="checkbox" onchange="myfunction(${client.prodId},${client.status})" checked>
												<span class="slider round"></span>
											</jstl:when>
											<jstl:otherwise>
												<input id="status${client.prodId}" type="checkbox" onchange="myfunction(${client.prodId},${client.status})">
												<span class="slider round"></span>
											</jstl:otherwise>
										</jstl:choose>

								</label></td>
								</tr>
					</jstl:forEach>
					</table>
				</div>
			</div>
		</div>
		</section> </section>

		<!--Main Section End-->
		<%@ include file="shared/shared-admin/Footer.jsp"%>
</div>

		<!--Footer Section Start-->
			<script type="text/javascript" charset="utf8"
	src="${dataTable}/datatables.js"></script>
	
		<script type="text/javascript">
			$(document).ready(function() {
				$('#table_id').DataTable();
			});
			function myfunction(id){
				
				var link="/SOD/admin/dashboard/statusUpdate?id="+id+"&status="+$("#status"+id).prop("checked");
				console.log(link);
				$.get(link);
			}
			
		</script>
</body>
</html>