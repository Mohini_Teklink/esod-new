<!--  @author Hardev Singh -->
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>

<%@taglib prefix="jstl" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@taglib prefix="sf" uri="http://www.springframework.org/tags/form"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">

<spring:url var="css" value="/resources/css"></spring:url>
<spring:url var="images" value="/resources/images"></spring:url>
<spring:url var="js" value="/resources/js"></spring:url>
<spring:url var="vendor" value="/resources/vendor"></spring:url>

<html>
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="description" content="">
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta name="robots" content="all,follow">
<link rel="shortcut icon" type="image/png" href="${images}/favicon.jpg" />
<!-- Bootstrap CSS-->
<link rel="stylesheet" href="${vendor}/bootstrap/css/bootstrap.min.css">
<!-- Font Awesome CSS-->
<link rel="stylesheet"
	href="${vendor}/font-awesome/css/font-awesome.min.css">
<!-- Fontastic Custom icon font-->
<link rel="stylesheet" href="${css}/fontastic.css">
<!-- Google fonts - Roboto -->
<link rel="stylesheet"
	href="https://fonts.googleapis.com/css?family=Roboto:300,400,500,700">
<!-- jQuery Circle-->
<link rel="stylesheet"
	href="${css}/grasp_mobile_progress_circle-1.0.0.min.css">
<!-- Custom Scrollbar-->
<link rel="stylesheet"
	href="${vendor}/malihu-custom-scrollbar-plugin/jquery.mCustomScrollbar.css">
<!-- theme stylesheet-->
<link rel="stylesheet" href="${css}/style.default.css"
	id="theme-stylesheet">
<!-- Custom stylesheet - for your changes-->
<link rel="stylesheet" href="${css}/custom.css">
<!-- Favicon-->
<link rel="shortcut icon" href="${images}/favicon.ico">

<title>Pricing Table</title>
<!-- Tweaks for older IEs-->
<!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script><![endif]-->

<style type="text/css">
 div#id1 .see_more_price {
    margin-top: 147px;
}
#id3 .see_more_price {
    margin-top: 24px;
}
</style>

</head>
<body>
	<!-- Side Navbar -->
	<%@ include file="shared/shared-admin/Header.jsp"%>
	<div class="page">
		<%@include file="shared/shared-admin/Navbar.jsp"%>
		<!--Main Section Start-->

		<section class="product_main">
		<div class="container">
			<div class="row">
			<div class="text-right">
					<div>
						<a class="btn btn-primary"
							href="/SOD/admin/dashboard/update_pricing_table">Edit Pricing </a>
					</div>
					
				</div>
				<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 col-xl-12">
					<div class="x_panel text-muted">
						<div class="x_title">
							<h1>Pricing Table</h1>
							<div class="clearfix"></div>
						</div>						

						<div class="row flex-items-xs-middle flex-items-xs-center">

							<!-- Table #1  -->
							<jstl:forEach items="${packageList}" var="packList">
								<div class="col-xs-12 col-lg-4">
									<div class="card text-xs-center">
										<div class="card-header">
											<h3 class="display-4 text-center" style="font-size: 3rem;">
												${fn:toUpperCase(packList.pack_name)}
											</h3>
										</div>
										<div class="card-block" id="id${packList.id}">
											<div  class="basic_plan text-center" >
												<h4 style="font-size:2.4rem; font-weight: 300;"  > <span class="currency">$</span> ${packList.pack_price}</h4>
											</div>
											<ul class="list-group">
											    <jstl:set value="${fn:split(packList.pack_description,';') }" var="arrayList"></jstl:set>
												<jstl:forEach items="${arrayList}" var="arr">
													<li class="list-group-item">${arr}</li>
												</jstl:forEach>
												<!-- <li class="list-group-item">Responsive Ready</li>
												<li class="list-group-item">Visual Composer Included</li>
												<li class="list-group-item">24/7 Support System</li> -->
											</ul>

											<div class="see_more_price">
												<a href="/SOD/admin/dashboard/pack_prod_desc/pack/${packList.id}"
													class="btn btn-primary">See More</a>
											</div>

										</div>
									</div>
								</div>
							</jstl:forEach>

						</div>
					</div>
				</div>
			</div>
		</div>
		</section>
		<%@ include file="shared/shared-admin/Footer.jsp"%>
	</div>
	<!--Main Section End-->



	<script type="text/javascript">
	/* $(window).on('resize', function() {
		$("#id1").css("height","auto");
		$("#id2").css("height","auto");
		$("#id3").css("height","auto");
		sameSize();
	}); */
	/* 
	$(document).ready(function() {
		sameSize();
	});

	function sameSize() {
		var maxheight = Math.max(Math.max($("#id1").height(), $("#id2")
				.height()), $("#id3").height()) +15.00;
		$("#id1").height(maxheight);
		$("#id2").height(maxheight);
		$("#id3").height(maxheight);
		
	} */
			
	</script>

</body>
</html>