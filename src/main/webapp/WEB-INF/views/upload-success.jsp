<!-- @author Shikha -->

<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="jstl" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<!DOCTYPE HTML>
<spring:url var="css" value="/resources/css"></spring:url>
<spring:url var="images" value="/resources/images"></spring:url>
<spring:url var="js" value="/resources/js"></spring:url>
<spring:url var="bootstrapCss"
	value="/resources/bootstrap-4.0.0-dist/css" />
<spring:url var="bootstrapJs" value="/resources/bootstrap-4.0.0-dist/js" />
<spring:url var="bootstrap" value="/resources/bootstrap-4.0.0-dist" />
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<meta charset="utf-8">
<meta name="viewport"
	content="width=device-width, initial-scale=1, shrink-to-fit=no">
<!--For Fabvicon Icon-->
<link rel="shortcut icon" type="image/png" href="${images}/favicon.png" />
<link href="${bootstrapCss}/bootstrap.min.css" rel="stylesheet">
<!--For Toggle button Responsive on Mobile-->
<script src="${bootstrap}/jquery/jquery-3.3.1.min.js"></script>
<!--For Toggle button Responsive on Mobile-->
<script src="${bootstrapJs}/bootstrap.min.js"></script>
<!--For Fa icon-->
<link href="${bootstrap}/font-awesome/css/font-awesome.min.css"
	rel="stylesheet">
<!--Custom Css-->
<link href="${css}/style_1.css" rel="stylesheet" type="text/css">
<!--Responsive Css-->
<link href="${css}/responsive.css" rel="stylesheet" type="text/css">
<head>
<meta charset="ISO-8859-1">
<title> </title>
</head>
<body>

<%@include file="shared/Header.jsp"%>

  <h3>Your file has been successfully uploaded.</h3>
    File Name: ${fileName}
    <br/>
    File Size: ${fileSize}
    
    <%@include file="shared/Footer.jsp"%></body>
</body>
</html>