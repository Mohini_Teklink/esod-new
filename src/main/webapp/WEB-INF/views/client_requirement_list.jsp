
<!--  @author Shikha Rohit-->
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@taglib prefix="jstl" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@taglib prefix="sf" uri="http://www.springframework.org/tags/form"%>

<!DOCTYPE html>

<spring:url var="css" value="/resources/css"></spring:url>
<spring:url var="images" value="/resources/images"></spring:url>
<spring:url var="js" value="/resources/js"></spring:url>
<spring:url var="vendor" value="/resources/vendor"></spring:url>
<spring:url var="dbt" value="/resources/DataTables"></spring:url>

<html>
<head>
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="description" content="">
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta name="robots" content="all,follow">
<link rel="shortcut icon" type="image/png" href="${images}/favicon.jpg" />
<!-- Bootstrap CSS-->
<link rel="stylesheet" href="${vendor}/bootstrap/css/bootstrap.min.css">
<!-- Font Awesome CSS-->
<link rel="stylesheet"
	href="${vendor}/font-awesome/css/font-awesome.min.css">
<!-- Fontastic Custom icon font-->
<link rel="stylesheet" href="${css}/fontastic.css">
<!-- Google fonts - Roboto -->
<link rel="stylesheet"
	href="https://fonts.googleapis.com/css?family=Roboto:300,400,500,700">
<!-- jQuery Circle-->
<link rel="stylesheet"
	href="${css}/grasp_mobile_progress_circle-1.0.0.min.css">
<!-- Custom Scrollbar-->
<link rel="stylesheet"
	href="${vendor}/malihu-custom-scrollbar-plugin/jquery.mCustomScrollbar.css">
<!-- theme stylesheet-->
<link rel="stylesheet" href="${css}/style.default.css"
	id="theme-stylesheet">
<!-- Toogle Slider -->
<link rel="stylesheet" href="${css}/toggle_slider.css">
<!-- Custom stylesheet - for your changes-->
<link rel="stylesheet" href="${css}/custom.css">
<link rel="stylesheet" type="text/css" href="${dbt}/datatables.css">

<title>Job List</title>
<!-- Tweaks for older IEs-->
<!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script><![endif]-->

<!-- Query for click handler in toogle  -->
<script
	src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>

</head>
<body>
	<!-- Side Navbar -->
	<%@ include file="shared/shared-admin/Header.jsp"%>
	
	<!--Main Section Start-->
<div class="page">
<%@include file="shared/shared-admin/Navbar.jsp"%>
	<section class="dashboard-counts"> <section
		class="product_main">

	<div class="row">
		<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 col-xl-12">
			<!-- <div class="text-right">
				<div>
					<a class="btn btn-primary" href="/SOD/admin/dashboard/add_job_form">ADD NEW JOB</a>
				</div>
			</div> -->
			<div class="x_panel text-muted">
				<div class="x_title">
					<h1>Client Requirement List</h1>
					<div class="clearfix"></div>
				</div>

				<table id="table_id" class="display">
					<thead>
						<tr>
						   
							<th>role</th>
							<th>skills</th>
							<th>start date</th>
							<th>Duration</th>
							<th>location</th>
							<th>Action</th>	
							
						 
						</tr>
					</thead>
					<jstl:forEach items="${clientList}" var="cllist">
						<tr>

							<td width="10%">${cllist.resourceRole}</td>
							<td width="15%"><jstl:out value="${cllist.skillRequired}"></jstl:out>
							</td>
							
							<td width="20%">${cllist.startDate}
							 <%-- <jstl:forEach items="${cllist.startDate}"
									var="listOfSkill" varStatus="state">
								 ${listOfSkill.skillName}
								<jstl:if test="${!state.last}">,</jstl:if>
								</jstl:forEach> --%>
								 </td>
		
							
							<td width="13%"><jstl:out
									value="${cllist.duration}" /></td>
							<td><jstl:out
									value="${cllist.location}" /></td>
								
									
									
							<td><a href="/SOD/admin/dashboard/submit_client_requirement_request"><button id="btnAccept" name="action" type="submit" value="Accept${client.action}" >Accept</button></a>
							<a href="/SOD/admin/dashboard/tbd_client_requirement_request"><button id="btnAccept" name="action" type="submit" value="Accept${client.action}" >TO BE Discussed</button></a></td>
                   	
									
							<%-- <td width="8%"><div class="pr_desc_button">
									<a href="/SOD/admin/dashboard/view_job_form?ViewJob=${clientlist.clientProducts.prodId}"
										class="btn btn-primary btn-xs"> View </a> 
										
										
									<a href="/SOD/admin/dashboard/update_job_form?updateJob=${clientlist.clientProducts.prodId}"
										class="btn btn-info btn-xs"><i class="fa fa-pencil"></i>
										Edit </a>
								</div></td>
							<td width="5%">
							<label class="switch "> <jstl:choose>
										<jstl:when test="${clientlist.clientProducts.status==true}">
											<input id="status${clientlist.clientProducts.prodId}"
												type="checkbox" onchange="myfunction(${clientlist.clientProducts.prodId})" checked>
											<span class="slider round"></span>
										</jstl:when>
										<jstl:otherwise>
											<input id="status${clientlist.clientProducts.prodId}"
												type="checkbox"
											onchange="myfunction(${clientlist.clientProducts.prodId})">
											<span class="slider round"></span>
										</jstl:otherwise>
									</jstl:choose>

							</label></td> --%>
						</tr>
					</jstl:forEach>
				</table>
			</div>
		</div>
	</div>
	</section> </section>
	<%@ include file="shared/shared-admin/Footer.jsp"%>
</div>
	<!--Main Section End-->


	<!--Footer Section Start-->
	
	<script type="text/javascript" charset="utf8" src="${dbt}/datatables.js"></script>

	<script type="text/javascript">
			$(document).ready(function() {
				$('#table_id').DataTable();
			});
			function myfunction(id){
				
				var link="/SOD/admin/dashboard/statusUpdate?id="+id+"&status="+$("#status"+id).prop("checked");
				console.log(link);
				$.get(link);
			}
			
		</script>
		
		
		
		
		</body>
</html>
		
		