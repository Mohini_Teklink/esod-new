<!-- @author Prateek Raj -->

<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="jstl" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="sf" uri="http://www.springframework.org/tags/form"%>
<!DOCTYPE html >
<spring:url var="css" value="/resources/admin/css"></spring:url>
<spring:url var="vendor" value="/resources/admin/vendor"></spring:url>
<spring:url var="images" value="/resources/admin/images"></spring:url>
<spring:url var="js" value="/resources/admin/js"></spring:url>
<html>
<head>
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="description" content="">
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta name="robots" content="all,follow">
<link rel="shortcut icon" type="image/png" href="${images}/favicon.jpg" />
<!-- Bootstrap CSS-->
<link rel="stylesheet" href="${vendor}/bootstrap/css/bootstrap.min.css">
<!-- Font Awesome CSS-->
<link rel="stylesheet"
	href="${vendor}/font-awesome/css/font-awesome.min.css">
<!-- Fontastic Custom icon font-->
<link rel="stylesheet" href="${css}/fontastic.css">
<!-- Google fonts - Roboto -->
<link rel="stylesheet"
	href="https://fonts.googleapis.com/css?family=Roboto:300,400,500,700">
<!-- jQuery Circle-->
<link rel="stylesheet"
	href="${css}/grasp_mobile_progress_circle-1.0.0.min.css">
<!-- Custom Scrollbar-->
<link rel="stylesheet"
	href="${vendor}/malihu-custom-scrollbar-plugin/jquery.mCustomScrollbar.css">
<!-- theme stylesheet-->
<link rel="stylesheet" href="${css}/style.default.css"
	id="theme-stylesheet">
<!-- Custom stylesheet - for your changes-->
<link rel="stylesheet" href="${css}/custom.css">
<title>Skills On Demand Dashboard</title>
<!-- Tweaks for older IEs-->
<!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script><![endif]-->
</head>
<body>

	<%@include file="shared/shared-admin/Header.jsp"%>
	<div class="page">
		<%@include file="shared/shared-admin/Navbar.jsp"%>
		<!-- Counts Section -->
		<section class="dashboard-counts section-padding ">
			<div class="container-fluid">
				<div class="row">
					<!-- Count item widget-->
					<div class="col-xl-2 col-md-4 col-6">
						<div class="wrapper count-title d-flex">
							<div class="icon">
								<i class="fa fa-user"></i>
							</div>
							<div class="name">
								<strong class="text-uppercase">New Clients</strong><span>Last
									7 days</span>
								<div class="count-number">25</div>
							</div>
						</div>
					</div>
					<!-- Count item widget-->
					<!-- <div class="col-xl-2 col-md-4 col-6">
						<div class="wrapper count-title d-flex">
							<div class="icon">
								<i class="fa fa-first-order"></i>
							</div>
							<div class="name">
								<strong class="text-uppercase">Work Orders</strong><span>Last
									5 days</span>
								<div class="count-number">400</div>
							</div>
						</div>
					</div> -->
					<!-- Count item widget-->
					<!-- <div class="col-xl-2 col-md-4 col-6">
						<div class="wrapper count-title d-flex">
							<div class="icon">
								<i class="fa fa-quote-left"></i>
							</div>
							<div class="name">
								<strong class="text-uppercase">New Quotes</strong><span>Last
									2 months</span>
								<div class="count-number">342</div>
							</div>
						</div>
					</div> -->
					<!-- Count item widget-->
					<!-- <div class="col-xl-2 col-md-4 col-6">
						<div class="wrapper count-title d-flex">
							<div class="icon">
								<i class="fa fa-bitbucket"></i>
							</div>
							 <div class="name">
								<strong class="text-uppercase">New Invoices</strong><span>Last
									2 days</span>
								<div class="count-number">123</div>
							</div> 
						</div> 
					</div> -->
					<!-- Count item widget-->
					<div class="col-xl-2 col-md-4 col-6">
						<div class="wrapper count-title d-flex">
							<div class="icon">
								<i class="fa fa-list"></i>
							</div>
							<div class="name">
								<strong class="text-uppercase">Open Cases</strong><span>Last
									3 months</span>
								<div class="count-number">92</div>
							</div>
						</div>
					</div>
					<!-- Count item widget-->
					<div class="col-xl-2 col-md-4 col-6">
						<div class="wrapper count-title d-flex">
							<div class="icon">
								<i class="fa fa-list-ol"></i>
							</div>
							<div class="name">
								<strong class="text-uppercase">New Cases</strong><span>Last
									7 days</span>
								<div class="count-number">70</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</section>
		<!-- Header Section End-->

		<!--Main Section Start-->

		<section class="dashboard">
			<div class="container">
				<div class="row">
					<div class="col-md-6 col-sm-6 col-xs-6 text-center sod_main_col ">
						<div class="sod_main">
							<h1>
								<a href="/SOD/admin/dashboard/pricing_table">Clients</a>
							</h1>
						</div>
					</div>
					<div class="col-md-6 col-sm-6 col-xs-6 text-center sod_main_col ">
						<div class="sod_main">
							<h1>
								<a href="/SOD/admin/dashboard/jobseeker">Experts</a>
							</h1>
						</div>
					</div>
				</div>
			</div>
		</section>

		<!--Main Section End-->


		<%@ include file="shared/shared-admin/Footer.jsp"%>
</body>
</html>