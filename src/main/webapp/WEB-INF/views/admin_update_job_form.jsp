<!--  @author Hardev Singh -->

<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>

<%@taglib prefix="jstl" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@taglib prefix="sf" uri="http://www.springframework.org/tags/form"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">

<spring:url var="css" value="/resources/css"></spring:url>
<spring:url var="images" value="/resources/images"></spring:url>
<spring:url var="jsmain" value="/resources/js"></spring:url>
<spring:url var="vendor" value="/resources/vendor"></spring:url>

<html>
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="description" content="">
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta name="robots" content="all,follow">
<link rel="shortcut icon" type="image/png" href="${images}/favicon.jpg" />
<!-- Bootstrap CSS-->
<!-- Dropdown list -->
<link rel="stylesheet" href="${css}/bootstrap-select.css">
<link rel="stylesheet"
	href="https://maxcdn.bootstrapcdn.com/bootstrap/4.1.0/css/bootstrap.min.css">
<!-- Font Awesome CSS-->
<link rel="stylesheet"
	href="${vendor}/font-awesome/css/font-awesome.min.css">
<!-- Fontastic Custom icon font-->
<link rel="stylesheet" href="${css}/fontastic.css">
<!-- Google fonts - Roboto -->
<link rel="stylesheet"
	href="https://fonts.googleapis.com/css?family=Roboto:300,400,500,700">
<!-- jQuery Circle-->
<link rel="stylesheet"
	href="${css}/grasp_mobile_progress_circle-1.0.0.min.css">
<!-- Custom Scrollbar-->
<link rel="stylesheet"
	href="${vendor}/malihu-custom-scrollbar-plugin/jquery.mCustomScrollbar.css">
<!-- theme stylesheet-->
<link rel="stylesheet" href="${css}/style.default.css"
	id="theme-stylesheet">
<!-- Custom stylesheet - for your changes-->
<link rel="stylesheet" href="${css}/custom.css">
<!-- Favicon-->
<link rel="shortcut icon" href="${images}/favicon.ico">
<style>
.error {
	padding: 1 1;
	border: none;
	border-radius: 0px;
	border-left: 5px solid red;
	background-color: #ffeaea;
	font-size: 12px;
	font-style: italic;
}
</style>
<script
	src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
<title>Update Job</title>
<!-- Tweaks for older IEs-->
<!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script><![endif]-->



</head>
<body>
	<!-- Side Navbar -->
	<%@ include file="shared/shared-admin/Header.jsp"%>
	<div class="page">
		<%@include file="shared/shared-admin/Navbar.jsp"%>
		<!--Main Section Start-->
		<section class="product_main">
		<div class="container">
			<div class="row">
				<div class="col-md-12 col-sm-12 col-xs-12">
					<div class="x_panel text-muted">
						<div class="x_title">
							<h4>Update Job</h4>
							<div class="clearfix"></div>
						</div>
						<div class="x_content">
							<div class="row">
								<div class="col-md-6 col-sm-12 col-xs-12 mx-auto">

									<sf:form action="/SOD/admin/dashboard/jobUpdatedSuccess?${_csrf.parameterName}=${_csrf.token}" method="POST"
										modelAttribute="UpdateJob" >
										<sf:hidden path="prodId" value="${UpdateJob.prodId}" />
										<div class="form-group">
											<label>Product Name<span class="required">*</span>
											</label>
											<div class="form-group">
												<sf:input path="prodName" cssClass="form-control"
													value="${UpdateJob.prodName}" readonly="true" />
											</div>
										</div>
										
										<div class="form-group">
											<label>Product Type </label>
												<div class="form-group">
												<%-- <sf:select path="prodType" cssClass="form-control" readOnly="true"> --%>
													<jstl:forEach items="${packageList}" var="pack">
															<jstl:if test="${pack.id == UpdateJob.prodType}">
															<sf:hidden path="prodType"  value="${pack.id}" />
																<input class="form-control" value="${pack.pack_name}" readonly="readonly"/>
															</jstl:if>
													</jstl:forEach>
													</div>
										</div>
										
										<div class="form-group">
											<label>How do you want to get paid </label>
											<div class="form-check">
												<jstl:choose>
													<jstl:when test="${UpdateJob.jobPaid=='Hourly Paid'}">
														<input type="radio" value="Hourly Paid" name="jobPaid"
															checked="checked">Hourley project 
											&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
											<input type="radio" value="Fixed Paid" name="jobPaid">Fixed Price Project
											</jstl:when>
													<jstl:otherwise>
														<input type="radio" value="Hourly Paid" name="jobPaid">Hourley project 
											&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
											<input type="radio" value="Fixed Paid" name="jobPaid"
															checked="checked">Fixed Price Project
											</jstl:otherwise>
												</jstl:choose>
											</div>
										</div>
									
										<div class="form-group">
											<label>Product Price<span class="required">*</span>
											</label>
											<div class="form-group">
												<sf:input path="prodPrice" cssClass="form-control"
													value="${UpdateJob.prodPrice}" readOnly="true"/>
												<sf:errors path="prodPrice"
													cssClass="form-control error text-danger" />
											</div>
										</div>
										
										<div class="form-group">
											<label>Skill<span class="required">*</span>
											</label>

											<jstl:set var = "skillArray" value = "${fn:split(UpdateJob.skills, ',')}" />
												<sf:select id="skill" cssClass="selectpicker form-control"
													title="Select Skills" path="skills" multiple="multiple"
													tabindex="10" data-live-search="true"> 
													<jstl:set var="skillGroupVar" value="" />
													<jstl:set var="counter" value="0" scope="page"></jstl:set>
													<jstl:forEach items="${skillsList}" var="skillListVar">
														<jstl:if test="${empty skillGroupVar}">
															<jstl:set var="skillGroupVar" value="${skillListVar.skillGroup}" />
															<optgroup label="${skillGroupVar}">
														</jstl:if>
														<jstl:if
															test="${skillGroupVar != skillListVar.skillGroup}">
															</optgroup>
															<jstl:set var="skillGroupVar" value="${skillListVar.skillGroup}" />
															<optgroup label="${skillGroupVar}">
														</jstl:if>
														<jstl:out value="${counter}"></jstl:out>
														<jstl:out value="${skillListVar.id==skillArray[counter]}"></jstl:out>
														<jstl:choose>
														<jstl:when test="${skillListVar.id==skillArray[counter]}">
														        <jstl:set var="counter" value="${counter+1}" scope="page"></jstl:set>
														         <sf:option value="${skillListVar.id}" selected="true">${skillListVar.skillName}</sf:option>
														</jstl:when>
														<jstl:otherwise>
														         <sf:option value="${skillListVar.id}">${skillListVar.skillName}</sf:option>
													    </jstl:otherwise>
														</jstl:choose>
													</jstl:forEach>
													</optgroup>
												</sf:select>
											<sf:errors path="skills"
												cssClass="form-control error text-danger" />
										</div>
										
										<div class="form-group">
											<label>Industry </label>
											<div class="form-group">
												<sf:select class="form-control" path="industryType">
													<jstl:forEach items="${industryList}" var="industryList">
														<jstl:if
															test="${industryList.industry_type eq UpdateJob.industryType }">
															<option selected="selected"
																value="${industryList.industry_type}">${industryList.industry_type}</option>
														</jstl:if>
														<jstl:if
															test="${industryList.industry_type != UpdateJob.industryType }">
															<option value="${industryList.industry_type}">${industryList.industry_type}</option>
														</jstl:if>
													</jstl:forEach>
												</sf:select>
												<sf:errors path="industryType"
													cssClass="form-control error text-danger" />
											</div>
										</div>
										<div class="form-group">
											<label>Product Description<span class="required">*</span>
											</label>
											<div class="form-group">
												<textarea name="prodDescription" rows="5" cols="30"
													class="form-control">${UpdateJob.prodDescription}</textarea>
												<sf:errors path="prodDescription"
													cssClass="form-control error text-danger" />
											</div>
										</div>
										<div class="form-group">
											<label>Employement Type </label>
											<div class="form-group">
												<sf:select cssClass="form-control" path="employmentType">
													<jstl:choose>
														<jstl:when test="${UpdateJob.employmentType=='Part Time'}">
															<sf:option value="Part Time" label="Part Time" />
															<sf:option value="Full time" label="Full Time" />
														</jstl:when>
														<jstl:otherwise>
															<sf:option value="Full time" label="Full Time" />
															<sf:option value="Part Time" label="Part Time" />
														</jstl:otherwise>
													</jstl:choose>
												</sf:select>
												<sf:errors path="employmentType"
													cssClass="form-control error text-danger" />
											</div>
										</div>

										<sf:hidden path="status" value="${UpdateJob.status}" />

										<sf:hidden path="prodImagePath" value="${UpdateJob.prodImagePath}" />
										
										<div class="ln_solid"></div>
										<div class="form-group">
											<div>
												<a href="/SOD/admin/dashboard/clientlist" type="submit" class="btn btn-danger">Cancel</a>
												<input value="submit" type="submit" class="btn btn-success">
											</div>
										</div>
									</sf:form>
								</div>
							</div>
						</div>


					</div>
				</div>
			</div>
		</div>
		</section>
		<%@ include file="shared/shared-admin/Footer.jsp"%>
	</div>
	<!--Main Section End-->


	<!--Footer Section Start-->
	
	<script src="${jsmain}/bootstrap-select/bootstrap-select.js"></script>
	<script>
		$(document).ready(function() {
			$("#skill").selectpicker({
				liveSearch : true
			});
		});
	</script>
</body>
</html>