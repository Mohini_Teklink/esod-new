
<!--  @author Hardev Singh -->
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@taglib prefix="jstl" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@taglib prefix="sf" uri="http://www.springframework.org/tags/form"%>

<!DOCTYPE html>

<spring:url var="css" value="/resources/css"></spring:url>
<spring:url var="images" value="/resources/images"></spring:url>
<spring:url var="js" value="/resources/js"></spring:url>
<spring:url var="vendor" value="/resources/vendor"></spring:url>
<spring:url var="dbt" value="/resources/DataTables"></spring:url>

<html>
<head>
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="description" content="">
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta name="robots" content="all,follow">
<link rel="shortcut icon" type="image/png" href="${images}/favicon.jpg" />
<!-- Bootstrap CSS-->
<link rel="stylesheet" href="${vendor}/bootstrap/css/bootstrap.min.css">
<!-- Font Awesome CSS-->
<link rel="stylesheet"
	href="${vendor}/font-awesome/css/font-awesome.min.css">
<!-- Fontastic Custom icon font-->
<link rel="stylesheet" href="${css}/fontastic.css">
<!-- Google fonts - Roboto -->
<link rel="stylesheet"
	href="https://fonts.googleapis.com/css?family=Roboto:300,400,500,700">
<!-- jQuery Circle-->
<link rel="stylesheet"
	href="${css}/grasp_mobile_progress_circle-1.0.0.min.css">
<!-- Custom Scrollbar-->
<link rel="stylesheet"
	href="${vendor}/malihu-custom-scrollbar-plugin/jquery.mCustomScrollbar.css">
<!-- theme stylesheet-->
<link rel="stylesheet" href="${css}/style.default.css"
	id="theme-stylesheet">
<!-- Toogle Slider -->
<link rel="stylesheet" href="${css}/toggle_slider.css">
<!-- Custom stylesheet - for your changes-->
<link rel="stylesheet" href="${css}/custom.css">
<link rel="stylesheet" type="text/css" href="${dbt}/datatables.css">

<title>Job List</title>
<!-- Tweaks for older IEs-->
<!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script><![endif]-->

<!-- Query for click handler in toogle  -->
<script
	src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>

</head>
<body>
	<!-- Side Navbar -->
	<%@ include file="shared/shared-admin/Header.jsp"%>
	<div class="page">
		<!-- navbar-->
		<header class="header">
			<nav class="navbar">
				<div class="container-fluid">
					<div
						class="navbar-holder d-flex align-items-center justify-content-between">
						<div class="navbar-header">
							<a id="toggle-btn" href="#" class="menu-btn"><i
								class="fa fa-bars"> </i></a><a href="dashboard.html"
								class="navbar-brand">
								<div class="brand-text d-none d-md-inline-block">
									<span>dashboard </span> <strong class="text-primary">Skills
										On Demand</strong>
								</div>
							</a>
						</div>
						<ul
							class="nav-menu list-unstyled d-flex flex-md-row align-items-md-center">

							<!-- Languages dropdown    -->
							<li class="nav-item dropdown user_img_nav"><a id="languages"
								rel="nofollow" data-target="#" href="#" data-toggle="dropdown"
								aria-haspopup="true" aria-expanded="false"
								class="nav-link language dropdown-toggle"><img
									src="${images}/user.png" alt="English"><span
									class="d-none d-sm-inline-block">User Name</span></a>
								<ul aria-labelledby="languages" class="dropdown-menu">
									<li><a rel="nofollow" href="#" class="dropdown-item"><span>Profile</span></a></li>
									<li><a rel="nofollow" href="#" class="dropdown-item"><span>Setting</span></a></li>
								</ul></li>
							<!-- Log out-->
							<li class="nav-item"><a href="login.html"
								class="nav-link logout"> <span
									class="d-none d-sm-inline-block">Logout</span><i
									class="fa fa-sign-out"></i></a></li>
						</ul>
					</div>
				</div>
			</nav>
		</header>



		<!--Main Containt Start-->


		<section class="product_main">
			<div class="container">
				<div class="row">
					<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 col-xl-12">
						
						<div class="x_panel text-muted">
							<div class="x_title">
								<h1>Client Profile List</h1>
								<div class="clearfix"></div>
							</div>
							<div class="x_content">
								<%-- <p>
									Simple table with project listing with <b>Id, Name</b> and <b>editing</b>
									options
									
								</p>
								${message} --%>
							</div>
							<div class="row">
								<div class="col-xs-12 col-sm-12 col-md-12">
									<table id="table_id" class="display"
	
										style="width: 100%; text-align: center; vertical-align: middle;">
										<thead>
											<tr>
												<th>Name</th>
												<!-- <th>Profile</th> -->
												<th>Email</th>
												<th>Mobile No.</th>
												<th>Address</th>
												<th>Action</th>
												<th>Status</th>
											</tr>
										</thead>
										<tbody>
											<jstl:forEach items="${clientMasterList}" var="list">
												<tr>
													<td>${list.client_username}</td>
													<td>${list.client_email}</td>
													<td>${list.client_phonenum}</td>
													<td>${list.client_address}</td>
												
													<td><a
														href="/SOD/admin/dashboard/clientlist/profile?id=${list.id}"
														class="btn btn-primary text-white btn-sm"><i
															class="fa fa-user"></i> view</a></td>
													<td><jstl:if
															test="${list.status == true}">
															<label class="switch"> <input type="checkbox"
																id="check${list.id}" onclick="myFunction(${list.id});"
																checked="checked" /> <span class="slider round">
															</span>
															</label>
														</jstl:if> <jstl:if test="${list.status == null || list.status == false}">
															<label class="switch"> <input type="checkbox"
																id="check${list.id}" onclick="myFunction(${list.id});" />
																<span class="slider round"> </span>
															</label>
														</jstl:if></td>

												</tr>
											</jstl:forEach>
										</tbody>
									</table>
								</div>
							</div>

						</div>
					</div>
				</div>
			</div>
		</section>
		<!--Main Section End-->

		<%@include file="shared/shared-admin/Footer.jsp"%>
	</div>
<script type="text/javascript" charset="utf8" src="${dbt}/datatables.js"></script>
	<script type="text/javascript">
	function myFunction(id){
		console.log(id+" - "+$("#check"+id).prop("checked"));
		var link ="/SOD/admin/dashboard/clientlist/activeInactive?id="+id+"&status="+$("#check"+id).prop("checked");
		$.get(link, function(data, status){
	       /* console.log("Data: " + data + "\nStatus: " + status); */
	    });
		
	}
	$(document).ready( function () {
	    $('#table_id').DataTable();
	} );
	</script>
</body>
</html>