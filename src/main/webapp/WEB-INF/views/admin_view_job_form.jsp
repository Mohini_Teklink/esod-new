<!--  @author Hardev Singh -->
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>

<%@taglib prefix="jstl" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@taglib prefix="sf" uri="http://www.springframework.org/tags/form"%>

<!DOCTYPE html>

<spring:url var="css" value="/resources/css"></spring:url>
<spring:url var="images" value="/resources/images"></spring:url>
<spring:url var="js" value="/resources/js"></spring:url>
<spring:url var="vendor" value="/resources/vendor"></spring:url>
<spring:url var="productImage" value="/resources/productImages"></spring:url>

<html>
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="description" content="">
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta name="robots" content="all,follow">
<link rel="shortcut icon" type="image/png" href="${images}/favicon.jpg" />
<!-- Bootstrap CSS-->
<link rel="stylesheet" href="${vendor}/bootstrap/css/bootstrap.min.css">
<!-- Font Awesome CSS-->
<link rel="stylesheet"
	href="${vendor}/font-awesome/css/font-awesome.min.css">
<!-- Fontastic Custom icon font-->
<link rel="stylesheet" href="${css}/fontastic.css">
<!-- Google fonts - Roboto -->
<link rel="stylesheet"
	href="https://fonts.googleapis.com/css?family=Roboto:300,400,500,700">
<!-- jQuery Circle-->
<link rel="stylesheet"
	href="${css}/grasp_mobile_progress_circle-1.0.0.min.css">
<!-- Custom Scrollbar-->
<link rel="stylesheet"
	href="${vendor}/malihu-custom-scrollbar-plugin/jquery.mCustomScrollbar.css">
<!-- theme stylesheet-->
<link rel="stylesheet" href="${css}/style.default.css"
	id="theme-stylesheet">
<!-- Custom stylesheet - for your changes-->
<link rel="stylesheet" href="${css}/custom.css">
<!-- Favicon-->
<link rel="shortcut icon" href="${images}/favicon.ico">

<title>View Job Post</title>
<!-- Tweaks for older IEs-->
<!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script><![endif]-->

</head>
<body>
	<!-- Side Navbar -->
	<%@ include file="shared/shared-admin/Header.jsp"%>

	<!--Main Section Start-->
	<div class="page">
		<%@include file="shared/shared-admin/Navbar.jsp"%>
		<section class="dashboard-counts"> <section
			class="product_main">
		<div class="row">
			<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 col-xl-12">
				<!--<div class="text-right">-->
				<!--<div>-->
				<!--<a class="btn btn-primary" href="add_job_form.html">ADD NEW JOB</a>-->
				<!--</div>-->
				<!--<form>-->
				<!--<div class="pull-right input-group search_basic">-->
				<!--<input class="form-control border-secondary py-2" type="search" value="search">-->
				<!--<div class="input-group-append">-->
				<!--<button class="btn btn-outline-secondary" type="button">-->
				<!--<i class="fa fa-search"></i>-->
				<!--</button>-->
				<!--</div>-->
				<!--</div>-->
				<!--</form>-->
				<!--</div>-->
				<div class="x_panel text-muted">
					<div>
						<h1>Job Description</h1>
						<div class="clearfix"></div>
					</div>
					<div class="row">
						<div class="col-xs-12 col-sm-12 col-md-12">
							<div>
								<p>Hi</p>
							</div>
							<div>
								<p>Urgent hiring for Windows Server Admin(Active Directory)</p>
							</div>
							<div>
								<p>
									Company Name : <span class="text-bold">
										${jobView.clientProducts.prodName} </span>
								</p>
							</div>
							<div>
								<p>Trusted IT partner since 1998, Progressive Infotech
									provides comprehensive suite of transformation and support
									services. The offerings span across cloud, digital and support
									operations, delivered through a matured and scalable service
									delivery model. In every client engagement, Progressive ensures
									clients realize higher ROI, stretch the intrinsic value of
									existing IT investments and are better prepared for emergent
									market changes. Progressive Infotech is consistently featured
									as a mature vendor for IT Outsourcing in the Gartner hype cycle
									report for ICT in India for last few years.</p>
							</div>
							<div>
								<p>
									Profile :- <span class="text-primary">Windows Server
										Admin(Active Directory)</span>
								</p>
							</div>
							<div class="text-bold">
								<p>Job Description :-</p>
							</div>
							<div>
								<p>${jobView.clientProducts.prodDescription}</p>
							</div>
							<div>
								<p>
									Experience : <span class="text-bold">3-6 years</span>
								</p>
							</div>
							<div>
								<p>
									Notice Period :- <span class="text-bold">Upto 15 Days.</span>
								</p>
							</div>
							<div>
								<p>
									Job location:- <span class="text-bold">Gurgaon</span>
								</p>
							</div>
							<div>
								<p class="text-bold">Keyskills :-</p>
								<jstl:set var="skillGroupVar" value="" />
								<jstl:forEach items="${jobView.skillList}" var="skillListVar">
									<b> <jstl:if test="${empty skillGroupVar}">
											<jstl:out value="${skillListVar.skillGroup}"></jstl:out>
											<jstl:set var="skillGroupVar"
												value="${skillListVar.skillGroup}" />
										</jstl:if> <jstl:if test="${skillGroupVar != skillListVar.skillGroup}">
											<jstl:set var="skillGroupVar"
												value="${skillListVar.skillGroup}" />
											<jstl:out value="${skillListVar.skillGroup}"></jstl:out>
										</jstl:if>
									</b>

									<ul style="list-style: circle; margin-left: 20px;">
										<li><jstl:out value="${skillListVar.skillName}"></jstl:out></li>
									</ul>
								</jstl:forEach>

							</div>
							<div class="cmp_prf"><br>
								<p class="text-bold">Company Profile :-</p>
								<p>In publishing and graphic design, lorem ipsum is a
								    placeholder text commonly used to demonstrate the visual form
									of a document without relying on meaningful content (also
									called greeking). Replacing the actual content with placeholder
									text allows designers to design the form of the content before
									the content.</p>
							</div>
							<div>
								<!-- <button class="btn btn-primary">Apply</button> -->
								<a class="btn btn-danger text-white"
									href="/SOD/admin/dashboard/clientlist">Back</a>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>

		</section> </section>

		<%@ include file="shared/shared-admin/Footer.jsp"%>
	</div>


</body>
</html>