<!-- 
@author Prateek Raj
@organization : Teklink Enterprise
 -->

<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@taglib prefix="jstl" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<jstl:set var="context" value="${pageContext.request.contextPath}" />
<spring:url var="bootstrapCss"
	value="resources/bootstrap-4.0.0-dist/css" />
<spring:url var="bootstrapJs" value="resources/bootstrap-4.0.0-dist/js" />
<spring:url var="bootstrapJQuery"
	value="resources/bootstrap-4.0.0-dist/jquery" />
<spring:url var="bootstrapFontAwesomeCss"
	value="resources/bootstrap-4.0.0-dist/font-awesome/css" />
<spring:url var="css" value="resources/css" />
<spring:url var="images" value="resources/images" />
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<meta charset="utf-8">
<meta name="viewport"
	content="width=device-width, initial-scale=1, shrink-to-fit=no">
<!--For Fabvicon Icon-->
<link rel="shortcut icon" type="image/png"
	href="${pageContext.request.contextPath}/${images}/favicon.png" />
<link
	href="${pageContext.request.contextPath}/${bootstrapCss}/bootstrap.min.css"
	rel="stylesheet">
<!--For Toggle button Responsive on Mobile-->
<script
	src="${pageContext.request.contextPath}/${bootstrapJQuery}/jquery-3.3.1.min.js"></script>
<!--For Toggle button Responsive on Mobile-->
<script
	src="${pageContext.request.contextPath}/${bootstrapJs}/bootstrap.min.js"></script>
<!--For Fa icon-->
<link
	href="${pageContext.request.contextPath}/${bootstrapFontAwesomeCss}/font-awesome.min.css"
	rel="stylesheet">
<!--Custom Css-->
<link href="${pageContext.request.contextPath}/${css}/style_1.css"
	rel="stylesheet" type="text/css">
<!--Responsive Css-->
<link href="${pageContext.request.contextPath}/${css}/responsive.css"
	rel="stylesheet" type="text/css">
<title>About us</title>
</head>

<body>

	<%@include file="shared/Header.jsp"%>


	<!--header-------------------------------------------------------->
	<header class="about_main text-center">
	<div class="container">
		<div class="row">
			<div
				class="col-xs-12 col-sm-12 col-md-12 col-xl-12 col-lg-12 mx-auto">
				<div class="text-uppercase about_main_head">
					<p>
						<span>About Us</span>
					</p>
				</div>
				<div class="aboutus_pg">
					<p>BUILDING A PROFESSIONAL NETWORK OF EXPERTS AND CLIENTS</p>
				</div>
			</div>
		</div>
	</div>
	</header>

	<!------about-----product-------------->
	<section>





	<div class="container">
		<div class="row">
			<div class="col-xs-12 col-sm-12 col-md-12">
				<div class="abt_head">
					<!-- <h2>About Us</h2>
					<p><b></b></p> -->
				</div>
				<div>
				   <p>
				     <b>TekLink</b> is a technology solutions and specialist services business, conceptualised for technology <b>Tek</b> (bridging)/ <b>Link</b>(ing) with business.<br><br>

                     We are truly a global group directly based in Australia, Singapore & India; and through Partners (under Teaming Agreements) in USA, Germany, UK, Switzerland, South Africa, UAE, China & some other locations. Each branch is legally and commercially an independent entity.<br><br>

				     We are true partners for OEMs & larger SIs. A large SI gets different solutions/parts of a program (or across programs) delivered under 3 arrangements -<br>
					 &nbsp;<b>1.</b> Subcontracting to a Partner who delivers solutions agreed under a SoW.<br>
					 &nbsp;<b>2.</b> Hires a Partner for quality/delivery assured services, not delivery of solutions.<br>
					 &nbsp;<b>3.</b> Hires a Partner as Supplier for augmenting FTE resources.
				   </p>
				   <br>
				   <h5 class="text-center">Skills On Demand</h5><br>
					<p>
					Skills on Demand is a platform for young and skilled professionals who are dedicated for establishing a pool of experts, who are looking for career opportunities for SIs, who require expert solution for their business.
					</p>
					
					<p>
					For clients, Skills on Demand understands the requirements and expectations thoroughly, explore through the vast pool of experts and select the right person suited for the work packages. In addition to offering the resources, we also commit delivery assurance, thereby making our services unique to the clients.
					</p>
					
					 <p>
					Skills on Demand will be used for online searching for skills and any one can search for jobs who have skills, he/she can get opportunity to utilize their skills and make money from home or anywhere else. Any customer can view and search for the packages according to their need and according to their required skills. The customer can select the packages and he can order that package

                    </p>

                    <p> 
                    Skill On Demand will allow the different kinds of skilled people to get the employment according to their skills, they can earn money, and they can proper use their skills.

					</p> 
				</div>
			</div>
		</div>

		<!-- <div class="row Philosophy_list">
			<div class="col-xs-12 col-sm-12 col-md-12">
				<div>
					<h3>Our Philosophy, Principles & Strengths</h3>
				</div>
				<div>
					<ul>
						<li><span><i class="fa fa-check-circle-o text-primary"></i></span>
							People first.</li>
						<li><span><i class="fa fa-check-circle-o text-primary"></i></span>
							Fit-for-purpose solutions. We service enterprises of all sizes,
							SMEs to Large Enterprises with our unbeatable agility &
							reasonable commercials.</li>
						<li><span><i class="fa fa-check-circle-o text-primary"></i></span>
							Track record of on-time, within-budget & high-quality deliveries.</li>
						<li><span><i class="fa fa-check-circle-o text-primary"></i></span>
							Focused on Customer Delight. Once a customer, always a customer -
							100% referenceable.
							for many Fortune 500 Clients.</li>
					</ul>
				</div>
			</div>
		</div> -->
	</div>
	</section>

	<!--------------footer-------------->
	<!-- <footer class="footer bg-dark">
    <div class="container">
        <div class="row">
            <div class="col-xs-4 col-sm-4 col-md-4 col-lg-4 col-xl-3">
                <div class="">
                    <p class="text-white">
                        SKILLS ON DEMAND
                    </p>
                    <p style="color: #ccc;">In publishing and graphic design, lorem ipsum is a placeholder text commonly used to demonstrate
                        the visual... <a href="about_us.html">READ MORE</a></p>
                    <img src="" alt="Logo" class="text-white">

                </div>
            </div>
            <div class="col-xs-4 col-sm-4 col-md-4 col-lg-4 col-xl-3">
                <div class="recent_news">
                    <p class="text-white">
                        RECENT NEWS
                    </p>
                </div>

                <div class="footer_pg_list">
                    <ul>
                        <li> <a href="index.html">Home</a></li>
                        <li><a href="about_us.html">About us</a></li>
                        <li><a href="contact_us.html">Contact Us</a></li>
                        <li><a href="services.html">Services</a></li>
                        <li><a href="application_form.html">Careers</a></li>
                    </ul>
                </div>
            </div>
            <div class="col-xs-4 col-sm-4 col-md-4 col-lg-4 col-xl-3">
                <div class="footer_categories">
                    <p class="text-white">CATEGORIES </p>
                </div>

                <div class="footer_pg_list">
                    <ul>
                        <li> <a href="#">Business</a></li>
                        <li><a href="#">Design</a></li>
                        <li><a href="#">Real life</a></li>
                        <li><a href="#">Science</a></li>
                        <li><a href="#">Tech</a></li>
                    </ul>
                </div>
            </div>
            <div class="col-xs-4 col-sm-4 col-md-4 col-lg-4 col-xl-3">
                <div class="footer_subsc">
                    <p class=" text-white">
                        SUBSCRIBE
                    </p>
                </div>
                <div class="Footer_res">
                    <p style="color: #fff;">
                        Get monthly updates and free resources.
                    </p>
                </div>
                <div class="media-container-column" data-form-type="formoid">
                    <form class="form-inline">
                        <input type="hidden" value="" data-form-email="true">
                        <div class="form-group">
                            <input type="email" class="form-control input-sm input-inverse my-2" name="email" required="" data-form-field="Email" placeholder="Email" id="email-footer-3h">
                        </div>
                        <div class="input-group-btn">
                            <button href="" class="btn btn-info footer_button" type="submit" role="button">Subscribe</button>
                        </div>
                    </form>
                </div>

            </div>
        </div>


        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 col-xl-12 footer-lower">
            <hr class="footer_line">
            <div class="row">
                <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6 col-xl-7">
                    <div class="footer_copyright">
                        <p class="text-white">
                            Â© Copyright 2017 Footer Template - All Rights Reserved
                        </p>
                    </div>
                </div>
                <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6 col-xl-5 text-right">
                    <div class="social_list">
                        <ul>
                            <li>
                                <a href="#" target="_blank">
                                    <span class="fa fa-twitter" media-simple="true"></span>
                                </a>
                            </li>
                            <li>
                                <a href="#" target="_blank">
                                    <span class="fa fa-facebook" media-simple="true"></span>
                                </a>
                            </li>
                            <li>
                                <a href="#" target="_blank">
                                    <span class="fa fa-instagram" media-simple="true"></span>
                                </a>
                            </li>
                            <li>
                                <a href="#" target="_blank">
                                    <span class="fa fa-youtube" media-simple="true"></span>
                                </a>
                            </li>
                            <li>
                                <a href="#" target="_blank">
                                    <span class="fa fa-google-plus" media-simple="true"></span>
                                </a>
                            </li>
                        </ul>

                    </div>
                </div>
            </div>
        </div>
    </div>
</footer> -->

	<%@include file="shared/Footer.jsp"%>





</body>
</html>
