<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="jstl" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib uri="http://www.springframework.org/security/tags"
	prefix="sec"%>
<!DOCTYPE html >
<html>
<head>
	<title>	Forget password</title>
	<meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css">
<style type="text/css">
	.ForgetPage {
    margin-top: 90px;
        width: max-content;
    background: #ffffffc9;
    box-shadow: 0px 0 35px 6px #0000002b;
    border-radius: 10px;
    height: 50vh;
}
.ForgetPageHedading {
    margin-top: 20px;
}
</style>
<style>
.error {
	padding: 1 1;
	border: none;
	margin-top: 8px;
	border-radius: 0px;
	border-left: 5px ;
	background-color: #f8f9fa;
	font-size: 12px;
	font-style: italic;
}
</style>
</head>
<body>
   <div class="container ForgetPage">
     <div class="row">     
      <div class="col-md-12">
      	<div class="ForgetPageHedading">
      		<h2 class="text-center">Reset you password</h2>
      	</div>
      	${failed1}
      	<div class="ForgetPageForm">
      	  <form:form  method="POST"  
      	  action="/SOD/clientSaveNewPassword?${_csrf.parameterName}=${_csrf.token}" modelAttribute="passwordUpdate">
			<div class="form-group">
		     <label for="exampleInputPassword">Enter your new Password</label>
		     <form:password  path="client_password" id="txtPassword" placeholder="new password" cssClass="form-control" />
		   	</div>
			<div class="form-group">
			 <label for="exampleInputPassword1">Re-Enter your new Password</label>
			 <form:password path="client_confirmpassword" id="txtConfirmPassword" placeholder="re enterPassword" cssClass="form-control" />
			 <span id="error2" class="form-control error "></span>
			</div>
			<form:hidden path="client_email" value="${passwordUpdate.client_email}"/>
                   <form:hidden path="id" value="${passwordUpdate.id}" />
                   <form:hidden path="client_username" value="${passwordUpdate.client_username}" />
                   <form:hidden path="client_phonenum" value="${passwordUpdate.client_phonenum}" />
                   <form:hidden path="client_address" value="${passwordUpdate.client_address}" />
                   <form:hidden path="client_role" value="${passwordUpdate.client_role}" />
			<button type="submit" class="btn btn-primary" id="btnSubmit">Submit</button>
		</form:form>	
      	</div>
      </div>
     </div>
   </div>



  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js"></script>
  
  <script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.8.3/jquery.min.js"></script>
<script type="text/javascript">
    $(function () {
        $("#btnSubmit").click(function () {
            var password = $("#txtPassword").val();
            var confirmPassword = $("#txtConfirmPassword").val();
            var message = document.getElementById("error2");
            if (password == "") {
            	message.innerHTML="Field can not be empty.";
                return false;
            }
            if (password != confirmPassword) {
            	message.innerHTML="Passwords do not match.";
                return false;
            }
            return true;
        });
    });
</script>
  
</body>
</html>