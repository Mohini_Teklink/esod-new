
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@taglib prefix="jstl" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@taglib prefix="sf" uri="http://www.springframework.org/tags/form"%>
<!--
	@author : Prateek Raj   
	@organization : Teklink Enterprise
	@Description: Index.jsp is the landing page of SOD web Application. This Page provides information about SOD and Reference link to register and login for the Client & the Job-Seeker/ Expert Side.
	
-->
<!DOCTYPE html>
<jstl:set var="context" value="${pageContext.request.contextPath}" />
<spring:url var="css" value="/resources/css"></spring:url>
<spring:url var="images" value="/resources/images"></spring:url>
<spring:url var="boots400" value="/resources/bootstrap-4.0.0-dist"></spring:url>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<meta charset="utf-8">
<meta name="viewport"
	content="width=device-width, initial-scale=1, shrink-to-fit=no">
<!--For Fabvicon Icon-->
<link rel="shortcut icon" type="image/png" href="${images}/favicon.png" />
<link href="${boots400}/css/bootstrap.min.css" rel="stylesheet">
<!--For Toggle button Responsive on Mobile-->
<script src="${boots400}/jquery/jquery-3.3.1.min.js"></script>
<!--For Toggle button Responsive on Mobile-->
<script src="${boots400}/js/bootstrap.min.js"></script>
<!--For Fa icon-->
<link href="${boots400}/font-awesome/css/font-awesome.min.css"
	rel="stylesheet">
<!--Custom Css-->
<link href="${css}/style_1.css" rel="stylesheet" type="text/css">
<!--Responsive Css-->
<link href="${css}/responsive.css" rel="stylesheet" type="text/css">

<title>Skills on demand</title>
</head>

<body>

	<!-- Navigation -->
	<%@ include file="shared/Header.jsp"%>


	<!--header-------------------------------------------------------->
	<header class="masthead text-center deman_head">
	<div class="container">
		<div class="row">
			<div
				class="col-xs-12 col-sm-12 col-md-12 col-xl-12 col-lg-12 mx-auto">
				<div class="text-uppercase skill_main_head">
					<p>
						<span></span>
					</p>
				</div>
			</div>
		</div>
	</div>
	<div class="hero-action">
		<button type="button" class="btn btn-info">
			<a href="${context}/AboutUs">More Details 
			<%-- <img
				src="${images}/more_details.gif" alt="" style="width: 25%"> --%></a>
		</button>
	</div>
	</header>


	<!--Section--container-------------------------------------------->
	<section class="index_cont">
	<div class="container">
		<div class="row vdivide">
			<div
				class="col-xs-6 col-sm-6 col-md-6 col-lg-6 col-xl-6 mx-auto text-center">
				<div class=" index_sod_cln">
					<div class=" index_sod_client">
						<h1>Client</h1>
						<hr class="light my-4 demad">


						<button type="button" class="btn btn-info register">
							<a href="${context}/client-register"> Register</a>
						</button>
						<div class="change_link">
							<p>
								Already a member ? <a href="${context}/clientLogin"
									class="to_register">Login</a>
							</p>
						</div>
					</div>
				</div>
			</div>

			<div
				class="col-xs-6 col-sm-6 col-md-6 col-lg-6 col-xl-6 mx-auto text-center">
				<div class="index_sod_exp">
					<div class=" index_sod_client">
						<h1>Expert</h1>
						<hr class="light my-4 demad">

						<button type="button" class="btn btn-info register">
							<a href="${context}/expert-register"> Register</a>
						</button>
						 <div class="change_link">
							<p>
								Already a member ? <a href="${context}/expertLogin"
									class="to_register">Login</a>
							</p>
						</div> 
					</div>
				</div>
			</div>
		</div>
	</div>
	</section>
	<!--------------footer-------------->
	<!-- Navigation -->
	<%@ include file="shared/Footer.jsp"%>


</body>
</html>
