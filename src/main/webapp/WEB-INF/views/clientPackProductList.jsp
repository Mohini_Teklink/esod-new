<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="jstl" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<!DOCTYPE HTML>
<spring:url var="css" value="/resources/css"></spring:url>
<spring:url var="images" value="/resources/images"></spring:url>
<spring:url var="boots400" value="/resources/bootstrap-4.0.0-dist"></spring:url>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<meta charset="utf-8">
<meta name="viewport"
	content="width=device-width, initial-scale=1, shrink-to-fit=no">
<!--For Fabvicon Icon-->
<link rel="shortcut icon" type="image/png" href="${images}/favicon.png" />
<link href="${boots400}/css/bootstrap.min.css" rel="stylesheet">
<!--For Toggle button Responsive on Mobile-->
<script src="${boots400}/jquery/jquery-3.3.1.min.js"></script>
<!--For Toggle button Responsive on Mobile-->
<script src="${boots400}/js/bootstrap.min.js"></script>
<!--For Fa icon-->
<link href="${boots400}/font-awesome/css/font-awesome.min.css"
	rel="stylesheet">
<!--Custom Css-->
<link href="${css}/style_1.css" rel="stylesheet" type="text/css">
<!--Responsive Css-->
<link href="${css}/responsive.css" rel="stylesheet" type="text/css">

<title>Basic Product</title>
</head>

<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">

</head>
<body>
	<%@include file="shared/Header.jsp"%>


	<section class="basic_product_main">
		<div class="container">
			<div class="row">
				<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 col-xl-12 ">
					<div class="back_page">
						<a class="fa fa-arrow-left" href="/SOD/client/clientPacks">&nbsp;Back</a>
					</div>
				</div>
				<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 col-xl-12 ">
					<div class="basic_head">
						<h1>
							<%-- ${fn:toUpperCase(fn:substring(clientPack.pack_name, 0, 1))}${fn:toLowerCase(fn:substring(clientPack.pack_name, 1,fn:length(clientPack.pack_name)))} --%>
							Services
						</h1>
					</div>
				</div>
			</div>
			<%-- <!--******************* Client Products List based on the package selected *******************-->
			<jstl:forEach items="${clientProducts}" var="list">
				<div class="row product_basic">
					<div class="col-xs-4 col-sm-4 col-md-4 col-lg-4 col-xl-2">
						<div class="hovereffect">
						<!--******************* Image of Product *******************-->
							<img src="${images}/${list.imagePath}" class="Img_prd_a img-responsive"
								width="150" height="120">
						</div>
					</div>
					<div class="col-xs-8 col-sm-8 col-md-8 col-lg-8 col-xl-10">
						<div class="basic_proj_head">
						<!--******************* Name of Product capitalizing First Letter *******************-->
							<h3>${fn:toUpperCase(fn:substring(list.name, 0, 1))}${fn:toLowerCase(fn:substring(list.name, 1,fn:length(list.name)))}
								Project </h3>
						</div>
						<!--******************* Displaying Description of Product *******************-->
						<div class="basic_proj_pg">
							<p>${list.prodDescpt}</p>
						<!--******************* Read More Link Specifying Product Id   *******************-->
							<a
								href="/SOD/client/clientPacks/${clientPack.id}/${clientPack.pack_name}/${list.id}/${list.name}/"
								class="readMore">Read More</a>
						</div>
					</div>
				</div>
			</jstl:forEach>
			<!--******************* Client Products List based on the package selected *******************-->
			 --%>

			<div class="row product_basic">
				<div class="col-xs-4 col-sm-4 col-md-4 col-lg-4 col-xl-2">
					<div class="hovereffect">
						<img src="${images}/forward.jpg" class="Img_prd_a img-responsive"
							width="150" height="120">
					</div>
				</div>
				<div class="col-xs-8 col-sm-8 col-md-8 col-lg-8 col-xl-10">
					<div class="basic_proj_head">
						<jstl:choose>
						<jstl:when test="${packageId == 1}">
						<h3>
							<%-- ${fn:toUpperCase(fn:substring(clientPack.pack_name, 0, 1))}${fn:toLowerCase(fn:substring(clientPack.pack_name, 1,fn:length(clientPack.pack_name)))} --%>
							Package 1- Consultant <%-- ${clientPack.id} --%>
						</h3>
						</jstl:when>
						<jstl:when test="${packageId == 2}">
						<h3>
							<%-- ${fn:toUpperCase(fn:substring(clientPack.pack_name, 0, 1))}${fn:toLowerCase(fn:substring(clientPack.pack_name, 1,fn:length(clientPack.pack_name)))} --%>
							Package 2- Senior Consultant <%-- ${clientPack.id} --%>
						</h3>
						</jstl:when>
						<jstl:when test="${packageId == 3}">
						<h3>
							<%-- ${fn:toUpperCase(fn:substring(clientPack.pack_name, 0, 1))}${fn:toLowerCase(fn:substring(clientPack.pack_name, 1,fn:length(clientPack.pack_name)))} --%>
							Package 3- Solution Architect <%-- ${clientPack.id} --%>
						</h3>
						</jstl:when>
						</jstl:choose>
						<%-- <h3>
							${fn:toUpperCase(fn:substring(clientPack.pack_name, 0, 1))}${fn:toLowerCase(fn:substring(clientPack.pack_name, 1,fn:length(clientPack.pack_name)))}
							Project ${clientPack.id}
						</h3> --%>
					</div>
					<div class="basic_proj_pg">

						<jstl:choose>
							<jstl:when test="${packageId == 1}">
								<p>This resource has an experience of more than 5 years in
									his core skill and has experience in Implementation, Support,
									Migration , and Upgrade Projects. He should be able to provide
									solutions on the core skill and also support the application if
									required.</p>
								<a
									href="/SOD/client/clientPacks/${clientPack.id}/${clientPack.pack_name}/${clientPack.id}/${clientPack.pack_name}/"
									class="readMore">Read More</a>
							</jstl:when>
							<jstl:when test="${packageId == 2}">
								<p>This resource has an experience of more than 10 years
									in IT Industry and also acted as a lead. He is superior in his
									core skill and also can provide proficient consultancy in one
									of the additional skills. He also has experience in multiple
									Implementation, Production Support, Migration, and Upgrade
									Projects.</p>
								<a
									href="/SOD/client/clientPacks/${clientPack.id}/${clientPack.pack_name}/${clientPack.id}/${clientPack.pack_name}/"
									class="readMore">Read More</a>
							</jstl:when>
							<jstl:when test="${packageId == 3}">
								<p>This resource has an experience of more than 12 years in
									IT Industry and also acted as a Lead and Project Manager. He is
									superior in his core skill and also can provide proficient
									consultancy in number of the additional skills. He also has
									experience in multiple Implementation, Production Support,
									Migration, and Upgrade Projects.</p>
								<a
									href="/SOD/client/clientPacks/${clientPack.id}/${clientPack.pack_name}/${clientPack.id}/${clientPack.pack_name}/"
									class="readMore">Read More</a>
							</jstl:when>
						</jstl:choose>
						<%-- <p>This project is based on idea through which user can order
							food from a restaurant using this website. This is full
							responsive website i.e. properly visible on desktop, mobiles and
							tablets. In this project there are 2 modules i.e Admin and Use
							Module....</p>
						<a
							href="/SOD/client/clientPacks/${clientPack.id}/${clientPack.pack_name}/${clientPack.id}/${clientPack.pack_name}/"
							class="readMore">Read More</a> --%>
					</div>
				</div>
			</div>
		</div>
	</section>

	<%@include file="shared/Footer.jsp"%>
</body>
</html>