<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="jstl" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="sf" uri="http://www.springframework.org/tags/form"%>
<%@ taglib uri="http://www.springframework.org/security/tags"
	prefix="sec"%>
<!DOCTYPE html >
<jstl:set var="context" value="${pageContext.request.contextPath}" />
<spring:url var="css" value="/resources/css"></spring:url>
<spring:url var="images" value="/resources/images"></spring:url>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="shortcut icon" type="image/png" href="${images}/favicon.png" />
<link rel="stylesheet"
	href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css">
<script
	src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
<script
	src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js"></script>
<script
	src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js"></script>
<link href="${css}/style_1.css" rel="stylesheet" type="text/css">
<link href="${css}/responsive.css" rel="stylesheet" type="text/css">
<title>LogIn | SOD</title>
<style>
.error {
	padding: 1 1;
	border-color: red;
	background-color: #ffeaea;
	font-size: 12px;
	font-style: italic;
}
</style>

</head>
<body>
	<%@include file="shared/Header.jsp"%>
	<section class="emp_log_in">
		<div class="container ">
			<div class="row">
				<div
					class="col-xs-12 col-sm-12 col-md-12 col-lg-12 col-xl-6 mx-auto emp_login_box">
					<div class="main_head form_heading text-center">
						<h1>Expert Login</h1>
						<hr class="light my-4 demad">
					</div>
                 
					<!-- Expert Login Form Action Url -->
					<jstl:url value="/expert/expertLoginProcess" var="loginVar" />

					<!-- *************Expert Login Form Start************* -->
					<form action="${loginVar}" method="POST">
						<div class="form-group">
							<label class="emp_login_common">Email:</label> <input type="text"
								name="expertemail" id="expertemail" class="form-control"
								placeholder="Enter Registered Email" required="required" />
						</div>
						<div class="form-group">
							<label class="emp_login_common">Password:</label> <input
								type="password" id="expertpassword" name="expertpassword"
								class="form-control" placeholder="Enter Password"
								required="required" />
						</div>
						
						<div class="form-check emp_login_common">
					<input type="checkbox" onclick="myFunction()"> Show Password
					</div>
						<div>
					
						</div>
						<div class="form-check emp_login_common">
						<input
								type="checkbox" id="remember" name="expert-remember-me"
								value="true"
								style="vertical-align: middle; top: 2px; left: 0; height: 14px; width: 14px; background-color: #eee;" />
							<label for="remember"> Remember Me</label> 
						</div>
						<sec:csrfInput />
						<c:if test="${param.error}">
							<p class="alert alert-warning text-center">Please enter valid Email and password</p>
						</c:if>
						 <span class="input-group-btn">
							<a type="button" class="btn btn-danger sub_btn register"
						    href="${context}">Cancel</a>
							<button type="submit" class="btn btn-info register sub_btn">Submit</button>
						</span>
					</form>
					<!-- *************Expert Login Form End************* -->

					<div class="inbox-body">
						<a href="#myModal" data-toggle="modal" title="Compose"
							class="forget_psw"> Forgot Password </a>
						<!-- Modal -->
						
						<div aria-hidden="true" aria-labelledby="myModalLabel"
							role="dialog" tabindex="-1" id="myModal" class="modal fade"
							style="display: none;">
							<div class="modal-dialog">
								<div class="modal-content">
									<div class="modal-header">
										<h4 class="modal-title">Forgot Password</h4>
										<button aria-hidden="true" data-dismiss="modal" class="close"
											type="button">�</button>
									</div>
									<div class="modal-body">
										<div class="text-center">
											<h3>
												<i class="fa fa-lock fa-4x"></i>
											</h3>
											<h2 class="text-center">Forgot Password?</h2>
											<div class="panel-body">
												<p class="Error">${Success}</p>
												<p class="Error1">${noUser}</p>
												<p class="Error1">${failed}</p>
												<form:form class="form"  action="/SOD/resetPassword"
													method="post">
													<fieldset>
														<div class="form-group">
															<div class="input-group">
																<div class="input-group-append">
																	<button class="btn btn-info" type="button">
																		<i class="fa fa-envelope"></i>
																	</button>
																</div>
																<input id="emailInput" placeholder="email address"
																	name="email" class="form-control" type="email"
																	oninvalid="setCustomValidity('Please enter a valid email address!')"
																	onchange="try{setCustomValidity('')}catch(e){}"
																	required="">
															</div>
														</div>
														<div class="form-group">
															<input class="btn btn-lg btn-info btn-block" id="submit_form"
																type="submit">
														</div>
													</fieldset>
												</form:form>

											</div>
										</div>
									</div>
								</div>
								<!-- /.modal-content -->
							</div>
							<!-- /.modal-dialog -->
						</div>
						<!-- /.modal -->
					</div>
				</div>

			</div>
		</div>

	</section>

	<%@include file="shared/Footer.jsp"%>

</body>
<script>
$(document).ready(function(){
	var wrongemail = ${resetEmailWrong};
	if(wrongemail==1)
    $("#myModal").modal("show");
    
   
});
</script>
<script>
function myFunction() {
    var x = document.getElementById("expertpassword");
    if (x.type === "password") {
        x.type = "text";
    } else {
        x.type = "password";
    }
}
</script>

</html>