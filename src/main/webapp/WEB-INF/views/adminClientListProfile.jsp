<!-- @author Prateek Raj -->

<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="jstl" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="sf" uri="http://www.springframework.org/tags/form"%>
<!DOCTYPE html>
<spring:url var="css" value="/resources/admin/css"></spring:url>
<spring:url var="vendor" value="/resources/admin/vendor"></spring:url>
<spring:url var="images" value="/resources/admin/images"></spring:url>
<spring:url var="js" value="/resources/admin/js"></spring:url>
<html>
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="description" content="">
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta name="robots" content="all,follow">
<link rel="shortcut icon" type="image/png" href="${images}/favicon.jpg" />
<!-- Bootstrap CSS-->
<link rel="stylesheet" href="${vendor}/bootstrap/css/bootstrap.min.css">
<!-- Font Awesome CSS-->
<link rel="stylesheet"
	href="${vendor}/font-awesome/css/font-awesome.min.css">
<!-- Fontastic Custom icon font-->
<link rel="stylesheet" href="${css}/fontastic.css">
<!-- Google fonts - Roboto -->
<link rel="stylesheet"
	href="https://fonts.googleapis.com/css?family=Roboto:300,400,500,700">
<!-- jQuery Circle-->
<link rel="stylesheet"
	href="${css}/grasp_mobile_progress_circle-1.0.0.min.css">
<!-- Custom Scrollbar-->
<link rel="stylesheet"
	href="${vendor}/malihu-custom-scrollbar-plugin/jquery.mCustomScrollbar.css">
<!-- theme stylesheet-->
<link rel="stylesheet" href="${css}/style.default.css"
	id="theme-stylesheet">
<!-- Custom stylesheet - for your changes-->
<link rel="stylesheet" href="${css}/custom.css">
<!-- Favicon-->
<link rel="shortcut icon" href="img/favicon.ico">

<title>Client List</title>
<!-- Tweaks for older IEs-->
<!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script><![endif]-->



</head>
<body>
	<%@include file="shared/shared-admin/Header.jsp"%>
	<div class="page">
		<!-- navbar-->
		<header class="header">
			<nav class="navbar">
				<div class="container-fluid">
					<div
						class="navbar-holder d-flex align-items-center justify-content-between">
						<div class="navbar-header">
							<a id="toggle-btn" href="#" class="menu-btn"><i
								class="fa fa-bars"> </i></a><a href="dashboard.html"
								class="navbar-brand">
								<div class="brand-text d-none d-md-inline-block">
									<span>dashboard </span> <strong class="text-primary">Skills
										On Demand</strong>
								</div>
							</a>
						</div>
						<ul
							class="nav-menu list-unstyled d-flex flex-md-row align-items-md-center">

							<!-- Languages dropdown    -->
							<li class="nav-item dropdown user_img_nav"><a id="languages"
								rel="nofollow" data-target="#" href="#" data-toggle="dropdown"
								aria-haspopup="true" aria-expanded="false"
								class="nav-link language dropdown-toggle"><img
									src="${images}/user.png" alt="English"><span
									class="d-none d-sm-inline-block">User Name</span></a>
								<ul aria-labelledby="languages" class="dropdown-menu">
									<li><a rel="nofollow" href="#" class="dropdown-item"><span>Profile</span></a></li>
									<li><a rel="nofollow" href="#" class="dropdown-item"><span>Setting</span></a></li>
								</ul></li>
							<!-- Log out-->
							<li class="nav-item"><a href="login.html"
								class="nav-link logout"> <span
									class="d-none d-sm-inline-block">Logout</span><i
									class="fa fa-sign-out"></i></a></li>
						</ul>
					</div>
				</div>
			</nav>
		</header>


		<!--Main Section Start-->
		<section class="product_main">
			<div class="container">
				<div class="row">
					<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 col-xl-12">
						<div class="x_panel text-muted">
							<div class="x_title">
								<h1>
									User Report <small> Activity Report</small>
								</h1>
								<div class="clearfix"></div>
							</div>
							<div class="x_content">
								<!-- <p>
									Simple table with project listing with <b>Id, Name</b> and <b>editing</b>
									options
								</p> -->
							</div>

							<div class="row">
								<div class="col-md-3 col-sm-3 col-xs-12 profile_left">
									<div class="image_profile">
										<img class="img-responsive" src="${images}/user.png"
											alt="Avatar" title="Change the avatar">
									</div>
									<div class="prf_ur_name">
										<h5>${clientDetails.client_username}</h5>
									</div>
									<ul>
										<li><i class="fa fa-map-marker user-profile-icon"></i>
											${clientDetails.client_address}</li>

										

										<li class="m-top-xs"><i
											class="fa fa-external-link user-profile-icon"></i> <a
											href="#">${clientDetails.client_email}</a></li>
									</ul>
									<!-- start skills -->
									 <div class="usr_prf_skills">
										
									</div>
									<jstl:set var="skillGroupVar" value="" />
									<jstl:forEach items="${clientSkills}" var="skillListVar">
										<jstl:if test="${empty skillGroupVar}">
											<jstl:set var="skillGroupVar"
												value="${skillListVar.skillGroup}" />
											<p style="font-size: 16px; font-weight: 400;">${skillGroupVar}</p>
											<ul style="list-style: circle; padding-left: 4px;"></ul>
										</jstl:if>
										<jstl:if test="${skillGroupVar != skillListVar.skillGroup}">
											
											<jstl:set var="skillGroupVar"
												value="${skillListVar.skillGroup}" />
											<p style="font-size: 16px; font-weight: 400;">${skillGroupVar}</p>
											<ul style="list-style: circle; padding-left: 4px;"></ul>
										</jstl:if>
										<li style="font-size: 12px; margin: 0px 20px;">
											<p >${skillListVar.skillName}</p>
										</li>

									</jstl:forEach>
									
								</div>
								<div class="col-md-9 col-sm-9 col-xs-12">

									<div class="row profile_title">
										<div class="col-md-6">
											<h4>User Activity Report</h4>
										</div>
										<div class="col-md-6">
											<div id="reportrange" class="pull-right"
												style="margin-top: 5px; background: #fff; cursor: pointer; padding: 5px 10px; border: 1px solid #E6E9ED">
												<i class="glyphicon glyphicon-calendar fa fa-calendar"></i>
												<!-- <span>April 23, 2018 - may 28, 2019</span> -->
											</div>
										</div>
									</div>





									<!-- Start Tabs -->
									<div class="nav-tabs-wrapper profile_activity">
										<ul class="nav nav-tabs dragscroll horizontal">
											<li class="nav-item"><a class="nav-link active"
												data-toggle="tab" href="#tabA">Recent Activity</a></li>
											<!-- <li class="nav-item"><a class="nav-link"
												data-toggle="tab" href="#tabB">Projects Worked On</a></li> -->
											<li class="nav-item"><a class="nav-link"
												data-toggle="tab" href="#tabC">Profile</a></li>
										</ul>
									</div>

									<span class="nav-tabs-wrapper-border" role="presentation"></span>

									<div class="tab-content">
										<div class="tab-pane fade show active" id="tabA">
											<div class="row">
												<div class="col-xs-2 col-sm-2 col-md-1">
													<img src="${images}/user.png" class="avatar" alt="Avatar">
												</div>
												<div class="col-xs-8 col-sm-8 col-md-10 message_wrapper">
													<h4 class="heading">Lorem Ipsum</h4>
													<blockquote class="message">In publishing and
														graphic design, lorem ipsum is a placeholder text commonly
														used to demonstrate the visual form of a document without
														relying on meaningful content (also called greeking).</blockquote>
												</div>
												<div class="col-xs-2 col-sm-2 col-md-1 message_date">
													<h3 class="date text-info">24</h3>
													<p class="month">May</p>
												</div>
											</div>
											<div class="row">
												<div class="col-xs-2 col-sm-2 col-md-1">
													<img src="${images}/user.png" class="avatar" alt="Avatar">
												</div>
												<div class="col-xs-8 col-sm-8 col-md-10 message_wrapper">
													<h4 class="heading">Lorem Ipsum</h4>
													<blockquote class="message">In publishing and
														graphic design, lorem ipsum is a placeholder text commonly
														used to demonstrate the visual form of a document without
														relying on meaningful content (also called greeking).</blockquote>
												</div>
												<div class="col-xs-2 col-sm-2 col-md-1 message_date">
													<h3 class="date text-info">24</h3>
													<p class="month">May</p>
												</div>
											</div>
										</div>
										<div class="tab-pane fade" id="tabB">
											<table class="data table table-striped no-margin">
												<thead>
													<tr>
														<th>#</th>
														<!-- <th>Project Name</th> -->
														<th>Client Company</th>
														<th class="hidden-phone">Hours Spent</th>
														<th>Contribution</th>
													</tr>
												</thead>
<!-- 												<tbody> -->
<!-- 													<tr> -->
<!-- 														<td>1</td> -->
<!-- 														<td>Project 4</td> -->
<!-- 														<td>Teklink</td> -->
<!-- 														<td class="hidden-phone">18</td> -->
<!-- 														<td class="vertical-align-mid"> -->
<!-- 															<div class="progress"> -->
<!-- 																<div class="progress-bar progress-bar-success" -->
<!-- 																	data-transitiongoal="35" style="width: 35%;"></div> -->
<!-- 															</div> -->
<!-- 														</td> -->
<!-- 													</tr> -->
<!-- 													<tr> -->
<!-- 														<td>2</td> -->
<!-- 														<td>Project 2</td> -->
<!-- 														<td>Teklink</td> -->
<!-- 														<td class="hidden-phone">13</td> -->
<!-- 														<td class="vertical-align-mid"> -->
<!-- 															<div class="progress"> -->
<!-- 																<div class="progress-bar progress-bar-danger" -->
<!-- 																	data-transitiongoal="15" style="width: 15%"></div> -->
<!-- 															</div> -->
<!-- 														</td> -->
<!-- 													</tr> -->
<!-- 													<tr> -->
<!-- 														<td>3</td> -->
<!-- 														<td>Project 3</td> -->
<!-- 														<td>Teklink</td> -->
<!-- 														<td class="hidden-phone">30</td> -->
<!-- 														<td class="vertical-align-mid"> -->
<!-- 															<div class="progress"> -->
<!-- 																<div class="progress-bar progress-bar-success" -->
<!-- 																	data-transitiongoal="45" style="width: 45%"></div> -->
<!-- 															</div> -->
<!-- 														</td> -->
<!-- 													</tr> -->
<!-- 													<tr> -->
<!-- 														<td>4</td> -->
<!-- 														<td>Project 1</td> -->
<!-- 														<td>Teklink</td> -->
<!-- 														<td class="hidden-phone">28</td> -->
<!-- 														<td class="vertical-align-mid"> -->
<!-- 															<div class="progress"> -->
<!-- 																<div class="progress-bar progress-bar-success" -->
<!-- 																	data-transitiongoal="75" style="width: 75%"></div> -->
<!-- 															</div> -->
<!-- 														</td> -->
<!-- 													</tr> -->
<!-- 												</tbody> -->
											</table>
										</div>
										<div class="tab-pane fade" id="tabC">
											<p>In publishing and graphic design, lorem ipsum is a
												placeholder text commonly used to demonstrate the visual
												form of a document without relying on meaningful content
												(also called greeking). Replacing the actual content with
												placeholder text allows designers to design the form of the
												content before the content itself has</p>
										</div>
									</div>

									<!--Tabs Responsive End-->

								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</section>




		<!--Footer Section Start-->
		<footer class="main-footer bg-light">
			<div class="text-right footer_logo_img">
				<img src="${images}/logo-SOD.png">
				<!-- Please do not remove the backlink to us unless you support further theme's development at https://bootstrapious.com/donate. It is part of the license conditions and it helps me to run Bootstrapious. Thank you for understanding :)-->
			</div>
			<div class="pull-right">
				<p>Copyright � 2018-2019 All Rights Reserved.</p>
			</div>
		</footer>

		<!--Footer Section End-->
	</div>
	<!-- JavaScript files-->
	<script src="${vendor}/jquery/jquery.min.js"></script>
	<script src="${vendor}/popper.js/umd/popper.min.js">
		
	</script>
	<script src="${vendor}/bootstrap/js/bootstrap.min.js"></script>
	<script src="${js}/grasp_mobile_progress_circle-1.0.0.min.js"></script>
	<script src="${vendor}/jquery.cookie/jquery.cookie.js">
		
	</script>
	<script src="${vendor}/chart.js/Chart.min.js"></script>
	<script src="${vendor}/jquery-validation/jquery.validate.min.js"></script>
	<script
		src="${vendor}/malihu-custom-scrollbar-plugin/jquery.mCustomScrollbar.concat.min.js"></script>
	<script src="${js}/charts-home.js"></script>
	<!-- Main File-->
	<script src="${js}/front.js"></script>
</body>
</html>
