<!--  @author Hardev Singh -->
<%@page import="java.awt.image.RenderedImage"%>
<%@page
	import="org.springframework.web.servlet.mvc.method.annotation.RequestResponseBodyMethodProcessor"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>

<%@taglib prefix="jstl" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@taglib prefix="sf" uri="http://www.springframework.org/tags/form"%>

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">

<spring:url var="css" value="/resources/css"></spring:url>
<spring:url var="images" value="/resources/images"></spring:url>
<spring:url var="productImage" value="/resources/productImages"></spring:url>
<spring:url var="js" value="/resources/js"></spring:url>
<spring:url var="vendor" value="/resources/vendor"></spring:url>

<html>
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="description" content="">
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta name="robots" content="all,follow">
<link rel="shortcut icon" type="image/png" href="${images}/favicon.jpg" />
<!-- Bootstrap CSS-->
<link rel="stylesheet" href="${vendor}/bootstrap/css/bootstrap.min.css">
<!-- Font Awesome CSS-->
<link rel="stylesheet"
	href="${vendor}/font-awesome/css/font-awesome.min.css">
<!-- Fontastic Custom icon font-->
<link rel="stylesheet" href="${css}/fontastic.css">
<!-- Google fonts - Roboto -->
<link rel="stylesheet"
	href="https://fonts.googleapis.com/css?family=Roboto:300,400,500,700">
<!-- jQuery Circle-->
<link rel="stylesheet"
	href="${css}/grasp_mobile_progress_circle-1.0.0.min.css">
<!-- Custom Scrollbar-->
<link rel="stylesheet"
	href="${vendor}/malihu-custom-scrollbar-plugin/jquery.mCustomScrollbar.css">
<!-- theme stylesheet-->
<link rel="stylesheet" href="${css}/style.default.css"
	id="theme-stylesheet">
<!-- Custom stylesheet - for your changes-->
<link rel="stylesheet" href="${css}/custom.css">
<!-- Favicon-->
<link rel="shortcut icon" href="${images}/favicon.ico">

<title>Product View</title>
<!-- Tweaks for older IEs-->
<!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script><![endif]-->



</head>
<body>
	<!-- Side Navbar -->
	<%@ include file="shared/shared-admin/Header.jsp"%>
	<div class="page">
		<%@include file="shared/shared-admin/Navbar.jsp"%>
		<!--Main Section Start-->
		 <section class="product_main">
<div class="container">
        <div class="row">
          <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 col-xl-12">
            <div class="x_panel text-muted">
              <div class="x_title">
                <h2>Product Name</h2>
                <div class="clearfix"></div>
              </div>
              <div class="x_content">
                <jstl:out value="${prodView.prodName}"></jstl:out>
              </div>
              <div class="row">
                <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6 col-xl-6">
                  <div class=" basic_prd_img">
                    <div class="images_prod">
                      <img src="${productImage}/${prodView.prodImagePath}" width="100%" height="auto">
                    </div>
                    <div class="img_desc"><p>A variation of the ordinary lorem ipsum text has been used in typesetting since the 1960s or earlier, when it was popularized by advertisements for Letraset transfer sheets. </p>
                    </div>
                  </div>
                  <div class="sale_prd_head">
                    <h4>Item Description :</h4></div>
                  <div class="user-html">
                    <jstl:out value="${prodView.prodDescription}" />                   
                    <br><br>

                    <b>Admin Module :-  </b>We can login as admin using special username and password.  As it is totally dynamic website. Everything in the website is connected to database. Admin can add add, update &amp; delete new categories, products in the website.  Whatever will be added by admin will automatically be shown to user.<br><br>
                    <b>User Module :- </b>One can become member of site using signup page. After logging into the website user can view and add products to cart.  There are various payment options for making payment.
                  </div>
                </div>



                <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6 col-xl-6">
                  <div class=" basic_prd_desc">
                    <div class="sale_prd_head">
                      <h4>Buy Project Now for just US $38  or Rs. 2000/- <span class="label1 label-danger">SALE</span></h4>
                    </div>
                    <div class="sale_prd_head">
                      <h5><span class="label-danger highlight">50% Off</span>Actual Price<strike> US $75</strike> or <strike> Rs.4000</strike>/-</h5>
                    </div>
                    <div class="alert alert-info">
                      This Project is compatible with all Visual Studio Versions from 2018-2020
                    </div>
                    <div class="alert alert-warning">
                      <strong>Free !! </strong> After Sales Support. 100% Working Guarantee. Call Now at +91XXXXXXXXXX
                    </div>
                    <div class="alert alert-info">
                      <strong>Delivery : </strong> Delivered  to your email id within 24 hours after payment
                    </div>
                    <div class="alert alert-success">
                      <strong>Customization : </strong> We can change this project for you as per your requirement. Send your requirements by <a href="#">clicking here</a>
                    </div></div></div>
              </div>
              <div>
								<br><br>
								<a class="btn btn-danger text-white"
									href="/SOD/admin/dashboard/pack_prod_desc/pack/${prodView.prodType}">Back</a>
							</div>
            </div>
          </div>
        </div>
      </div>
      </section>
		<%@ include file="shared/shared-admin/Footer.jsp"%>
	</div>
</body>
</html>