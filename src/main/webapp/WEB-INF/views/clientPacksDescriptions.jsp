<!-- @author Prateek Raj -->

<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="jstl" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<!DOCTYPE HTML>
<jstl:set var="context" value="${pageContext.request.contextPath}" />
<spring:url var="css" value="/resources/css"></spring:url>
<spring:url var="images" value="/resources/images"></spring:url>
<spring:url var="bootstrapCss"
	value="/resources/bootstrap-4.0.0-dist/css" />
<spring:url var="bootstrapJs" value="/resources/bootstrap-4.0.0-dist/js" />
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<meta charset="utf-8">
<meta name="viewport"
	content="width=device-width, initial-scale=1, shrink-to-fit=no">
<meta name="description" content="">
<meta name="author" content="Prateek Raj">
<link rel="shortcut icon" type="image/png" href="${images}/favicon.png" />
<link href="${bootstrapCss}/bootstrap.min.css" rel="stylesheet">
<script
	src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
<script src="${bootstrapJs}/bootstrap.min.js"></script>
<link href="${css}/style_1.css" rel="stylesheet" type="text/css">
<link href="${css}/responsive.css" rel="stylesheet" type="text/css">
<link rel="stylesheet"
	href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
<title>${fn:toUpperCase(fn:substring(clientPack.pack_name, 0, 1))}${fn:toLowerCase(fn:substring(clientPack.pack_name, 1,fn:length(clientPack.pack_name)))}
	Product ${clientPack.id} Description</title>
</head>

<body>

	<%@ include file="shared/Header.jsp"%>
	<section class="basic_product_main">
		<div class="container">
			<div class="row">
				<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 col-xl-12 ">
					<div class="back_page">
						<a class="fa fa-arrow-left"
							href="/SOD/client/clientPacks/${clientPack.id}/${clientPack.pack_name}/">
							&nbsp; Back</a>
					</div>
				</div>
				<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 col-xl-12 ">


					<h1 class="basic_head">
						<%-- ${fn:toUpperCase(fn:substring(clientPack.pack_name, 0, 1))}${fn:toLowerCase(fn:substring(clientPack.pack_name, 1,fn:length(clientPack.pack_name)))} --%>
						<!-- Project -->
						<%-- ${clientPack.id} --%>
						<jstl:choose>
							<jstl:when test="${packageId == 1}">

								<%-- ${fn:toUpperCase(fn:substring(clientPack.pack_name, 0, 1))}${fn:toLowerCase(fn:substring(clientPack.pack_name, 1,fn:length(clientPack.pack_name)))} --%>
							Consultant <%-- ${clientPack.id} --%>

							</jstl:when>
							<jstl:when test="${packageId == 2}">

								<%-- ${fn:toUpperCase(fn:substring(clientPack.pack_name, 0, 1))}${fn:toLowerCase(fn:substring(clientPack.pack_name, 1,fn:length(clientPack.pack_name)))} --%>
							Senior Consultant <%-- ${clientPack.id} --%>

							</jstl:when>
							<jstl:when test="${packageId == 3}">

								<%-- ${fn:toUpperCase(fn:substring(clientPack.pack_name, 0, 1))}${fn:toLowerCase(fn:substring(clientPack.pack_name, 1,fn:length(clientPack.pack_name)))} --%>
							Solution Architect <%-- ${clientPack.id} --%>

							</jstl:when>
						</jstl:choose>

					</h1>
				</div>
			</div>
			<div class="row">
				<div class="col-xs-12 col-sm-12 col-md-6">
					<div class=" basic_prd_img">
						<div class="images_prod">
							<img src="${images}/forward.jpg" width="100%" height="auto">
						</div>
						<div class="img_desc">
							<jstl:choose>
								<jstl:when test="${packageId == 1}">
									<p>This resource has an experience of more than 5 years in
										his core skill and has experience in Implementation, Support,
										Migration , and Upgrade Projects. He should be able to provide
										solutions on the core skill and also support the application
										if required.</p>
									<%-- <a
										href="/SOD/clien`t/clientPacks/${clientPack.id}/${clientPack.pack_name}/${clientPack.id}/${clientPack.pack_name}/"
										class="readMore">Read More</a> --%>
								</jstl:when>
								<jstl:when test="${packageId == 2}">
									<p>This resource has an experience of more than 10 years in
										IT Industry and also acted as a lead. He is superior in his
										core skill and also can provide proficient consultancy in one
										of the additional skills. He also has experience in multiple
										Implementation, Production Support, Migration, and Upgrade
										Projects.</p>
									<%-- <a
										href="/SOD/client/clientPacks/${clientPack.id}/${clientPack.pack_name}/${clientPack.id}/${clientPack.pack_name}/"
										class="readMore">Read More</a> --%>
								</jstl:when>
								<jstl:when test="${packageId == 3}">
									<p>This resource has an experience of more than 12 years in
										IT Industry and also acted as a Lead and Project Manager. He
										is superior in his core skill and also can provide proficient
										consultancy in number of the additional skills. He also has
										experience in multiple Implementation, Production Support,
										Migration, and Upgrade Projects.</p>
									<%-- <a
										href="/SOD/client/clientPacks/${clientPack.id}/${clientPack.pack_name}/${clientPack.id}/${clientPack.pack_name}/"
										class="readMore">Read More</a> --%>
								</jstl:when>
							</jstl:choose>
						</div>
					</div>
				</div>

				<%-- <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6 col-xl-6 ">
					<div class="basic_prd_desc">
						<div class="sale_prd_head">
							<h4>Post Your Requirments</h4>
							<button style="background: #41bef6 !important;
    border: none;
    width: 42%;
    font-weight: 600;
    border-radius: 21px;" type="button" class="btn btn-primary" onclick="location.href='${context}/client/client-contract?&pid=${clientPack.id}';">Request</button>
						</div>

						<input
							onclick="location.href='${context}/client/client-contract?&pid=${clientPack.id}';"
							onclick="location.href='/SOD/client/clientPacks/${clientPack.id}/${clientPack.pack_name}/${clientPack.id}/${clientPack.pack_name}/clientContract';"
								class="mybutton"
							src="${images}/xbuynow.png" alt="projects for learning"
							type="image">
						<!-- <div class="message-box-wrap">
							<p>
								<strong>or Deposit money in our SBI account no.
									000000XXXXXXXXXX under the name of TEKLINK INTERNATIONAL with
									IFS Code as SBIN0011967 </strong>
							</p>
						</div> -->
						<!-- <div class="alert alert-warning">
							<strong>Free !! </strong> For Any Support Call Now at
							+91XXXXXXXXXX
						</div> -->
						<!-- <div class="alert alert-info">
							<strong>Delivery !! </strong> Delivered to your email id within
							24 hours after payment
						</div> -->

						<div class="container" style="padding: 50px; border-radius: 15px;">
							<form action="#">
								<div class="form-group">
									<label for="email">Resource Role</label> <input type="text"
										class="form-control" id="text" placeholder="Resource Role"
										name="Resou">
								</div>
								<!-- <div class="form-group">
									<label for="pwd">No. of Resources Required</label> <input
										type="Number" class="form-control" id="res"
										placeholder="No. of Resources Required" name="Resources">
								</div> -->

								<div class="form-group">
									<label for="pwd">Skills Required</label> <input type="text"
										class="form-control" id="tt" placeholder="Skills Required"
										name="Skills Required" max>
								</div>

								<div class="form-group">
									<label>Start Date</label>
									
									<input id="startDate"
										        name="startDate" class="form-control" placeholder="MM/DD/YYYY"
										type="date" onchange="changeStartDate()" value=""
										min="2018-10-08">
								</div>

								<div class="form-group">
									<label for="pwd">Duration</label> <input type="text"
										class="form-control" id="Duration" placeholder="Duration"
										name="Duration">
								</div>

								<!-- <div class="form-group">
									<label>Application End Date</label> <input id="endDate"
										name="startDate" class="form-control" placeholder="MM/DD/YYYY"
										type="date" onchange="changeStartDate()" value=""
										min="2018-10-08">
								</div> -->

								<div class="form-group">
									<label for="pwd">Location</label> <input type="text"
										class="form-control" id="location" placeholder="Location"
										name="location">
								</div>

								<!-- <div class="form-group">
     <label>End Date</label>
      <input id="startDate" name="startDate" class="form-control" placeholder="MM/DD/YYYY" type="date" onchange="changeStartDate()" value="" min="2018-10-08">              
    </div> -->


								<!--   <button type="submit" class="btn btn-default">Submit</button> -->
								<a href="${context}/client/thankyou">
									<button
										style="background: #41bef6 !important; border: none; width: 42%; font-weight: 600; border-radius: 21px;"
										type="button" class="btn btn-primary">Request</button>
								</a>
							</form>
							onclick="location.href='${context}/client/client-contract?&pid=${clientPack.id}';"
						</div>


					</div>
				</div> --%>
				
				
				
				
				<div class="col-xs-12 col-sm-12 col-md-6 col-lg-6 col-xl-6 ">
					<div class="basic_prd_desc">
						<div class="sale_prd_head">
							<h4>Post Your Requirements</h4>
							<%-- <button style="background: #41bef6 !important;
    border: none;
    width: 42%;
    font-weight: 600;
    border-radius: 21px;" type="button" class="btn btn-primary" onclick="location.href='${context}/client/client-contract?&pid=${clientPack.id}';">Request</button> --%>
						</div>

						<%-- <input
							onclick="location.href='${context}/client/client-contract?&pid=${clientPack.id}';"
							onclick="location.href='/SOD/client/clientPacks/${clientPack.id}/${clientPack.pack_name}/${clientPack.id}/${clientPack.pack_name}/clientContract';"
								class="mybutton"
							src="${images}/xbuynow.png" alt="projects for learning"
							type="image"> --%>
						<!-- <div class="message-box-wrap">
							<p>
								<strong>or Deposit money in our SBI account no.
									000000XXXXXXXXXX under the name of TEKLINK INTERNATIONAL with
									IFS Code as SBIN0011967 </strong>
							</p>
						</div> -->
						<!-- <div class="alert alert-warning">
							<strong>Free !! </strong> For Any Support Call Now at
							+91XXXXXXXXXX
						</div> -->
						<!-- <div class="alert alert-info">
							<strong>Delivery !! </strong> Delivered to your email id within
							24 hours after payment
						</div> -->

						<div class="container" style="padding: 50px; border-radius: 15px;">
							<form:form method="post" action="/SOD/clientRequirement" modelAttribute="clientRequirement">
								<div class="form-group">
									<label for="email">Resource Role</label> <input type="text" 
										class="form-control" id="text" placeholder="Resource Role"
										name="resourceRole">
								</div>
								<!-- <div class="form-group">
									<label for="pwd">No. of Resources Required</label> <input
										type="Number" class="form-control" id="res"
										placeholder="No. of Resources Required" name="Resources">
								</div> -->

								<div class="form-group">
									<label for="skillsRequired">Skills Required</label> <input type="text" 
										class="form-control" id="tt" placeholder="Skills Required"
										name="skillRequired" >
								</div>

								<div class="form-group">
									<label  for="startDt">Start Date</label>
									
									<input type="date" 
										        name="startDate" class="form-control" placeholder="MM/DD/YYYY"
										 id="startDate" onchange="changeStartDate()"
										min="2018-10-08">
								</div>

								<div class="form-group">
									<label for="duration">Duration</label> <input type="text"
										class="form-control" id="duration" placeholder="Duration"
										name="duration">
								</div>

								 <div class="form-group">
									<label>Application End Date</label> <input id="endDate"
										name="startDate" class="form-control" placeholder="MM/DD/YYYY"
										type="date" onchange="changeStartDate()" value=""
										min="2018-10-08">
								</div> 

								<div class="form-group">
									<label for="pwd">Location</label> <input type="text"
										class="form-control" id="Location" placeholder="Location"
										name="location">
								</div>

								<!-- <div class="form-group">
     <label>End Date</label>
      <input id="startDate" name="startDate" class="form-control" placeholder="MM/DD/YYYY" type="date" onchange="changeStartDate()" value="" min="2018-10-08">              
    </div> -->


								<!--   <button type="submit" class="btn btn-default">Submit</button> -->
								<a <%-- href="${context}/client/thankyou" --%>>
									<button
										style="background: #41bef6 !important; border: none; width: 42%; font-weight: 600; border-radius: 21px;"
										type="submit" class="btn btn-primary">Request</button>
								</a></form:form>
							<%-- onclick="location.href='${context}/client/client-contract?&pid=${clientPack.id}';" --%>
						</div>


					</div>
				</div>
			</div>
			<!-- <div class="sale_prd_head">
				<h4>Item Description :</h4>
			</div>
			<div class="user-html">
				This project is based on idea through which user can order food from
				a restaurant using this website. This is full responsive website
				i.e. properly visible on desktop, mobiles and tablets. In this
				project there are 2 modules which are as follows:-<br> <br>
				<b>Admin Module :- </b>We can login as admin using special username
				and password. As it is totally dynamic website. Everything in the
				website is connected to database. Admin can add add, update &amp;
				delete new categories, products in the website. Whatever will be
				added by admin will automatically be shown to user.<br> <br>
				<b>User Module :- </b>One can become member of site using signup
				page. After logging into the website user can view and add products
				to cart. There are various payment options for making payment.
			</div> -->

		</div>
	</section>
	<%@ include file="shared/Footer.jsp"%>
</body>
<script>
		$(document)
				.ready(
						function() {
							$("#startDate").prop("min", getTodayDate(0));
							$("#endDate").prop("min", getTodayDate(1));
							
						});
		function changeStartDate() {
			var startDate = document.getElementById("startDate");
			var endDate = document.getElementById("endDate");
			endDate.setAttribute('min', $("#startDate").val());

		}

		function getTodayDate(dateInx) {
			var today = new Date();
			var dd = today.getDate() + dateInx;
			var mm = today.getMonth() + 1; //January is 0!
			var yyyy = today.getFullYear();
			if (dd < 10) {
				dd = '0' + dd
			}
			if (mm < 10) {
				mm = '0' + mm
			}
			return yyyy + '-' + mm + '-' + dd;
		}
	</script>
</html>