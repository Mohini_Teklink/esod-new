
<!--  @author Hardev Singh -->
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>

<%@taglib prefix="jstl" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@taglib prefix="sf" uri="http://www.springframework.org/tags/form"%>

<!DOCTYPE html>

<spring:url var="css" value="/resources/css"></spring:url>
<spring:url var="images" value="/resources/images"></spring:url>
<spring:url var="jsMain" value="/resources/js"></spring:url>
<spring:url var="vendor" value="/resources/vendor"></spring:url>

<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Add Job</title>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="description" content="">
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta name="robots" content="all,follow">
<link rel="shortcut icon" type="image/png" href="${images}/favicon.jpg" />

<!-- Include Editor style. -->
<!--  <link href="https://cdnjs.cloudflare.com/ajax/libs/froala-editor/2.5.1/css/froala_editor.pkgd.min.css" rel="stylesheet" type="text/css" />
  <link href="https://cdnjs.cloudflare.com/ajax/libs/froala-editor/2.5.1/css/froala_style.min.css" rel="stylesheet" type="text/css" />
 -->
<!-- Bootstrap CSS-->
<!-- Dropdown list -->
<link rel="stylesheet" href="${css}/bootstrap-select.css">

<link rel="stylesheet"
	href="https://maxcdn.bootstrapcdn.com/bootstrap/4.1.0/css/bootstrap.min.css">

<!-- Font Awesome CSS-->
<link rel="stylesheet"
	href="${vendor}/font-awesome/css/font-awesome.min.css">
<!-- Fontastic Custom icon font-->
<link rel="stylesheet" href="${css}/fontastic.css">
<!-- Google fonts - Roboto -->
<link rel="stylesheet"
	href="https://fonts.googleapis.com/css?family=Roboto:300,400,500,700">
<!-- jQuery Circle-->
<link rel="stylesheet"
	href="${css}/grasp_mobile_progress_circle-1.0.0.min.css">
<!-- Custom Scrollbar-->
<link rel="stylesheet"
	href="${vendor}/malihu-custom-scrollbar-plugin/jquery.mCustomScrollbar.css">
<!-- theme stylesheet-->
<link rel="stylesheet" href="${css}/style.default.css"
	id="theme-stylesheet">
<!-- Custom stylesheet - for your changes-->
<link rel="stylesheet" href="${css}/custom.css">
<!-- Favicon-->
<link rel="shortcut icon" href="${images}/favicon.ico">
<style>
.error {
	padding: 1 1;
	border: none;
	border-radius: 0px;
	border-left: 5px solid red;
	background-color: #ffeaea;
	font-size: 12px;
	font-style: italic;
}
</style>
<script
	src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>

<!-- Include Editor JS files. -->

<title>Add New Job</title>
<!-- Tweaks for older IEs-->
<!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script><![endif]-->
</head>

<body>
	<!-- Side Navbar -->
	<%@ include file="shared/shared-admin/Header.jsp"%>
	<div class="page">
		<%@include file="shared/shared-admin/Navbar.jsp"%>
		<!--Main Section Start-->
		<section class="product_main">

			<div class="container">
				<div class="row">
					<div class="col-md-12 col-sm-12 col-xs-12">
						<div class="x_panel text-muted">
							<div class="x_title">
								<h4>Add New Job</h4>
								<div class="clearfix"></div>
							</div>
							<div class="x_content">
								<div class="row">
									<div class="col-md-6 col-sm-12 col-xs-12 mx-auto">

										<sf:form action="/SOD/admin/dashboard/jobAddSuccess?${_csrf.parameterName}=${_csrf.token}"
											method="POST" modelAttribute="adminClientProduct"
											enctype="multipart/form-data">

											<div class="form-group">
												<label>Job Name<span class="required">*</span>
												</label>
												<div class="form-group">
													<sf:input path="prodName" cssClass="form-control" />
													<sf:errors path="prodName"
														cssClass="form-control error text-danger" />
												</div>
											</div>

											<div class="form-group">
												<label>Job Type<span class="required">*</span>
												</label>
												<div class="form-group">
													<sf:select class="form-control" id="packid" path="prodType"
														items="${packageList}" itemLabel="pack_name"
														itemValue="id" onchange="priceFunction()">
													</sf:select>
													<sf:errors path="prodType"
														cssClass="form-control error text-danger" />
												</div>

												<div class="form-group">
													<label>How do you want to get paid </label>
													<div class="form-check">
														<sf:radiobutton value="Hourly Paid"
															cssClass="form-check-input" path="jobPaid" />
														Hourley project &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
														<sf:radiobutton Value="Fixed Paid"
															cssClass="form-check-input " path="jobPaid"
															checked="true" />
														Fixed Price Project
														<sf:errors path="jobPaid"
															cssClass="form-control error text-danger" />
													</div>
												</div>
												<div class="form-group">
													<label>Project Price </label>
													<div class="form-group">
														<sf:input id="packprice" path="prodPrice"
															value=""
															cssClass="form-control" readonly="true" />
														<sf:errors path="prodPrice"
															cssClass="form-control error text-danger" />
													</div>
												</div>
												<div class="form-group">
													<label>Skill<span class="required">*</span>
													</label>

													<sf:select id="skill" cssClass="selectpicker form-control"
														title="Select Skills" path="skills" multiple="multiple"
														tabindex="10" data-live-search="true">
														<jstl:set var="skillGroupVar" value="" />
														<jstl:forEach items="${skillsList}" var="skillListVar">
															<jstl:if test="${empty skillGroupVar}">
																<jstl:set var="skillGroupVar"
																	value="${skillListVar.skillGroup}" />
																<optgroup label="${skillGroupVar}">
															</jstl:if>
															<jstl:if
																test="${skillGroupVar != skillListVar.skillGroup}">
																</optgroup>
																<jstl:set var="skillGroupVar"
																	value="${skillListVar.skillGroup}" />
																<optgroup label="${skillGroupVar}">
															</jstl:if>
															<%-- <sf:option value="${skillListVar.skillName}">${skillListVar.skillName}</sf:option> --%>
															<sf:option value="${skillListVar.id}">${skillListVar.skillName}</sf:option>
														</jstl:forEach>
														</optgroup>
													</sf:select>
													<sf:errors path="skills"
														cssClass="form-control error text-danger" />

												</div>
												<div class="form-group">
													<label>Industry<span class="required">*</span>
													</label>
													<div class="form-group">
														<sf:select class="form-control" path="industryType">
															<%-- <sf:select class="form-control" path="industryType" id="industryType" items="${industryList}" 
												itemLabel="industry_type" itemValue="industry_type"> --%>
															<sf:option value="" label="--- Select ---" />
															<sf:options items="${industryList}"
																itemValue="industry_type" itemLabel="industry_type" />
														</sf:select>
														<sf:errors path="industryType"
															cssClass="form-control error text-danger" />
													</div>
												</div>
												<div class="form-group">
													<label>Job Description<span class="required">*</span>
													</label>
													<div class="form-group">
														<sf:textarea path="prodDescription" rows="5" cols="20"
															cssClass="form-control"></sf:textarea>
														<sf:errors path="prodDescription"
															cssClass="form-control error text-danger" />
													</div>
												</div>
												<div class="form-group">
													<label>Employement Type<span class="required">*</span>
													</label>
													<div class="form-group">
														<sf:select cssClass="form-control" path="employmentType">
															<sf:option value="" label="--- Select ---" />
															<sf:option value="Part Time" label="Part Time" />
															<sf:option value="Full time" label="Full Time" />
														</sf:select>
														<sf:errors path="employmentType"
															cssClass="form-control error text-danger" />
													</div>
												</div>

												<sf:hidden path="status" value="true" />

												<div class="col-xs-6 col-sm-6 col-md-6">
													<div class="form-group">
														<label>Upload Image:</label>
														<div class="form-group inputDnD">
															<label class="sr-only" for="inputFile">File
																Upload</label> <input type="file" name="productimage"
																class="form-control-file text-info font-weight-bold"
																id="inputFile" accept="image/*" onchange="readUrl(this)"
																data-title="Drag and drop a file" />
														</div>
													</div>
												</div>

												<div class="job_bdr"></div>
												<div class="form-group">
													<div>
														<jstl:if test="${! empty error}">
															<div class="alert alert-danger">${error}</div>
														</jstl:if>
														<a href="/SOD/admin/dashboard/clientlist" type="submit"
															class="btn btn-danger">Cancel</a> <input type="submit"
															class="btn btn-success" value="Submit">
													</div>
												</div>
										</sf:form>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
		</section>
		<%@ include file="shared/shared-admin/Footer.jsp"%>
	</div>
	<!--Main Section End-->


	<!--Footer Section Start-->


	<script src="${jsMain}/bootstrap-select/bootstrap-select.js"></script>
	<!-- <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/froala-editor/2.5.1//js/froala_editor.pkgd.min.js">
</script> -->
	<script>
		$(document).ready(function() {
			$("#skill").selectpicker({
				liveSearch : true
			});
			priceFunction();
		});
	</script>

	<script>
function priceFunction() {
	var varas;
	varas = document.getElementById("packid").value;
	<jstl:forEach items="${packageList}" var="list" >
	if(${list.id}==varas){
	    var packPrice="${list.pack_price}";
	    console.log(packPrice);
	    document.getElementById("packprice").value = packPrice;
	}
	</jstl:forEach>
      
    console.log(varas);
}
</script>

	<!--  <script>
    $(function() {
      $('#edit').froalaEditor({
        // Set the image upload URL.
       /*  imageUploadURL: '/upload_image', */
        imageUploadParams: {
          id: 'my_editor'
        }
      })
    });
  </script> -->
</body>
</html>

