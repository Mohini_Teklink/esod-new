<!-- @author Prateek Raj -->

<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="jstl" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="sf" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<spring:url var="css" value="/resources/css"></spring:url>
<spring:url var="images" value="/resources/images"></spring:url>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="shortcut icon" type="image/png" href="${images}/favicon.png" />
<link rel="stylesheet"
	href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css">
<script
	src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
<script
	src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js"></script>
<script
	src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js"></script>
<link rel="stylesheet"
	href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
<link href="${css}/style_1.css" rel="stylesheet" type="text/css">
<link href="${css}/responsive.css" rel="stylesheet" type="text/css">
<title>Packs | SOD</title>
<style>
.error {
	padding: 1 1;
	border-color: red;
	background-color: #ffeaea;
}
</style>

</head>
<body>
	<%@include file="shared/Header.jsp"%>

	<section class="pricing_services">
	<div class="container">
		<div class="row">
			<div class="col-lg-8 mx-auto text-center">
				<div class="main_head">
					<h1>Our Packages</h1>
					<hr class="light my-4 demad">
					<p class="text-center">Choose from our pre-designed packages 
					as per your requirement for a hassle-free experience</p>
				</div>
				<!-- <div class="">
					<p>In publishing and graphic design, lorem ipsum is a filler
						text or greeking commonly used to demonstrate the textual elements
						of a graphic document or visual presentation.</p>
				</div> -->
			</div>
		</div>
	</div>
	<div class="container price-table">
		<div class="row">

		<!--********** Client Package for loop starts **********-->
			<jstl:forEach items="${clientPacks}" var="clientPacks">

				<div class="col-md-4 col-sm-6">

				<!--********** Client Package ID is specified to the particular div which is used to maintain the height of the block **********-->
					<div id="id${clientPacks.id}" class="pricingTable">
					
					<!--********** Client Package Name **********-->
						<h3 class="title">${clientPacks.pack_name}</h3>

						<div class="price-value ">
						<!--********** Client Package Price **********-->
							<i class="fa fa-dollar"></i> 
							 <!-- <span class="month">Per hour</span> -->
						</div>
						<ul class="pricing-content">
						<!--********** Client Package Description List separated by ";" is stored in the array descp **********-->
							<jstl:set var="descp"
								value="${fn:split(clientPacks.pack_description,';')}"></jstl:set>
							<!--********** Client Package Description is displayed by the array descp **********-->
							<jstl:forEach items="${descp}" var="desc">
								<li>${desc}</li>
							</jstl:forEach>
						</ul>
						<!--********** Client Package See More Button **********-->
						<a id="bottom_link${clientPacks.id}"
							href="/SOD/client/clientPacks/${clientPacks.id}/${clientPacks.pack_name}"
							class="pricingTable-signup">See More</a>
					</div>

				</div>

			</jstl:forEach>
		<!--********** Client Package for loop ends **********-->
		</div>
	</div>
	</section>
	
	<div class="text-center">
	 <p>Is there a relevant MSA in place with you? </p>
	  <form action="">
	     <input type="radio" name="vehicle" value="Bike">&ensp; Yes<br>
  		 <input type="radio" name="vehicle" value="Car">&ensp; No<br>
  		<input type="submit" value="Submit">
	  </form>
	</div>

	<%@include file="shared/Footer.jsp"%>

	<script type="text/javascript">
		/************* On Page Resizing the height Become default *************/ 
		$(window).on('resize', function() {
			$("#id1").css("height","auto"); // div id = id1 is specified in the for loop is specified the defualt height 
			$("#id2").css("height","auto"); // div id = id2 is specified in the for loop is specified the defualt height
			$("#id3").css("height","auto"); // div id = id3 is specified in the for loop is specified the defualt height
			sameSize(); // Calling the sameSize() 
		});
		$(document).ready(function() {
			sameSize(); // Calling the sameSize()
		});
		/******************** sameSize() Change the size of the div according to the max-height ********************/
		function sameSize() {
			var maxheight = Math.max(Math.max($("#id1").height(), $("#id2")
					.height()), $("#id3").height()) +15.00; // Calculating Maxheight
			$("#id1").height(maxheight); // div id = id1 is specified in the for loop is specified the max height
			$("#id2").height(maxheight); // div id = id2 is specified in the for loop is specified the max height
			$("#id3").height(maxheight); // div id = id3 is specified in the for loop is specified the max height
			
			// Specifing the position of the see more button
			$('#bottom_link1').css({
			    position: 'absolute',
			    bottom: maxheight-(maxheight - $('#bottom_link1').height()),
			    left: (($("#id1").width()+2)-($('#bottom_link1').width()+2*7+2*30))/2.0
			});
			// Specifing the position of the see more button
			$('#bottom_link2').css({
			    position: 'absolute',
			    bottom: maxheight-(maxheight - $('#bottom_link2').height()),
			    left: (($("#id2").width()+2)-($('#bottom_link2').width()+2*7+2*30))/2.0
			});
			// Specifing the position of the see more button
			$('#bottom_link3').css({
			    position: 'absolute',
			    bottom: maxheight-(maxheight - $('#bottom_link3').height()),
			    left: (($("#id3").width()+2)-($('#bottom_link3').width()+2*7+2*30))/2.0
			});
		}
	</script>

</body>
</html>