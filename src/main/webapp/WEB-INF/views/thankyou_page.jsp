<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="jstl" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<!DOCTYPE HTML>
<spring:url var="css" value="/resources/css"></spring:url>
<spring:url var="images" value="/resources/images"></spring:url>
<spring:url var="js" value="/resources/js"></spring:url>
<spring:url var="bootstrapCss"
	value="/resources/bootstrap-4.0.0-dist/css" />
<spring:url var="bootstrapJs" value="/resources/bootstrap-4.0.0-dist/js" />
<spring:url var="bootstrap" value="/resources/bootstrap-4.0.0-dist" />
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<meta charset="utf-8">
<meta name="viewport"
	content="width=device-width, initial-scale=1, shrink-to-fit=no">
<!--For Fabvicon Icon-->
<link rel="shortcut icon" type="image/png" href="${images}/favicon.png" />
<link href="${bootstrapCss}/bootstrap.min.css" rel="stylesheet">
<!--For Toggle button Responsive on Mobile-->
<script src="${bootstrap}/jquery/jquery-3.3.1.min.js"></script>
<!--For Toggle button Responsive on Mobile-->
<script src="${bootstrapJs}/bootstrap.min.js"></script>
<!--For Fa icon-->
<link href="${bootstrap}/font-awesome/css/font-awesome.min.css"
	rel="stylesheet">
<!--Custom Css-->
<link href="${css}/style_1.css" rel="stylesheet" type="text/css">
<!--Responsive Css-->
<link href="${css}/responsive.css" rel="stylesheet" type="text/css">
<%-- <jstl:if test="${msg eq false}">
<title>Transaction Failed</title>
</jstl:if>
<jstl:if test="${msg eq true}">
<title>Transaction Successful</title>
</jstl:if> --%>

</head>

<body>
	<%@ include file="shared/Header.jsp"%>
	<section class="successWindow">
		<div class="container">
			<div class="row">
			<%-- ${msg} --%>
				<div
					class="col-xs-6 col-sm-12 col-md-12 col-lg-10 col-xl-5 mx-auto ">
					<%-- <jstl:if test="${msg eq true}"> --%>
						<div
							class="col-xs-12 col-sm-12 col-md-12 col-lg-12 col-xl-12 payment_form text-center">
							<img src="${images}/checked.png"
								style="height: 120px; width: 120px;" />
							<h2 style="color: #49cc85;">Thank You</h2>
							<!-- <h4 style="color: #49cc85;">Transaction Successful</h4>
							<p style="color: #737373;">Transaction Id: Id-001</p> -->
							<p style="color: #737373;">Your request for service has been received.<br> We shall check & revert you soon.
In case your request is urgent please call directly on +61 3 9005 8366
							</p>
						</div>

					<%-- </jstl:if> --%>
					<%-- <jstl:if test="${msg eq false}"> --%>
						<%-- <div
							class="col-xs-12 col-sm-12 col-md-12 col-lg-12 col-xl-12 payment_form text-center">
							<img src="${images}/cancel.png"
								style="height: 120px; width: 120px;" />
							<h2 style="color: #e24c4b;">Oops!</h2>
							<h4 style="color: #e24c4b;">Something Went Wrong</h4>
							<p style="color: #737373;">Please contact our support for any
								query.</p>
							<h5 style="color: #737373;">Thank You!</h5>
						</div> --%>
					<%-- </jstl:if> --%>

				</div>
			</div>
		</div>
	</section>

	<%@ include file="shared/Footer.jsp"%>
</body>
</html>