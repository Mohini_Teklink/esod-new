<!-- @author Prateek Raj -->

<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="jstl" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="sf" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>
<!DOCTYPE html >
<spring:url var="css" value="/resources/admin/css"></spring:url>
<spring:url var="vendor" value="/resources/admin/vendor"></spring:url>
<spring:url var="images" value="/resources/admin/images"></spring:url>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<title>Skills On Demand Log in</title>
<meta name="description" content="">
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta name="robots" content="all,follow">
<link rel="shortcut icon" type="image/png" href="${images}/favicon.jpg" />
<!-- Bootstrap CSS-->
<link rel="stylesheet" href="${vendor}/bootstrap/css/bootstrap.min.css">
<!-- Font Awesome CSS-->
<link rel="stylesheet"
	href="${vendor}/font-awesome/css/font-awesome.min.css">
<!-- Fontastic Custom icon font-->
<link rel="stylesheet" href="${css}/fontastic.css">
<!-- Google fonts - Roboto -->
<link rel="stylesheet"
	href="https://fonts.googleapis.com/css?family=Roboto:300,400,500,700">
<!-- jQuery Circle-->
<link rel="stylesheet"
	href="${css}/grasp_mobile_progress_circle-1.0.0.min.css">
<!-- Custom Scrollbar-->
<link rel="stylesheet"
	href="${vendor}/malihu-custom-scrollbar-plugin/jquery.mCustomScrollbar.css">
<!-- theme stylesheet-->
<link rel="stylesheet" href="${css}/style.default.css"
	id="theme-stylesheet">
<!-- Custom stylesheet - for your changes-->
<link rel="stylesheet" href="${css}/custom.css">
<!-- Tweaks for older IEs-->
<!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
        <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script><![endif]-->
<style>
.error {
	padding: 4px 4px;
	border: none;
	border-radius: 0px;
	border-left: 5px solid red;
	font-size: 12px;
}
</style>
</head>
<body>
	<div class="page login-page">
		<div class="container">
			<div class="form-outer text-center d-flex align-items-center">
				<div class="form-inner">
					<div class="logo text-uppercase">
						<span><img src="${images}/logo-SOD.png"></span> <strong
							class="text-primary">Dashboard</strong>
					</div>
					<div>
						<p></p>
					</div>
					<jstl:url value="/admin/adminLoginProcess" var="url"></jstl:url>
					<form method="post"
						action="${url}"
						class="text-left form-validate">
						<div class="form-group-material">
							<%-- <sf:input path="userName" cssClass="input-material"
								placeholder="Username" />
							<sf:errors path="userName" cssClass="input-material error text-danger" /> --%>
							<input type="text" placeholder="Enter Username" name="adminUsername" class="input-material"/>
						</div>
						<div class="form-group-material">
							<%-- <sf:password path="password"
								cssClass="input-material label-material" placeholder="Password" />
							<sf:errors path="password" cssClass="input-material error text-danger" />
 --%>						
 							<input type="password" name="adminPassword" placeholder="Enter Password" class="input-material"/>
 							<sec:csrfInput/>
						</div>
						<div class="form-group text-center">
							<input type="submit" value="login" class="btn btn-primary" />
							<!-- This should be submit button but I replaced it with <a> for demo purposes-->
						</div>
					</form>
				</div>
				<div class="copyrights text-center">
					<p>
						Copyright � <a href="https://bootstrapious.com" class="external">2018-2019
							All Rights Reserved.</a>
					</p>
					<!-- Please do not remove the backlink to us unless you support further theme's development at https://bootstrapious.com/donate. It is part of the license conditions. Thank you for understanding :)-->
				</div>
			</div>
		</div>
	</div>
</body>
</html>