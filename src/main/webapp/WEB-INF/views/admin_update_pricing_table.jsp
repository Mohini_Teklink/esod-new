<!--  @author Hardev Singh -->
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>

<%@taglib prefix="jstl" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@taglib prefix="sf" uri="http://www.springframework.org/tags/form"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">

<spring:url var="css" value="/resources/css"></spring:url>
<spring:url var="images" value="/resources/images"></spring:url>
<spring:url var="jsmain" value="/resources/js"></spring:url>
<spring:url var="vendor" value="/resources/vendor"></spring:url>
<spring:url var="productImage" value="/resources/productImages"></spring:url>

<html>
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="description" content="">
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta name="robots" content="all,follow">
<link rel="shortcut icon" type="image/png" href="${images}/favicon.jpg" />
<!-- Bootstrap CSS-->
<!-- Dropdown list -->
<link rel="stylesheet" href="${css}/bootstrap-select.css">
<link rel="stylesheet"
	href="https://maxcdn.bootstrapcdn.com/bootstrap/4.1.0/css/bootstrap.min.css">
<!-- Font Awesome CSS-->
<link rel="stylesheet"
	href="${vendor}/font-awesome/css/font-awesome.min.css">
<!-- Fontastic Custom icon font-->
<link rel="stylesheet" href="${css}/fontastic.css">
<!-- Google fonts - Roboto -->
<link rel="stylesheet"
	href="https://fonts.googleapis.com/css?family=Roboto:300,400,500,700">
<!-- jQuery Circle-->
<link rel="stylesheet"
	href="${css}/grasp_mobile_progress_circle-1.0.0.min.css">
<!-- Custom Scrollbar-->
<link rel="stylesheet"
	href="${vendor}/malihu-custom-scrollbar-plugin/jquery.mCustomScrollbar.css">
<!-- theme stylesheet-->
<link rel="stylesheet" href="${css}/style.default.css"
	id="theme-stylesheet">
<!-- Custom stylesheet - for your changes-->
<link rel="stylesheet" href="${css}/custom.css">
<!-- Favicon-->
<link rel="shortcut icon" href="${images}/favicon.ico">
<style>
.error {
	padding: 1 1;
	border: none;
	border-radius: 0px;
	border-left: 5px solid red;
	background-color: #ffeaea;
	font-size: 12px;
	font-style: italic;
}
</style>
<script
	src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
<title>Update Pricing Table</title>
<!-- Tweaks for older IEs-->
<!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script><![endif]-->



</head>
<body>
	<!-- Side Navbar -->
	<%@ include file="shared/shared-admin/Header.jsp"%>
	<div class="page">
		<%@include file="shared/shared-admin/Navbar.jsp"%>
		<!--Main Section Start-->
		<section class="product_main">

		<div class="row">
			<div class="col-md-12 col-sm-12 col-xs-12">
				<div class="x_panel text-muted">
					<div class="x_title">
						<h4>Update Pricing Table</h4>
						<div class="clearfix"></div>
					</div>
					<div class="x_content">
						<div class="row">
							<div class="col-md-6 col-sm-12 col-xs-12 mx-auto">
								<sf:form
									action="/SOD/admin/dashboard/updatepack?${_csrf.parameterName}=${_csrf.token}"
									onsubmit="validateForm()" modelAttribute="clientPacks"
									method="POST">
									<div class="form-group">
										<label>Pack Name<span class="required"></span>
										</label>
										<div class="form-group">
											<sf:select class="form-control" id="packid" path="pack_name"
												items="${packageList}" itemLabel="pack_name" itemValue="id"
												onchange="priceFunction()">
											</sf:select>
										</div>
									</div>

									<div class="form-group">
										<label>Pack Price <span class="required"> *</span>
										</label>
										<div class="form-group">
											<div class="form-group">
												<sf:input id="packprice" path="pack_price"
													class="form-control" />
												<sf:errors path="pack_price"
													cssClass="form-control error text-danger"></sf:errors>

											</div>
										</div>
									</div>

									<div class="form-group">
										<label>Pack description 1<span class="required"></span>
										</label>
										<div class="form-group">
											<input id="packdescription0" name="packdescription0"
												class="form-control" value="" />
										</div>

									</div>
									<div class="form-group">
										<label>Pack description 2<span class="required"></span>
										</label>
										<div class="form-group">
											<input id="packdescription1" name="packdescription1"
												class="form-control" value="" />
										</div>
									</div>
									<div class="form-group">
										<label>Pack description 3<span class="required"></span>
										</label>
										<div class="form-group">
											<input id="packdescription2" name="packdescription2"
												class="form-control" value="" />
										</div>
									</div>
									<div class="form-group">
										<label>Pack description 4<span class="required"></span>
										</label>
										<div class="form-group">
											<input id="packdescription3" name="packdescription3"
												class="form-control" value="" />
										</div>
									</div>
									<div class="form-group">
										<label>Pack description 5<span class="required"></span>
										</label>
										<div class="form-group">
											<input id="packdescription4" name="packdescription4"
												class="form-control" value="" />
										</div>
									</div>
									<div class="form-group">
										<label>Pack description 6<span class="required"></span>
										</label>
										<div class="form-group">
											<input id="packdescription5" name="packdescription5"
												class="form-control" value="" />
										</div>
									</div>
									<div class="form-group">
										<label>Pack description 7<span class="required"></span>
										</label>
										<div class="form-group">
											<input id="packdescription6" name="packdescription6"
												class="form-control" value="" />
										</div>
									</div>
									<div class="form-group">
										<label>Pack description 8<span class="required"></span>
										</label>
										<div class="form-group">
											<input id="packdescription7" name="packdescription7"
												class="form-control" value="" />
										</div>
									</div>
									<div class="form-group">
										<label>Pack description 9<span class="required"></span>
										</label>
										<div class="form-group">
											<input id="packdescription8" name="packdescription8"
												class="form-control" value="" />
										</div>
									</div>
									<sf:hidden path="pack_description" id="updatedpack" value="" />

									<div class="ln_solid"></div>
									<div class="form-group">
										<div>

											<a href="/SOD/admin/dashboard/pricing_table" type="submit"
												class="btn btn-danger">Cancel</a> <input value="submit"
												type="submit" class="btn btn-success">
										</div>
									</div>
								</sf:form>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
		</section>
		<%@ include file="shared/shared-admin/Footer.jsp"%>
		<!--Main Section End-->
	</div>

	<!--Footer Section Start-->

	<script src="${jsmain}/bootstrap-select/bootstrap-select.js"></script>

	<script>
		$(document).ready(function() {
			priceFunction();
		});
	</script>

	<script>
		function validateForm() {
			var d = [];
			d[0] = document.getElementById("packdescription0").value;
			var description = d[0];
			for (var i = 1; i < 9; i++) {
				d[i] = document.getElementById("packdescription" + i).value;
				if (d[i] != " ") {
					description = description.concat(";" + d[i]);
				} else {
					break;
				}
			}
			document.getElementById("updatedpack").value = description;
		}
	</script>

	<script>
		function priceFunction() {
			var varas = 1;
			varas = document.getElementById("packid").value;
			<jstl:forEach items="${packageList}" var="list" >
			if ("${list.id}" == varas) {
				var packPrice = "${list.pack_price}";
				var packDesc = "${list.pack_description}";
				var packarr = packDesc.split(";");
				for (var i = 0; i < 9; i++) {
					console.log(packarr[i]);
					if (packarr[i] == null)
						packarr[i] = " ";
					document.getElementById("packdescription" + i).value = packarr[i];
				}
				document.getElementById("packprice").value = packPrice;
			}
			</jstl:forEach>
		}
	</script>


</body>
</html>