<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib uri="http://www.springframework.org/security/tags"
	prefix="sec"%>
<spring:url var="images" value="/resources/images" />
<sec:authorize access="authenticated" var="authenticated" />
<nav class="navbar navbar-expand-lg bg-dark navbar-light fixed-top"
	id="mainNav">
	<div class="container">
		<!--***********  Defining the main link ***********-->
		<c:set var="mainUrl" value="/SOD/"></c:set>

		<!--***********  Checking if login user is expert ***********-->
		<c:if test="${!empty expert}">
			<c:set var="mainUrl" value="${mainUrl}expert/jdp"></c:set>
		</c:if>
		<!--***********  Checking if login user is expert ***********-->

		<!--***********  Checking if login user is client ***********-->
		<c:if test="${!empty client}">
			<c:set var="mainUrl" value="${mainUrl}client/clientPacks"></c:set>
		</c:if>
		<!--***********  Checking if login user is expert ***********-->

		<!--***********  Define the logo with the mainurl ***********-->
		<a href="${mainUrl}"><img src="${images}/logo-SOD.png"
			alt="sod logo" class="sod-logo"> </a>
		<!--***********  Checking if login user is expert ***********-->
		<button class="navbar-toggler navbar-toggler-right" type="button"
			data-toggle="collapse" data-target="#navbarResponsive"
			aria-controls="navbarResponsive" aria-expanded="false"
			aria-label="Toggle navigation">
			<span class="navbar-toggler-icon"></span>
		</button>

		<!--***********  Nav bar item started ***********-->
		<div class="collapse navbar-collapse" id="navbarResponsive">
			<ul class="navbar-nav ml-auto">
				<!-- Home Link on the top of the Nav bar checking if any of the user is log in -->
				<c:if test="${empty expert and empty client}">
					<li class="nav-item"><a class="nav-link js-scroll-trigger"
						href="/SOD/">Home</a></li>
				</c:if>
				<li class="nav-item"><a class="nav-link js-scroll-trigger"
					href="/SOD/AboutUs">About us</a></li>
				<li class="nav-item"><a class="nav-link js-scroll-trigger"
					href="/SOD/Technologies">Technologies</a></li>
				<li class="nav-item"><a class="nav-link js-scroll-trigger"
					href="/SOD/ContactUs">Contact us</a></li>

			</ul>
		</div>
		<!--***********  Nav bar item started ***********-->

		<!--***********  Checking if authenticated var is true or false ***********-->
		<c:if test="${authenticated}">

			<!--***********  Checking if login user is expert ***********-->
			<c:if test="${!empty expert}">
				<div class="dropdown user_logout">
					<a href="#" class="dropdown-toggle user_main"
						data-toggle="dropdown" role="button" aria-haspopup="true"
						aria-expanded="false"> <span
						class="fa fa-user-circle user_nav_icon" aria-hidden="true">
					</span> ${ expert.firstName }<span class="caret"></span></a>
					<ul class="dropdown-menu">
						<li><a href="/SOD/expert/changePassword"><span
								class="glyphicon glyphicon-cog" aria-hidden="true"></span>
								Change Password </a></li>
					<!-- <li><a href="/SOD/expert/profileEdit"><span
								class="glyphicon glyphicon-cog" aria-hidden="true"></span>
								Edit Profile</a></li> -->
						<li><a id="expertLogout" href="#"><span
								class="glyphicon glyphicon-log-out" aria-hidden="true"></span>
								Logout</a></li>
					</ul>
				</div>
				<form id="expert-logout-form"
					action="<spring:url value="/expert/expertLogout"/>" method="POST">
					<sec:csrfInput />
				</form>
			</c:if>
			<!--***********  Checking if login user is expert ***********-->

			<!--***********  Checking if login user is client ***********-->
			<c:if test="${!empty client}">

				<c:if test="${!empty client.expertList}">
					<div class="dropdown user_logout">
						<a href="#" class="dropdown-toggle user_main"
							data-toggle="dropdown" role="button" aria-haspopup="true"
							aria-expanded="false"> <span
							class="fa fa-user-circle user_nav_icon" aria-hidden="true">
						</span> Contract Form<span class="caret"></span></a>
						<ul class="dropdown-menu">
							<c:forEach items="${client.expertList}" var="list">
								<li><a
									href="/SOD/client/client_contract_management/${list.id}"><span
										class="glyphicon glyphicon-cog" aria-hidden="true"></span>
										${list.firstName}&nbsp;${list.lastName} </a></li>
								<!--  <li><a href="/SOD/client/client-contract" ><span
							class="glyphicon glyphicon-cog" aria-hidden="true"></span> Contract Form
					        </a></li> -->

								<!-- <li><a id="clientLogout" href="#"><span
								class="glyphicon glyphicon-log-out" aria-hidden="true"></span>
								Logout</a></li> -->
							</c:forEach>
						</ul>
					</div>
				</c:if>
				<div class="dropdown user_logout">
					<a href="#" class="dropdown-toggle user_main"
						data-toggle="dropdown" role="button" aria-haspopup="true"
						aria-expanded="false"> <span
						class="fa fa-user-circle user_nav_icon" aria-hidden="true">
					</span> ${ client.client_username }<span class="caret"></span></a>
					<ul class="dropdown-menu">
						<li><a href="/SOD/client/changePassword" target="_blank"><span
								class="glyphicon glyphicon-cog" aria-hidden="true"></span>
								Change Password </a></li>
						<li><a href="/SOD/client/client-contract"><span
								class="glyphicon glyphicon-cog" aria-hidden="true"></span>
								Contract Form </a></li>

						<li><a id="clientLogout" href="#"><span
								class="glyphicon glyphicon-log-out" aria-hidden="true"></span>
								Logout</a></li>
					</ul>
				</div>
				<form id="client-logout-form"
					action="<spring:url value="/client/clientLogout"/>" method="POST">
					<sec:csrfInput />
				</form>
			</c:if>
			<!--***********  Checking if login user is client ***********-->
		</c:if>
		<!--***********  Checking if authenticated var is true or false ***********-->
		<!--  <ul>
      <li class="nav-item"><a class="nav-link js-scroll-trigger" href="#" style="border: 1px solid;
    border-radius: 20px; text-transform: none;" data-toggle="modal" data-target="#myModal">TekLink Login</a></li>	
     </ul> -->

		<div class="header-search-wrapper">
			<span class="search-main"> <i
				class="fa fa-search nav_search_icon"></i>
				<div class="search-form-main clearfix">
					<form role="search" method="get" class="search-form"
						action="/searchP/searchRes">
						<label> <span class="screen-reader-text"></span> <input
							type="search" class="search-field" placeholder="search"
							name="searchTxt">
						</label> <input type="submit" class="search-submit" value="search">
					</form>
				</div>
			</span>
		</div>

	</div>
</nav>

<!-- Modal -->
<div class="modal fade" id="myModal" role="dialog">
	<div class="modal-dialog">

		<!-- Modal content-->
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal">&times;</button>

			</div>
			<div class="modal-body">
				<form>
					<div class="form-group">
						<label for="exampleInputEmail1">Email address</label> <input
							type="email" class="form-control" id="exampleInputEmail1"
							aria-describedby="emailHelp" placeholder="Enter email">
						<!--  <small id="emailHelp" class="form-text text-muted">We'll never share your email with anyone else.</small> -->
					</div>
					<div class="form-group">
						<label for="exampleInputPassword1">Password</label> <input
							type="password" class="form-control" id="exampleInputPassword1"
							placeholder="Password">
					</div>
					<div class="form-check">
						<input type="checkbox" class="form-check-input" id="exampleCheck1">
						<label class="form-check-label" for="exampleCheck1">Remember
							Me</label><br>
						<br>
					</div>
					<button type="submit" class="btn btn-primary"
						style="display: block; margin-left: auto; margin-right: auto; width: 50%;">Login</button>
				</form>
			</div>
			<!-- <div class="modal-footer">
          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        </div> -->
		</div>

	</div>
</div>