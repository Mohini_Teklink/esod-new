<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<spring:url var="js" value="/resources/js"></spring:url>

<footer class="footer bg-dark">
	<div class="container">
		<div class="row">
			<div class="col-xs-4 col-sm-4 col-md-4 col-lg-4 col-xl-3">
				<div class="">
					<p class="text-white">SKILLS ON DEMAND</p>
					<p style="color: #ccc;">
						Skills on Demand is a platform for young and skilled professionals who are dedicated for establishing a pool of experts....
						<a href="<%=request.getContextPath()%>/AboutUs">READ MORE</a>
					</p>
					<%-- <img src="${pageContext.request.contextPath}/resources/images/logo-SOD.png" alt="Logo" class="text-white"> --%>

				</div>
			</div>
			<div class="col-xs-4 col-sm-4 col-md-4 col-lg-4 col-xl-3">
				<div class="recent_news">
					<p class="text-white">QUICK LINKS</p>
				</div>

				<div class="footer_pg_list">
					<ul>
						<li><a href="/SOD/">Home</a></li>
						<li><a
							href="/SOD/AboutUs">About
								us</a></li>
						<li><a
							href="/SOD/ContactUs">Contact
								Us</a></li>
						<li><a
							href="/SOD/Technologies">Technologies</a></li>
						<%-- <li><a href="${pageContext.request.contextPath}/jobseeker/loginJobSeeker">Careers</a></li> --%>
					</ul>
				</div>
			</div>
			<div class="col-xs-4 col-sm-4 col-md-4 col-lg-4 col-xl-3">
				<div class="footer_categories">
					<p class="text-white">TECHNOLOGIES</p>
				</div>

				<div class="footer_pg_list">
					<ul>
						<li><a
							href="#">SAP</a></li>
						<li><a
							href="#">Oracle</a></li>
						<li><a
							href="#">Microsoft</a></li>
						<li><a
							href="#">Salesforce</a></li>
						<li>
						  <a href="<%=request.getContextPath()%>/Technologies" style="color:#007bff;">MORE</a>
						</li>	
						<!-- <li><a
							href="#">SAP ByD</a></li>
						<li><a
							href="#"> SAP SuccessFactors</a></li> -->
					</ul>
				</div>
			</div>
			<div class="col-xs-4 col-sm-4 col-md-4 col-lg-4 col-xl-3">
				<div class="footer_subsc">
					<p class=" text-white">SOD Support</p>
				</div>
				<div class="Footer_res">
					<p style="color: #fff;     margin-bottom: 0px;">Customer care number
					</p><span style="color:#fff;">+61 3 9005 8366</span>
				</div>
				<div class="media-container-column" data-form-type="formoid">
					<form class="form-inline">
						<!-- <input type="hidden" value="" data-form-email="true"> -->
						<!-- <div class="form-group">
							<input type="email"
								class="form-control input-sm input-inverse my-2" name="email"
								required="" data-form-field="Email" placeholder="Email"
								id="email-footer-3h">
						</div> -->
						<!-- <div class="input-group-btn">
							<button href="" class="btn btn-info footer_button" type="submit"
								role="button">Subscribe</button>
						</div> -->
					</form>
				</div>



			</div>
		</div>


		<div
			class="col-xs-12 col-sm-12 col-md-12 col-lg-12 col-xl-12 footer-lower">
			<hr class="footer_line">
			<div class="row">
				<div class="col-xs-6 col-sm-6 col-md-6 col-lg-6 col-xl-7">
					<div class="footer_copyright">
						<p class="text-white">� Copyright TekLink - All
							Rights Reserved</p>
					</div>
				</div>
				<div class="col-xs-6 col-sm-6 col-md-6 col-lg-6 col-xl-5 text-right">
					<div class="social_list">
						<ul>
							<li><a href="https://twitter.com/TekLinkInter" target="_blank"> <span
									class="fa fa-twitter" media-simple="true"></span>
							</a></li>
							<li><a href="https://www.facebook.com/TekLinkInternationalCompany/" target="_blank">
									<span class="fa fa-facebook" media-simple="true"></span>
							</a></li>
							<li><a href="https://www.linkedin.com/company/teklinkinternational/"
								target="_blank"> <span class="fa fa-linkedin"
									media-simple="true"></span>
							</a></li>
							<li><a href="https://www.instagram.com/teklinkinternational/" target="_blank"> <span
									class="fa fa-instagram" media-simple="true"></span>
							</a></li>
							<li><a href="https://plus.google.com/u/0/118112446869972464852"
								target="_blank"> <span class="fa fa-google-plus"
									media-simple="true"></span>
							</a></li>
						</ul>

					</div>
				</div>
			</div>
		</div>
	</div>
	</footer>
	
	<script type="text/javascript" src="${js}/global.js"></script>