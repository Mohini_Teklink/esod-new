<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="jstl"%>
<%@ taglib uri="http://www.springframework.org/security/tags" prefix="sec"%>
<header class="header">
	<nav class="navbar">
		<div class="container-fluid">
			<div
				class="navbar-holder d-flex align-items-center justify-content-between">
				<div class="navbar-header">
					<a id="toggle-btn" href="#" class="menu-btn"><i
						class="fa fa-bars"> </i></a><a href="#" class="navbar-brand">
						<div class="brand-text d-none d-md-inline-block">
							<span>Dashboard </span> 
						</div>
					</a>
				</div>
				<ul
					class="nav-menu list-unstyled d-flex flex-md-row align-items-md-center">
					<jstl:if test="${authenticated}">

						<!-- Languages dropdown    -->
						<li class="nav-item dropdown user_img_nav"><a id="languages"
							rel="nofollow" data-target="#" href="#" data-toggle="dropdown"
							aria-haspopup="true" aria-expanded="false"
							class="nav-link language dropdown-toggle"><img
								src="${images}/user.png" alt="English"><span
								class="d-none d-sm-inline-block">${admin.firstName}
									${admin.lastName}</span></a>
							<ul aria-labelledby="languages" class="dropdown-menu">
								<li><a rel="nofollow" href="/SOD/admin/dashboard/changePassword" class="dropdown-item"><span>Change password</span></a></li>
								<!-- <li><a rel="nofollow" href="#" class="dropdown-item"><span>Setting</span></a></li> -->
							</ul></li>
						<!-- Log out-->
						<li class="nav-item"><a href="#" id="adminLogout"
							class="nav-link logout"> <span
								class="d-none d-sm-inline-block">Logout</span><i
								class="fa fa-sign-out"></i></a></li>
						<form id="admin-logout-form"
							action="<spring:url value="/admin/adminLogout"/>" method="POST">
							<sec:csrfInput />
						</form>

					</jstl:if>

				</ul>
			</div>
		</div>
	</nav>
</header>
