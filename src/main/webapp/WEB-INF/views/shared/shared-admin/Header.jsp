<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="jstl" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="sec"
	uri="http://www.springframework.org/security/tags"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<!DOCTYPE html >
<spring:url var="css" value="/resources/admin/css"></spring:url>
<spring:url var="vendor" value="/resources/admin/vendor"></spring:url>
<spring:url var="images" value="/resources/admin/images"></spring:url>
<spring:url var="js" value="/resources/admin/js"></spring:url>
<sec:authorize access="authenticated" var="authenticated" />

<!-- Side Navbar -->


<nav class="side-navbar">
	<div class="side-navbar-wrapper">
		<!-- Sidebar Header    -->
		<div
			class="sidenav-header d-flex align-items-center justify-content-center">
			<!-- User Info-->
			<div class="sidenav-header-inner text-center">
				<img src="${images}/user.png" alt="person"
					class="img-fluid rounded-circle">
				<h2 class="h5">${admin.firstName} ${admin.lastName}</h2>
				<span> <jstl:if test="${admin.role eq 'ROLE_SUPERADMIN'}">
				Super Admin
				</jstl:if> <jstl:if test="${admin.role eq 'ROLE_EXPERTADMIN'}">
				Expert Admin
				</jstl:if> <jstl:if test="${admin.role eq 'ROLE_CLIENTADMIN'}">
				Client Admin
				</jstl:if>
				</span>
			</div>
			<!-- Small Brand information, appears on minimized sidebar-->
			<div class="sidenav-header-logo">
				<a href="index.html" class="brand-small text-center"> <strong>${fn:substring(admin.firstName, 0, 1)}</strong><strong
					class="text-primary">${fn:substring(admin.lastName, 0, 1)}</strong></a>
			</div>
		</div>
		<!-- Sidebar Navigation Menus-->
		<div class="main-menu">
			<h1 class="sidenav-heading">Main</h1>
			<ul id="side-main-menu" class="side-menu list-unstyled">

				<jstl:if test="${activeNav eq 'dashboard'}">
					<li class="active"><a href="/SOD/admin/dashboard/"> <i
							class="fa fa-home"></i>Home
					</a></li>
				</jstl:if>
				<jstl:if test="${activeNav != 'dashboard'}">
					<li><a href="/SOD/admin/dashboard/"> <i class="fa fa-home"></i>Home
					</a></li>
				</jstl:if>


				<li><a href="#productmanagement" aria-expanded="false"
					data-toggle="collapse"> <i class="fa fa-product-hunt"></i>Product
				</a>
					<ul id="productmanagement" class="collapse list-unstyled ">
						  <li><a href="/SOD/admin/dashboard/pricing_table">Pricing
								Table</a></li> 
						<!-- <li><a href="/SOD/admin/dashboard/pack_prod_desc/pack/1">Package-1</a></li>
						<li><a href="/SOD/admin/dashboard/pack_prod_desc/pack/2">Package-2</a></li>
						<li><a href="/SOD/admin/dashboard/pack_prod_desc/pack/3">Package-3</a></li> -->
					</ul></li>
				<li><a href="#ordermanagement" aria-expanded="false"
					data-toggle="collapse"> <i class="fa fa-product-hunt"></i>Order
				</a>
					<ul id="ordermanagement" class="collapse list-unstyled ">
						<li><a href="/SOD/admin/dashboard/order_management">Order
								Management</a></li>
						<li><a href="/SOD/admin/dashboard/order_cancel">Cancel
								Order</a></li>
					</ul></li>
				<li><a href="/SOD/admin/dashboard/adminContractManagement" aria-expanded="false">
					 <i class="fa fa-product-hunt"></i>Contract Form
				</a>
					<!-- <ul id="contractmanagement" class="collapse list-unstyled ">
						<li><a href="/SOD/admin/dashboard/contractManagement">Contract Form
								</a></li>
						<li><a href="/SOD/admin/dashboard/contract_form">Cancel
								Order</a></li>
					</ul> --></li>
			</ul>
		</div>
		<div class="admin-menu">
			<h5 class="sidenav-heading">User Manage</h5>
			<ul id="side-admin-menu" class="side-menu list-unstyled">
				<jstl:if test="${activeNav eq 'user'}">
					<li class="active"><a
						href="/SOD/admin/dashboard/adminAddNewUser"> <i
							class="fa fa-user"> </i>Add User
					</a></li>
				</jstl:if>
				<jstl:if test="${activeNav != 'user'}">
					<li><a href="/SOD/admin/dashboard/adminAddNewUser"> <i
							class="fa fa-user"> </i>Add User
					</a></li>
				</jstl:if>
			</ul>
		</div>
		<div class="admin-menu">
			<h5 class="sidenav-heading">Job Seeker</h5>
			<ul id="side-admin-menu" class="side-menu list-unstyled">
				<jstl:if test="${activeNav eq 'expert'}">
					<li class="active"><a href="/SOD/admin/dashboard/jobseeker">
							<i class="fa fa-users"> </i>Expert List
					</a></li>
				</jstl:if>
				<jstl:if test="${activeNav != 'expert'}">
					<li><a href="/SOD/admin/dashboard/jobseeker"> <i
							class="fa fa-users"> </i>Expert List
					</a></li>
				</jstl:if>
				<jstl:if test="${activeNav eq 'client'}">
					<li class="active"><a href="/SOD/admin/dashboard/clientlist">
							<i class="fa fa-users"> </i>Client List
					</a></li>
				</jstl:if>
				<jstl:if test="${activeNav != 'client'}"> 
				<li><a href="/SOD/admin/dashboard/clientlist"> <i
						class="fa fa-users"> </i>Client List
				</a></li>
				 </jstl:if> 
				<li><a href="/SOD/get_client_list"> <i
						class="fa fa-bell"><span class="fa fa-comment"></span><span class="num">3</span> </i>Notification 
				</a></li>
			</ul>
		</div>
		
	</div>
</nav>
