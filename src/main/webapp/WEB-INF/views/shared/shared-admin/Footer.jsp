<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>

<spring:url var="css" value="/resources/admin/css"></spring:url>
<spring:url var="vendor" value="/resources/admin/vendor"></spring:url>
<spring:url var="images" value="/resources/admin/images"></spring:url>
<spring:url var="js" value="/resources/admin/js"></spring:url>
<spring:url var="js2" value="/resources/js"></spring:url>

<!--Footer Section Start-->
<footer class="main-footer bg-light">
	<div class="text-right footer_logo_img">
		<img src="${images}/logo-SOD.png">
		<!-- Please do not remove the backlink to us unless you support further theme's development at https://bootstrapious.com/donate. It is part of the license conditions and it helps me to run Bootstrapious. Thank you for understanding :)-->
	</div>
	<div class="pull-right">
		<p>Copyright � 2018-2019 All Rights Reserved.</p>
	</div>
</footer>

<!--Footer Section End-->
<!-- JavaScript files-->
<script src="${vendor}/jquery/jquery.min.js"></script>
<script src="${vendor}/popper.js/umd/popper.min.js">
	
</script>
<script src="${vendor}/bootstrap/js/bootstrap.min.js"></script>
<script src="${js}/grasp_mobile_progress_circle-1.0.0.min.js"></script>
<script src="${vendor}/jquery.cookie/jquery.cookie.js">
	
</script>
<script src="${vendor}/chart.js/Chart.min.js"></script>
<script src="${vendor}/jquery-validation/jquery.validate.min.js"></script>
<script
	src="${vendor}/malihu-custom-scrollbar-plugin/jquery.mCustomScrollbar.concat.min.js"></script>
<script src="${js}/charts-home.js"></script>
<!-- Main File-->
<script src="${js}/front.js"></script>
<script src="${js2}/global.js"></script>

