/* *******************Validating the card number inputs******************* */
$("#cardNum")
		.keyup(
				function() {
					var status = false;
					var lengthofcvv = 3;
					if (!status) {
						$("#cardNum").css("border", "2px solid red");
					}
					var inputValue = $("#cardNum").val();
					var visaRegx = new RegExp('^4[0-9]{12}(?:[0-9]{3})?$');// Regex for Visa Card
					var masterRegx = new RegExp(
							'^(?:5[1-5][0-9]{2}|222[1-9]|22[3-9][0-9]|2[3-6][0-9]{2}|27[01][0-9]|2720)[0-9]{12}$');// Regex for Master Card
					var discoverRegx = new RegExp(
							'^6(?:011|5[0-9]{2})[0-9]{12}$');// Regex for Discover Card
					var americanRegx = new RegExp('^3[47][0-9]{13}$');// Regex for American Express Card
					
					if (visaRegx.test(inputValue)) {
						$('#CreditCardType option[value=Visa]').attr('selected','selected');
						$("#cardNum").css("border", "1px solid #ced4da");
						$("#cardNum").attr("pattern", ".{13}|.{16}");
						status = true;
					} else if (masterRegx.test(inputValue)) {
						$('#CreditCardType option[value=MasterCard]').attr('selected','selected');
						$("#cardNum").css("border", "1px solid #ced4da");
						$("#cardNum").attr("pattern", ".{16}");
						status = true;
					} else if (discoverRegx.test(inputValue)) {
						$('#CreditCardType option[value=Discover]').attr('selected','selected');
						$("#cardNum").css("border", "1px solid #ced4da");
						$("#cardNum").attr("pattern", ".{16}");
						status = true;
					} else if (americanRegx.test(inputValue)) {
						$('#CreditCardType option[value=AmericanExpress]').attr('selected','selected');
						$("#cardNum").css("border", "1px solid #ced4da");
						$("#cardNum").attr("pattern", ".{15}");
						$("#cardNum").attr("maxlength", "15");
						lengthofcvv = 4
						status = true;
					}
					$("#cvvCard").attr("maxlength", lengthofcvv);// Length of CVV based on the Card 
				});
/* *******************Validating the card number inputs******************* */

/* *******************Validating the card number length******************* */
$("#cardNum").on("keypress keyup blur", function(event) {
	$(this).val($(this).val().replace(/[^\d].+/, ""));
	switch ($(this).val().length) {
	case 0:
		$("#CreditCardType").val("null");
		$("#cardNum").attr("pattern", ".{13} | .{15} | .{16}");
		if(event.which < 50 || event.which > 54 ) {
			event.preventDefault();
		}
		break;
	default:
		if (!(event.which == 8) && (event.which < 48 || event.which > 57)) {
			event.preventDefault();
		}
	}
});
/* *******************Validating the card number length******************* */

/* *******************Preventing the cvv number other than numeric******************* */
$("#cvvCard").on("keypress keyup blur", function(event) {
	$(this).val($(this).val().replace(/[^\d].+/, ""));
	if ((event.which < 48 || event.which > 57)) {
		event.preventDefault();
	}
});
/* *******************Preventing the cvv number other than numeric******************* */

/* *******************Preventing to inspect the page******************* */
$("body")
		.on(
				"keypress keydown",
				function(event) {
					console.log(event.which);
					if (event.which == 123
							|| event.which == 93
							|| (event.ctrlKey && event.shiftKey && (event.keyCode == 'I'
									.charCodeAt(0)
									|| event.keyCode == 'J'.charCodeAt(0)
									|| event.keyCode == 'U'.charCodeAt(0)
									|| event.keyCode == 'S'.charCodeAt(0)
									|| event.keyCode == 'A'.charCodeAt(0) || event.keyCode == 'E'
									.charCodeAt(0)))
							|| (event.shiftKey && event.keyCode == 'ContextMenu'
									.charCodeAt(0))) {
						console.log(event);
						return false;
					}

				});
/* *******************Preventing to inspect the page******************* */

/* *******************Validating expiry date inputs & cvv information window******************* */
$(document).ready(
		function() {
			var today = new Date();
			$("#dateExpiry").val(
					today.getFullYear() + "-" + n(today.getMonth() + 2));
			$("#dateExpiry").prop("min",
					today.getFullYear() + "-" + n(today.getMonth() + 2));

			$("#whatCvv").hover(function() {
				$("#windowCvv").css("display", "block");
			});
			$("#whatCvv").mouseout(function() {
				$("#windowCvv").css("display", "none");
			});
		});
/* *******************Validating expiry date inputs & cvv information window******************* */

/* function to add 0 before month if month is less than 9*/
function n(n) {
	return n > 9 ? "" + n : "0" + n;
}
/* function to add 0 before month if month is less than 9*/
